//
//  CardTextField.swift
//  oribiky
//
//  Created by Aymen Harrath on 06/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class CardTextField: UITextField {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(select(_:)) {
            return false
        }
        if action == #selector(selectAll(_:)) {
            return false
        }
        if action == #selector(cut(_:)) {
            return false
        }
        if action == #selector(copy(_:)) {
            return false
        }
        if action == #selector(paste(_:)) {
            return false
        }
        if action == #selector(delete(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)

    }
}
