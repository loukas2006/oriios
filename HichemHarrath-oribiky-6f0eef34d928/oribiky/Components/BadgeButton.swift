//
//  BadgeButton.swift
//  oribiky
//
//  Created by Hichem Harrath on 01/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class BadgeButton: UIButton {
    
    //MARK: - Constants
    enum BadgeStyle {
        case `default`
        case custom(font: UIFont?, backgroundColor: UIColor, textColor: UIColor)
    }
    
    //MARK: - Properties
    private var badgeLabel = UILabel()
    private let badgeSize: CGFloat = 14.0
    
    // Badge Style
    var badgeStyle: BadgeStyle = .default {
        didSet (newValue) {
            switch newValue {
            case .default:
                badgeLabel.font = AppStyle.Text.bold(size: 8.0).font
                badgeLabel.textColor = AppStyle.Colors.white
                badgeLabel.backgroundColor = AppStyle.Colors.red
            case .custom(let font, let backgroundColor, let textColor):
                badgeLabel.font = font
                badgeLabel.textColor = textColor
                badgeLabel.backgroundColor = backgroundColor
            }
        }
    }
    
    //MARK: - Override
    override init(frame: CGRect) {
        super.init(frame: frame)
        updateBadgeValue("")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateBadgeValue("")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateBadgeValue("")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        handleLabelFrame()
    }
    
    //MARK: - Public
    func updateBadgeValue(_ value: String, badgeStyle: BadgeStyle = .default) {
        
        // Set style
        self.badgeStyle = badgeStyle
        
        // Customize label
        badgeLabel.text = value
        badgeLabel.sizeToFit()
        badgeLabel.textAlignment = .center
        badgeLabel.baselineAdjustment = .alignCenters
        
        // Handle Label frame
        if badgeLabel.superview != nil {
            badgeLabel.removeFromSuperview()
        }
        addSubview(badgeLabel)
        handleLabelFrame()
        
        // Misc
        badgeLabel.layer.cornerRadius = badgeSize/2.0
        badgeLabel.layer.masksToBounds = true
        
        // Show or hide the label
        badgeLabel.isHidden = (Int(value) ?? 0) > 0 ? false : true
    }
    
    //MARK: - Private
    fileprivate func handleLabelFrame() {
        
        badgeLabel.frame = CGRect(x: frame.width - badgeSize * 0.7,
                                       y: frame.height - badgeSize * 2,
                                       width: badgeSize,
                                       height: badgeSize)
    }
}

