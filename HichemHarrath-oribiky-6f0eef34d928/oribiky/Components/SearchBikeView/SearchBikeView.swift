//
//  SearchBikeView.swift
//  oribiky
//
//  Created by Hichem Harrath on 13/01/2019.
//  Copyright © 2019 Oribiky. All rights reserved.
//

import UIKit

class SearchBikeView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var backView: UIView!
    @IBOutlet var validateBtn: UIButton!
    @IBOutlet var bikeIdTextField: UITextField!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var searchTextChanged : ((String) -> ())?
    var searchTextValidated : ((String) -> ())?

    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        backView.round(value: backView.frame.height/2)
        validateBtn.round(value: validateBtn.frame.height/2)

    }
    
    //MARK: - Actions
    @IBAction func searchTextFieldTextChanged(_ sender: UITextField) {
        
        guard  let text = sender.text?.lowercased() else {
            return
        }
        validateBtn.isEnabled  = text.count != 0

        searchTextChanged?(text)
    }
    
    @IBAction func validateBtnPressed(_ sender: UIButton) {
        activityIndicator.startAnimating()
        bikeIdTextField.resignFirstResponder()
        searchTextValidated?(bikeIdTextField.text ?? "")
    }

    @IBAction func searchTextFieldPrimaryActionTriggered(_ sender: UITextField) {
        activityIndicator.startAnimating()
        bikeIdTextField.resignFirstResponder()
        searchTextValidated?(bikeIdTextField.text ?? "")
    }
    
    //MARK: - Private
    private func setupView() {
        Bundle.main.loadNibNamed(String(describing: SearchBikeView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        contentView.layoutIfNeeded()

        backView.layer.borderColor = AppStyle.Colors.mainBlue.cgColor
        backView.layer.borderWidth = 1.0
        
        titleLabel.textColor = AppStyle.Colors.mainBlue
        titleLabel.font = AppStyle.Text.medium(size: 14.0).font
        titleLabel.text = APPStrings.Map.SearchView.title
        
        validateBtn.backgroundColor = AppStyle.Colors.mainBlue
        validateBtn.setImage(Asset.Navigation.rightArrow.image, for: .normal)
        validateBtn.isEnabled = false
        
        bikeIdTextField.font = AppStyle.Text.medium(size: 14.0).font
        bikeIdTextField.placeholder = APPStrings.Map.SearchView.placeholder
        bikeIdTextField.textColor = AppStyle.Colors.mainBlue
        activityIndicator.tintColor = AppStyle.Colors.mainBlue
    }

}
