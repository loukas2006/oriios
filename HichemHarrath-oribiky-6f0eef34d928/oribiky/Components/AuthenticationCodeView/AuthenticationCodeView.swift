//
//  AuthenticationCodeView.swift
//  oribiky
//
//  Created by Hichem Harrath on 27/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class AuthenticationCodeView: UIView {

    private var textFields = [UITextField]()
    private let textFieldFrame = CGRect(x: 0, y: 0, width: 40, height: 40)
    
    // by default we have only 4 degits in code
    private var title = "0000"

    var isCodeValid: Bool {
        if textFields.count > 0 {
            return textFields.filter({ $0.text == ""}).count == 0
        }
        return false
    }
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    convenience init(title: String) {
        self.init(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        self.title = title
        setupView()
    }
    
    //MARK: - Public
    func launch() {
        textFields.first?.becomeFirstResponder()
    }
    
    func retreiveCode() -> String {
        return textFields.compactMap({ $0.text ?? "" }).joined()
    }
    //MARK: - Private
    private func setupView() {
        
        for i in 1...title.count {
            let textField = UITextField(frame: textFieldFrame)
            textField.delegate = self
            textField.tag = i
            textField.font = AppStyle.Text.bold(size: 18).font
            textField.textColor = AppStyle.Colors.mainBlue
            textField.placeholder = "-"
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
            textField.round()
            textField.border()
            textFields.append(textField)
        }

        let stackView = UIStackView(arrangedSubviews: textFields)
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.distribution = .equalCentering

        addSubview(stackView)
        constraintViewToBounds(viewToBound: stackView)
        
        textFields.forEach { (view) in
            view.widthAnchor.constraint(equalToConstant: 40).isActive = true
            view.heightAnchor.constraint(equalToConstant: 40).isActive = true
            view.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        }
        layoutIfNeeded()
    }

}

extension AuthenticationCodeView: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let text = textField.text , !text.isEmpty {
            textField.text = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if !string.isNumbersOnly {
            return false
        }
        textField.resignFirstResponder()
        textField.text = string
        textFields.filter ({ $0.tag == (textField.tag + 1) }).first?.becomeFirstResponder()
        return false
        
    }
}
