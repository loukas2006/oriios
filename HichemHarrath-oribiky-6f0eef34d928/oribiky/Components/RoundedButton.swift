//
//  RoundedButton.swift
//  oribiky
//
//  Created by Aymen Harrath on 13/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit


class RoundedButton: UIButton {

    
    //MARK: - Variables
    var hasShadow: Bool = false {
        didSet {
            if hasShadow {
                layer.shadowColor = AppStyle.Colors.shadow.cgColor
                layer.shadowOffset = CGSize(width: 0, height: 5)
                layer.masksToBounds = false
                layer.shadowRadius = 10
                layer.shadowOpacity = 1
            }  else {
                layer.shadowOpacity = 0
            }
        }
    }
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK: - Private
    private func setup() {
        layer.cornerRadius = frame.height / 2
        layer.masksToBounds = true
    }

}
