//
//  BikeAnnotationView.swift
//  oribiky
//
//  Created by Hichem Harrath on 25/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit
import MapKit

class BikeAnnotationView: MKAnnotationView {
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        guard let bikeAnnotation = annotation as? BikeAnnotation else { return }
        image = bikeAnnotation.image
    }
}
