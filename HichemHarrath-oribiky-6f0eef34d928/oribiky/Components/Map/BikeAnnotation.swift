//
//  BikeAnnotation.swift
//  oribiky
//
//  Created by Hichem Harrath on 25/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit
import MapKit

enum BikeAnnotationType: String {
    case onService
    case offService
    case reserved
}

class BikeAnnotation: NSObject, MKAnnotation  {
    
    var coordinate: CLLocationCoordinate2D
    var type: BikeAnnotationType
    var bike: Bike
    
    var image: UIImage {
        switch type {
        case .onService:
            return Asset.Map.bikePin.image
        case .offService:
            return Asset.Map.bikePin.image
        case .reserved:
            return Asset.Map.bikeReserved.image
        }
    }

    init(_ bike: Bike) {
        self.bike = bike
        self.coordinate = CLLocationCoordinate2D(latitude: bike.latitude,
                                                 longitude: bike.longitude)
        self.type = bike.inUse ? .reserved : (bike.onService ? .onService : .offService)
        
    }
    
}
