//
//  DemoPageView.swift
//  oribiky
//
//  Created by Hichem Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit


class DemoPageView: UIView {
    
    //MARK: - Variables
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var stepImageView: UIImageView!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!

    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //MARK: - Public
    func setImage(_ stepImage: UIImage?) {
        stepImageView.image = stepImage
    }
    
    func updateImagePosition(constant: CGFloat) {
        imageViewTopConstraint.constant = constant
        layoutIfNeeded()
    }

    //MARK: - Private
    private func setupView() {
        Bundle.main.loadNibNamed(String(describing: DemoPageView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
}
