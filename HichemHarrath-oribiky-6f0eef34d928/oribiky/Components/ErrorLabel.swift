//
//  ErrorLabel.swift
//  oribiky
//
//  Created by Hichem Harrath on 01/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class ErrorLabel: UILabel {

    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK: - Private
    private func setup() {
        font = AppStyle.Text.regular(size: 12).font
        textColor = AppStyle.Colors.red
    }

}
