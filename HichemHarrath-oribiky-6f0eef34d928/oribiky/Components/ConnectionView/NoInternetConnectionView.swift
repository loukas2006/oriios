//
//  NoInternetConnectionView.swift
//  oribiky
//
//  Created by Aymen Harrath on 10/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit
fileprivate let defaultFrame = CGRect(x: 0, y: -20, width: UIScreen.main.bounds.width, height: 20)

class NoInternetConnectionView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    var isVisible: Bool = false

    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()

    }
    convenience init() {
        self.init(frame: defaultFrame)
        setupView()

    }

    //MARK: - Parivate
    private func setupView() {
        
        Bundle.main.loadNibNamed(String(describing: NoInternetConnectionView.self), owner: self, options: nil)
        guard let contentView = contentView else { return }
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        contentView.backgroundColor = AppStyle.Colors.mainBlue
        addSubview(contentView)

        messageLabel.font = AppStyle.Text.regular(size: 12).font
        messageLabel.backgroundColor = AppStyle.Colors.red
        messageLabel.textColor = AppStyle.Colors.white
        messageLabel.text = APPStrings.Common.noInternetConnection
        
    }
    func show () {
        if !isVisible {
            yTranslationWithFadeIn(value:0, completion: { finished in
                if finished {
                    self.isVisible = true
                }
            })
        }
    }

    func hide (animated: Bool) {
        
        if isVisible {
            if animated {
                yTranslationWithFadeOut(value: -frame.height, completion: { finished in
                    if finished {
                        self.isVisible = false
                        self.removeFromSuperview()
                    }
                })
            } else {
                self.isVisible = false
                self.removeFromSuperview()
            }
        }
    }

}
