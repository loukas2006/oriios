//
//  PopupAction.swift
//  oribiky
//
//  Created by Hichem Harrath on 24/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class PopupAction{
    
    typealias BtnAction = ()->()

    enum ActionStyle {
        
        case validate
        case cancel
        
        var backgroundColor : UIColor {
            switch self {
            case .cancel:
                return AppStyle.Colors.red
            case .validate:
                return AppStyle.Colors.mainBlue
            }
        }
    }
    
    let title: String
    let style: ActionStyle
    let action: BtnAction
    
    init(title: String, style: ActionStyle, action: @escaping BtnAction) {
        self.title = title
        self.style = style
        self.action = action
    }
}
