//
//  PopupInfo.swift
//  oribiky
//
//  Created by Hichem Harrath on 24/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class PopupInfo: UIView {

    //MARK: - Variables
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var popupBtn: RoundedButton!
    
    private var btnAction: (() -> ())?
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    convenience init(title: String? = "" ,
                     titleColor: UIColor = AppStyle.Colors.mainBlue,
                     subTitle: String? = "",
                     subTitleColor: UIColor = AppStyle.Colors.mainBlue,
                     popupAction: PopupAction) {
        
        self.init(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
       
        titleLabel.text = title
        titleLabel.font = AppStyle.Text.bold(size: 16.0).font
        titleLabel.textColor = titleColor

        
        subTitleLabel.text = subTitle
        subTitleLabel.font = AppStyle.Text.medium(size: 15.0).font
        subTitleLabel.textColor = subTitleColor
        
        btnAction = popupAction.action
        
        popupBtn.setTitle(popupAction.title.uppercased(), for: .normal)
        popupBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        popupBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        popupBtn.backgroundColor = popupAction.style.backgroundColor

    }
    
    //MARK: - Private
    private func setupView() {
        Bundle.main.loadNibNamed(String(describing: PopupInfo.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    //MARK: - ACtions
    @IBAction func BtnPressed(_ sender: UIButton) {
        btnAction?()
    }
    

}
