//
//  PopupChoice.swift
//  oribiky
//
//  Created by Hichem Harrath on 24/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit
class PopupChoice: UIView {
 
    //MARK: - Variables
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var yesBtn: RoundedButton!
    @IBOutlet weak var noBtn: RoundedButton!
    private var yesBtnAction: (() -> ())?
    private var noBtnAction: (() -> ())?
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var imageViewsBackView: UIView!
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    convenience init(title: NSAttributedString? = nil,
                     subTitle: NSAttributedString? = nil,
                     leftImage: UIImage? = nil,
                     rightImage: UIImage? = nil,
                     yesAction: PopupAction,
                     noAction: PopupAction) {
        
        self.init(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        
        if leftImage == nil && rightImage == nil {
            hideImages()
        } else {
            leftImageView.image = leftImage
            rightImageView.image = rightImage
        }
        titleLabel.textColor = AppStyle.Colors.mainBlue
        titleLabel.attributedText = title
        titleLabel.font = AppStyle.Text.bold(size: 15.0).font
        subTitleLabel.textColor = AppStyle.Colors.mainBlue
        subTitleLabel.attributedText = subTitle
        subTitleLabel.font = AppStyle.Text.regular(size: 14.0).font
        
        
        yesBtnAction = yesAction.action
        noBtnAction = noAction.action

        yesBtn.setTitle(yesAction.title, for: .normal)
        yesBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        yesBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        yesBtn.backgroundColor = yesAction.style.backgroundColor
        
        noBtn.setTitle(noAction.title, for: .normal)
        noBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        noBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        noBtn.backgroundColor = noAction.style.backgroundColor

    }
    
    //MARK: - Private
    private func setupView() {
        Bundle.main.loadNibNamed(String(describing: PopupChoice.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    private func hideImages() {
        imageViewsBackView.addHeightConstraint()
    }
    
    //MARK: - Actions
    @IBAction func yesBtnPressed(_ sender: UIButton) {
        yesBtnAction?()
//        AppCoordinator.shared.pop(animated: true , completion: {[weak self] in
//            self?.
//        })
        
    }
    @IBAction func noBtnPressed(_ sender: UIButton) {
        noBtnAction?()
//        AppCoordinator.shared.pop(animated: true, completion: {[weak self] in
//            self?.
//        })
    }

}
