//
//  PopupCountries.swift
//  oribiky
//
//  Created by Hichem Harrath on 26/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit
class PopupCountries: UIView {
    
    //MARK: - Variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    private var selectCountryCompletion : ((Country) -> ())?
   
    private var countriesDatasource = CountryList.all {
        didSet {
            tableView.reloadData()
        }
    }
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    convenience init(completion: @escaping (Country) -> ()) {
        self.init(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        selectCountryCompletion = completion
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK: - Private
    private func setupView() {
        Bundle.main.loadNibNamed(String(describing: PopupCountries.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        let cellNib = UINib(nibName: String(describing: CountryPopupTableViewCell.self), bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: String(describing: CountryPopupTableViewCell.self))
        
        searchTextField.placeholder = APPStrings.Country.searchTextFieldTitle
        searchTextField.font = AppStyle.Text.medium(size: 14).font
        searchTextField.textColor = AppStyle.Colors.mainBlue
        
        separatorView.backgroundColor = AppStyle.Colors.mainBlue
        tableViewHeightConstraint.constant = UIScreen.main.bounds.height * 0.4
    }
    
    @IBAction func SearchTextFieldTextChanged(_ sender: UITextField) {
        
        guard  let text = sender.text?.lowercased() else {
            return
        }
        
        if text == "" {
            countriesDatasource = CountryList.all
        } else {
            countriesDatasource = CountryList.all.filter({ $0.name.lowercased().contains(text)})
        }
    }
    
}

extension PopupCountries: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countriesDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CountryPopupTableViewCell.self)) as? CountryPopupTableViewCell else {
            return UITableViewCell()
        }
        let country = countriesDatasource[indexPath.row]
        cell.configure(country)
        return cell
    }
    
    
}
extension PopupCountries: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchTextField.resignFirstResponder()
        selectCountryCompletion?(countriesDatasource[indexPath.row])
    }
}
