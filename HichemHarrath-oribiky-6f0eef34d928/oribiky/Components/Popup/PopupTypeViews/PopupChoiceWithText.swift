//
//  PopupChoiceWithText.swift
//  oribiky
//
//  Created by Hichem Harrath on 10/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class PopupChoiceWithText: UIView {

    //MARK: - Variables
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var topDescriptionLabel: UILabel!
    @IBOutlet weak var bottomDescriptionLabel: UILabel!
    @IBOutlet weak var validateBtn: RoundedButton!
    @IBOutlet weak var cancelBtn: RoundedButton!

    private var validateBtnAction: (() -> ())?
    private var cancelBtnAction: (() -> ())?

    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    convenience init(topDescriptionText: NSAttributedString? = nil ,
                     bottomDescriptionText: NSAttributedString? = nil,
                     validateAction: PopupAction,
                     cancelAction: PopupAction) {
        
        self.init(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        
        topDescriptionLabel.attributedText  = topDescriptionText
        bottomDescriptionLabel.attributedText = bottomDescriptionText
        
        validateBtnAction = validateAction.action
        cancelBtnAction = cancelAction.action
        
        validateBtn.setTitle(validateAction.title.uppercased(), for: .normal)
        validateBtn.titleLabel?.font = AppStyle.Buttons.medium10.font
        validateBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        validateBtn.backgroundColor = validateAction.style.backgroundColor
        
        cancelBtn.setTitle(cancelAction.title.uppercased(), for: .normal)
        cancelBtn.titleLabel?.font = AppStyle.Buttons.medium10.font
        cancelBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        cancelBtn.backgroundColor = cancelAction.style.backgroundColor

    }
    
    //MARK: - Private
    private func setupView() {
        Bundle.main.loadNibNamed(String(describing: PopupChoiceWithText.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

    //MARK: - Actions
    @IBAction func validateBtnPressed(_ sender: UIButton) {
        validateBtnAction?()
    }
    
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        cancelBtnAction?()
    }
    

}
