//
//  CountryPopupTableViewCell.swift
//  oribiky
//
//  Created by Hichem Harrath on 26/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class CountryPopupTableViewCell: UITableViewCell {

    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var phoneCodeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCell()

    }

    func configure(_ country: Country) {
        flagImageView.image = country.flag
        countryNameLabel.text = country.name
        phoneCodeLabel.text = country.phoneCode
    }
    
    // MARK: - Private
    private func setupCell () {
        countryNameLabel.font = AppStyle.Text.medium(size: 14).font
        countryNameLabel.textColor = AppStyle.Colors.mainBlue
        
        phoneCodeLabel.font = AppStyle.Text.bold(size: 13).font
        phoneCodeLabel.textColor = AppStyle.Colors.red
        
        flagImageView.round(value: 2)
    }

    
}
