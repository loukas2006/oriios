//
//  PopupBikes.swift
//  oribiky
//
//  Created by Hichem Harrath on 05/01/2019.
//  Copyright © 2019 Oribiky. All rights reserved.
//

import UIKit

class PopupBikes: UIView {

    //MARK: - Variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    private var selectBikeCompletion : ((Bike) -> ())?
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    private var bikesModels = [AvailableBikeViewModel]()
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    convenience init(bikes: [Bike], completion: @escaping (Bike) -> ()) {
        self.init(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        selectBikeCompletion = completion
        bikesModels = bikes.compactMap({AvailableBikeViewModel(bike: $0)})
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        tableView.separatorStyle = .none
    }

    //MARK: - Private
    private func setupView() {
        
        Bundle.main.loadNibNamed(String(describing: PopupBikes.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        titleLabel.font = AppStyle.Text.medium(size: 14).font
        titleLabel.textColor = AppStyle.Colors.black
        titleLabel.text = APPStrings.Map.Cluster.AvailableBikes.title
        titleLabel.textAlignment = .center
        
        let cellNib = UINib(nibName: String(describing: AvailableBikeTableViewCell.self), bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: String(describing: AvailableBikeTableViewCell.self))
        
        tableViewHeightConstraint.constant = UIScreen.main.bounds.height * 0.4
    }
}

extension PopupBikes: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bikesModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AvailableBikeTableViewCell.self)) as? AvailableBikeTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(to: bikesModels[indexPath.row])
        return cell
    }
    
}
extension PopupBikes: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectBikeCompletion?(bikesModels[indexPath.row].bike)
    }
}

