//
//  PopupScrollable.swift
//  oribiky
//
//  Created by Hichem Harrath on 19/01/2019.
//  Copyright © 2019 Oribiky. All rights reserved.
//

import UIKit

class PopupScrollable: UIView {

    //MARK: - Variables
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var popupBtn: RoundedButton!
    @IBOutlet weak var contentTextViewHeightConstraint: NSLayoutConstraint!

    private var btnAction: (() -> ())?
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    convenience init(title: NSAttributedString? = nil ,
                     titleColor: UIColor = AppStyle.Colors.mainBlue,
                     content: NSAttributedString? = nil,
                     subTitleColor: UIColor = AppStyle.Colors.mainBlue,
                     popupAction: PopupAction) {
        
        self.init(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        
        titleLabel.attributedText = title
        titleLabel.font = AppStyle.Text.bold(size: 16.0).font
        titleLabel.textColor = titleColor
        
        contentTextView.attributedText = content
//        contentTextView.font = AppStyle.Text.medium(size: 15.0).font
//        contentTextView.textColor = subTitleColor
        
        let neededHeight = contentTextView.sizeThatFits(CGSize(width: contentTextView.frame.width, height: .greatestFiniteMagnitude)).height
        
        if neededHeight > UIScreen.main.bounds.height * 0.65 {
            contentTextView.isScrollEnabled = true
            contentTextViewHeightConstraint.constant = UIScreen.main.bounds.height * 0.75
        } else {
            contentTextView.isScrollEnabled = false
            contentTextViewHeightConstraint.constant = neededHeight
        }
        contentView.layoutIfNeeded()
        contentTextView.textAlignment = .center

        btnAction = popupAction.action
        
        popupBtn.setTitle(popupAction.title.uppercased(), for: .normal)
        popupBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        popupBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        popupBtn.backgroundColor = popupAction.style.backgroundColor
        
    }

    
    //MARK: - Private
    private func setupView() {
        Bundle.main.loadNibNamed(String(describing: PopupScrollable.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    //MARK: - ACtions
    @IBAction func btnPressed(_ sender: UIButton) {
        btnAction?()
    }


}
