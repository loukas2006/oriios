//
//  PopupViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 24/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

enum PopupType {
    case info(title: String?, subTitle: String?, action: PopupAction)
    case infoCustom(title: String?, titleColor: UIColor, subTitle: String?, subtitleColor: UIColor, action: PopupAction)
    case infoScrollable(title: NSAttributedString?, titleColor: UIColor, content: NSAttributedString?, subtitleColor: UIColor, action: PopupAction)
    case choice(title: NSAttributedString?, subTitle: NSAttributedString?, leftImage: UIImage?, rightImage: UIImage?, yesActions: PopupAction, noAction: PopupAction)
    case choiceWithText(topDescriptionText: NSAttributedString?, bottomDescriptionText: NSAttributedString?, validateAction: PopupAction, cancelAction: PopupAction)
    case countries((Country) -> ())
    case bikes([Bike] , ((Bike) -> ()))
    case unknown
    
}

class PopupViewController: UIViewController, ViewModelConfigurable {
    
    
    // MARK: - Variables
    private var type: PopupType = .unknown
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var closeBtnBackView: UIView!
    @IBOutlet weak var closeBtnImageView: UIImageView!

    var viewModel: PopupViewModel!
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        self.type = viewModel.type
        setupPopupView()
    }
    
    // MARK: - Init
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        containerView.popIn()
    }
    
    // MARK: - Private
    private func setupPopupView() {
        
        containerView.round(value: 12)
        containerView.alpha = 0.0

        closeBtnBackView.round()
        closeBtnBackView.backgroundColor = AppStyle.Colors.mainBlue
        closeBtnImageView.image = Asset.Popup.closeCross.image
        
        switch self.type {
        case let .info(title, subTitle, action):
            let popupView = PopupInfo(title: title,
                                      subTitle: subTitle,
                                      popupAction: action)
            contentView.addSubview(popupView)
            contentView.constraintViewToBounds(viewToBound: popupView)
        case let .infoScrollable(title, titleColor, content, subtitleColor, action):
            let popupView = PopupScrollable(title: title, titleColor: titleColor, content: content, subTitleColor: subtitleColor, popupAction: action)
            contentView.addSubview(popupView)
            contentView.constraintViewToBounds(viewToBound: popupView)

        case let .infoCustom(title, titleColor, subTitle, subtitleColor, action):
            let popupView = PopupInfo(title: title, titleColor: titleColor, subTitle: subTitle, subTitleColor: subtitleColor, popupAction: action)
            contentView.addSubview(popupView)
            contentView.constraintViewToBounds(viewToBound: popupView)

        case let .choice(title, subTitle, leftImage, rightImage, yesAction, noAction):
            let popupView = PopupChoice(title: title,
                                        subTitle: subTitle,
                                        leftImage: leftImage,
                                        rightImage: rightImage,
                                        yesAction: yesAction,
                                        noAction: noAction)
            contentView.addSubview(popupView)
            contentView.constraintViewToBounds(viewToBound: popupView)
            
        case let .choiceWithText(topDescription, bottomDescription, validateAction, cancelAction):
            let popupView = PopupChoiceWithText(topDescriptionText: topDescription, bottomDescriptionText: bottomDescription, validateAction: validateAction, cancelAction: cancelAction)
            contentView.addSubview(popupView)
            contentView.constraintViewToBounds(viewToBound: popupView)
            
        case let .countries(completion):
            let popupView = PopupCountries(completion: completion)
            contentView.addSubview(popupView)
            contentView.constraintViewToBounds(viewToBound: popupView)
        case let .bikes(bikesList, completion):
            let popupView = PopupBikes(bikes: bikesList, completion: completion)
            contentView.addSubview(popupView)
            contentView.constraintViewToBounds(viewToBound: popupView)
        default:
            break
        }
    }
    
    // MARK: - Private
    @IBAction func closeBtnPressed(_ sender: Any) {
        containerView.popOut()
        viewModel.dismiss()
    }
    
}
