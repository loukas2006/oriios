//
//  MenuItemTableViewCell.swift
//  oribiky
//
//  Created by Aymen Harrath on 19/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {

    //MARK: - variables
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView ()
    }

    private func setupView () {
        selectionStyle = .none
        backgroundColor = AppStyle.Colors.clear
        itemTitleLabel.textColor = AppStyle.Colors.white
        itemTitleLabel.textAlignment = .right
        itemTitleLabel.font = AppStyle.Text.medium(size: 13).font
//        contentView.backgroundColor = AppStyle.Colors.clear

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
