//
//  HamburgerMenuView.swift
//  oribiky
//
//  Created by Aymen Harrath on 19/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

fileprivate let calculatedHeight: CGFloat = 5*40
fileprivate var defaultFrame = CGRect(x: 0, y: -calculatedHeight, width: UIScreen.main.bounds.width, height: calculatedHeight)


enum HamburgerItem {
    case login
    case logout
    case faq
    case coupons
    case tickets
    case legalNotice
    
    var title: String {
        switch self {
        case .login:
            return APPStrings.HamburgerMenu.login
        case .logout:
            return APPStrings.HamburgerMenu.logout
        case .faq:
            return APPStrings.HamburgerMenu.faq
        case .coupons:
            return APPStrings.HamburgerMenu.coupons
        case .tickets:
            return APPStrings.HamburgerMenu.tickets
        case .legalNotice:
            return APPStrings.HamburgerMenu.legalNotice

        }
    }
    
    var height: CGFloat {
        return 40
    }
    
    var icon: UIImage? {
        switch self {
        case .login:
            return Asset.HamburgerMenu.login.image
        case .logout:
            return Asset.HamburgerMenu.logout.image
        case .faq, .coupons, .tickets, .legalNotice:
            return nil
        }
    }
}

class HamburgerMenuView: UIView {


    //MARK: - Variables
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var isVisible: Bool = false
    
    private var dataSource: [HamburgerItem] = []

    private var overlayView =  UIView()
    private var presentingViewController: UIViewController?
    private var itemsTableView: UITableView?

    
    private var topBarsTotalHeigh: CGFloat {
      return (presentingViewController?.navigationController?.navigationBar.frame.height ?? 0) + UIApplication.shared.statusBarFrame.height
    }

    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    convenience init(from viewController: UIViewController) {
        self.init(frame: defaultFrame)
        dataSource = [.tickets, .legalNotice, .faq, .coupons, DefaultsManager.shared.isConnected ? .logout : .login]
        presentingViewController = viewController
        isHidden = false
        setupView()
    }
    
    //MARK: - Parivate
    private func setupView() {
        
        Bundle.main.loadNibNamed(String(describing: HamburgerMenuView.self), owner: self, options: nil)
        guard let contentView = contentView else { return }
        contentView.frame = bounds
//        contentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: calculatedHeight)
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        contentView.backgroundColor = AppStyle.Colors.mainBlue
        addSubview(contentView)
        
        let nib = UINib.init(nibName: String(describing: MenuItemTableViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: MenuItemTableViewCell.self))
        tableView.backgroundColor = AppStyle.Colors.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.separatorColor = AppStyle.Colors.mainGray.withAlphaComponent(0.4)

        overlayView.backgroundColor = AppStyle.Colors.black.withAlphaComponent(0.2)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.overlayViewTapped))
        overlayView.addGestureRecognizer(tap)
        applyShadow(color: AppStyle.Colors.black, radius: 30)
        
    }
    @objc func overlayViewTapped () {
        toggle(sender: nil)
    }

    //MARK: - Public
    func toggle(sender: UIBarButtonItem?, completion: (() -> Void)? = nil) {
        
        tableView.reloadData()
        sender?.isEnabled = false

        if isVisible {
            overlayView.yTranslationWithFadeOut(value: -calculatedHeight + topBarsTotalHeigh, completion: {finished in
                if finished {
                    self.overlayView.removeFromSuperview()
                }
            })
            
            yTranslation(value: -calculatedHeight, completion: { finished in
                if finished {
                    self.isVisible = !self.isVisible
                    sender?.isEnabled = true
                    self.alpha = 0
                    self.removeFromSuperview()
                    completion?()
                }
            })

            
        } else {
            
            guard let keyWindow = UIApplication.shared.keyWindow else { return }
            overlayView.frame = CGRect(x: 0,
                                       y: topBarsTotalHeigh,
                                       width: keyWindow.bounds.width,
                                       height: keyWindow.bounds.height)
            keyWindow.addSubview(overlayView)
            overlayView.yTranslationWithFadeIn(value: calculatedHeight + topBarsTotalHeigh)
            
            yTranslationWithFadeIn(value: 0, completion: { finished in
                if finished {
                    self.isVisible = !self.isVisible
                    sender?.isEnabled = true
                    completion?()

                }
            })

        }

    }
}


//MARK: - UITableViewDataSource
extension HamburgerMenuView:  UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MenuItemTableViewCell.self), for: indexPath) as? MenuItemTableViewCell else {
            return UITableViewCell()
        }
        cell.itemTitleLabel.text = dataSource[indexPath.row].title
        cell.iconImageView.image = dataSource[indexPath.row].icon
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dataSource[indexPath.row].height
    }
}

//MARK: - UITableViewDelegate
extension HamburgerMenuView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        toggle(sender: nil, completion: { [weak self] in
            guard let `self` = self else {return}
            switch self.dataSource[indexPath.row] {
            case .logout:
                self.logout()
            case .coupons:
                self.showCoupons()
            case .login:
                self.login()
            case .faq:
                self.showFaq()
            case .tickets:
                self.showReport()
            case .legalNotice:
                self.showLegalNotice()
            }
        })
    }
}

//MARK: - Actions
extension HamburgerMenuView {
    
    private func logout () {
        
        if BikeRentManager.shared.hasRide ||  BikeRentManager.shared.hasReservation {
            return
        }
        let logoutAction = PopupAction(title: APPStrings.Common.yes, style: .validate) {
            // hide popup
            AppCoordinator.shared.pop(animated: true, completion: {
                
                DefaultsManager.shared.removeToken()
                // start again from tabbar controller
                AppCoordinator.shared.transition(to: .tabbar(TabbarViewModel()), type: .root, animated: true)

            })
        }
        let cancelAction = PopupAction(title: APPStrings.Common.no, style: .cancel) {
            // hide popup
            AppCoordinator.shared.pop(animated: true)
        }
        
        let viewModel = PopupViewModel(type: .choice(title: nil,
                                                     subTitle: NSAttributedString(string:APPStrings.HamburgerMenu.Lougout.ConfimationPopup.subtitle),
                                                     leftImage: nil,
                                                     rightImage: nil,
                                                     yesActions: logoutAction, noAction: cancelAction))
        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)

    }
    
    private func login () {
        
        AppCoordinator.shared.transition(to: .authenticate(AuthenticateViewModel(index: 4, pushed: true), true),
                                         type: .modal,
                                         animated: true)
    }

    private func showFaq () {
        
        let viewModel = PdfViewerViewModel(withPDF: .faq)
        AppCoordinator.shared.transition(to: .pdfViewer(viewModel, true), type: .modal, animated: true)
    }
    
    private func showCoupons () {
        if DefaultsManager.shared.isConnected {
            AppCoordinator.shared.transition(to: .coupons(CouponsViewModel()), type: .modal, animated: true)
        } else {
            ReservationRequirementsProcessManager.shared.handleSteps()
            ReservationRequirementsProcessManager.shared.start(finalStep:  Step(type: .authentication)) {
                ReservationRequirementsProcessManager.shared.ignoreSteps()
                AppCoordinator.shared.transition(to: .coupons(CouponsViewModel()), type: .modal, animated: true)
            }
        }
    }
    
    private func showReport () {
        if AppCoordinator.shared.currentViewController is MyTicketsViewController {
            return
        }
        AppCoordinator.shared.transition(to: .tabbarItem, type: .tabbarItem(selectIndex: true, index: 2), animated: true, completion: {
            AppCoordinator.shared.transition(to: .myTicketsList(MyTicketsViewModel()), type: .push, animated: false)
        })
    }
    
    private func showLegalNotice () {
        AppCoordinator.shared.transition(to: .legalNotice(LegalNoticeViewModel()), type: .modal, animated: true)

    }

}
