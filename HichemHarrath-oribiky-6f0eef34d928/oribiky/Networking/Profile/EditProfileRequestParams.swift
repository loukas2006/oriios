//
//  EditProfileRequestParams.swift
//  oribiky
//
//  Created by Aymen Harrath on 21/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct EditProfileRequestParams: RequestBody {

    var email: String
    var firstname: String
    var lastname: String
    var device: String = HTTPAPI.deviceOS
    var fcmRegistrationId: String
    var newsletter: Bool
    
}
