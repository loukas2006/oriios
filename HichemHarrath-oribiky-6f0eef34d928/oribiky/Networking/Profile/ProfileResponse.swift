//
//  ProfileResponse.swift
//  oribiky
//
//  Created by Aymen Harrath on 21/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct ProfileResponse: Decodable {
    
    // MARK/ - Variables
    let phoneNumber: String
    let email: String?
    let firstname: String?
    let lastname: String?
    let lastLogin: String?
    let fcmRegistrationId: String?
    let newsletter: Bool?
    let device: String?
    let packages: [Package]?
    let coupons: [Coupon]?
    let subscription: Subscription?
    let address:Address?
    let card: Card?

    // MARK: - Types
    struct Card: Decodable {
        let cardBrand: String?
        let lastDigits: String?
    }
    /*
     "packages": [{
        "credit": 1440,
        "expireAt": "2018-11-19 13:00:36"
     }],
     */
    
    struct Package: Decodable {
        let credit: Double
        let expireAt: String
    }
    struct Coupon: Decodable {
        let name: String
    }

    struct Address: Decodable {
        let streetNumber: String?
        let streetName: String?
        let locality: String?
        let postalCode: Int?
    }
    
    struct Subscription: Decodable {
        let status: Bool?
        let details: SubscriptionDetails?
        let endsAt: String?
        let billingPeriodEndsAt: String?
        
    }
    
    struct SubscriptionDetails: Decodable {
        let type: String?
        let freeMinutes: Double?
        let freeMinutesWe: Double?
        let extraMinutePrice: Double?
        let validated: Int?
    }

    
    
}

