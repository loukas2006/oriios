//
//  EditAddressRequestParams.swift
//  oribiky
//
//  Created by Aymen Harrath on 21/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct EditAddressRequestParams: RequestBody {
    
    var streetNumber: Int
    var streetName: String
    var locality: String
    var postalCode: Int

    

}
