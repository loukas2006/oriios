//
//  ProfileAPI.swift
//  oribiky
//
//  Created by Aymen Harrath on 21/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire


enum ProfileAPI: URLRequestConvertible {
    
    case getProfile
    case editProfile(EditProfileRequestParams)
    case editAddress(EditAddressRequestParams)
    
    private var method: HTTPMethod {
        switch self {
        case .getProfile:
            return .get
        case .editProfile(_):
            return .patch
        case .editAddress(_):
            return .put
        }
    }
    
    private var path: String {
        switch self {
        case .getProfile:
            return HTTPAPI.Profile.get
        case .editProfile:
            return HTTPAPI.Profile.edit
        case .editAddress:
            return HTTPAPI.Profile.editAddress
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        urlRequest.addAuthorizationToken()
        
        switch self {
        case .getProfile:
            break
        case let .editProfile(params):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: params.asDictionary())
        case .editAddress(let params):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: params.asDictionary())

        }
        return urlRequest
    }
    
}

struct ProfileService {
    
    static func get(handleSessionExpired: Bool = true, success:@escaping (ProfileResponse) -> Void,
                    failure:@escaping BaseServiceFailure) {
        BaseService.call(ProfileAPI.getProfile, requireToken: true, checkForSession: handleSessionExpired, success: success, failure: failure)
    }
    
    static func editProfile(params: EditProfileRequestParams,
                            success:@escaping (ProfileResponse) -> Void,
                            failure:@escaping BaseServiceFailure) {
        
        BaseService.call(ProfileAPI.editProfile(params), requireToken: true, success: success, failure: failure)
    }
    
    static func editAddress(params: EditAddressRequestParams,
                            success:@escaping (ProfileResponse) -> Void,
                            failure:@escaping BaseServiceFailure) {
        BaseService.call(ProfileAPI.editAddress(params), requireToken: true, success: success, failure: failure)
    }

}




