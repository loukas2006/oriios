//
//  Alamofire+Oribiky.swift
//  oribiky
//
//  Created by Hichem Harrath on 17/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

extension DataRequest {

    @discardableResult func responseObject<T: Decodable> (
        queue: DispatchQueue? = nil,
        completionHandler: @escaping (DataResponse<T>) -> Void ) -> Self {
        
        let responseSerializer = DataResponseSerializer<T> { request, response, data, error in
           
            guard error == nil else {
                if !ReachabilityManager.shared.isConnectedToInternet {
                    // no internet connection
                    return .failure(ServiceError.network(reason: error?.localizedDescription ?? "ServiceError - network" ))
                }
                return .failure(ServiceError.invalidRequest(reason: error?.localizedDescription ?? "ServiceError - Could not execute request - Server error" ))
            }
            
            let result = DataRequest.serializeResponseData(response: response, data: data, error: error)

            // status code 200, request succeeded
            let code = response?.statusCode ?? 404
            
            guard code >= 200 &&  code <= 299 else {
                // status code != 200 / service failure
                // web service respond with success but he sends a error message/code
                
                if code == 400 {
                    // validation errors, should parse all errors
                    if let data = data , let errorObject = try? JSONDecoder().decode(ServiceError400.self, from: data) {
                        return .failure(ServiceError.invalidRequestData(error: errorObject))
                    }
                }
                if code == 401 {
                    // we should refresh token
                    if let data = data , let errorObject = try? JSONDecoder().decode(ServiceErrorObject.self, from: data) {
                        return .failure(ServiceError.JWTInvalid(error: errorObject))
                    }
                }
                if code == 404 {
                    if let data = data , let errorObject = try? JSONDecoder().decode(ServiceErrorObject.self, from: data) {
                        return .failure(ServiceError.notFound(error: errorObject))
                    }
                }
                return .failure(ServiceError.invalidRequest(reason: error?.localizedDescription ?? "ServiceError - invalidRequest  statusCode = \(String(describing: response?.statusCode))" ))
            }
            
            guard case var .success(jsonData) = result else {
               
                return .failure(ServiceError.objectSerialization(reason: result.error?.localizedDescription ??
                    "ServiceError - objectSerialization" ))
            }
            /// this case we have a succeded ws, but without a data, we init the data with an ampty dict
            if jsonData.isEmpty {
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: [:], options: [])

                } catch {
                    return .failure(ServiceError.objectSerialization(reason: "JSON object of type - \(T.self)  - could not be serialized"))
                }
            }
            print(String(data: jsonData, encoding: .utf8) ?? "No data")
            guard let responseObject = try? JSONDecoder().decode(T.self, from: jsonData) else {
                return .failure(ServiceError.objectSerialization(reason: "JSON object of type - \(T.self)  - could not be serialized"))
            }
            return .success(responseObject)
        }
        return response(queue: queue, responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
    
    /// completionHandler handles JSON Array [T]
    @discardableResult func responseCollection<T: Decodable> ( queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<[T]>) -> Void ) -> Self {
        
        let responseSerializer = DataResponseSerializer<[T]> { request, response, data, error in
            
            guard error == nil else {
                if !ReachabilityManager.shared.isConnectedToInternet {
                    // no internet connection
                    return .failure(ServiceError.network(reason: error?.localizedDescription ?? "ServiceError - network" ))
                }
                return .failure(ServiceError.invalidRequest(reason: error?.localizedDescription ?? "ServiceError - Could not execute request - Server error" ))
            }
            
            let result = DataRequest.serializeResponseData(response: response, data: data, error: error)
            
            // status code 200, request succeeded
            let code = response?.statusCode ?? 404
            
            guard code >= 200 &&  code <= 299 else {
                // status code != 200 / service failure
                // web service respond with success but he sends a error message/code
                
                if code == 400 {
                    // validation errors, should parse all errors
                    if let data = data , let errorObject = try? JSONDecoder().decode(ServiceError400.self, from: data) {
                        return .failure(ServiceError.invalidRequestData(error: errorObject))
                    }
                }
                if code == 401 {
                    // we should refresh token
                    if let data = data , let errorObject = try? JSONDecoder().decode(ServiceErrorObject.self, from: data) {
                        return .failure(ServiceError.JWTInvalid(error: errorObject))
                    }
                }
                if code == 404 {
                    if let data = data , let errorObject = try? JSONDecoder().decode(ServiceErrorObject.self, from: data) {
                        return .failure(ServiceError.notFound(error: errorObject))
                    }
                }
                return .failure(ServiceError.invalidRequest(reason: error?.localizedDescription ?? "ServiceError - invalidRequest  statusCode = \(String(describing: response?.statusCode))" ))
            }
            
            guard case var .success(jsonData) = result else {
                
                return .failure(ServiceError.objectSerialization(reason: result.error?.localizedDescription ??
                    "ServiceError - objectSerialization" ))
            }
            /// this case we have a succeded ws, but without a data, we init the data with an ampty dict
            if jsonData.isEmpty {
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: [:], options: [])
                    
                } catch {
                    return .failure(ServiceError.objectSerialization(reason: "JSON object of type - \([T].self)  - could not be serialized"))
                }
            }

            guard let responseArray = try? JSONDecoder().decode([T].self, from: jsonData) else {
                return .failure(ServiceError.objectSerialization(reason: "JSON object of type - \([T].self)  - could not be serialized"))
            }
            return .success(responseArray)

        }
        
        return response(queue: queue, responseSerializer: responseSerializer, completionHandler: completionHandler)
    }

    
}
