//
//  ReachabilityManager.swift
//  oribiky
//
//  Created by Hichem Harrath on 18/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire
class ReachabilityManager: NSObject {
    
    static let shared = ReachabilityManager()
    
    var manager = NetworkReachabilityManager()
    
    var isConnectedToInternet:Bool {
        return manager?.isReachable ?? false
    }
    
    func startListening() {
        manager?.listener = { status in
                switch status {
                case .reachable:
                    AppCoordinator.shared.currentViewController?.hideReachabilityView(animated: true)
                case .notReachable:
                    AppCoordinator.shared.currentViewController?.showReachabilityView()
                default:
                    print("It is unknown whether the network is reachable")
                }
            }
        manager?.startListening()
        
    }
    
}
