//
//  ServiceError.swift
//  oribiky
//
//  Created by Hichem Harrath on 11/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

enum ServiceError: Error {
   
    case network(reason: String)
    case invalidRequest(reason: String)
    case invalidRequestData(error: ServiceError400)
    case objectSerialization(reason: String)
    case notFound(error: ServiceErrorObject)
    case JWTInvalid(error: ServiceErrorObject)
    
    var errorDescription: String {
        switch self {
        case .network(let reason), .invalidRequest(let reason), .objectSerialization(let reason):
            return reason
        case .notFound(let errorObject), .JWTInvalid(let errorObject):
            return errorObject.message
        case .invalidRequestData(_):
            return ""
        }
    }
}

struct ServiceErrorObject: Decodable {
    //* EXEMPLE *// 
    /*
     {
     "code": 404,
     "message": "Id was not found"
     }
     */
    let code: Int
    let message: String
}


struct ServiceError400: Decodable {
    /*
     {
     "errors": {
     "streetNumber": [
     null
     ],
     "streetName": [
     null
     ],
     "locality": [
     null
     ],
     "postalCode": [
     null
     ]
     }
     }
     }*/
    
//    let code: Int?
//    let message: String?
    private let errors: [String:[String]]

    func getErrorMessages() -> [String:[String]] {
        return errors
    }
}
