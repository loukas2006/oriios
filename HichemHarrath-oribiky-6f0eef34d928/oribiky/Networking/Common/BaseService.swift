//
//  BaseService.swift
//  oribiky
//
//  Created by Hichem Harrath on 01/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

typealias BaseServiceFailure = (String, [String : [String]]?) -> Void

struct BaseService {
    
    static func call<T: Decodable>(_ urlRequest: URLRequestConvertible,
                                   requireToken: Bool = false,
                                   checkForSession: Bool = true,
                                   success: @escaping (T) -> Void,
                                   failure:@escaping BaseServiceFailure) {
                

        Alamofire.request(urlRequest).responseObject { (response: DataResponse<T>) in
            switch response.result {
            case .success(let repsonseObject):
                success(repsonseObject)
            case .failure(let serviceError):
                guard let error = serviceError as? ServiceError else {
                    failure("Unknown error", nil)
                    return
                }
                switch error {
                case let .JWTInvalid(error):
                    if requireToken {
                        // we should refresh token if needed
                        AuthenticateService.refresh(token: DefaultsManager.shared.token, success: { (refreshReponse) in
                            // save token, and recall ws
                            DefaultsManager.shared.saveToken(tokerStr: refreshReponse.token)
                            call(urlRequest, requireToken: requireToken, success: success, failure: failure)

                        }, failure: { (errorStr, errors) in
                            DefaultsManager.shared.removeToken()

                            if checkForSession {
                                showSessionExpiredPopup()
                            }
                            failure (errorStr, errors)
                        })
                    } else {
                        failure(error.message, nil)
                    }
                case let .invalidRequestData(error):
                    failure("", error.getErrorMessages())
                case let .network(reason):
                    failure(reason, nil)
                default:
                    failure(error.errorDescription, nil)
                }
            }
        }
    }
    static func callColelction<T: Decodable>(_ urlRequest: URLRequestConvertible,
                                   requireToken: Bool = false,
                                   checkForSession: Bool = true,
                                   success: @escaping ([T]) -> Void,
                                   failure:@escaping BaseServiceFailure) {
        
        
        Alamofire.request(urlRequest).responseCollection { (response: DataResponse<[T]>) in
            switch response.result {
            case .success(let repsonseObject):
                success(repsonseObject)
            case .failure(let serviceError):
                guard let error = serviceError as? ServiceError else {
                    failure("Unknown error", nil)
                    return
                }
                switch error {
                case let .JWTInvalid(error):
                    if requireToken {
                        // we should refresh token if needed
                        AuthenticateService.refresh(token: DefaultsManager.shared.token, success: { (refreshReponse) in
                            // save token, and recall ws
                            DefaultsManager.shared.saveToken(tokerStr: refreshReponse.token)
                            
                            call(urlRequest, requireToken: requireToken, success: success, failure: failure)
                            
                        }, failure: { (errorStr, errors) in
                            DefaultsManager.shared.removeToken()

                            if checkForSession {
                                showSessionExpiredPopup()
                            }
                            failure (errorStr, errors)
                        })
                        
                    } else {
                        failure(error.message, nil)
                    }
                case let .invalidRequestData(error):
                    failure("", error.getErrorMessages())
                case let .network(reason):
                    failure(reason, nil)

                default:
                    failure(error.errorDescription, nil)
                }
            }
        }
    }

    private static func showSessionExpiredPopup() {
    // 1- show popup
        let action = PopupAction(title: APPStrings.Common.ok.uppercased(), style: .validate) {
            // start again from tabbar controller
            AppCoordinator.shared.transition(to: .tabbar(TabbarViewModel()), type: .root, animated: true, completion: {
                AppCoordinator.shared.transition(to: .authenticate(AuthenticateViewModel(index: -1, pushed: true), true),
                                                 type: .modal,
                                                 animated: true)
            })}
        
        let popupViewModel = PopupViewModel(type: .info(title: APPStrings.Common.sessionExpired,
                                                        subTitle: nil,
                                                        action: action))
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
    }
    
}
