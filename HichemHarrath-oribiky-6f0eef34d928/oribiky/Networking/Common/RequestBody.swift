//
//  RequestBody.swift
//  oribiky
//
//  Created by Hichem Harrath on 14/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

protocol RequestBody: Encodable {
   func asDictionary() throws -> [String: Any]
}

extension RequestBody {
   
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}
