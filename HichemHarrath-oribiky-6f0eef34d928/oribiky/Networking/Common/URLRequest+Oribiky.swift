//
//  URLRequest+Oribiky.swift
//  oribiky
//
//  Created by Aymen Harrath on 06/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation


extension URLRequest {
    
    mutating func addAuthorizationToken() {
        addValue(DefaultsManager.shared.token , forHTTPHeaderField: HTTPAPI.AuthorizationHeaderKey)
    }
    
    mutating func addTicketAPIHeaders() {
        let tokenBase64Str = (HTTPAPI.LiveChatTicket.agentEmail + ":" + HTTPAPI.LiveChatTicket.agentAPIKey).toBase64()
        addValue(String(format: HTTPAPI.LiveChatTicket.TokenFormat, tokenBase64Str) , forHTTPHeaderField: HTTPAPI.AuthorizationHeaderKey)
        addValue(HTTPAPI.LiveChatTicket.APIVersionValue, forHTTPHeaderField: HTTPAPI.LiveChatTicket.APIVersionKey)

    }

}
