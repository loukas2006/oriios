//
//  AuthenticateAPI.swift
//  oribiky
//
//  Created by Hichem Harrath on 11/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

enum AuthenticateAPI: URLRequestConvertible {
    
    // Cases
    case autheticate(AuthenticateRequestBody)
    case verify(code: String, identifier: String)
    case refresh(String)
    
    private var method: HTTPMethod {
        return .post
    }
    
    private var path: String {
        switch self {
        case .autheticate(_):
            return HTTPAPI.Authentication.authenticate
        case .verify(_, let identifier):
            return String(format: HTTPAPI.Authentication.verify, identifier)
        case .refresh(_):
            return HTTPAPI.Authentication.refreshToken
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .autheticate(let body):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: body.asDictionary())
        case let .verify(code,_):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["code" : code])
        case let .refresh(token):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["refresh_token" : token])
        }
        return urlRequest
    }
}


struct AuthenticateService {
    
    static func authenticate (requestBody: AuthenticateRequestBody,
                              success:@escaping (String) -> Void,
                              failure:@escaping BaseServiceFailure) {
        
        Alamofire.request(AuthenticateAPI.autheticate(requestBody)).responseObject { (response: DataResponse<VoidResponse>) in
            switch response.result {
            case .success:
                var code = ""
                let loctaionComponents = ((response.response?.allHeaderFields["Location"] ?? "") as? String)?.components(separatedBy: "/")
                if let components = loctaionComponents, components.count > 1 {
                    code = components[components.count - 2 ]
                }
                success(code)
            case .failure(let serviceError):
                guard let error = serviceError as? ServiceError else {
                    failure("Unknown error", nil)
                    return
                }
                switch error {
                case let .invalidRequestData(error):
                    failure("", error.getErrorMessages())
                default:
                    failure(error.errorDescription, nil)
                }
            }
        }
    }
    
    static func verify(code: String, identifier: String,
                       success:@escaping (VerifyResponse) -> Void,
                       failure:@escaping BaseServiceFailure) {
       BaseService.call(AuthenticateAPI.verify(code: code, identifier: identifier), success: success, failure: failure)
       
    }
    
    static func refresh(token: String,
                        success:@escaping (RefreshReponse) -> Void,
                        failure:@escaping BaseServiceFailure) {
        BaseService.call(AuthenticateAPI.refresh(token), success: success, failure: failure)
        
    }
}














