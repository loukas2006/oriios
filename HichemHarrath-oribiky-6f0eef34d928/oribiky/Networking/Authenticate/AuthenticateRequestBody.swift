//
//  AuthenticateRequestBody.swift
//  oribiky
//
//  Created by Hichem Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct AuthenticateRequestBody: RequestBody {
    /* { "phoneNumber": "+33612547896",
         "device": "IOS",
         "fcmRegistrationId": "string" }
     */
    var phoneNumber: String
    let device = HTTPAPI.deviceOS
    var fcmRegistrationId: String
    var local: String
    
    init(phone: String, deviceToken: String) {
        phoneNumber = phone
        fcmRegistrationId = deviceToken
        local = Locale.current.identifier
    }

}
