//
//  VerifyResponse.swift
//  oribiky
//
//  Created by Hichem Harrath on 29/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct VerifyResponse: Decodable {
 
    let token: String
    let refreshToken: String
    let user: ProfileResponse
    
    enum CodingKeys: String, CodingKey {
        case refreshToken = "refresh_token"
        case token, user
    }
}
