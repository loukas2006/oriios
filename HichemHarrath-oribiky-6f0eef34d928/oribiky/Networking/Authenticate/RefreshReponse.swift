//
//  RefreshReponse.swift
//  oribiky
//
//  Created by Hichem Harrath on 02/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct RefreshReponse: Decodable {
    /*
     { "token": "string",
       "refresh_token": "string"
     }
     */
    
    let token: String
    let refreshToken: String
    
    enum CodingKeys: String, CodingKey {
        case refreshToken = "refresh_token"
        case token
    }
}
