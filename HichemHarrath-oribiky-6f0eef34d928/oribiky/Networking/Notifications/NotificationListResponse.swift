//
//  NotificationListResponse.swift
//  oribiky
//
//  Created by Aymen Harrath on 25/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct NotificationListResponse : Decodable {
    
    let meta: Meta
    let notifications: [NotificationResponse]
    
    enum CodingKeys: String, CodingKey {
        case meta
        case notifications = "data"
    }
    
}
