//
//  NotificationsAPI.swift
//  oribiky
//
//  Created by Aymen Harrath on 24/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

enum NotificationAPI: URLRequestConvertible {
    
    case getAll(NotificationRequestParams) //page, limit
    case getNew
    case markAsRead([String]) // Liste des ids de notifications séparés par des virgules
    
    private var method: HTTPMethod {
        switch self {
        case .getAll(_), .getNew:
            return .get
        case .markAsRead(_):
            return .patch
        }
    }
    
    private var path: String {
        switch self {
        case .getAll(_):
            return HTTPAPI.Notification.getAll
        case .getNew:
            return HTTPAPI.Notification.getNew
        case .markAsRead(_):
            return HTTPAPI.Notification.markAsRead
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.addAuthorizationToken()
        
        switch self {
        case .getAll (let params):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: params.asDictionary())
        case .markAsRead(let ids):
            if let urlString = urlRequest.url?.absoluteString , let url = URL(string: urlString + "?ids=\(ids.joined(separator: ","))") {
                urlRequest = URLRequest(url: url )
            }
        default:
            break
        }
        return urlRequest
    }
    
}

struct NotificationService {
    
    static func getAll (params: NotificationRequestParams,
                        success:@escaping (NotificationListResponse) -> Void,
                        failure:@escaping BaseServiceFailure) {
        BaseService.call(NotificationAPI.getAll(params), requireToken: true, success: success, failure: failure)
    }
    
    static func getNew(handleSessionExpired: Bool = true, success:@escaping ([NotificationResponse]) -> Void,
                       failure:@escaping BaseServiceFailure) {
        BaseService.callColelction(NotificationAPI.getNew, requireToken: true, checkForSession: handleSessionExpired, success: success, failure: failure)
    }

    static func markAsRead(ids: [String],
                           completion:@escaping (Bool) -> Void) {
        
        //BaseService.call(NotificationAPI.markAsRead(ids), requireToken: true, success: success, failure: failure)
        
        if let serviceURL = NotificationAPI.markAsRead(ids).urlRequest?.url?.absoluteString , let url = URL(string: serviceURL){
            
            var request = URLRequest(url: url)
            request.httpMethod = "PATCH"
            let headers = [  "content-type": "application/json",
                             HTTPAPI.AuthorizationHeaderKey: DefaultsManager.shared.token ]
            
            request.allHTTPHeaderFields = headers
            
            let dataTask = URLSession.shared.dataTask(with: request,
                                                      completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    completion(false)
                } else {
                    if ((response as? HTTPURLResponse)?.statusCode  ?? 404) == 204 {
                        completion(true)
                    } else {
                        completion(false)
                    }
                }
            })
            dataTask.resume()
            
        }
        
        
    }
    

}

