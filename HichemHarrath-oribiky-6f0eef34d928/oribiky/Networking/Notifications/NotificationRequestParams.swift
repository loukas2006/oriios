//
//  NotificationRequestParams.swift
//  oribiky
//
//  Created by Aymen Harrath on 25/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct NotificationRequestParams: RequestBody {
    
    var page: Int
    var limit: Int
    
}
