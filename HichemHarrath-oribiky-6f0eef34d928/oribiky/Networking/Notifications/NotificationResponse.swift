//
//  NotificationResponse.swift
//  oribiky
//
//  Created by Aymen Harrath on 24/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
struct NotificationResponse: Decodable {
    /*
     {
     "id": 0,
     "title": "string",
     "body": "string",
     "seen": true,
     "type": "WARNING",
     "priority": "normal",
     "createdAt": "2018-05-12 15:01:30",
     "updatedAt": "2018-05-12 15:01:30"
     }
     */
    
    let notificationId: Int
    let title: String?
    let body: String?
    let seen: Bool?
    let type: String?
    let priority: String?
    let createdAt: String?
    let updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case notificationId = "id"
        case title, body, seen, type, priority, createdAt, updatedAt
    }
}


