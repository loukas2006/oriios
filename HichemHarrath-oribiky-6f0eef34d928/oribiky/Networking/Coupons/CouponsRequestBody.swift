//
//  CouponsRequestBody.swift
//  oribiky
//
//  Created by Aymen Harrath on 30/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct CouponsRequestBody: RequestBody {
    
    var code: String
}
