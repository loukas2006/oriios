//
//  CouponsAPI.swift
//  oribiky
//
//  Created by Aymen Harrath on 30/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

enum CouponsAPI: URLRequestConvertible {
    
    case add(CouponsRequestBody)
    
    private var method: HTTPMethod {
        switch self {
        case .add(_):
            return .post
        }
    }
    
    private var path: String {
        switch self {
        case .add(_):
            return HTTPAPI.Coupon.add
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.addAuthorizationToken()
        
        switch self {
        case .add(let body):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: body.asDictionary())
        }
        
        return urlRequest
        
    }
}

class CouponsService {
    
    static func add(body: CouponsRequestBody,
                    success:@escaping (ProfileResponse) -> Void,
                    failure:@escaping BaseServiceFailure) {
        BaseService.call(CouponsAPI.add(body), success: success, failure: failure)
    }
}
