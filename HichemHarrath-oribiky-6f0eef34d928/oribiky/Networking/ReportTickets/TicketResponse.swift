//
//  TicketResponse.swift
//  oribiky
//
//  Created by Aymen Harrath on 06/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

enum AuthorType: String, Decodable {
    case client
    case agent
}

struct Author: Decodable {
    let name: String?
    let id: String?
    let type: AuthorType?
}
enum EventType: String {
    case message = "message"
    case assigneeChanged = "assignee_changed"
    case statusChanged = "status_changed"
    case assigneeRemoved = "assignee_removed"
    case skillAdded = "skill_added"
    case currentSkillChanged = "current_skill_changed"
    case subjectChanged = "subject_changed"
    case ccChanged = "cc_changed"
    case followUpSent = "follow_up_sent"
    case ratingOfferSent = "rating_offer_sent"
    case unknown = ""

}

enum TicketStatus: String {
    case open
    case pending
    case solved
    case spam
}
struct TicketEvent: Decodable {
    let author: Author?
    var type: String?

    let date: String?
    let isPrivate: Bool?
    let message: String?
    let assignee: Assignee?
    let current: String?
    let previous: String?

    enum CodingKeys: String, CodingKey {
        case isPrivate = "is_private"
        case assignee = "to"
        case author, type, date, message, current, previous
    }
}

struct Assignee: Decodable {
    let name: String?
    let id: String?
}

struct TicketResponse: Decodable {
    
    struct Requester: Decodable {
        let mail: String?
        let name: String?
    }
    let id: String
    let assignee: Assignee?
    let requester: Requester?
    let status: String?
    let events: [TicketEvent]?
    let date: String?
    let subject: String?
}

/*
 {
 "assignee": {
 "name": "Service client Oribiky",
 "id": "tech@oribiky.com"
 },
 "events": [
 {
 "author": {
 "id": "harrath.hichem@gmail.com",
 "name": "Harrath Hichem_+33768718540",
 "type": "client"
 },
 "date": "2018-12-03T20:14:44Z",
 "is_private": false,
 "message": "message",
 "type": "message",
 "source": {
 "type": "lc2",
 "url": null
 }
 },
 {
 "date": "2018-12-05T18:15:31Z",
 "to": {
 "name": "Service client Oribiky",
 "id": "tech@oribiky.com"
 },
 "type": "assignee_changed",
 "author": {
 "name": "Service client Oribiky",
 "id": "tech@oribiky.com"
 }
 },
 {
 "date": "2018-12-05T18:15:31Z",
 "current": "solved",
 "previous": "open",
 "type": "status_changed",
 "author": {
 "name": "Service client Oribiky",
 "id": "tech@oribiky.com"
 }
 }
 ],
 "id": "JORX7",
 "requester": {
 "mail": "harrath.hichem@gmail.com",
 "name": "Harrath Hichem_+33768718540",
 "ip": "77.136.87.26"
 },
 "groups": [
 {
 "id": 0,
 "name": "All operators"
 }
 ],
 "status": "solved",
 "subject": "sujet",
 "modified": "2018-12-05T18:15:31Z",
 "source": {
 "type": "lc2",
 "url": null,
 "id": null
 },
 "opened": [
 {
 "from": "2018-12-03T20:14:44Z",
 "to": "2018-12-05T18:15:31Z"
 }
 ],
 "resolutionDate": "2018-12-05T18:15:31Z",
 "firstResponse": {},
 "tags": [],
 "rate": "not_rated",
 "date": "2018-12-03T20:14:44Z",
 "currentGroup": {
 "id": 0,
 "name": "All operators"
 }
 }
 */
