//
//  TicketRequestParams.swift
//  oribiky
//
//  Created by Aymen Harrath on 07/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct TicketRequestParams: RequestBody {
    
    var requesterMail: String
    
    enum CodingKeys: String, CodingKey {
        case requesterMail = "requester[mail]"
    }
}
