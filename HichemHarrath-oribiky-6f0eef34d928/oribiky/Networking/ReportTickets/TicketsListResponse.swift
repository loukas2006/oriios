//
//  TicketsListResponse.swift
//  oribiky
//
//  Created by Aymen Harrath on 06/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct TicketsListResponse: Decodable {
    let pages: Int
    let total: Int
    let tickets: [TicketResponse]
}
