//
//  TicketRequestBody.swift
//  oribiky
//
//  Created by Aymen Harrath on 07/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct TicketRequestBody: RequestBody {
    
    var requesterMail: String
    var message: String
    var subject: String

    enum CodingKeys: String, CodingKey {
        case requesterMail = "requester[mail]"
        case message, subject
    }

}
