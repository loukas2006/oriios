//
//  TicketAPI.swift
//  oribiky
//
//  Created by Aymen Harrath on 06/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

enum TicketAPI: URLRequestConvertible {
    
    case getList(TicketRequestParams)
    case create(TicketRequestBody)

    
    private var method: HTTPMethod {
        switch self {
        case .getList(_):
            return .get
        case .create(_):
            return .post
        }
    }
    
    private var path: String {
        switch self {
        case .getList(_):
            return HTTPAPI.LiveChatTicket.getList
        case .create(_):
            return HTTPAPI.LiveChatTicket.create
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let baseURL = try HTTPAPI.LiveChatTicket.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.addTicketAPIHeaders()
        switch self {
        case .getList(let params):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: params.asDictionary())
        case .create(let body):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: body.asDictionary())

            
        }
        
        return urlRequest
        
    }
}

class TicketService {
    
    static func getAll (params:TicketRequestParams,
                        success:@escaping (TicketsListResponse) -> Void,
                        failure:@escaping BaseServiceFailure) {
        BaseService.call(TicketAPI.getList(params), requireToken: true, success: success, failure: failure)
    }
    
    static func create (body:TicketRequestBody,
                        success:@escaping (TicketResponse) -> Void,
                        failure:@escaping BaseServiceFailure) {
        BaseService.call(TicketAPI.create(body), requireToken: true, success: success, failure: failure)
    }


}
