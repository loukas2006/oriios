//
//  APIManager.swift
//  oribiky
//
//  Created by Hichem Harrath on 11/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

/*
// result enum wil be used in every completion of ws
 // we have success with the Type T (T : is the type of returned object if WS succeeded)
 // and error with the given ServiceError object
*/
enum Result<T> {
    case success(T)
    case failure(ServiceError)
}
