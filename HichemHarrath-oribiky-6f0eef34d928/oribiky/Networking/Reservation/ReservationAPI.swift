//
//  ReservationAPI.swift
//  oribiky
//
//  Created by Aymen Harrath on 05/01/2019.
//  Copyright © 2019 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

enum ReservationAPI: URLRequestConvertible {
    
    case create(Int) //Int: bike id
    case current
    case cancel(Int) //Int: reservation id
    
    private var method: HTTPMethod {
        switch self {
        case .create:
            return .post
        case .current:
            return .get
        case .cancel:
            return .patch
        }
    }
    
    private var path: String {
        switch self {
        case .create:
            return HTTPAPI.Reservation.create
        case .current:
            return HTTPAPI.Reservation.current
        case .cancel(let reservationId):
            return String(format: HTTPAPI.Reservation.cancel, "\(reservationId)")

        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.addAuthorizationToken()
        
        switch self {
        case .create(let bikeId):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["bike" : bikeId])
        default:
            break
        }
        return urlRequest
        
    }
}

class ReservationService {
    
    static func create(bikeId: Int,
                    success:@escaping (ReservationResponse) -> Void,
                    failure:@escaping BaseServiceFailure) {
        BaseService.call(ReservationAPI.create(bikeId), success: success, failure: failure)
    }
    
    static func current(handleSessionExpired: Bool = true,
                        success:@escaping (ReservationResponse) -> Void,
                       failure:@escaping BaseServiceFailure) {
        BaseService.call(ReservationAPI.current, success: success, failure: failure)
    }
    
    static func cancel(reservationId: Int,
                       success:@escaping (ReservationResponse) -> Void,
                       failure:@escaping BaseServiceFailure) {
        BaseService.call(ReservationAPI.cancel(reservationId), success: success, failure: failure)
    }

}
