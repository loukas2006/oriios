//
//  ReservationResponse.swift
//  oribiky
//
//  Created by Aymen Harrath on 05/01/2019.
//  Copyright © 2019 Oribiky. All rights reserved.
//

import Foundation

struct ReservationResponse: Decodable {
    /*
     {
     "id": 158,
     "startedAt": "2019-01-05 19:04:52",
     "endedAt": "2019-01-05 19:14:52",
     "canceledAt": null,
     "canceled": false,
     "notified": false,
     "transformed": false,
     "transformedAt": null,
     "bike": {},
     "createdAt": "2019-01-05T19:04:52+00:00",
     }
     */
    
    let reservationId: Int
    let startedAt: String?
    let endedAt: String?
    let createdAt: String?
    let canceled: Bool?
    let canceledAt: String?
    let transformed: Bool?
    let transformedAt: String?
    let notified: Bool?
    let bike: Bike?
    
    enum CodingKeys: String, CodingKey {
        case reservationId = "id"
        case startedAt, endedAt, createdAt, canceled, canceledAt, transformed, transformedAt, notified, bike
    }
}
