//
//  RideRequestBody.swift
//  oribiky
//
//  Created by Aymen Harrath on 03/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

enum BikeStatusType: String, Codable {
    case brake = "BRAKE"
    case light = "LIGHT"
    case tires = "TIRES"
}

struct BikeLocation: Codable {
    let latitude: Double
    let longitude: Double
}

struct BikeStatus: Codable {
    var status: Bool
    var type: BikeStatusType
    
}

struct RideRequestBody: RequestBody {
/*
     {
     "startAddress": "string",
     "bike": 0,
     "reservation": 0,
     "locations": [
     {
     "latitude": 0,
     "longitude": 0
     }
     ],
     "status": [
     {
     "status": true,
     "type": "BRAKE"
     }
     ],
     "canceled": true
     }
*/
    var startAddress: String
    var bike: Int
//    var reservation: Int
    var locations : [BikeLocation]
    var bikeStatus: [BikeStatus]
    
//    var canceled: Bool
    
    enum CodingKeys: String, CodingKey {
        case bikeStatus = "status"
        case startAddress, bike, locations
        
    }
}

