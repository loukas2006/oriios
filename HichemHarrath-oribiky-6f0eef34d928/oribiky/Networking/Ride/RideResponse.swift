//
//  RideResponse.swift
//  oribiky
//
//  Created by Aymen Harrath on 03/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct RideResponse: Decodable {
    /*
     {
     "id": 362,
     "startedAt": "2018-11-04 19:06:56",
     "endedAt": null,
     "startAddress": "7335 Avenue des Nations Unies, 75116 Paris, France",
     "endAddress": null,
     "canceled": false,
     "bike": {
     "breakdown": true,
     "code": "ORI-100",
     "distance": 12,
     "id": 12,
     "imei": "68199",
     "inUse": true,
     "latitude": 48.60934232,
     "longitude": 2.24008763,
     "numberRides": 1,
     "onService": false
     },
     "locations": [
     {
     "latitude": 48.860629877924,
     "longitude": 2.2911112741564,
     "createdAt": "2018-11-04 19:06:56",
     "updatedAt": null
     }
     ],
     "status": [
     {
     "type": "BRAKE",
     "status": true,
     "createdAt": "2018-11-04 19:06:56",
     "updatedAt": null
     }
     ],
     "createdAt": null,
     "updatedAt": null,
     "price": 0,
     "distance": 0,
     "minutes": 0,
     "_links": {
     "self": {
     "href": "https://api2.oribiky.com/api/rides/362"
     },
     "finish": {
     "href": "https://api2.oribiky.com/api/rides/362/finish"
     }
     }
     }
     */

    let rideId: Int
    let startAddress: String?
    let endAddress: String?
    let startedAt: String?
    let endedAt: String?
    let canceled: Bool?
    let price: Double?
    let minutes: Double?
    let distance: Double?
    let locations: [BikeLocation]?
    let bikeStatus: [BikeStatus]?
    let bike: Bike
    
    enum CodingKeys: String, CodingKey {
        case rideId = "id"
        case bikeStatus = "status"
        case startAddress, endAddress, startedAt, endedAt, canceled, price, minutes, distance, locations, bike
    }
}
