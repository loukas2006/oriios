//
//  FinishRideRequestBody.swift
//  oribiky
//
//  Created by Aymen Harrath on 10/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct FinishRideRequestBody: RequestBody {
    
    var endAddress: String
    var locations : [BikeLocation]
    
}
