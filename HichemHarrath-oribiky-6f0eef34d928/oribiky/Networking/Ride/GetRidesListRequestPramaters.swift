//
//  GetRidesListRequestPramaters.swift
//  oribiky
//
//  Created by Hichem Harrath on 24/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct GetRidesListRequestPramaters: RequestBody {
    var page: Int
    var limit: Int
}
