//
//  RideAPI.swift
//  oribiky
//
//  Created by Aymen Harrath on 03/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

enum RideAPI: URLRequestConvertible {
    
    case getRides(GetRidesListRequestPramaters)
    case createRide(RideRequestBody)
    case deleteRide([Int]) // Liste des ids des trajets séparés par des virgules
    case deleteAllRides
    case getRide(Int) // ride id
    case getCurrentRide
    case finishRide(Int,FinishRideRequestBody) // ride id & Body
    
    private var method: HTTPMethod {
        switch self {
        case .getRides(_), .getRide(_), .getCurrentRide:
            return .get
        case .createRide(_):
            return .post
        case .finishRide(_):
            return .put
        case .deleteRide(_), .deleteAllRides:
            return .delete
            
        }
    }
    
    private var path: String {
        switch self {
        case .getRides(_), .getRide(_):
            return HTTPAPI.Ride.get
        case .createRide(_):
            return HTTPAPI.Ride.create
        case .deleteRide(_):
            return HTTPAPI.Ride.delete
        case .deleteAllRides:
            return HTTPAPI.Ride.deleteAll
        case .getCurrentRide:
            return HTTPAPI.Ride.getCurrent
        case .finishRide (let rideId, _):
            return String(format: HTTPAPI.Ride.finish, "\(rideId)")
            
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.addAuthorizationToken()
        
        switch self {
        case let .getRides(params):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: params.asDictionary())
        case let .createRide (body):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: body.asDictionary())
        case .finishRide(_, let body):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: body.asDictionary())
        case let .deleteRide(ids):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: ["ids": ids.compactMap({String($0)}).joined(separator: ",")])
        default:
            break
        }
        return urlRequest
    }
    
}

struct RideService {
    
    //    static func get(success:@escaping (ProfileResponse) -> Void,
    //                    failure:@escaping BaseServiceFailure) {
    //        BaseService.call(ProfileAPI.getProfile, requireToken: true, success: success, failure: failure)
    //    }
    //
    static func createRide(body: RideRequestBody,
                           success:@escaping (RideResponse) -> Void,
                           failure:@escaping BaseServiceFailure) {
        
        BaseService.call(RideAPI.createRide(body), requireToken: true, success: success, failure: failure)
    }
    
    static func getCurrent(handleSessionExpired: Bool = true, success:@escaping (RideResponse) -> Void,
                           failure:@escaping BaseServiceFailure) {
        BaseService.call(RideAPI.getCurrentRide, requireToken: true, checkForSession: handleSessionExpired, success: success, failure: failure)
    }
    
    static func finishRide (rideId: Int,
                            body: FinishRideRequestBody,
                            success:@escaping (RideResponse) -> Void,
                            failure:@escaping BaseServiceFailure) {
        print("finishRide")
        BaseService.call(RideAPI.finishRide(rideId, body), requireToken: true, success: success, failure: failure)
        
    }
    
    static func getRides(params: GetRidesListRequestPramaters,
                         success:@escaping (RidesListResponse) -> Void,
                         failure:@escaping BaseServiceFailure) {
        BaseService.call(RideAPI.getRides(params), success: success, failure: failure)
    }
    
    static func deleteRides(ids: [Int],
                            success:@escaping (RidesListResponse) -> Void,
                            failure:@escaping BaseServiceFailure) {
        BaseService.call(RideAPI.deleteRide(ids), success: success, failure: failure)
    }
    
    
}
