//
//  RidesListResponse.swift
//  oribiky
//
//  Created by Hichem Harrath on 24/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
struct RidesListResponse : Decodable{
    
    let meta: Meta
    let rides: [RideResponse]
    
    enum CodingKeys: String, CodingKey {
        case meta
        case rides = "data"
    }

}
