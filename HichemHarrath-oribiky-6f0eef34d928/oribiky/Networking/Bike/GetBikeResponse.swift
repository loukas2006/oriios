//
//  GetBikeResponse.swift
//  oribiky
//
//  Created by Hichem Harrath on 19/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct Bike: Decodable {
    /*
     "breakdown": false,
     "code": "ORI-100",
     "distance": 47,
     "id": 5,
     "imei": "12341",
     "inUse": false,
     "latitude": 48.924322,
     "longitude": 2.362731,
     "onService": true
     */
    let breakdown: Bool
    let code: String
    let distance: Double
    let bikeId: Int
    let imei: String
    let inUse: Bool
    let latitude: Double
    let longitude: Double
    let onService: Bool
    enum CodingKeys: String, CodingKey {
        case bikeId = "id"
        case breakdown, code, distance, imei, inUse, latitude, longitude, onService
    }
}
struct Meta: Decodable {
    /*limit: 25,
     total_items: 150,
     next_page: "http://api.oribiky.com/api/bikes?imei=&limit=25&location=&page=2",
     previous_page: ""*/
    let limit: Int
    let totalItems: Int
    let nextPage: String
    let previousPage: String
    
    enum CodingKeys: String, CodingKey {
        case limit
        case totalItems = "total_items"
        case nextPage = "next_page"
        case previousPage = "previous_page"
    }
}

struct GetBikeResponse: Decodable {

    let meta: Meta
    let bikes : [Bike]
    enum CodingKeys: String, CodingKey {
        case meta
        case bikes = "data"
    }
}
