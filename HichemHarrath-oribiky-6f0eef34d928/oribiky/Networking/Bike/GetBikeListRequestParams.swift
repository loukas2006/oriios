//
//  GetBikeListRequestParams.swift
//  oribiky
//
//  Created by Hichem Harrath on 18/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct GetBikeListRequestParams: RequestBody {

    var location: String
    var imei: String
    var page: Int
    var limit: Int

}
