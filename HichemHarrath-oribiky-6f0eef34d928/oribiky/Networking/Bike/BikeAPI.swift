//
//  BikeAPI.swift
//  oribiky
//
//  Created by Hichem Harrath on 18/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

enum BikeAPI: URLRequestConvertible {
    // Cases
    case getBikesList(params: GetBikeListRequestParams)
    case getBike(imei: String)
    case getBikeByCode(code: String)
    private var method: HTTPMethod {
        return .get
    }
    
    private var path: String {
        return HTTPAPI.Bike.get
    }
    
    internal func asURLRequest() throws -> URLRequest {
        
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        switch self {
        case .getBikesList(let params):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: params.asDictionary())
        case .getBike(let imei):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: ["imei":imei])
        case .getBikeByCode(let code):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: ["code":code])

        }
        return urlRequest
    }
}


struct BikeService {
    
    static func getList(params: GetBikeListRequestParams,
                        success:@escaping (GetBikeResponse) -> Void,
                        failure:@escaping BaseServiceFailure) {
        
        BaseService.call(BikeAPI.getBikesList(params: params),
                         success: success,
                         failure: failure)
        
    }
    
    static func get(imei: String,
                    success:@escaping (GetBikeResponse) -> Void,
                    failure:@escaping BaseServiceFailure) {
        
        BaseService.call(BikeAPI.getBike(imei: imei),
                         success: success,
                         failure: failure)
        
    }
    static func get(code: String,
                    success:@escaping (GetBikeResponse) -> Void,
                    failure:@escaping BaseServiceFailure) {
        
        BaseService.call(BikeAPI.getBikeByCode(code: code),
                         success: success,
                         failure: failure)
        
    }
}

