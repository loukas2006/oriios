//
//  PackagesAPI.swift
//  oribiky
//
//  Created by Hichem Harrath on 18/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

import Alamofire

enum PackagesAPI: URLRequestConvertible {

    case packages(PackageRequestBody)
    
    private var method: HTTPMethod {
        switch self {
        case .packages(_):
            return .post
        }
    }
    
    private var path: String {
        switch self {
        case .packages(_):
            return HTTPAPI.Package.packages
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        urlRequest.addAuthorizationToken()
        
        switch self {
        case .packages(let body):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: body.asDictionary())
        }
        
        return urlRequest

    }
}

class PackagesService {
    
    static func add(body: PackageRequestBody,
                    success:@escaping (ProfileResponse) -> Void,
                    failure:@escaping BaseServiceFailure) {
        BaseService.call(PackagesAPI.packages(body), success: success, failure: failure)
    }
}
