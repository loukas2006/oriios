//
//  PackageRequestBody.swift
//  oribiky
//
//  Created by Hichem Harrath on 18/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct PackageRequestBody: RequestBody {
    
    var type: String
}
