//
//  ParkingRequestBody.swift
//  oribiky
//
//  Created by Aymen Harrath on 10/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
struct ParkingRequestBody: RequestBody{
    /*
     {
     "latitude": 0,
     "longitude": 0,
     "address": "string"
     }

     */
    
    var latitude: Double
    var longitude: Double
    var address: String

}
