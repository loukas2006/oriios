//
//  ParkingResponse.swift
//  oribiky
//
//  Created by Aymen Harrath on 10/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct Parking: Codable {
    var parkingId: Int
    var address: String
    var latitude: Double
    var longitude: Double

    enum CodingKeys: String, CodingKey {
        case parkingId = "id"
        case address, latitude, longitude
        
    }

}
struct ParkingResponse: Decodable {
    /*
    {
    "parking": {
    "id": 0,
    "address": "string",
    "latitude": 0,
    "longitude": 0
    },
    "distance": 0
    }
    */
    var parking: Parking
    var distance: String
    
}
