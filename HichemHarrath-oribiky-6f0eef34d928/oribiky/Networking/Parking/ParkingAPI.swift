//
//  ParkingAPI.swift
//  oribiky
//
//  Created by Aymen Harrath on 10/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

enum ParkingAPI: URLRequestConvertible {
    
    case search(Double, Double) //latitude & longitude
    case getAll
    case addNew(ParkingRequestBody)
    
    private var method: HTTPMethod {
        switch self {
        case .search, .getAll:
            return .get
        case .addNew(_):
            return .post
            
        }
    }
    
    private var path: String {
        switch self {
        case .search:
            return HTTPAPI.Parking.search
        case .getAll:
            return HTTPAPI.Parking.getAll
        case .addNew(_):
            return HTTPAPI.Parking.addNew
            
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.addAuthorizationToken()
        
        switch self {
        case .search(let latitude, let longitude):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: ["location": "\(latitude),\(longitude)"])
        case .addNew(let body):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: body.asDictionary())

        default:
            break
        }
        return urlRequest
    }
    
}
class ParkingService {
    
    static func search(latitude: Double,
                       longitude: Double,
                       success:@escaping (ParkingResponse) -> Void,
                       failure:@escaping BaseServiceFailure) {
        
        BaseService.call(ParkingAPI.search(latitude, longitude), requireToken: false, success: success, failure: failure)
    }
    
    static func add(body: ParkingRequestBody,
                       success:@escaping (Parking) -> Void,
                       failure:@escaping BaseServiceFailure) {
        
        BaseService.call(ParkingAPI.addNew(body), success: success, failure: failure)
    }


}

