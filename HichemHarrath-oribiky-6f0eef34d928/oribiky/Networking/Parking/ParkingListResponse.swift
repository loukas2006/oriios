//
//  ParkingListResponse.swift
//  oribiky
//
//  Created by Aymen Harrath on 10/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
struct ParkingListResponse: Decodable {
    /*
     {
     "meta": {
     "limit": 0,
     "total_items": 0,
     "next_page": "string",
     "previous_page": "string"
     },
     "data": [
     {
     "id": 0,
     "address": "string",
     "latitude": 0,
     "longitude": 0
     }
     ]
     }
     */
    let meta: Meta
    let parkings : [Parking]
    enum CodingKeys: String, CodingKey {
        case meta
        case parkings = "data"
    }

}
