//
//  PaymentAPI.swift
//  oribiky
//
//  Created by Aymen Harrath on 06/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import  Alamofire

enum PaymentAPI: URLRequestConvertible {
    
    // Cases
    case addCard(PaymentRequestBody)
    case charge(PaymentRequestBody)
    case updateCard(PaymentRequestBody)
    
    private var method: HTTPMethod {
        switch self {
        case .addCard, .charge:
            return .post
        case .updateCard:
            return .put
        }
    }
    
    private var path: String {
        switch self {
        case .addCard:
            return HTTPAPI.Payment.addCard
        case .charge:
            return HTTPAPI.Payment.charge
        case .updateCard:
            return HTTPAPI.Payment.updateCard
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        urlRequest.addAuthorizationToken()
        
        switch self {
        case let .addCard(body):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: body.asDictionary())
        case let .charge(body):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: body.asDictionary())
        case let .updateCard(body):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: body.asDictionary())
        }
        return urlRequest
    }
}

struct PaymentService {
    
    static func addCard(body: PaymentRequestBody,
                    success:@escaping (ProfileResponse) -> Void,
                    failure:@escaping BaseServiceFailure) {
        BaseService.call(PaymentAPI.addCard(body), requireToken: true, success: success, failure: failure)
    }
    
    static func updateCard(body: PaymentRequestBody,
                        success:@escaping (ProfileResponse) -> Void,
                        failure:@escaping BaseServiceFailure) {
        BaseService.call(PaymentAPI.updateCard(body), requireToken: true, success: success, failure: failure)
    }
    
    static func charge(body: PaymentRequestBody,
                           success:@escaping (ProfileResponse) -> Void,
                           failure:@escaping BaseServiceFailure) {
        BaseService.call(PaymentAPI.charge(body), requireToken: true, success: success, failure: failure)
    }


}

