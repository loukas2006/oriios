//
//  PaymentRequestBody.swift
//  oribiky
//
//  Created by Aymen Harrath on 06/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
struct PaymentRequestBody: RequestBody {

    var tokenId : String
    
    init(aTokenId: String) {
        tokenId = aTokenId
    }
    
    enum CodingKeys: String, CodingKey {
        case tokenId = "id"
    }

    
}
