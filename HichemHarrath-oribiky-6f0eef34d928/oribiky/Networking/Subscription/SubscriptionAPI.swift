//
//  SubscriptionAPI.swift
//  oribiky
//
//  Created by Hichem Harrath on 06/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Alamofire

enum SubscriptionAPI: URLRequestConvertible {
   
    case subscribe(SubscribeRequestBody)
    case upload
    case unsubscribe
    case previewChange(SubscribeRequestBody)
    case change(SubscribeRequestBody)
    case reactivate
    
    private var method: HTTPMethod {
        switch self {
        case .subscribe(_):
            return .post
        case .upload:
            return .post
        case .unsubscribe:
            return .delete
        case .previewChange(_):
            return .put
        case .change(_):
            return .put
        case .reactivate:
            return .post
        }
    }
    private var path: String {
        switch self {
        case .subscribe(_):
            return HTTPAPI.Subscribe.subscribe
        case .upload:
            return "subscriptions/upload/documents"
        case .unsubscribe:
            return HTTPAPI.Subscribe.unsubscribe
        case .previewChange(_):
            return HTTPAPI.Subscribe.previewChange
        case .change(_):
            return HTTPAPI.Subscribe.change
        case .reactivate:
            return HTTPAPI.Subscribe.reactivate
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let baseURL = try HTTPAPI.baseURL.asURL()
        
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.addAuthorizationToken()

        switch self {
        case let .subscribe(body):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: body.asDictionary())
        case let .previewChange(params):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: params.asDictionary())
        case let .change(params):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: params.asDictionary())
        default:
            break
            
        }
        return urlRequest
    }
}

struct SubscriptionService {
    
    static func subscribe(body: SubscribeRequestBody,
                          success:@escaping (ProfileResponse) -> Void,
                          failure:@escaping BaseServiceFailure){
        BaseService.call(SubscriptionAPI.subscribe(body), requireToken: true, success: success, failure: failure)
    }
    
    static func cancelSubscription (success:@escaping (ProfileResponse) -> Void,
                                    failure:@escaping BaseServiceFailure) {
        
        BaseService.call(SubscriptionAPI.unsubscribe, requireToken: true, success: success, failure: failure)
    }
    
    static func reactivateSubscription(success:@escaping (ProfileResponse) -> Void,
                                       failure:@escaping BaseServiceFailure) {
        BaseService.call(SubscriptionAPI.reactivate, requireToken: true, success: success, failure: failure)
    }
    
    static func uploadFiles(images: [Data],
                            success:@escaping (ProfileResponse) -> Void,
                            failure:@escaping BaseServiceFailure) {
        
        let imageParamName = "documents[%i][documentFile]"
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (index, imageData) in images.enumerated() {
                print(String(format: imageParamName, index))
                multipartFormData.append(imageData, withName: String(format: imageParamName, index),
                                         fileName: "\(Date().timeIntervalSince1970).jpeg",
                    mimeType: "image/jpeg")
            }

        }, with: SubscriptionAPI.upload) { result in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in

                    guard let data = response.data , let responseObject = try? JSONDecoder().decode(ProfileResponse.self, from: data) else {
                        failure("Problème d'upload", nil)
                        return
                    }
                    success(responseObject)
                }
            case .failure(let error):
                failure(error.localizedDescription, nil)
            }

        }
    }
}
