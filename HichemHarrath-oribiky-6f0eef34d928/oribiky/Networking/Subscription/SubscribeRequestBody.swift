//
//  SubscribeRequestBody.swift
//  oribiky
//
//  Created by Hichem Harrath on 06/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

/*
 {
 "subscription_type": "MONTHLY",
 "documents": [
 {
 "name": "string",
 "file": "string"
 }
 ]
 }

 */
struct Document: Encodable {
    var name: String
    var file: String
}

struct SubscribeRequestBody: RequestBody {
  
    // MARK: - Variables
    var subscriptionType: String
    var documents: [Document]?

    enum CodingKeys: String, CodingKey {
        case subscriptionType = "subscription_type"
        case documents
    }
    
    
    // MARK: - Init
    init(type: SubscriptionType, images: [UIImage]) {
        subscriptionType = type.rawValue
        var tmpDocuments = [Document]()

        for (index, image) in images.enumerated() {
            if let imagedata = image.base64(format: .jpeg(0.1)) {
                tmpDocuments.append(Document(name: "file\(index + 1)", file: imagedata))
            }
        }

        //test.plist
        documents = tmpDocuments
        
    }

    init(type: SubscriptionType) {
        subscriptionType = type.rawValue
    }
}
