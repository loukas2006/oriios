//
//  TicketDetailsViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 03/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class TicketDetailsViewModel: BaseViewModel {
    
    var ticket: TicketResponse
    var events = [TicketEvent]()
    init(ticket: TicketResponse) {
        if let events = ticket.events?.filter({ (event) -> Bool in [EventType.message, EventType.assigneeChanged, EventType.statusChanged].contains(EventType(rawValue: event.type ?? ""))}) {
            self.events = events
        }
        self.ticket = ticket
    }
}
