//
//  ReportViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 25/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class ReportViewModel: BaseViewModel {
    
    //MARK: - Actions
    func navigateToPdfViewer(withPdf pdf: PDF)  {
        let viewModel = PdfViewerViewModel(withPDF: pdf)
        AppCoordinator.shared.transition(to: .pdfViewer(viewModel, false), type: .push, animated: true)
    }

    func navigateToMyTicketsList()  {
        if checkUserRights() {
            AppCoordinator.shared.transition(to: .myTicketsList(MyTicketsViewModel()), type: .push, animated: true)

        }
    }

    func navigateToChat()  {
        if checkUserRights() {
            LiveChatManager.shared.present()
        }
    }
    
    func callSupport()  {
        if let url = URL(string: "tel://+33\(Constant.ClientSupport.phoneNumber.replacingOccurrences(of: " ", with: ""))") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    private func checkUserRights () -> Bool {
        if !DefaultsManager.shared.isConnected {
            AppCoordinator.shared.transition(to: .authenticate(AuthenticateViewModel(index: -1, pushed: true), true),
                                             type: .modal,
                                             animated: true)
            return false
        }
        if !BikeRentManager.shared.getUser().email.isAValidEmailAddress() {
            showErrorPopuo(subtitle: APPStrings.Assistance.NoEmailError.subtitle)
            return false
        }
        return true
    }
    func showCallSupportPopup(completion: @escaping () -> Void) {
        /*
        let callAction = PopupAction(title: APPStrings.Common.yes, style: .validate) {
            // hide popup
            AppCoordinator.shared.pop(animated: true, completion: {
                completion()
            })
        }
        let cancelAction = PopupAction(title: APPStrings.Common.no, style: .cancel) {
            // hide popup
            AppCoordinator.shared.pop(animated: true)
        }
        
        let viewModel = PopupViewModel(type: .choice(title: nil,
                                                     subTitle: APPStrings.CurrentRide.ConfirmationPopup.subtitle,
                                                     leftImage: nil,
                                                     rightImage: nil,
                                                     yesActions: callAction, noAction: cancelAction))
        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)
         */
    }
}
