//
//  TicketEventViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 03/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

extension TicketEvent {
    var title: String {

        if let type = author?.type, type == .client {
            return APPStrings.Assistance.TicketDetails.titleFormat(date?.getTicketDateStr() ?? "" , APPStrings.Common.youHave)
        } else {
            return APPStrings.Assistance.TicketDetails.titleFormat(date?.getTicketDateStr() ?? "" , APPStrings.Common.oribikyHave)
        }
    }
}

extension TicketStatus {
    var title: String {
        switch self {
        case .open:
            return APPStrings.Assistance.TicketStatus.open
        case .pending:
            return APPStrings.Assistance.TicketStatus.pending
        case .solved:
            return APPStrings.Assistance.TicketStatus.solved
        case .spam:
            return APPStrings.Assistance.TicketStatus.spam
        }
    }
}
class TicketEventViewModel: BaseViewModel {
    
    //MARK - Variables
    var event : TicketEvent
    
    //MARK - Init
    init(event: TicketEvent) {
        self.event = event
    }
    
}
