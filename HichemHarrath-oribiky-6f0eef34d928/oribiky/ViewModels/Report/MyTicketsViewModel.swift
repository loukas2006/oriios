//
//  MyTicketsViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 28/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
class MyTicketsViewModel: BaseViewModel {
    
    //MARK: - variables
    
    //MARK: - Web service
    func callWs (success:@escaping ([TicketResponse]) -> Void,
                 failure:@escaping (String) -> Void) {
        
        let user = BikeRentManager.shared.getUser()
        if !user.email.isAValidEmailAddress() {
            success([])
            return
        }
        let params = TicketRequestParams(requesterMail : user.email)
        
        TicketService.getAll(params: params, success: { (ticketsList) in
            success(ticketsList.tickets)
        }) { (errorStr, errors) in
            failure(errorStr)
        }
    }
    //MARK: - Actions
    func navigateToCreateTicket()  {
        AppCoordinator.shared.transition(to: .createTicket(CreateTicketViewModel()), type: .push, animated: true)
    }
    
    func navigateToTicketDetails(ticketViewModel: TicketViewModel)  {
        AppCoordinator.shared.transition(to: .ticketDetails(TicketDetailsViewModel(ticket: ticketViewModel.ticket)), type: .push, animated: true)
    }
    
    

}
