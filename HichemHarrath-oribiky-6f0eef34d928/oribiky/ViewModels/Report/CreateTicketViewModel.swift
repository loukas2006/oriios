//
//  CreateTicketViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 28/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
class CreateTicketViewModel: BaseViewModel {
    
    //MARK: - WS
    
    func callWS (subject: String, message: String, success: @escaping () -> Void, failure: @escaping (String) -> Void) {
        let user = BikeRentManager.shared.getUser()
        let body = TicketRequestBody(requesterMail: user.email, message: message, subject: subject)
        
        TicketService.create(body: body, success: { (ticket) in
            success()
        }) { (errorStr, errors) in
            failure(errorStr)
        }
        
    }
    
}
