//
//  TicketViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 28/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

extension TicketResponse {
    
    var statusImage: UIImage?  {
        guard let status = status else {return nil}
        switch status {
        case TicketStatus.open.rawValue:
            return Asset.Assistance.reclamationOpened.image
        case TicketStatus.pending.rawValue:
            return Asset.Assistance.reclamationPending.image
        case TicketStatus.spam.rawValue:
            return Asset.Assistance.reclamationSpam.image
        case TicketStatus.solved.rawValue:
            return Asset.Assistance.reclamationSolved.image
        default:
            return Asset.Assistance.reclamationOpened.image

        }
    }

}
class TicketViewModel: BaseViewModel {
    
    //MARK: - Variables
    var ticket: TicketResponse
    
    
    //MARK: - Init

    init(ticket: TicketResponse) {
        self.ticket = ticket
    }

}
