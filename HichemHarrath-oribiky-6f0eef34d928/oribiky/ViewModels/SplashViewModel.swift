//
//  SplashViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 22/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class SplashViewModel: BaseViewModel {
    
    // MARK: - Variables
    let titleLabelText =  APPStrings.Splash.title
    let subTitleLabelText =  APPStrings.Splash.subtitle
    let howItWorksBtnTitle = APPStrings.Splash.howItWorksBtnTitle
    let startNowBtnTitle = APPStrings.Splash.startNowBtnTitle
    
    // MARK: - Actions
    func navigateToDemo() {
        AppCoordinator.shared.transition(to: .demo(DemoViewModel()), type: .root, animated: true)
    }
    
}
