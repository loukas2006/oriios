//
//  BaseViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 29/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation


class BaseViewModel: NSObject {
    
    // MARK: - Variable

    // MARK: - Actions
    func dismiss() {
        AppCoordinator.shared.pop(animated: true)
    }

    func showErrorPopuo(title: String = "",
                        subtitle: String,
                        buttonTitle: String = APPStrings.Common.ok) {
        let popupBtnAction = PopupAction(title: buttonTitle, style: .validate, action: {
            AppCoordinator.shared.pop(animated: true)
        })
        let popupViewModel = PopupViewModel(type: .info(title: title,
                                                        subTitle: subtitle,
                                                        action: popupBtnAction))
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
    }
    
    func navigateToTabbar() {
        AppCoordinator.shared.transition(to: .tabbar(TabbarViewModel()), type: .root, animated: true)
    }
    // MARK: - Authentication process
    func showScanCardPopup(user: UserProfile)  {
        
        let popupBtnAction = PopupAction(title: APPStrings.ScanCard.Popup.scanBtnTitle, style: .validate, action: {
            // cardViewModel
            let editProfileViewModel = EditProfileViewModel(aUserProfile: user, anEditType: .card)
            AppCoordinator.shared.pop(animated: true, completion: {
                
                AppCoordinator.shared.transition(to: .newCard(editProfileViewModel, true), type: .modal, animated: true)
            })
        })
        
        let popupViewModel = PopupViewModel(type: .info(title: nil, subTitle: APPStrings.ScanCard.Popup.subtitle, action: popupBtnAction))
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
        
    }
}
