//
//  NewCardViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 24/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

enum TermsOptions {
    case termsOfUseAndSale
}

class ScanCardViewModel {

    
    private var isTermsOfUseAndSale = false
    
    let defaultStateCheckBoxImage = Asset.Card.checkboxOff.image
    
    var scanCompletionHandler: ((CardIOCreditCardInfo) -> Void)
    
    init(completion: @escaping (CardIOCreditCardInfo) -> Void) {
        scanCompletionHandler = completion
    }
    

    func toggleOption (option: TermsOptions, value: Bool) {
        switch option {
        case .termsOfUseAndSale:
            isTermsOfUseAndSale = value
        }
    }
    
    func isTermsAccepted() -> Bool {
        return isTermsOfUseAndSale
    }
    
    func showTermsErrorPopUp() {
        
        let popupBtnAction = PopupAction(title: APPStrings.NewCard.TermsAcceptationPopup.okBtnTitle, style: .validate, action: {
            // here we should call ws
            AppCoordinator.shared.pop(animated: true, completion:nil)
        })
        
        let popupViewModel = PopupViewModel(type: .info(title: nil, subTitle: APPStrings.NewCard.TermsAcceptationPopup.subtitle, action: popupBtnAction))
        
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)

    }
    
    func showCautionRequiredPopup (completion: @escaping () -> Void) {
        
        let confirmAction = PopupAction(title: APPStrings.Common.confirm, style: .validate) {
            // hide popup
            AppCoordinator.shared.pop(animated: true, completion: {
                completion()
            })
        }
        let cancelAction = PopupAction(title: APPStrings.Common.cancel, style: .cancel) {
            // hide popup
            AppCoordinator.shared.pop(animated: true)
        }
        
        let viewModel = PopupViewModel(type: .choice(title: nil,
                                                     subTitle: NSAttributedString(string:  APPStrings.NewCard.CautionRequiredPopup.subtitle),
                                                     leftImage: nil,
                                                     rightImage: nil,
                                                     yesActions: confirmAction, noAction: cancelAction))
        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)
        
        
    }
    
    
//    func callAddCardWs(streetNumber: Int, streetName: String, locality: String, postalCode: Int,
//                           success:@escaping (UserProfile) -> Void,
//                           failure:@escaping BaseServiceFailure) {
//        
//        let params = EditAddressRequestParams(streetNumber: streetNumber,
//                                              streetName: streetName,
//                                              locality: locality,
//                                              postalCode: postalCode)
//        
//        ProfileService.editAddress(params: params, success: { (profileResponse) in
//            success(UserProfile(profileResponse: profileResponse))
//        }, failure: failure)
//    }

    func navigateToPdfViewer(withPdf pdf: PDF)  {
        let viewModel = PdfViewerViewModel(withPDF: pdf)
        AppCoordinator.shared.transition(to: .pdfViewer(viewModel, false), type: .push, animated: true)
    }

}
