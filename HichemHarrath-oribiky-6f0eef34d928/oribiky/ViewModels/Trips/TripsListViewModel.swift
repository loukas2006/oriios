//
//  TripsListViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 25/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class TripsListViewModel: BaseViewModel {
    
    // MARK: - Service Call
    func callws(success:@escaping ([RideResponse]) -> Void,
                failure:@escaping (String) -> Void)  {
        let params = GetRidesListRequestPramaters(page: 1, limit: 25)
        RideService.getRides(params: params, success: { (ridesListResponse) in
            success(ridesListResponse.rides)
        }) { (error, errors) in
            failure(error)
        }
    }
    
    func callDeleteWs(ids:[Int],
                      success:@escaping ([RideResponse]) -> Void,
                      failure:@escaping (String) -> Void) {
        RideService.deleteRides(ids: ids
            , success: { (ridesListResponse) in
                success(ridesListResponse.rides)
        }) {(error, errors) in
            failure(error)
        }
    }
    
    //MARK: - Actions
    func navigateToRideDetails(ride: RideResponse) {
        AppCoordinator.shared.transition(to: .rideResume(RideResumeViewModel(ride: ride), false), type: .push, animated: true)
    }
    
    func showDeleteTripsConfirmationPopup(tripsCount : Int, completion: @escaping () -> Void)  {
        let deleteAction = PopupAction(title: APPStrings.Common.yes, style: .validate) {
            // hide popup
            AppCoordinator.shared.pop(animated: true, completion: {
                completion()
            })
        }
        let cancelAction = PopupAction(title: APPStrings.Common.no, style: .cancel) {
            // hide popup
            AppCoordinator.shared.pop(animated: true)
        }
        
        let viewModel = PopupViewModel(type: .choice(title: nil,
                                                     subTitle: NSAttributedString(string: tripsCount > 1 ? APPStrings.Trips.DeleteConfirmationPopup.SubTitle.mutiple : APPStrings.Trips.DeleteConfirmationPopup.SubTitle.single),
                                                     leftImage: nil,
                                                     rightImage: nil,
                                                     yesActions: deleteAction, noAction: cancelAction))
        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)

    }

}
