//
//  TripViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 25/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class TripViewModel : BaseViewModel {
    
    // MARK: - Variables
    let trip: RideResponse
    var startDateStr = "-"
    var isEditMode: Bool = false
    var isSelectedToDelete = false 
    
    // MARK: - Init
    init(trip: RideResponse) {
        self.trip = trip
        let dateFormatter = DateFormatter(withFormat: Constant.Date.format, locale: "fr_FR")
        guard let dateStr = trip.startedAt, let startDate = dateFormatter.date(from: dateStr) else { return }
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        startDateStr = dateFormatter.string(from: startDate)
    }
}
