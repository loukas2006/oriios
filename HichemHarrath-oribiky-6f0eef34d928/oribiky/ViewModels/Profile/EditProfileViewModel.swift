//
//  EditProfileViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 19/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Stripe

enum EditProfileType {
    case personalInformation
    case address
    case card
}

class EditProfileViewModel: BaseViewModel {
    
    // MARK: - Variables
    var userProfile: UserProfile
    let editType: EditProfileType
    let isFromAuthenticationProcess: Bool
    
    private var isNewsletterOn = false
    
    // MARK: - Initilizer
    init(aUserProfile: UserProfile, anEditType: EditProfileType, isFromAuthenticationProcess: Bool = false) {
        userProfile = aUserProfile
        editType = anEditType
        self.isFromAuthenticationProcess = isFromAuthenticationProcess
        isNewsletterOn = userProfile.newsletter
    }
    
    
    // MARK: - Methods
    func prepareDataSource () -> [EditableFormItemViewModel] {
        
        var dataSource: [EditableFormItemViewModel] = []
        
        switch editType {
        case .personalInformation:
            
            var formItem = EditableFormItemViewModel(type: .firstName, value: userProfile.firstname)
            dataSource.append(formItem)
            
            formItem = EditableFormItemViewModel(type: .lastName, value: userProfile.lastname)
            dataSource.append(formItem)
            
            formItem = EditableFormItemViewModel(type: .email, value: userProfile.email)
            dataSource.append(formItem)
            
        case .address:
            
            var formItem = EditableFormItemViewModel(type: .streetNumber, value:  userProfile.address.streetNumber == nil ? "" : "\(userProfile.address.streetNumber!)" )
            dataSource.append(formItem)
            
            formItem = EditableFormItemViewModel(type: .streetName, value: userProfile.address.streetName ?? "")
            dataSource.append(formItem)
            
            formItem = EditableFormItemViewModel(type: .locality, value: userProfile.address.locality ?? "")
            dataSource.append(formItem)
            
            formItem = EditableFormItemViewModel(type: .postalCode, value: userProfile.address.postalCode == nil ? "" : "\(userProfile.address.postalCode!)")
            
            dataSource.append(formItem)
            
        case .card:
            
            var formItem = EditableFormItemViewModel(type: .cardOwner, value : "")
            dataSource.append(formItem)
            
            formItem = EditableFormItemViewModel(type: .cardNumber, value : "")
            dataSource.append(formItem)
            
            formItem = EditableFormItemViewModel(type: .expireDate, value : "")
            dataSource.append(formItem)
            
            formItem = EditableFormItemViewModel(type: .cvvCode, value : "")
            dataSource.append(formItem)

        }
        
        return dataSource
    }
    
    func editableItem (forType type:FormItemType, in dataSource: [EditableFormItemViewModel]) -> EditableFormItemViewModel? {
        return dataSource.filter({ $0.type == type}).first
    }
    
    func isDataSourceValid (dataSource: [EditableFormItemViewModel]) -> Bool {
        dataSource.forEach { (item) in
            item.validate()
        }
        return !dataSource.map({$0.isValid}).contains(false)
    }
    
    func togglePromotionalOffersOption (value: Bool){
        isNewsletterOn = value
    }
    func isNewsLetterAccespted () -> Bool{
       return isNewsletterOn == true
    }
    
    // MARK: - Web Service
    func callWs(dataSource: [EditableFormItemViewModel],
                success:@escaping (UserProfile) -> Void,
                failure:@escaping BaseServiceFailure)  {
        
        switch editType {
        case .personalInformation:
            
            if let fName = editableItem(forType: .firstName, in: dataSource)?.editedValue,
                let lName = editableItem(forType: .lastName, in: dataSource)?.editedValue,
                let mail = editableItem(forType: .email, in: dataSource)?.editedValue {
                callEditPersonalInoformationWs(firstname: fName, lastName: lName, email: mail, newsletter: isNewsletterOn, success: success, failure: failure)
            }
        case .address:
            if let streetNumber =  Int(editableItem(forType: .streetNumber, in: dataSource)?.editedValue ?? ""),
                let streetName =  editableItem(forType: .streetName, in: dataSource)?.editedValue,
                let locality =  editableItem(forType: .locality, in: dataSource)?.editedValue,
                let postalCode =  Int(editableItem(forType: .postalCode, in: dataSource)?.editedValue ?? "") {
                
                callEditAddressWs(streetNumber: streetNumber, streetName: streetName, locality: locality, postalCode: postalCode, success: success, failure: failure)
            }
        default:
            break
        }
    }
    
    //MARK : - Actions
    func handleAddCardSuccess() {
        if isFromAuthenticationProcess {
//            delegate?.userDidAddACard()
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.reservationRequirementsNotificationName), object: StepType.payment)

        } else {
            dismiss()
        }
    }
    
    //completion: (CardIOCreditCardInfo) -> Void
    func navigateToScanCard(newCardViewModel: ScanCardViewModel) {
        AppCoordinator.shared.transition(to: .scanCard(newCardViewModel), type: .push, animated: true)
    }
    
    func navigateToProfileViewController() {
        AppCoordinator.shared.pop(animated: true)
    }
    
    // MARK: - Private
    private func callEditPersonalInoformationWs(firstname: String, lastName: String, email: String, newsletter: Bool,
                                                success:@escaping (UserProfile) -> Void,
                                                failure:@escaping BaseServiceFailure)  {
        
        let parmas = EditProfileRequestParams(email: email,
                                              firstname: firstname,
                                              lastname: lastName,
                                              device: userProfile.device,
                                              fcmRegistrationId: userProfile.fcmRegistrationId,
                                              newsletter: newsletter)
        
        ProfileService.editProfile(params: parmas, success: {[weak self] (profileResponse) in
            guard let `self` = self else { return }
            self.userProfile = UserProfile(profileResponse: profileResponse)
            //Update  User
            BikeRentManager.shared.updateUser(user: self.userProfile)
            success(self.userProfile)
        }, failure: failure)
        
    }
    
    private func callEditAddressWs(streetNumber: Int, streetName: String, locality: String, postalCode: Int,
                                   success:@escaping (UserProfile) -> Void,
                                   failure:@escaping BaseServiceFailure) {
        
        let params = EditAddressRequestParams(streetNumber: streetNumber,
                                              streetName: streetName,
                                              locality: locality,
                                              postalCode: postalCode)
        
        ProfileService.editAddress(params: params, success: {[weak self] (profileResponse) in
            guard let `self` = self else { return }
            self.userProfile = UserProfile(profileResponse: profileResponse)
            //Update  User
            BikeRentManager.shared.updateUser(user: self.userProfile)
            success(self.userProfile)
        }, failure: failure)
    }
    
     //added to perform add card redirection process for 3D secure cards dataSource: [EditableFormItemViewModel],
    func callAddCardWs(dataSource: [EditableFormItemViewModel],
                       redirection:@escaping () -> Void,
                       success:@escaping (UserProfile) -> Void,
                       failure:@escaping BaseServiceFailure) {
        
        
        if let cardOwner =  editableItem(forType: .cardOwner, in: dataSource)?.editedValue,
            let cardNumber =  editableItem(forType: .cardNumber, in: dataSource)?.editedValue,
            let expireData =  editableItem(forType: .expireDate, in: dataSource)?.editedValue,
            let cvvCode =  editableItem(forType: .cvvCode, in: dataSource)?.editedValue {
            //view controller is always not nil if edit type is a param
            
            let card = STPCardParams()
            card.name = cardOwner
            card.number = cardNumber
            card.expMonth = UInt(expireData.components(separatedBy: AppStyle.expirationDateSeparator).first ?? "") ?? 0
            card.expYear = UInt(expireData.components(separatedBy: AppStyle.expirationDateSeparator).last ?? "") ?? 0
            card.cvc = cvvCode
            StripePaymentManager.shared.addCardProcess(card: card, redirection: redirection,
                                                       success: {  [weak self] (profileResponse) in
                                                        guard let `self` = self else { return }
                                                        self.userProfile = UserProfile(profileResponse: profileResponse)
                                                        //Update  User
                                                        BikeRentManager.shared.updateUser(user: self.userProfile)

                                                        success(self.userProfile)
                }, failure: failure)
            
        }
    }
    
    
    func submitCardWs(dataSource: [EditableFormItemViewModel],
                       success:@escaping (UserProfile) -> Void,
                       failure:@escaping BaseServiceFailure) {
        
        
        if let cardOwner =  editableItem(forType: .cardOwner, in: dataSource)?.editedValue,
            let cardNumber =  editableItem(forType: .cardNumber, in: dataSource)?.editedValue,
            let expireData =  editableItem(forType: .expireDate, in: dataSource)?.editedValue,
            let cvvCode =  editableItem(forType: .cvvCode, in: dataSource)?.editedValue {
            //view controller is always not nil if edit type is a param
            
            let card = STPCardParams()
            card.name = cardOwner
            card.number = cardNumber
            card.expMonth = UInt(expireData.components(separatedBy: AppStyle.expirationDateSeparator).first ?? "") ?? 0
            card.expYear = UInt(expireData.components(separatedBy: AppStyle.expirationDateSeparator).last ?? "") ?? 0
            card.cvc = cvvCode
            
            StripePaymentManager.shared.submit(withCardParams: card,
                                               success: {  [weak self] (profileResponse) in
                                                guard let `self` = self else { return }
                                                self.userProfile = UserProfile(profileResponse: profileResponse)
                                                //Update  User
                                                BikeRentManager.shared.updateUser(user: self.userProfile)
                                                success(self.userProfile)
                }, failure: failure)
            
        }
    }
    
    //Add card image to user defaults workaround
    
}
