//
//  ProfileItemViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 27/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation


class ProfileItemViewModel {
    let title: String
    let cellConfig: CellConfiguration
    let value: String
    
    init(title: String, cellConfig: CellConfiguration, value: String) {
        self.title = title
        self.cellConfig = cellConfig
        self.value = value
    }
}
