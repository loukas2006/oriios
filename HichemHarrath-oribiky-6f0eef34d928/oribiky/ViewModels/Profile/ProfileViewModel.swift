//
//  ProfileViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 16/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class ProfileViewModel: BaseViewModel {
    
    // MARK: - Variables
    private var userProfile = UserProfile()
    
    // MARK: - Service Call
    func callGetProfileWs(success:@escaping (UserProfile) -> Void,
                          failure:@escaping (String) -> Void)  {
        
        ProfileService.get(success: {[weak self] (response) in
            guard let strongSelf = self else { return }
            strongSelf.userProfile = UserProfile(profileResponse: response)
            //Update  User
            BikeRentManager.shared.updateUser(user: strongSelf.userProfile)

            success(strongSelf.userProfile)
            }, failure: { (errorStr, errors) in
                failure(errorStr)
        })
    }
    
    // MARK: - Cells DataSource
    func preparedataSource() -> [[ProfileItemViewModel]] {
        
        var presonalInformationDataSource: [ProfileItemViewModel] = []
        
        var profileItem = ProfileItemViewModel(title: APPStrings.ProfileCell.firstName,
                                               cellConfig: .other,
                                               value: userProfile.firstname)
        presonalInformationDataSource.append(profileItem)
        
        profileItem = ProfileItemViewModel(title: APPStrings.ProfileCell.lastName,
                                           cellConfig: .other,
                                           value: userProfile.lastname)
        presonalInformationDataSource.append(profileItem)
        
        profileItem = ProfileItemViewModel(title: APPStrings.ProfileCell.email,
                                           cellConfig: .other,
                                           value: userProfile.email)
        presonalInformationDataSource.append(profileItem)
        
        profileItem = ProfileItemViewModel(title: APPStrings.ProfileCell.phone,
                                           cellConfig: .other,
                                           value: userProfile.phoneNumber)
        presonalInformationDataSource.append(profileItem)
        
        
        var coordinatesDataSource: [ProfileItemViewModel] = []
        
        profileItem = ProfileItemViewModel(title: APPStrings.ProfileCell.address,
                                           cellConfig: .address,
                                           value: userProfile.address.literal())
        coordinatesDataSource.append(profileItem)
        
        profileItem = ProfileItemViewModel(title: APPStrings.ProfileCell.card,
                                           cellConfig: userProfile.haveCard() ? .cardEdit : .cardAdd,
                                           value: userProfile.card.lastDigits)
        coordinatesDataSource.append(profileItem)
        
        return [presonalInformationDataSource, coordinatesDataSource]
    }
    
    // MARK: - Actions
    func navigateToEditProfile() {
        let viewModel = EditProfileViewModel(aUserProfile: userProfile, anEditType: .personalInformation)
        AppCoordinator.shared.transition(to: .editProfile(viewModel), type: .push, animated: true)
    }
    
    func navigateToEditAdress() {
        let viewModel = EditProfileViewModel(aUserProfile: userProfile, anEditType: .address)
        AppCoordinator.shared.transition(to: .editProfile(viewModel), type: .push, animated: true)
    }
    
    func navigateToAddCard() {
        let viewModel = EditProfileViewModel(aUserProfile: userProfile, anEditType: .card)
        AppCoordinator.shared.transition(to: .newCard(viewModel, false), type: .push, animated: true)
    }
    
    func navigateToSubscriptionsOrScanCard () {
        if userProfile.haveCard() {
            
//            let viewModel =  SubscriptionListViewModel()
//            AppCoordinator.shared.transition(to: .subscriptionList(viewModel, false), type: .push, animated: true)

        } else {
            showScanCardPopup(user: userProfile)
        }
    }
}
