//
//  PromoCodeCellViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 01/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation


class PromoCodeCellViewModel: BaseViewModel {
    //MARK: - Variables
    var name: String
    var height: CGFloat {
        return 50
    }

    //MARK: - Init
    init(name: String) {
        self.name = name
    }

}
