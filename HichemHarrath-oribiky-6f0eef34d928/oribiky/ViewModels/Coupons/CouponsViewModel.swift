//
//  CouponsViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 30/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class CouponsViewModel: BaseViewModel {
    
    //MARK: - Variables
    var items : [PromoCodeCellViewModel] {
        
        let user = BikeRentManager.shared.getUser()
        
        if user.coupons.count > 0 {
            return user.coupons.compactMap({PromoCodeCellViewModel(name:$0.name)})
        } else {
            //default item when user do not have any coupon
            return [PromoCodeCellViewModel(name: APPStrings.Coupons.Message.AucunCode.title)]
        }

    }
    
    
    //MARK: - Actions
    func callWs(code: String,
                success:@escaping (UserProfile) -> Void,
                failure:@escaping BaseServiceFailure)  {
        CouponsService.add(body: CouponsRequestBody(code: code), success: {(profileResponse) in
            success(UserProfile(profileResponse: profileResponse))
        }, failure: failure)
    }
    
}
