//
//  TabbarViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 22/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class TabbarViewModel {
    // MARK: - Variables
    
    // MARK: - Actions

    func selectetTabbarItem(index: Int, _ rootViewController: UIViewController?) {
        
        //Ignore received notification, ReservationRequirementsProcessManager should listen to this notification only from map
        //if we change tabbar selected item we ignore the notification 
        ReservationRequirementsProcessManager.shared.ignoreSteps()
        
        AppCoordinator.shared.transition(to: .tabbarItem, type: .tabbarItem(selectIndex: true, index: index), animated: true, completion: {
            
            if DefaultsManager.shared.isConnected  {
                //if we connected from the process
                switch index {
                    // case 0 added to check after connection if the user had a ride or a reservation in progress.
                    // in this case we should show the CurrentReservationViewController or CurrentRideViewController
                case 0 where (!(rootViewController is CurrentReservationViewController) || !(rootViewController is CurrentRideViewController)):
                    if BikeRentManager.shared.hasRide {
                        AppCoordinator.shared.transition(to: .currentRide(CurrentRideViewModel()), type: .rootController, animated: true)
                    } else if BikeRentManager.shared.hasReservation {
                        AppCoordinator.shared.transition(to: .currentReservation(CurrentReservationViewModel()), type: .rootController, animated: true)
                    }
                case 1:
                    if BikeRentManager.shared.getUser().isSubscribed() {
                        let viewModel = SubscriptionViewModel(SubscriptionType(rawValue: BikeRentManager.shared.getUser().subscription.details.type) ?? .minutePrice)
                        AppCoordinator.shared.transition(to: .mySubscription(viewModel), type: .rootController, animated: false)
                    } else {
                        AppCoordinator.shared.transition(to: .subscriptionList(SubscriptionListViewModel(isDisplayedInTabbarItem: true), false), type: .rootController, animated: false)
                    }
                case 2 where !(rootViewController is ReportViewController):
                    AppCoordinator.shared.transition(to: .report(ReportViewModel()), type: .rootController, animated: false)
                case 3 where !(rootViewController is ProfileViewController):
                    AppCoordinator.shared.transition(to: .profile(ProfileViewModel()), type: .rootController, animated: false)
                default:
                    break
                }
            } else {
                // if not connected, sho authentication in tabbar item
                if index != 0 && index != 2  && !(rootViewController is AuthenticationViewController) {
                    AppCoordinator.shared.transition(to: .authenticate(AuthenticateViewModel(index: index), false), type: .rootController, animated: false)
                }
                
            }
        })
    }

    func handleSubscriptionNotification (index: Int, subscription: SubscriptionType) {
        
        if index != 1 { return } // we are not on subscriptionn tabbar item, we should ingone this notification
        print(subscription)
        //refresh tabbar item
        if BikeRentManager.shared.getUser().isSubscribed() {
            let viewModel = SubscriptionViewModel(SubscriptionType(rawValue: BikeRentManager.shared.getUser().subscription.details.type) ?? .minutePrice)
            AppCoordinator.shared.transition(to: .mySubscription(viewModel), type: .rootController, animated: false)
        }

    }


}
