//
//  CurrentReservationViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 05/01/2019.
//  Copyright © 2019 Oribiky. All rights reserved.
//

import Foundation
import MapKit

class CurrentReservationViewModel: BaseViewModel {
    
    //MARK: - Variables
    var reservation: ReservationResponse? {
        return BikeRentManager.shared.getCurrentReservation()
    }

    
    //MARK: - WS
    func callCreateReservationWS (success: @escaping () -> Void) {
        
        guard let bike = BikeRentManager.shared.getBike() else {return}
        
        ReservationService.create(bikeId: bike.bikeId, success: {(reservation) in
            //update reservation
            BikeRentManager.shared.updateCurrentReservation(reservation: reservation)
            success()
        }) { (errorStr, errors) in
            self.showErrorPopuo(subtitle: APPStrings.Reservation.ErrorPopup.subtitle)
        }
    }
    
    func callCancelReservationWS(success: @escaping () -> Void, failure: @escaping (String) -> Void) {
        
        guard let currentReservation = reservation else {return}
        
        ReservationService.cancel(reservationId: currentReservation.reservationId, success: {(reservation) in
            success()
            //update current reservation
            BikeRentManager.shared.updateCurrentReservation(reservation: nil)

        }) { (errorStr, errors) in
            failure(errorStr)
        }

    }
    
    //MARK: - Public
    func remainingReservationTime () -> Int {
        
        let dateFormatter = DateFormatter(withFormat: Constant.Date.format, locale: "fr_FR")
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        guard let reservationEndDateStr = reservation?.endedAt, let reservationEndDate = dateFormatter.date(from: reservationEndDateStr) else { return 0}

        return Int(reservationEndDate.timeIntervalSinceNow)
        
    }
    
    func remainingReservationDistance (userLocation : CLLocation?) -> Double {
        
        guard let bike = reservation?.bike  else {return 0}
        guard let `userLocation` = userLocation else {return 0}
        let bikeLocation = CLLocation(latitude: bike.latitude, longitude: bike.longitude)
        return abs(bikeLocation.distance(from: userLocation))
        
    }

    //MARK: - Navigation

    func goBackToTabBar () {
        // start again from tabbar controller
//        AppCoordinator.shared.transition(to: .tabbar(TabbarViewModel()), type: .root, animated: true)
        AppCoordinator.shared.transition(to: .map(MapViewModel()), type: .rootController, animated: false)

    }
    
    func goToConnectBike()  {
        
        guard let bike = reservation?.bike else {return}

        ReservationRequirementsProcessManager.shared.handleSteps()
        
        ReservationRequirementsProcessManager.shared.start(completion: {
            
            ReservationRequirementsProcessManager.shared.ignoreSteps()
            
            // push bike reservation process
            if BluetoothManager.shared.isPoweredOn {
                // show turn on smart lock
                AppCoordinator.shared.transition(to: .connectToBike(ConnectToBikeViewModel()), type: .push, animated: true)
            } else {
                // show bluetooth view
                AppCoordinator.shared.transition(to: .activateBluetooth(ActivateBluetoothViewModel(bike: bike)), type: .push, animated: true)
            }

        })

    }

}
