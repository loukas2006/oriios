//
//  SubscriptionEligibilityViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 06/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

enum EligibilityType {
   
    case retired
    case cmu
    case rsa
    case ass
    case student
    
//    case under25
//    case jobSeaching
//    case rsa
//    case promotional
    
    var title: String {
        switch self {
        case .retired:
            return APPStrings.Subscription.Eligibility.Retired.title
        case .cmu:
            return APPStrings.Subscription.Eligibility.Cmu.title
        case .rsa:
            return APPStrings.Subscription.Eligibility.Rsa.title
        case .ass:
            return APPStrings.Subscription.Eligibility.Ass.title
        case .student:
            return APPStrings.Subscription.Eligibility.Student.title
        }
    }
    var details: String {
        switch self {
        case .retired:
            return APPStrings.Subscription.Eligibility.Retired.details
        case .cmu:
            return APPStrings.Subscription.Eligibility.Cmu.details
        case .rsa:
            return APPStrings.Subscription.Eligibility.Rsa.details
        case .ass:
            return APPStrings.Subscription.Eligibility.Ass.details
        case .student:
            return APPStrings.Subscription.Eligibility.Student.details
        }
    }
}

class SubscriptionEligibilityViewModel: BaseViewModel{
    
    // MARK: - Variables
    var documentsDatasource = [UIImage]()
    let neededDocumentsCategories: [EligibilityType] = [.retired, .cmu, .rsa, .ass, .student]
    var subcriptionType: SubscriptionType
    var isDisplayedInTabbarItem = false
    // MARK: - Init
    init(type: SubscriptionType , isDisplayedInTabbarItem: Bool) {
        self.isDisplayedInTabbarItem = isDisplayedInTabbarItem
        subcriptionType = type
    }
    
    // MARK: - WS
    func callSubscribeWS(success: @escaping () -> Void, failure:@escaping (String) -> Void) {
        
        SubscriptionService.uploadFiles(images: documentsDatasource.compactMap({UIImageJPEGRepresentation($0, 0.2)}), success: { (profileResponse) in
            BikeRentManager.shared.updateUser(user: UserProfile(profileResponse: profileResponse))
            success()
        }) { (errorStr, errors) in
            failure(errorStr)
        }
    }

    // MARK: - Actions
    func handleSubscriptionSuccess() {
//        delegate?.userDidSubscribe()
        if isDisplayedInTabbarItem {

            if BikeRentManager.shared.getUser().isSubscribed() {
                let viewModel = SubscriptionViewModel(SubscriptionType(rawValue: BikeRentManager.shared.getUser().subscription.details.type) ?? .minutePrice)
                AppCoordinator.shared.transition(to: .mySubscription(viewModel), type: .rootController, animated: false)
            } else {
                AppCoordinator.shared.transition(to: .subscriptionList(SubscriptionListViewModel(isDisplayedInTabbarItem: true), false), type: .rootController, animated: false)
            }
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.reservationRequirementsNotificationName), object: StepType.subscription(subcriptionType))
        }

    }
    
    func deleteImageAtIndex(_ index: Int) {
        if index < documentsDatasource.count {
            documentsDatasource.remove(at: index)
        }
    }
    
    func chooseFromMyPictures( completion: @escaping () -> Void) {
        ImagePickerManager.shared.photoLibrary { [weak self] (image) in
            guard let image = image , let strongSelf = self else { return }
            if strongSelf.documentsDatasource.count < 3 {
                strongSelf.documentsDatasource.append(image)
            }
            completion()
        }
    }
    
    func takeAPicture(completion: @escaping() -> Void) {
        ImagePickerManager.shared.openCamera{ [weak self] (image) in
            guard let image = image , let strongSelf = self else { return }
            if strongSelf.documentsDatasource.count < 3 {
                strongSelf.documentsDatasource.append(image)
            }
            completion()
        }
    }
}
