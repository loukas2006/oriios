//
//  MySubscriptionDetailsViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 18/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class MySubscriptionDetailsViewModel: BaseViewModel {
    
    let title: String
    let value: String
    let state: Bool?
    
    init(title: String, value: String, state: Bool?) {
        self.title = title
        self.value = value
        self.state = state
    }
    
    

}
