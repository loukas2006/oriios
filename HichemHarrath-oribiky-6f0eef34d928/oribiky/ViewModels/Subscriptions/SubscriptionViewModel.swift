//
//  SubscriptionViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 04/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation



enum SubscriptionType: String {
   
    // YEARLY, SOLIDARITY, YEARLY_DISCOUNTED
    // 1_DAY, 1_DAY_DISCOUNTED, 240_MIN
    case unlimitedOneYear = "YEARLY" // TRAJETS ILLIMITES - 1 AN
    case launchOffer = "YEARLY_DISCOUNTED"// Offre de lancement (meilleure vente !)
    case students = "SOLIDARITY" // Offre étudiants, bas revenus ou retraités
    case unlimitedOneDay = "1_DAY" // TRAJETS ILLIMITES - 1 JOUR
    case goodPlan = "1_DAY_DISCOUNTED"// Bon plan (-40%)
    case discoveryPrice = "240_MIN" // 240 MINUTES PREPAYES Tarif découverte
    case minutePrice = "FREEMIUM" // 0,12 € la minute au lieu de 0,15 €
    case delivery = "DELIVERY"
    case none = ""

//    case monthly = "MONTHLY"
//    case annual = "YEARLY"
//    case young = "SOLIDARITY"
//    case discount = "DISCOUNT_MOBILITY"
//    case freeUse = ""

    var title: String {
        switch self {
        case .unlimitedOneYear: // TRAJETS ILLIMITES - 1 AN
            return APPStrings.Subscription.UnlimitedOneYear.title
        case .launchOffer: // Offre de lancement
            return APPStrings.Subscription.LaunchOffer.title
        case .students: // Offre étudiants, bas revenus ou retraités
            return APPStrings.Subscription.Students.title
        case .unlimitedOneDay: // TRAJETS ILLIMITES - 1 JOUR
            return APPStrings.Subscription.Package.UnlimitedOneDay.title
        case .goodPlan: // Bon plan (-40%)
            return APPStrings.Subscription.Package.GoodPlan.title
        case .discoveryPrice: // 240 MINUTES PREPAYES Tarif découverte
            return APPStrings.Subscription.Package.DiscoveryPrice.title
        case .minutePrice: // 0,12 € la minute au lieu de 0,15 €
            return APPStrings.Subscription.MinuteUse.title
        case .delivery:
            return APPStrings.Subscription.Delivery.title
        default:
            return ""

        }
    }
    
    var price: String {
        switch self {
        case .unlimitedOneYear: // 32,99 € / mois
            return APPStrings.Subscription.UnlimitedOneYear.price
        case .launchOffer: // 19,99 € / mois
            return APPStrings.Subscription.LaunchOffer.price
        case .students: // 14,99 € / mois
            return APPStrings.Subscription.Students.price
        case .unlimitedOneDay: // 15 € la journée
            return APPStrings.Subscription.Package.UnlimitedOneDay.price
        case .goodPlan: // 9 € la journée
            return APPStrings.Subscription.Package.GoodPlan.price
        case .discoveryPrice: // 24 € soit 0,10 € la minute
            return APPStrings.Subscription.Package.DiscoveryPrice.price
        case .minutePrice: // 0,12 € la minute au lieu de 0,15 €
            return APPStrings.Subscription.MinuteUse.price
        case .delivery:
            return ""
        default:
            return ""

        }
    }
    
    var category: SubscriptionCategory {
        switch self {
        case .unlimitedOneYear, .launchOffer, .students:
            return .regularUse
        case .unlimitedOneDay, .goodPlan, .discoveryPrice, .minutePrice:
            return .ponctualUse
        case .delivery:
            return .none
        default:
            return .none

        }
    }
        
    var priceHighlighted: Bool {
        switch self {
        case .unlimitedOneYear,  .unlimitedOneDay:
            return false
        default:
            return false
        }
    }
    
    var hasPadding: Bool {
        switch self {
        case .launchOffer,  .students, .goodPlan,  .discoveryPrice:
            return false
        default:
            return false
        }
    }
    // temprory fixed cell size
    var cellHeight: CGFloat {
        switch self {
        case .unlimitedOneYear:
            return 50.0
        case .launchOffer:
            return 50.0
        case .students:
            return 60.0
        case .unlimitedOneDay:
            return 50.0
        case .goodPlan:
            return 50.0
        case .discoveryPrice:
            return 60.0
        case .minutePrice:
            return 50.0
        case .delivery:
            return 0.0
        default:
            return 0.0

        }
    }

    var fullDetails: NSAttributedString {
        switch self {
        case .unlimitedOneYear:
            return APPStrings.Subscription.UnlimitedOneYear.details.makeBold(highlightedTexts: [APPStrings.Subscription.UnlimitedOneYear.Details.highlighted], fontSize: 14.0)
        case .launchOffer:
            return APPStrings.Subscription.LaunchOffer.details.makeBold(highlightedTexts: [APPStrings.Subscription.LaunchOffer.Details.highlighted], fontSize: 14.0)
        case .students:
            return APPStrings.Subscription.Students.details.makeBold(highlightedTexts: [APPStrings.Subscription.Students.Details.highlighted], fontSize: 14.0)
        case .unlimitedOneDay:
            return APPStrings.Subscription.Package.UnlimitedOneDay.details.makeBold(highlightedTexts: [APPStrings.Subscription.Package.UnlimitedOneDay.Details.highlighted, APPStrings.Subscription.Package.Details.Highlighted.l22118], fontSize: 14.0)
        case .goodPlan:
            return APPStrings.Subscription.Package.GoodPlan.details.makeBold(highlightedTexts: [APPStrings.Subscription.Package.GoodPlan.Details.highlighted, APPStrings.Subscription.Package.Details.Highlighted.l22118], fontSize: 14.0)
        case .discoveryPrice:
            return APPStrings.Subscription.Package.DiscoveryPrice.details.makeBold(highlightedTexts: [APPStrings.Subscription.Package.DiscoveryPrice.Details.highlighted], fontSize: 14.0)
        case .minutePrice:
            return APPStrings.Subscription.MinuteUse.details.makeBold(highlightedTexts: [APPStrings.Subscription.Package.Details.Highlighted.l22118], fontSize: 14.0)
        case .delivery:
            return APPStrings.Subscription.Delivery.details.makeBold(highlightedTexts: [APPStrings.Subscription.Delivery.Details.highlighted], fontSize: 14.0)
        default:
            return NSAttributedString()
        }
    }

}

class SubscriptionViewModel: BaseViewModel {
    
    // MARK: - Variables
    var type: SubscriptionType
    var neededDocumentsCategories =  [EligibilityType]()
    var isDisplayedInTabbarItem = false 
    // MARK: - Init
    init(_ subscriptionsType: SubscriptionType) {
        type = subscriptionsType
        if type == .students {
            neededDocumentsCategories = [.retired, .cmu, .rsa, .ass, .student]
        }
        // not subscribed
        /*if type == .minutePrice {
            details = [MySubscriptionDetailsViewModel(title: "Abonnement", value: "Pas encore abonnée", state: nil)]
            return
        }
        // subscribed
        let user = BikeRentManager.shared.getUser()
        if user.isSubscribed() {
            
            let subscriptionState = MySubscriptionDetailsViewModel(title: "Abonnement", value: "Active", state: true)
            let subscriptionDate = MySubscriptionDetailsViewModel(title: "Prochaine date de renouvellement", value: user.subscription.billingPeriodEndsAt ?? "" , state: nil)
            let packages = MySubscriptionDetailsViewModel(title: "Minutes prépayées dans mon compte", value: "\(user.packages.compactMap({  Int($0.credit) }).reduce(0, +)) minutes", state: nil)
            details = [subscriptionState, subscriptionDate, packages ]
            
        }*/
    }
    
    // MARK: - Actions
    func showValidationPopup(subscription: SubscriptionViewModel, completion:@escaping () -> Void) {
        // show popup according to the type
        var subscribeActionTitle = ""
        var cancelActionTitle = ""
        var popupSubtitle = ""

//        if type == .students {
//            subscribeActionTitle = APPStrings.Subscription.Details.subscribe.uppercased()
//            cancelActionTitle = APPStrings.Common.cancel.uppercased()
//            popupSubtitle = APPStrings.Subscription.Eligibility.popupTitle
//        } else {
            subscribeActionTitle = APPStrings.Subscription.Details.ValidationPopup.yesAction.uppercased()
            cancelActionTitle = APPStrings.Subscription.Details.ValidationPopup.noAction.uppercased()
            popupSubtitle = APPStrings.Subscription.Details.ValidationPopup.subtitle(subscription.type.title)
       // }
        
        let subscribeAction = PopupAction(title: subscribeActionTitle, style: .validate) { [weak self] in
            AppCoordinator.shared.pop(animated: true, completion: { completion() })
        }

        let cancelAction = PopupAction(title: cancelActionTitle, style: .cancel) {
            AppCoordinator.shared.pop(animated: true)
        }

        let viewModel = PopupViewModel(type: .choice(title: nil, subTitle:NSAttributedString(string:popupSubtitle), leftImage: nil, rightImage: nil, yesActions: cancelAction, noAction: subscribeAction))

        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)

    }

    func navigateToEligibility() {
        AppCoordinator.shared.transition(to: .subscriptionEligibility(SubscriptionEligibilityViewModel(type: type, isDisplayedInTabbarItem: isDisplayedInTabbarItem)),
                                         type: .push,
                                         animated: true)
    }
    
    func handleSubscriptionSuccess() {
//        delegate?.userDidSubscribe()
        if isDisplayedInTabbarItem {
            
            if BikeRentManager.shared.getUser().isSubscribed() {
                let viewModel = SubscriptionViewModel(SubscriptionType(rawValue: BikeRentManager.shared.getUser().subscription.details.type) ?? .minutePrice)
                AppCoordinator.shared.transition(to: .mySubscription(viewModel), type: .rootController, animated: false)
            } else {
                AppCoordinator.shared.transition(to: .subscriptionList(SubscriptionListViewModel(isDisplayedInTabbarItem: true), false), type: .rootController, animated: false)
            }
            
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.reservationRequirementsNotificationName), object: StepType.subscription(type))
        }

    }
    
    func handleCancelSubscriptionAction (success: @escaping () -> Void, failure:@escaping (String) -> Void) {
        
        if BikeRentManager.shared.getUser().isSubscriptionActive() {
            // cancel subbscription
            SubscriptionService.cancelSubscription(success: { (profileResponse) in
                BikeRentManager.shared.updateUser(user: UserProfile(profileResponse: profileResponse))
                success()
            }, failure: { (errorStr, errors) in
                failure(errorStr)
            })
        } else {
            // reactivate subscription
            SubscriptionService.reactivateSubscription(success: { (profileResponse) in
                BikeRentManager.shared.updateUser(user: UserProfile(profileResponse: profileResponse))
                success()
            }, failure: { (errorStr, errors) in
                failure(errorStr)
            })
        }

    }
    
    func callWS(success: @escaping () -> Void, failure:@escaping (String) -> Void) {
        
        let packages: [SubscriptionType] = [.unlimitedOneDay, .goodPlan, .discoveryPrice]
        if packages.contains(type)  {
            // package ws
            let parameters = PackageRequestBody(type: type.rawValue)
            PackagesService.add(body: parameters,
                                success: { (profileResponse) in
                                    BikeRentManager.shared.updateUser(user: UserProfile(profileResponse: profileResponse))
                                    success()
            }, failure: { (errorStr, errors) in
                failure(errorStr)
            })
        } else {
            // call subscription
            let subscribeParameters = SubscribeRequestBody(type: type)
            SubscriptionService.subscribe(body: subscribeParameters, success: { (profileResponse) in
                BikeRentManager.shared.updateUser(user: UserProfile(profileResponse: profileResponse))
                success()
            }, failure: { (errorStr, errors) in
                failure(errorStr)
            })

        }
    }
}
