//
//  SubscriptionListViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 04/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
class SubscriptionListViewModel: BaseViewModel {
    // MARK: - Variables
    private let subscriptionList = [SubscriptionViewModel(.launchOffer),
                                    SubscriptionViewModel(.students),
                                    SubscriptionViewModel(.unlimitedOneDay),
                                    SubscriptionViewModel(.minutePrice),
                                    SubscriptionViewModel(.discoveryPrice)
                                    ]

    var  dataSource = [SubscriptionCategoryViewModel]()
    var isDisplayedInTabbarItem: Bool = false
    
    init(isDisplayedInTabbarItem: Bool = false ) {
        self.isDisplayedInTabbarItem = isDisplayedInTabbarItem
        subscriptionList.compactMap { $0.isDisplayedInTabbarItem = isDisplayedInTabbarItem }
        dataSource =     [SubscriptionCategoryViewModel(category: .regularUse,
                                                        items: subscriptionList.filter({ $0.type.category == .regularUse})),
                          SubscriptionCategoryViewModel(category: .ponctualUse,
                                                        items: subscriptionList.filter({ $0.type.category == .ponctualUse})),
                          /*SubscriptionCategoryViewModel(category: .immediateUse,
             items: subscriptionList.filter({ $0.type.category == .immediateUse}))*/]
    }
    
    
    func navigateToDetails(_ viewModel :SubscriptionViewModel) {

        if viewModel.type == .minutePrice {

            handlePackageMinutesSelection()
            //            delegate?.userDidSubscribe()
        } else if !BikeRentManager.shared.getUser().isSubscribed() {
            AppCoordinator.shared.transition(to: .subscriptionDetails(viewModel), type: .push, animated: true)
        }
        
    }
    
    func calculateMinutesBalance() -> NSAttributedString{
        let balanceStr = APPStrings.Subscription.Package.balanceMinutes("\(BikeRentManager.shared.getUser().packages.compactMap({ Int($0.credit) }).reduce(0, +))")
     return APPStrings.Subscription.Package.balance(balanceStr).highlightColor(highlightedTexts: [balanceStr], font: AppStyle.Text.regular(size: 13).font, color: AppStyle.Colors.mainBlue)

    }
    
    func handlePackageMinutesSelection() {
        if isDisplayedInTabbarItem {
            AppCoordinator.shared.transition(to: .tabbarItem, type: .tabbarItem(selectIndex: true, index: 0), animated: true)
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.reservationRequirementsNotificationName), object: StepType.subscription(.minutePrice))
        }
    }
}
