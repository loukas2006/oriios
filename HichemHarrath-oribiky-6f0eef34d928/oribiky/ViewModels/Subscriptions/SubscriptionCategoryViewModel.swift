//
//  SubscriptionCategoryViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 17/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

enum SubscriptionCategory {
    
    case regularUse
    case ponctualUse
    case none
    
    var title: String {
        switch self {
        case .regularUse: // UTILISATION REGULIERE
            return "UTILISATION REGULIERE"
        case .ponctualUse: // UTILISATION PONCTUELLE
            return "UTILISATION PONCTUELLE"
        case .none:
            return ""
//        case .immediateUse: // UTILISATION IMMEDIATE, sans abonnement
//            return "UTILISATION IMMEDIATE, sans abonnement"
        }
    }
    
//    var subtitle: String {
//        switch self {
//        case .regularUse: // (+ 8 trajets par mois)
//            return "(+8 trajets par mois)"
//        default:
//            return ""
//        }
//    }

    
    // temprory fixed cell size
    var cellHeight: CGFloat {
        switch self {
        case .regularUse:
            return 280.0
        case .ponctualUse: // UTILISATION PONCTUELLE
            return 280.0
        case .none: 
            return 0.0
//        case .immediateUse: // UTILISATION IMMEDIATE, sans abonnement
//            return 180.0
        }
    }
}

class SubscriptionCategoryViewModel: BaseViewModel {

    var category: SubscriptionCategory
    var items: [SubscriptionViewModel]
    
    init(category: SubscriptionCategory, items: [SubscriptionViewModel] ) {
        self.category = category
        self.items = items
    }
}
