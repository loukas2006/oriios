//
//  LegalNoticeViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 08/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class LegalNoticeViewModel: BaseViewModel {
    
    
    //MARK: - Actions
    func navigateToTermsOfUse(pdf: PDF)  {
        let viewModel = PdfViewerViewModel(withPDF: pdf)
        AppCoordinator.shared.transition(to: .pdfViewer(viewModel, false), type: .push, animated: true)
    }
    

}
