//
//  LaunchViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 27/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class LaunchViewModel: BaseViewModel {
    
    // MARK: - Web Service
    private func getUserProfile (completion: @escaping () -> Void) {
        
        ProfileService.get(handleSessionExpired: false, success: { (profileResponse) in
            // Update User
            BikeRentManager.shared.updateUser(user: UserProfile(profileResponse: profileResponse))
            completion()
            }, failure: { (errorStr, errors) in
                completion()
        })
    }
    
    
    private func checkRidesInProgress (completion: @escaping () -> Void) {

        RideService.getCurrent(handleSessionExpired: false, success: { (rideResponse) in
            //user have current Ride
            BikeRentManager.shared.updateCurrentRide(ride: rideResponse)
            //start tracking
            LocationManager.shared.startTrackingRide(currentRideLocations: rideResponse.locations ?? [])

            completion()
        }, failure:  { (errStr, errors) in
            // user do not have current ride
            print(errStr)
            completion()
        })
        
    }
    
    private func checkReservationInProgress (completion: @escaping () -> Void) {
        
        ReservationService.current(handleSessionExpired: false, success: { (reservation) in
            //user have current reservation
            BikeRentManager.shared.updateCurrentReservation(reservation: reservation)
            completion()
        }) { (errStr, errors) in
            print(errStr)
            completion()
        }
        
    }

    func updateUserLaunchData (completion: @escaping () -> Void) {
        getUserProfile (completion:{ [weak self] in
            guard let `self` = self else { return }
            self.getNewNotifications (completion: {
                self.checkReservationInProgress(completion: {
                    self.checkRidesInProgress(completion: {
                        completion()
                    })
                })
            })
        })
    }
    
    func updateUserDataAfterLogin(completion: @escaping () -> Void) {

        getNewNotifications {}
        
        checkReservationInProgress(completion: { [weak self] in
            
            guard let `self` = self else {
                return }
            self.checkRidesInProgress(completion: {
                completion()
            })
        })
    }
    
    private func getNewNotifications(completion: @escaping () -> Void) {
        NotificationService.getNew(handleSessionExpired: false, success: { (responseList) in
            let items = responseList.compactMap({NotificationViewModel(notification: $0)})
            BikeRentManager.shared.updateNewNotifications(notifications: items)
            completion()
        }, failure: { (errorStr, errors) in
            print(errorStr)
            completion()
        })

    }
    
    
    func showApplicationHome () {
        
        if DefaultsManager.shared.shouldShowSplash {
            // show splash
            AppCoordinator.shared.transition(to: .splash(SplashViewModel()), type: .root, animated: false, completion:{ DefaultsManager.shared.didShowSplash() })
        } else {
            // start with tabbar controller
            AppCoordinator.shared.transition(to: .tabbar(TabbarViewModel()), type: .root, animated: true)
        }
    }

    
}
