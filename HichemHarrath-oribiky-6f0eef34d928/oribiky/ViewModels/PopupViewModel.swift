//
//  PopupViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 24/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class PopupViewModel: BaseViewModel {
    
    // MARK: - Varaibles
    var type: PopupType = .unknown
    
    // MARK: - Init
    required init(type: PopupType) {
        self.type = type
    }
    
    // MARK: - Actions
}
