//
//  StartBikeDiagnosticViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 09/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation


class StartBikeDiagnosticViewModel: BaseViewModel {
    
    //MARK: - Actions
    func navigateToBikeDiagnostic()  {
        AppCoordinator.shared.transition(to: .bikeDiagnostic(BikeDiagnosticViewModel()), type: .push, animated: true)
    }

}
