//
//  LockSmartLockViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 18/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
class LockSmartLockViewModel: BaseViewModel {
    
    //MARK: - Linka Lock
    func lockSmartLock(completion: @escaping (String, LinkaOperationState) -> Void) {
        
        LinkaManager.shared.setupAndScan(imei: BikeRentManager.shared.getBike()?.imei).then { peripheral in
            
            LinkaManager.shared.paire(peripheral: peripheral)
            
            }.then { managers in
                
                LinkaInteractionManager.shared.connectAndLock(managers: managers)
                
            }.done{ (state) in
                
                switch state {
                case .finished:
                    completion(APPStrings.LinkaStatus.Lock.finished.uppercased(), state)
                case .stalled, .inMove:
                    LinkaInteractionManager.shared.reset()
                    completion("", state)
                default:
                    print("A problem with LinkaOperationState - state returned \(state)")
                    
                }
            }.catch { (error) in
                if let err = error as? LinkaSetupError {
                    completion(err.message, .unknown)
                }
        }
        
    }
    //MARK: - Methods
    func getEndAddress(completion: @escaping (String) -> Void){
        
        
        if let lastLocation = LocationManager.shared.rideLocations.last {
            
            var endLocation: (latitude: Double, longitude: Double) = (lastLocation.latitude, lastLocation.longitude)
            if let userCurrentLocation = LocationManager.shared.userCurrentLocation {
                endLocation = (userCurrentLocation.coordinate.latitude, userCurrentLocation.coordinate.longitude)
            }
            
            LocationManager.shared.geocode(latitude: endLocation.latitude, longitude: endLocation.longitude) { (placemark, error) in
                guard let placemark = placemark, error == nil else {
                    completion("")
                    return
                }
                completion(placemark.fullAddress())
            }
        }else {
            completion("")
        }
        
    }
    func callFinishRideWs (address: String,
                           success:@escaping (RideResponse) -> Void,
                           failure:@escaping (String) -> Void) {
        
        guard let ride = BikeRentManager.shared.getCurrentRide() else { return }
        
        var locationsArray = LocationManager.shared.rideLocations.map{BikeLocation(latitude:$0.latitude, longitude:$0.longitude)}
        if let userLocation = LocationManager.shared.userCurrentLocation {
            locationsArray.append(BikeLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude))
        }
        let body = FinishRideRequestBody(endAddress: address,
                                         locations: locationsArray)
        RideService.finishRide(rideId: ride.rideId, body: body, success: { (ride) in
            // stop location updates
            BikeRentManager.shared.updateCurrentRide(ride: nil)
            LocationManager.shared.endTrackingRide()
            
            success(ride)
        }, failure: { (errorStr, errors) in
            failure(errorStr)
        })
        
    }
    func callSearchParkingWs (success:@escaping (ParkingResponse) -> Void,
                              failure:@escaping (String) -> Void) {
        
        if let lastLocation = LocationManager.shared.rideLocations.last {
            //search available parking with last location
            ParkingService.search(latitude: lastLocation.latitude, longitude: lastLocation.longitude, success: success, failure: { (errorStr, errors) in
                failure(errorStr)
            })
        }
    }
    
    func callAddNewParkingWs (address: String,
                              success:@escaping (Parking) -> Void,
                              failure:@escaping (String) -> Void) {
        
        if let lastLocation = LocationManager.shared.rideLocations.last {
            let body = ParkingRequestBody(latitude: lastLocation.latitude, longitude: lastLocation.longitude, address: address)
            ParkingService.add(body: body, success: success) { (errorStr, errors) in
                failure(errorStr)
            }
        } else {
            failure("Impossible de déclarer un nouveau parking, Veuillez réessayer!")
        }
        
    }
    
    
    func showLinkaStalledPopup(completion: @escaping () -> Void) {
        
        let okAction = PopupAction(title: APPStrings.Common.ok, style: .validate) {
            // hide popup
            AppCoordinator.shared.pop(animated: true, completion: {
                completion()
            })
        }
        
        let viewModel = PopupViewModel(type: .info(title: nil, subTitle: APPStrings.LockSmartlockk.StalledPopup.subtitle, action: okAction))
        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)
    }
    func showLinkaInMovePopup(completion: @escaping () -> Void) {
        
        let okAction = PopupAction(title: APPStrings.Common.ok, style: .validate) {
            // hide popup
            AppCoordinator.shared.pop(animated: true, completion: {
                completion()
            })
        }
        
        let viewModel = PopupViewModel(type: .info(title: nil, subTitle: APPStrings.LockSmartlockk.InMove.subtitle, action: okAction))
        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)
    }
    
    
    func showDeclareBikeParkingPopup(address: String, completion: @escaping () -> Void) {
        
        let descriptionAttributedText = NSMutableAttributedString(string: APPStrings.DeclareParking.fullDescription)
        // Default font
        descriptionAttributedText.addAttributes([NSAttributedString.Key.font : AppStyle.Text.medium(size: 15).font], range: NSRange(location: 0, length: APPStrings.DeclareParking.fullDescription.count))
        let range = APPStrings.DeclareParking.fullDescription.range(of: APPStrings.DeclareParking.freeMinutes)
        let startLocation = range?.lowerBound.encodedOffset ?? 0
        descriptionAttributedText.addAttributes([NSAttributedString.Key.font : AppStyle.Text.bold(size: 15).font], range: NSRange(location: startLocation, length: APPStrings.DeclareParking.freeMinutes.count))
        let noticeRange = APPStrings.DeclareParking.fullDescription.range(of: APPStrings.DeclareParking.notice)
        let startNoticeLocation = noticeRange?.lowerBound.encodedOffset ?? 0
        descriptionAttributedText.addAttributes([NSAttributedString.Key.font : AppStyle.Text.italic(size: 12).font,
                                                 NSAttributedString.Key.foregroundColor: AppStyle.Colors.mainGray],
                                                range: NSRange(location: startNoticeLocation, length: APPStrings.DeclareParking.notice.count))
        let validateAction = PopupAction(title: APPStrings.DeclareParking.validateBtnTitle, style: .validate) {
            AppCoordinator.shared.pop(animated: true, completion: {[weak self] in
                //call add parking Ws
                completion()
            })
        }
        let cancelAction = PopupAction(title: APPStrings.Common.cancel, style: .validate) {
            AppCoordinator.shared.pop(animated: true)
        }
        
        let popupViewModel = PopupViewModel(type: .choiceWithText(topDescriptionText: descriptionAttributedText, bottomDescriptionText: nil, validateAction: validateAction, cancelAction: cancelAction))
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
        
    }
    
    func showParkingNotAvailablePopup(completion: @escaping (Bool) -> Void) {
        
        let popupTitleAttributedText = NSMutableAttributedString(string: APPStrings.DeclareParking.UnavailablePopup.title)
        popupTitleAttributedText.addAttributes([NSAttributedString.Key.font : AppStyle.Text.bold(size: 15).font], range: NSRange(location: 0, length: APPStrings.DeclareParking.UnavailablePopup.title.count))
        let popupNoticeAttributedText = NSMutableAttributedString(string: APPStrings.DeclareParking.UnavailablePopup.notice)
        popupNoticeAttributedText.addAttributes([NSAttributedString.Key.font : AppStyle.Text.regular(size: 13).font], range: NSRange(location: 0, length: APPStrings.DeclareParking.UnavailablePopup.notice.count))
        
        let localiseAction = PopupAction(title: APPStrings.DeclareParking.UnavailablePopup.localiseActionTitle, style: .validate) {
            AppCoordinator.shared.pop(animated: true, completion: {
                completion(true)
            })
        }
        let declareAction = PopupAction(title: APPStrings.DeclareParking.UnavailablePopup.declareActionTitle, style: .validate) {
            AppCoordinator.shared.pop(animated: true, completion: {
                completion(false)
            })
        }
        
        let popupViewModel = PopupViewModel(type: .choiceWithText(topDescriptionText: popupTitleAttributedText, bottomDescriptionText: popupNoticeAttributedText, validateAction: localiseAction, cancelAction: declareAction))
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
        
    }
    
    func navigateToRideResume (ride: RideResponse) {
        AppCoordinator.shared.transition(to: .map(MapViewModel()), type: .rootController, animated: true, completion: {
            AppCoordinator.shared.transition(to: .rideResume(RideResumeViewModel(ride: ride), false), type: .push, animated: false)
        })
    }
    
    func navigateToParkingActionSheet(parking: Parking) {
        AppCoordinator.shared.transition(to: .parkingAlert(ParkingAlertViewModel(parking: parking)), type: .alert, animated: true)
    }
}
