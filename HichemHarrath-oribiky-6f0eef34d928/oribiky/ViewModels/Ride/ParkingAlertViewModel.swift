//
//  ParkingAlertViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 25/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import MapKit

class ParkingAlertViewModel: BaseViewModel {
    
    //MARK: - Variables
    var parking: Parking
    var canOpenGoogleMaps: Bool {
        guard let url = URL(string:"comgooglemaps://") else {
            return false
        }
        return UIApplication.shared.canOpenURL(url)
    }
    
    
    
    var cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { action -> Void in
        // Just dismiss the action sheet
        
    }
    
    //MARK: - Init
    init(parking: Parking) {
        self.parking = parking
    }
    
    //MARK: - Actions

    func openGoogleMapsAction() -> UIAlertAction {
        return UIAlertAction(title: "Google maps", style: .default) {[weak self] (action) in
            guard let `self` = self else {return}
            if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(self.parking.latitude),\(self.parking.longitude)&directionsmode=bicycling") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }

    }
    
    func openApplePlans() -> UIAlertAction {
        print("parking == \(self.parking)")
        return UIAlertAction(title: "Plans", style: .default) {[weak self] (action) in
            guard let `self` = self else {return}
//            let coordinate = CLLocationCoordinate2DMake(48.8335113329, 2.35718836907)
            let coordinate = CLLocationCoordinate2DMake(self.parking.latitude, self.parking.longitude)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
            mapItem.name = self.parking.address
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeWalking])
        }

    }
    
    
    
    
    
}
