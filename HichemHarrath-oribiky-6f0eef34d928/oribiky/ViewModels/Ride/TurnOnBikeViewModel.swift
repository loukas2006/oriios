//
//  TurnOnBikeViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 07/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class TurnOnBikeViewModel: BaseViewModel {
    
    //MARK: - Actions
    func navigateToBikeDiagnostic()  {
        AppCoordinator.shared.transition(to: .startDiagnostic(StartBikeDiagnosticViewModel()), type: .push, animated: true)
    }
}
