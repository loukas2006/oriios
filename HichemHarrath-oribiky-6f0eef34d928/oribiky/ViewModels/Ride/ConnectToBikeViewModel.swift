//
//  ConnectToBikeViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 11/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import LinkaAPIKit

class ConnectToBikeViewModel: BaseViewModel {
    // MARK: - Variables
    
    // MARK: - Actions
    func handleLinkaScan (success: @escaping () -> Void, failure: @escaping (_ errorStr: String) -> Void) {
        
        
        LinkaManager.shared.setupAndScan(imei: BikeRentManager.shared.getBike()?.imei).done { _ in
                success()
            }.catch { (error) in
                if let err = error as? LinkaSetupError {
                    failure(err.message)
                }
        }
        
    }
    
    func navigateToBikeDiagnostic()  {
        
        AppCoordinator.shared.transition(to: .turnOnBike(TurnOnBikeViewModel()), type: .push, animated: true)
    }
    
    func showScanTimedOutError()  {
        
    }
    
}
