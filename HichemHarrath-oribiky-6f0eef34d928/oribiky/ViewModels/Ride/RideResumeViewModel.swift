//
//  RideResumeViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 20/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import MapKit

class RideResumeViewModel: BaseViewModel {
    
    //MARK: - Variables
    var ride: RideResponse
    
//    let testLocations = [BikeLocation(latitude: 38.0283608, longitude: -78.4814684),
//                         BikeLocation(latitude: 38.02844, longitude: -78.48174),
//                         BikeLocation(latitude: 38.0284428, longitude: -78.4817437),
//                         BikeLocation(latitude: 38.02877, longitude: -78.48159),
//                         BikeLocation(latitude: 38.02885, longitude: -78.48156),
//                         BikeLocation(latitude: 38.02899, longitude: -78.48149),
//                         BikeLocation(latitude: 38.02902, longitude: -78.48147),
//                         BikeLocation(latitude: 38.02913, longitude: -78.48144),
//                         BikeLocation(latitude: 38.02981, longitude: -78.4811),
//                         BikeLocation(latitude: 38.03007, longitude: -78.48096),
//                         BikeLocation(latitude: 38.03057, longitude: -78.48074),
//                         BikeLocation(latitude: 38.03094, longitude: -78.48058),
//                         BikeLocation(latitude: 38.03103, longitude: -78.48054)
//    ]
    
   // MARK: - Init
    init(ride: RideResponse) {
        self.ride = ride
    }
    
    //MARK: - Methods
    
    func createSnapshot(rect: CGRect, completion: @escaping (UIImage?) -> Void) {
        
        let mapSnapshotOptions = MKMapSnapshotOptions()
        
        // Set the region of the map that is rendered.
//        let location = CLLocationCoordinate2DMake(37.332077, -122.02962) // Apple HQ
//        let region = MKCoordinateRegionMakeWithDistance(location, 1000, 1000)
        
        // Set the region of the map that is rendered. (by polyline)
        guard let rideLocations = ride.locations?.compactMap({CLLocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude)}) else {
            completion(nil)
            return
        }
        
        let polyLine = MKPolyline(coordinates:rideLocations , count: rideLocations.count)

        var region = MKCoordinateRegionForMapRect(polyLine.boundingMapRect)
        mapSnapshotOptions.region = region
        print("snapchot heitght \(polyLine.boundingMapRect.height)")
        print("snapchot width \(polyLine.boundingMapRect.width)")

        // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
        mapSnapshotOptions.scale = UIScreen.main.scale
        
        // Set the size of the image output.
        mapSnapshotOptions.size = rect.size
        
        // Show buildings and Points of Interest on the snapshot
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = true
        
        let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
        snapShotter.start { (snapshot, error) in
            completion(self.drawLineOnImage(snapshot: snapshot, imageRect: rect, coordinates: rideLocations))
        }
    }
    
    
    func drawLineOnImage(snapshot: MKMapSnapshot?, imageRect: CGRect, coordinates: [CLLocationCoordinate2D]) -> UIImage? {
        guard let snapshot = snapshot else { return UIImage()}

        let image = snapshot.image
        
        // for Retina screen
        UIGraphicsBeginImageContextWithOptions(imageRect.size, true, 0)
        
        // draw original image into the context
        image.draw(at: CGPoint.zero)
        
        // get the context for CoreGraphics
        let context = UIGraphicsGetCurrentContext()
        
        // set stroking width and color of the context
        context!.setLineWidth(3.0)
        context!.setStrokeColor(AppStyle.Colors.mainBlue.cgColor)
        context!.setLineCap(.round)
        context!.setLineJoin(.round)

        // Here is the trick :
        // We use addLine() and move() to draw the line, this should be easy to understand.
        // The diificult part is that they both take CGPoint as parameters, and it would be way too complex for us to calculate by ourselves
        // Thus we use snapshot.point() to save the pain.
        
        if coordinates.count > 0 {
            
            context!.move(to: snapshot.point(for: coordinates[0]))
            for i in 0...coordinates.count-1 {
                
                print("point = \(snapshot.point(for: coordinates[i])), coordinate = \(coordinates[i])")
                context!.addLine(to: snapshot.point(for: coordinates[i]))
                context!.setShadow(offset: CGSize(width: 5, height: 1), blur: 2, color: AppStyle.Colors.black.withAlphaComponent(0.3).cgColor)
                context!.move(to: snapshot.point(for: coordinates[i]))
            }

        }
        
        // apply the stroke to the context
        context!.strokePath()
        
        // get the image from the graphics context
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // end the graphics context
        UIGraphicsEndImageContext()
        
        
        let tmpImage = drawPin(image: resultImage!, coordinate: coordinates.first, snapshot: snapshot, imageRect: imageRect)

        return drawPin(image: tmpImage!, coordinate: coordinates.last, snapshot: snapshot, imageRect: imageRect)
    }
    
    func drawPin(image: UIImage, coordinate: CLLocationCoordinate2D?, snapshot: MKMapSnapshot, imageRect: CGRect) -> UIImage? {
        
        guard let coordinate = coordinate else {return UIImage()}
        UIGraphicsBeginImageContextWithOptions(imageRect.size, true, 0)
        image.draw(at: .zero)
        
        let pinView = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
       pinView.image = Asset.Ride.point.image
        let pinImage = pinView.image
        
        var point = snapshot.point(for: coordinate)
        
        
//        if imageRect.contains(point) {
            point.x -= pinView.bounds.size.width / 2
            point.y -= pinView.bounds.size.height / 2
            pinImage?.draw(at: point)
//        }
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        // do whatever you want with this image, e.g.
        return newImage
    }

}




