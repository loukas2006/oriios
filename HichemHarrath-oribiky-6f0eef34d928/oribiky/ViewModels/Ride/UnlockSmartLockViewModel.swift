//
//  UnlockSmartLockViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 14/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import LinkaAPIKit

class UnlockSmartLockViewModel: BaseViewModel {
    
    // MARK: - Variables
    var bikeStatus: [BikeStatus]
    public var unlockdelegate :LockConnectionServiceDelegate?

    //Mark: Init
    init(status: [BikeStatus]) {
        bikeStatus = status
    }
    
    
    func callFinishRideWs (address: String,
                           success:@escaping (RideResponse) -> Void,
                           failure:@escaping (String) -> Void) {
        
        guard let ride = BikeRentManager.shared.getCurrentRide() else { return }
        
        var locationsArray = LocationManager.shared.rideLocations.map{BikeLocation(latitude:$0.latitude, longitude:$0.longitude)}
        if let userLocation = LocationManager.shared.userCurrentLocation {
            locationsArray.append(BikeLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude))
        }
        let body = FinishRideRequestBody(endAddress: address,
                                         locations: locationsArray)
        RideService.finishRide(rideId: ride.rideId, body: body, success: { (ride) in
            // stop location updates
            BikeRentManager.shared.updateCurrentRide(ride: nil)
            LocationManager.shared.endTrackingRide()
            
            success(ride)
        }, failure: { (errorStr, errors) in
            failure(errorStr)
        })
        
    }
    
    func navigateToMapViewController () {
        AppCoordinator.shared.transition(to: .map(MapViewModel()), type: .rootController, animated: false)
    }
    
    func navigateToRideResume (ride: RideResponse) {
        AppCoordinator.shared.transition(to: .map(MapViewModel()), type: .rootController, animated: true, completion: {
            AppCoordinator.shared.transition(to: .rideResume(RideResumeViewModel(ride: ride), false), type: .push, animated: false)
        })
    }
    
    
    func getEndAddress(completion: @escaping (String) -> Void){
        
        
        if let lastLocation = LocationManager.shared.rideLocations.last {
            
            var endLocation: (latitude: Double, longitude: Double) = (lastLocation.latitude, lastLocation.longitude)
            if let userCurrentLocation = LocationManager.shared.userCurrentLocation {
                endLocation = (userCurrentLocation.coordinate.latitude, userCurrentLocation.coordinate.longitude)
            }
            
            LocationManager.shared.geocode(latitude: endLocation.latitude, longitude: endLocation.longitude) { (placemark, error) in
                guard let placemark = placemark, error == nil else {
                    completion("")
                    return
                }
                completion(placemark.fullAddress())
            }
        }else {
            completion("")
        }
        
    }

    //Mark: WS call
    func callCreateRideWs (success:@escaping (RideResponse) -> Void,
                           failure:@escaping (String) -> Void)  {
        
        guard  let currentBike = BikeRentManager.shared.getBike() else {
            print("Unable to get current Bike")
            failure("")
            return
        }
        var startLocation: (latitude: Double, longitude: Double) = (currentBike.latitude, currentBike.longitude)
        if let userCurrentLocation = LocationManager.shared.userCurrentLocation {
            startLocation = (userCurrentLocation.coordinate.latitude, userCurrentLocation.coordinate.longitude)
        }
        
        getStartAddress(latitude: startLocation.latitude, longitude: startLocation.longitude) {[weak self] (address) in
            guard let `self` = self else { return }
            if !address.isEmpty {
                
                let body = RideRequestBody(startAddress: address,
                                           bike: currentBike.bikeId,
                                           locations: [BikeLocation(latitude:startLocation.latitude, longitude:startLocation.longitude)],
                                           bikeStatus: self.bikeStatus)
                
                //check if bike is still available

                BikeService.get(imei:currentBike.imei, success: { (bikeResponse) in
                    
                    if let bike = bikeResponse.bikes.first, bike.inUse == false, bike.onService == true {
                        RideService.createRide(body: body, success: { (ride) in
                            
                            BikeRentManager.shared.updateCurrentRide(ride: ride)
                            LocationManager.shared.startTrackingRide(currentRideLocations: ride.locations ?? [])
                            
                            success(ride)
                        }, failure: {(errorStr, errors) in
                            failure(errorStr)
                        })
                    } else {
                        failure(APPStrings.UnlockSmartlock.ErrorPopup.subtitle)
                    }
                    
                }, failure: { (errorStr, errors) in
                    failure(APPStrings.UnlockSmartlock.ErrorPopup.subtitle)
                })
                
            } else {
                //couldn't get address
                failure("Unable to create ride start address")
                
            }
        }
        
    }
    
    func unlockSmartLock(completion: @escaping (String, LinkaOperationState) -> Void) {
        
        let linkaLockService:LockConnectionService =   LockConnectionService.sharedInstance
        
        linkaLockService.doUnLock(macAddress: LinkaManager.shared.getMacAdress(), delegate: self.unlockdelegate)
//        LinkaManager.shared.setupAndScan(imei: BikeRentManager.shared.getBike()?.imei).then { peripheral in
//
//            LinkaManager.shared.paire(peripheral: peripheral)
//
//            }.then { managers in
//
//                LinkaInteractionManager.shared.connectAndUnlock(managers: managers)
//
//            }.done { (state) in
//                switch state {
//              //  case .finished, .lostConnection:
////                    completion(APPStrings.LinkaStatus.Unlock.finished.uppercased(), state)
//                default:
//                    print("LinkaManager A problem with LinkaOperationState - state returned \(state)")
//                }
//            }.catch { (error) in
//                if let err = error as? LinkaSetupError {
//                    completion(err.message, .unknown)
//                }
//
//        }
        
    }
    
    func showUnlockingTimeOutPopup(okHandler: @escaping () -> Void,noHandler:@escaping () -> Void)  {
        
        let okAction = PopupAction(title: APPStrings.Common.ok, style: .validate) {
            // hide popup
            AppCoordinator.shared.pop(animated: true, completion: {
                
                okHandler()

            })
        }
        
        let noAction = PopupAction(title: APPStrings.Common.cancel, style: .cancel) {
            // hide popup
            
           
            AppCoordinator.shared.pop(animated: true, completion: {
                noHandler()
              
            })
        }
        
        
        let viewModel = PopupViewModel(type: .choice(title: nil,
                                                     subTitle: NSAttributedString(string:"Problème de connexion au vélo ! \n merci de réessayer une autre fois."),
                                                     leftImage: nil,
                                                     rightImage: nil,
                                                     yesActions: okAction, noAction: noAction))
        
        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)
    }

    //MARK: - Actions
    func getStartAddress(latitude: Double, longitude: Double, completion: @escaping (String) -> Void){
        LocationManager.shared.geocode(latitude: latitude, longitude: longitude) { (placemark, error) in
            guard let placemark = placemark, error == nil else {
                completion("")
                return
            }
            completion(placemark.fullAddress())
        }
        
    }

    
    func navigateToCurrentRideViewController () {
        AppCoordinator.shared.transition(to: .currentRide(CurrentRideViewModel()), type: .rootController, animated: false)
    }

    
}

    
    



