//
//  BikeDiagnosticViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 14/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct DiagnosticStepViewModel {
    var title: String?
    var subTitle: String?
    var stepDescription: String?
    var stepImage: UIImage?
    var stepImageTopMargin: CGFloat?
    
    var state: BikeStatus
    
    mutating func stateOK ()  {
        state.status = true
    }
    mutating func stateKO ()  {
        state.status = false
    }

}

class BikeDiagnosticViewModel: BaseViewModel {
    
    // MARK: - Variables
    var pagesModels: [DiagnosticStepViewModel] = BikeDiagnosticViewModel.build()
    // MARK: - Private
    private static func build() ->  [DiagnosticStepViewModel] {
//        let loopPage = DiagnosticStepViewModel(title: APPStrings.BikeDiagnostic.stepTitle.uppercased(),
//                                               subTitle: APPStrings.BikeDiagnostic.Loop.subTitle.uppercased(),
//                                               stepDescription: APPStrings.BikeDiagnostic.Loop.description,
//                                          stepImage: Asset.Diagnostic.isLoopThere.image,
//                                          stepImageTopMargin: 0.0)
        let brakesPage = DiagnosticStepViewModel(title: APPStrings.BikeDiagnostic.stepTitle.uppercased(),
                                           subTitle:APPStrings.BikeDiagnostic.Brakes.subTitle.uppercased(),
                                           stepDescription: nil,
                                           stepImage: Asset.Diagnostic.brakes.image,
                                           stepImageTopMargin: 0.0,
                                           state: BikeStatus(status: true, type: .brake))
        let lightsPage = DiagnosticStepViewModel(title: APPStrings.BikeDiagnostic.stepTitle.uppercased(),
                                          subTitle: APPStrings.BikeDiagnostic.Lights.subTitle.uppercased(),
                                          stepDescription: nil,
                                          stepImage: Asset.Diagnostic.lights.image,
                                          stepImageTopMargin: 0.0,
                                          state: BikeStatus(status: true, type: .light))
        let tiresPage = DiagnosticStepViewModel(title: APPStrings.BikeDiagnostic.stepTitle.uppercased(),
                                           subTitle: APPStrings.BikeDiagnostic.Tires.subTitle.uppercased(),
                                           stepDescription: nil,
                                           stepImage: Asset.Diagnostic.tires.image,
                                           stepImageTopMargin: 0.0,
                                           state: BikeStatus(status: true, type: .tires))
        
        return [brakesPage, lightsPage, tiresPage]
    }
    
    func validateDiagnostic () -> Bool {
        for state in pagesModels.map({$0.state}) {
            if !state.status {
                return false
            }
        }
        return true
    }
    
    func navigateToRideConfirmation ()  {
        AppCoordinator.shared.transition(to: .rideConfirmation(RideConfirmationViewModel(status: pagesModels.map{$0.state})), type: .push, animated: true)
    }
    
    func navigateToUnLockSmartLock ()  {
        AppCoordinator.shared.transition(to: .unlockSmartLock(UnlockSmartLockViewModel(status: pagesModels.map{$0.state})), type: .push, animated: true)
    }

}
