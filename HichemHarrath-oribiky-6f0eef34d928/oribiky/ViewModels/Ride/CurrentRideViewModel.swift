//
//  CurrentRideViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 07/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class CurrentRideViewModel: BaseViewModel {

   
    
    func calculateTime(for ride: RideResponse?) -> Int  {
        guard let ride = ride else {return 0}
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.Date.format
        formatter.timeZone = TimeZone(identifier: "UTC")
        if let startDate =  formatter.date(from: ride.startedAt ?? "") {
            return Int(Date().timeIntervalSince(startDate))
        }
        return 0
    }


    //MARK: - Actions
    func showFinishRideConfirmationPopUp(completion: @escaping () -> Void) {
        
        let finishRideAction = PopupAction(title: APPStrings.Common.yes, style: .validate) {
            // hide popup
            AppCoordinator.shared.pop(animated: true, completion: {
                completion()
            })
        }
        let cancelAction = PopupAction(title: APPStrings.Common.no, style: .cancel) {
            // hide popup
            AppCoordinator.shared.pop(animated: true)
        }
        
        let viewModel = PopupViewModel(type: .choice(title: nil,
                                                     subTitle: NSAttributedString(string:APPStrings.CurrentRide.ConfirmationPopup.subtitle),
                                                     leftImage: nil,
                                                     rightImage: nil,
                                                     yesActions: finishRideAction, noAction: cancelAction))
        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)
    }
    
    func showPauseRideConfirmationPopUp(completion: @escaping () -> Void) {
        
        
        let pauseRideAction = PopupAction(title: APPStrings.Common.ok.uppercased(), style: .validate) {
            // hide popup
            AppCoordinator.shared.pop(animated: true, completion: {
                completion()
            })
        }
        let popupViewModel = PopupViewModel(type: .infoCustom(title: APPStrings.CurrentRide.PauseConfirmationPopup.title, titleColor: AppStyle.Colors.red, subTitle:APPStrings.CurrentRide.PauseConfirmationPopup.subtitle, subtitleColor: AppStyle.Colors.mainBlue, action: pauseRideAction))
        
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
    }


    
    func navigateToLockSmartLock() {
        AppCoordinator.shared.transition(to: .lockSmartLock(LockSmartLockViewModel()), type: .push, animated: true)

    }
    
}
