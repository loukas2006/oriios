//
//  RideConfirmationViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 07/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import PromiseKit

class RideConfirmationViewModel: BaseViewModel {
    
    // MARK: - Variables
    var bikeStatus: [BikeStatus]
    
    //Mark: Init
    init(status: [BikeStatus]) {
        bikeStatus = status
    }
    
    
    func navigateToUnLockSmartLockViewController () {
        AppCoordinator.shared.transition(to: .unlockSmartLock(UnlockSmartLockViewModel(status: bikeStatus)), type: .push, animated: true)
    }
    
    func navigateToMapViewController () {
        AppCoordinator.shared.transition(to: .map(MapViewModel()), type: .rootController, animated: false)
    }

}
