//
//  NotificationListViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 01/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation


class NotificationListViewModel: BaseViewModel {
    
    
    func callGetNotificationsWS(success:@escaping ([NotificationViewModel]) -> Void,
                                failure:@escaping (String) -> Void) {
        let params = NotificationRequestParams(page: 1, limit: 50)
        
        NotificationService.getAll(params: params, success: { (response) in
            let items = response.notifications.compactMap({NotificationViewModel(notification: $0)})
            success(items)
        }, failure: { (errorStr, errors) in
            failure(errorStr)
        })
    }
    
    func callSetReadNotificationsWS(newIds: [String], completion:@escaping () -> ()) {
        if newIds.count == 0 { return }
        NotificationService.markAsRead(ids: newIds) { (success) in
            if success {
                completion()
            }
        }
    }
    
    func handleNotificationSelectionAction (viewModel: NotificationViewModel) {
        let popupAction = PopupAction(title: APPStrings.Common.ok, style: .validate) {
            AppCoordinator.shared.pop(animated: true)
        }
        let popupViewModel = PopupViewModel(type: .infoScrollable(title: viewModel.title, titleColor: AppStyle.Colors.red, content: viewModel.body, subtitleColor: AppStyle.Colors.mainBlue, action: popupAction))
        
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
    }

}
