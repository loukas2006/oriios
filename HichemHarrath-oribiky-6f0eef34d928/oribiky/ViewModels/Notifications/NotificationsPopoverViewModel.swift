//
//  NotificationsPopoverViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 27/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class NotificationsPopoverViewModel: BaseViewModel {
    
    let preferredSize = CGSize(width: UIScreen.main.bounds.width - 40,
                               height: 240.0)
    
    func navigateToNotificationList(controller: NotificationPopoverViewController){
        controller.dismiss(animated: true) {
            AppCoordinator.shared.transition(to: .notificationList(NotificationListViewModel()),
                                             type: .modal,
                                             animated: true)
        }
    }
    func callSetReadNotificationsWS(newIds: [String], completion:@escaping () -> ()) {
        if newIds.count == 0 { return }
        NotificationService.markAsRead(ids: newIds) { (success) in
            if success {
                completion()
            }
        }
    }
}
