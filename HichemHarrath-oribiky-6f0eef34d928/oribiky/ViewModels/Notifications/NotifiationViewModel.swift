//
//  NotificationViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 01/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class NotificationViewModel: BaseViewModel {

    let identifier: Int
    let title: NSAttributedString
    let body: NSAttributedString
    let seen: Bool
    let createAtString: String
    private let dateFormatter = DateFormatter(withFormat: Constant.Date.Ticketsformat, locale: "fr_FR")

    init(notification: NotificationResponse) {
        identifier = notification.notificationId
        title = NSAttributedString(string: (notification.title ?? "") )
        body = (notification.body ?? "").asHTMLAttributedString(font: AppStyle.Text.regular(size: 13.0).font)
        seen = notification.seen ?? false
        if let dateStr = notification.createdAt, let date = dateFormatter.date(from: dateStr) {
            createAtString = date.timeAgoString
        } else {
            createAtString = ""
        }
    }


}
