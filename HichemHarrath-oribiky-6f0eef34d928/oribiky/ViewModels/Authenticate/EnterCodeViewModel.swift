//
//  EnterCodeViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 27/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation


class EnterCodeViewModel: BaseViewModel {
   
    // MARK: Variables
    private var code: String
    private let phoneNumber: String
    private let tabbarItemIndex: Int
    private var user: UserProfile?
    private let phoneNumberErrorKey = "phoneNumber"

    var errorMessage = ""
    // MARK: Init
    init(athenticationCode: String, phone: String, tabbarIndex: Int) {
        code = athenticationCode
        phoneNumber = phone
        tabbarItemIndex = tabbarIndex
    }
    
    // MARK: Public
    func updateCode(_ newCode: String) {
        code = newCode
    }
    
    // MARK: WS calls
    func callWSVerify(enteredCode: String,
                success:@escaping () -> Void,
                failure:@escaping (String) -> Void) {
        AuthenticateService.verify(code: enteredCode, identifier: code, success: {[weak self] verifyResponse in
            guard let `self` = self else { return }
            DefaultsManager.shared.saveToken(tokerStr: verifyResponse.token)
            self.user = UserProfile(profileResponse: verifyResponse.user)
            success()
        }, failure: {[weak self] (errorStr, errors) in
            self?.errorMessage = errors?.values.compactMap({ $0.joined(separator: "-")}).joined(separator: "-") ?? ""
            failure(errorStr)
        })
    }
    
    func callWSAuthenticate(success:@escaping (String) -> Void,
                failure:@escaping (String) -> Void) {
        
        let params = AuthenticateRequestBody(phone: phoneNumber, deviceToken: DefaultsManager.shared.deviceToken)
        AuthenticateService.authenticate(requestBody: params, success: success) {[weak self] (errorStr, erorrs) in
            guard let `self`  = self else { return }
            failure(erorrs?[self.phoneNumberErrorKey]?.first ?? "")
        }
    }
  
    // MARK: Actions
    func handleAuthenticationSuccess() {
        // here we should reset all tabbar items root view controller
        BikeRentManager.shared.updateUser(user: user)
        switch tabbarItemIndex {
        case 0:
            // notify authentication succeeded
//            delegate?.userDidAuthenticate()
            if BikeRentManager.shared.hasRide ||  BikeRentManager.shared.hasReservation{
                ReservationRequirementsProcessManager.shared.ignoreSteps()
                AppCoordinator.shared.pop(animated: true, completion: {
                    // start again from tabbar controller
                    AppCoordinator.shared.transition(to: .tabbar(TabbarViewModel()), type: .root, animated: true)
                })
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.reservationRequirementsNotificationName), object: StepType.authentication)

            }
        case 1:
            if BikeRentManager.shared.getUser().isSubscribed() {
                let viewModel = SubscriptionViewModel(SubscriptionType(rawValue: BikeRentManager.shared.getUser().subscription.details.type) ?? .minutePrice)
                AppCoordinator.shared.transition(to: .mySubscription(viewModel), type: .rootController, animated: false)
            } else {
                AppCoordinator.shared.transition(to: .subscriptionList(SubscriptionListViewModel(isDisplayedInTabbarItem: true), false), type: .rootController, animated: false)
            }
        case 2:
            AppCoordinator.shared.transition(to: .report(ReportViewModel()), type: .rootController, animated: false)
        case 3:
            AppCoordinator.shared.transition(to: .profile(ProfileViewModel()), type: .rootController, animated: false)
        // case 4 added to handle authentication from burguer menu, we should check if we has a reservation or location in progress
        case 4:
            AppCoordinator.shared.pop(animated: true, completion: {
                if BikeRentManager.shared.hasRide ||  BikeRentManager.shared.hasReservation{
                    ReservationRequirementsProcessManager.shared.ignoreSteps()
                    // start again from tabbar controller
                    AppCoordinator.shared.transition(to: .tabbar(TabbarViewModel()), type: .root, animated: true)
                }
            })
            
        default:
            AppCoordinator.shared.pop(animated: true)
        }

    }
    
}
