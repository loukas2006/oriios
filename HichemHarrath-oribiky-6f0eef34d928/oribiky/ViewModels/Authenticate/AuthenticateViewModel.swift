//
//  AuthenticateViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 27/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class AuthenticateViewModel: BaseViewModel {

    // MARK: - Variables
    var selectedCountry = CountryList.france
    var tabbarItemIndex: Int
    var isPushed: Bool
    private let phoneNumberErrorKey = "phoneNumber"

    init(index: Int = 1, pushed: Bool = false) {
        tabbarItemIndex = index
        isPushed = pushed
    }
    
    // MARK: - Web service call
    func callWS(phoneNumber: String,
                success:@escaping (String) -> Void,
                failure:@escaping (String) -> Void) {
        
        let params = AuthenticateRequestBody(phone: phoneNumber, deviceToken: DefaultsManager.shared.deviceToken)
        AuthenticateService.authenticate(requestBody: params, success: success) { [weak self](errorStr, erorrs) in
            guard let `self` = self else { return }
            failure(erorrs?[self.phoneNumberErrorKey]?.first ?? "") }

    }

    // MARK: - Actions
    func showCountryPopup(completion: @escaping () -> ()) {
        AppCoordinator.shared.transition(to: .popup(PopupViewModel(type:
            .countries({ [weak self](country) in
                self?.selectedCountry = country
                AppCoordinator.shared.pop(animated: true)
                completion()
            }))),
                                         type: .popup,
                                         animated: true)
    }
    
    func showInfoPopup(completion:@escaping () -> ()) {
        
        let popupBtnAction = PopupAction(title: APPStrings.Authenticate.askForCodeBtnTitle, style: .validate, action: {
            // here we should call ws
            AppCoordinator.shared.pop(animated: true, completion: {
                completion()
            })
        })
        
        let popupViewModel = PopupViewModel(type: .info(title: APPStrings.Authenticate.askForCodePopupTitle, subTitle: APPStrings.Authenticate.askForCodePopupSubtitle, action: popupBtnAction))
            
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
    }
    
    func navigateToEnterCode(code: String, phoneNumber: String) {
        // ws success
        let viewModel = EnterCodeViewModel(athenticationCode: code,
                                           phone: phoneNumber,
                                           tabbarIndex: tabbarItemIndex)
        AppCoordinator.shared.transition(to: .enterCode(viewModel), type: .push, animated: true)
    }
    
}
