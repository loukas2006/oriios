//
//  PdfViewerViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 13/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

enum PDF {

    case termsOfUseAndSale
    case faq
    case privacyPolicy
    //PDF file url
    private var path: URL? {
        switch self {
        case .termsOfUseAndSale:
            return URL(string: HTTPAPI.PDF.termsOfUseAndSale)
        case .faq:
            return URL(string: HTTPAPI.PDF.faq)
        case .privacyPolicy:
            return URL(string: HTTPAPI.PDF.privacyPolicy)

        }
    }
    
    var request: URLRequest? {
        guard let path = path else {return nil}
        return URLRequest(url: path)
    }

    var title: String {
        switch self {
        case .termsOfUseAndSale:
            return APPStrings.LegalNotice.termsBtnTitle
        case .faq:
            return APPStrings.PdfViewer.Faq.title
        case .privacyPolicy:
            return APPStrings.LegalNotice.privacyPolicyBtnTitle


        }

    }
}
class PdfViewerViewModel: BaseViewModel {
    
    var pdf:PDF
    init(withPDF pdf: PDF) {
        self.pdf = pdf
    }

}
