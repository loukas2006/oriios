//
//  AvailableBikeViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 13/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class AvailableBikeViewModel: BaseViewModel {

    let bike: Bike
    init(bike: Bike) {
        self.bike = bike
    }
}
