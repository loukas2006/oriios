//
//  ActivateBluetoothViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 11/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import CoreBluetooth

class ActivateBluetoothViewModel: BaseViewModel {
    
    // MARK: - Variables
    var bike: Bike
    // MARK: - Init
    init(bike: Bike) {
        self.bike = bike
    }

    // MARK: - Actions
    func handleBluetoothActivation() {
        if BluetoothManager.shared.isPoweredOn {
            AppCoordinator.shared.transition(to: .connectToBike(ConnectToBikeViewModel()), type: .push, animated: true)
        }
    }
    func showActivationPopup() {
       /* let action = PopupAction(title: APPStrings.Common.yes.uppercased(), style: .validate) {
            AppCoordinator.shared.pop(animated: true, completion: {
                AppCoordinator.shared.transition(to: .turnOnSmartLock(TurnOnSmartLockViewModel()), type: .push, animated: true)
            })
        }*/
        let action = PopupAction(title: APPStrings.Common.ok.uppercased() , style: .validate) {
           
            AppCoordinator.shared.pop(animated: true, completion: {
                BluetoothManager.shared.stateChangeHandler = {
                    if BluetoothManager.shared.isPoweredOn {
                        AppCoordinator.shared.transition(to: .connectToBike(ConnectToBikeViewModel()), type: .push, animated: true)
                        
                    }
                }
            })
        }
        
        let popupViewModel = PopupViewModel(type: .info(title: APPStrings.ActivateBluetooth.Popup.title.uppercased(), subTitle: APPStrings.ActivateBluetooth.Popup.subTitle, action: action))
        
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
    }
}

