//
//  MapViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 22/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import MapKit

class MapViewModel: BaseViewModel {
    // MARK: - Types
    typealias UserLocationCompletion =  ((_ location: CLLocation?) -> ())
    
    // MARK: - Varaibles
    private var bikesArray = [Bike]()
    private var userLocationCompletion: UserLocationCompletion?
    
    // MARK: - Init
    override init() {
        _ = BluetoothManager.shared //to Init bluetooth state
    }
    
    // MARK: - Service Call
    func callWS(code: String, success:@escaping ([Bike]) -> Void , failure:@escaping (String) -> Void){
        BikeService.get(code: code, success: { (bikeResponse) in
            success(bikeResponse.bikes)
        }, failure:  { (errorStr, errors) in
            failure(errorStr)
        })
    }
    
    func callws(userLocation: String,
                success:@escaping ([Bike]) -> Void,
                failure:@escaping (String) -> Void)  {
        
        let requestParams = GetBikeListRequestParams(location: userLocation,
                                                     imei: "",
                                                     page: 1,
                                                     limit: 150)
        
        BikeService.getList(params: requestParams,
                            success: {  [weak self] bikeResponse in
                                guard let strongSelf = self else { return }
                                strongSelf.bikesArray = bikeResponse.bikes
                                success(strongSelf.bikesArray)
            }, failure: { (errorStr, errors) in
                failure(errorStr)
        })
    }
    
    // MARK: - Location
    
    func askEnablingLocationIfNeeded(completion: UserLocationCompletion? = nil) {
        
        // save completion handler
        userLocationCompletion = completion
        
        LocationManager.shared.verifyAuthorizationStatus {[weak self] (status) in
            switch status {
            case .notDetermined, .notAccorded:
                // we should show popup
                print("we should show popup")
                self?.showAskLocalisationPopup()
                
            case .accorded:
                // we should get user location
                print("accorded")
                LocationManager.shared.getUserLocation(completion: { [weak self] (location) in
                    self?.userLocationCompletion?(location)
                })
            }
        }
    }
    
    func stopUpdatingUserLocation () {
        userLocationCompletion = nil
    }
    
    private func showAskLocalisationPopup() {
        
        let askForLocationAction = PopupAction(title: APPStrings.Common.yes, style: .validate) { [weak self] in
            // hide popup
            AppCoordinator.shared.pop(animated: true)
            // ask user location
            LocationManager.shared.verifyAuthorizationStatus(completion: { (authorizationStatus) in
                if authorizationStatus == .notAccorded {
                    // user refused location, should go to settings
                    if let url = URL(string: "app-settings:root=Privacy&path=LOCATION"),  UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else if let url = URL(string: UIApplicationOpenSettingsURLString), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }else {
                        self?.userLocationCompletion?(nil)
                    }
                }else if  authorizationStatus == .notDetermined {
                    LocationManager.shared.askAuthorization(completion: { (state) in
                        if (state == .denied || state == .restricted){
                            self?.userLocationCompletion?(nil)
                        } else {
                            LocationManager.shared.getUserLocation(completion: { (location) in
                                self?.userLocationCompletion?(location)
                            })
                            
                        }
                    })
                } else {
                    LocationManager.shared.getUserLocation(completion: { (location) in
                        self?.userLocationCompletion?(location)
                    })
                }
                
            })
        }
        let cancelAction = PopupAction(title: APPStrings.Common.no, style: .cancel) { [weak self] in
            // hide popup
            AppCoordinator.shared.pop(animated: true)
            self?.userLocationCompletion?(nil)
        }
        
        let viewModel = PopupViewModel(type: .choice(title: NSAttributedString(string: APPStrings.Map.Localisation.Popup.title),
                                                     subTitle: NSAttributedString(string: APPStrings.Map.Localisation.Popup.subtitle),
                                                     leftImage: nil,
                                                     rightImage: nil,
                                                     yesActions: askForLocationAction, noAction: cancelAction))
        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)
    }
    
    // MARK: - Actions
    func handleBikeSelection(_ bike: Bike) {
        // create popup actions
        let popupNowAction = PopupAction(title: APPStrings.Map.RentBikePopup.nowBtnTitle, style: .validate) {
            AppCoordinator.shared.pop(animated: true, completion: { [weak self] in
                guard let `self` = self else { return }
                self.rentNowAction(bike)
            })
        }
        
        let popupLaterAction = PopupAction(title: APPStrings.Map.RentBikePopup.layterBtnTitle, style: .validate) {
            // dismiss popup
            AppCoordinator.shared.pop(animated: true, completion: { [weak self] in
                guard let `self` = self else { return }
                self.rentLaterAction(bike)
            })
        }
        
        let popupTitle = APPStrings.Map.RentBikePopup.title(bike.code, "\(bike.distance)")
        let attributedTitle = popupTitle.highlightColor(highlightedTexts: [bike.code, "\(bike.distance)"], font: AppStyle.Text.bold(size: 17.0).font, color: AppStyle.Colors.red, lineSpace: 10.0 )
        
        let popupViewModel = PopupViewModel(type: .choice(title: attributedTitle,
                                                          subTitle: nil,
                                                          leftImage: Asset.Map.bikeNow.image,
                                                          rightImage: Asset.Map.bikeLater.image,
                                                          yesActions: popupLaterAction,
                                                          noAction: popupNowAction))
        
        // present popup
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
    }
    
    func rentNowAction(_ bike: Bike) {
        
        ReservationRequirementsProcessManager.shared.handleSteps()
        
        ReservationRequirementsProcessManager.shared.start(completion: {
            
            ReservationRequirementsProcessManager.shared.ignoreSteps()
            
            //Update current Bike
            BikeRentManager.shared.updateBike(bike: bike)
            
            // push bike reservation process
            if BluetoothManager.shared.isPoweredOn {
                // show turn on smart lock
                AppCoordinator.shared.transition(to: .connectToBike(ConnectToBikeViewModel()), type: .push, animated: true)
            } else {
                // show bluetooth view
                AppCoordinator.shared.transition(to: .activateBluetooth(ActivateBluetoothViewModel(bike: bike)), type: .push, animated: true)
            }
        })
        
    }
    func rentLaterAction(_ bike: Bike) {
        
        ReservationRequirementsProcessManager.shared.handleSteps()
        
        ReservationRequirementsProcessManager.shared.start(finalStep: Step(type: .payment), completion: {
            
            ReservationRequirementsProcessManager.shared.ignoreSteps()
            //Update current Bike
            BikeRentManager.shared.updateBike(bike: bike)
            //call create reservation web service
            let reservationViewModel = CurrentReservationViewModel()
            
            reservationViewModel.callCreateReservationWS(success: {
                AppCoordinator.shared.transition(to: .currentReservation(reservationViewModel), type: .rootController, animated: false)
            })
        })
    }
    
    func handleBikeClusterSelection(bikes: [Bike]) {
        
        let popupViewModel = PopupViewModel(type: .bikes(bikes, { [weak self] (selectedBike) in
            AppCoordinator.shared.pop(animated: true, completion: {
                self?.handleBikeSelection(selectedBike)
            })
        }))
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
    }
    func handleBikeNotFound(text: String) {

        let popupAction = PopupAction(title: APPStrings.Common.ok.uppercased(), style: .validate) {
            AppCoordinator.shared.pop(animated: true)
        }
        let popupViewModel = PopupViewModel(type: .infoCustom(title: text, titleColor: AppStyle.Colors.red, subTitle:APPStrings.Map.SearchView.noBikeFound, subtitleColor: AppStyle.Colors.mainBlue, action: popupAction))
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
    }
}
