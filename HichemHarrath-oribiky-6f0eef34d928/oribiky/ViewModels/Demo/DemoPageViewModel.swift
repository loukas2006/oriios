//
//  DemoPageViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

struct DemoPageViewModel {
    var title: String?
    var subTitle: String?
    var stepImage: UIImage?
    var stepImageTopMargin: CGFloat?
}
