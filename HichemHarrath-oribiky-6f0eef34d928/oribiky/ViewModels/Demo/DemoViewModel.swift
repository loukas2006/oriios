//
//  DemoViewModel.swift
//  oribiky
//
//  Created by Hichem Harrath on 30/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
class DemoViewModel: BaseViewModel {

    // MARK: - Variables
    var pagesModels = [DemoPageViewModel]()
    
    // MARK: - Init
    override init() {
        super.init()
        build()
    }
    
    // MARK: - Private
    private func build() {
        let firstPage = DemoPageViewModel(title: APPStrings.Demo.firstStepTitle,
                                          subTitle: APPStrings.Demo.firstStepSubTitle,
                                          stepImage: Asset.Demo.step1.image,
                                          stepImageTopMargin: 10.0)
        let secondPage = DemoPageViewModel(title: APPStrings.Demo.secondStepTitle,
                                           subTitle: APPStrings.Demo.secondStepSubTitle,
                                           stepImage: Asset.Demo.step2.image,
                                           stepImageTopMargin: 20.0)
        let thirdPage = DemoPageViewModel(title: APPStrings.Demo.thirdStepTitle,
                                          subTitle: APPStrings.Demo.thirdStepSubTitle,
                                          stepImage: Asset.Demo.step3.image,
                                          stepImageTopMargin: 20.0)
        let fourthPage = DemoPageViewModel(title: APPStrings.Demo.fourthStepTitle,
                                           subTitle: APPStrings.Demo.fourthStepSubTitle,
                                           stepImage: Asset.Demo.step4.image,
                                           stepImageTopMargin: 0.0)
        
        pagesModels =  [firstPage, secondPage, thirdPage, fourthPage]
    }
    

}
