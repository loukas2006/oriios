//
//  EditableFormItemViewModel.swift
//  oribiky
//
//  Created by Aymen Harrath on 27/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Stripe


enum FormItemType: String {
    case firstName = "firstname"
    case lastName =  "lastname"
    case email
    case streetNumber
    case streetName
    case locality
    case postalCode
    case cardOwner
    case cardNumber
    case expireDate
    case cvvCode
    
    var title: String {
        switch self {
        case .firstName: return APPStrings.ProfileCell.firstName
        case .lastName: return APPStrings.ProfileCell.lastName
        case .email: return APPStrings.ProfileCell.email
        case .streetNumber: return APPStrings.AddressCell.number
        case .streetName: return APPStrings.AddressCell.street
        case .locality: return APPStrings.AddressCell.city
        case .postalCode: return APPStrings.AddressCell.zipCode
        case .cardOwner: return APPStrings.NewCard.cardOwner
        case .cardNumber: return APPStrings.NewCard.cardNumber
        case .expireDate: return APPStrings.NewCard.expireDate
        case .cvvCode: return APPStrings.NewCard.cvvCode
        }
    }
    
}


class EditableFormItemViewModel {
    
    let type: FormItemType
    let value: String
    var editedValue: String
    
    var isValid: Bool
    
    var keyboardType: UIKeyboardType {
        
        switch type {
        case .firstName, .lastName, .streetName, .locality, .cardOwner:
            return .default
        case .email:
            return .emailAddress
        case .streetNumber, .postalCode, .cardNumber, .cvvCode, .expireDate:
            return .numberPad
        }
        
    }
    
    var capitalizationType: UITextAutocapitalizationType {
        switch type {
        case .firstName, .lastName, .locality, .streetName:
            return .sentences
        case .cardOwner:
            return .allCharacters
        case  .email, .streetNumber, .postalCode, .cardNumber, .cvvCode, .expireDate:
            return .none
        }
    }
    
    var wsErrorStr: String? = ""
    
    var isValidStr: String {
        
        switch type {
        case .firstName:
            return isValid ? "" : APPStrings.Firstname.required
        case .lastName:
            return isValid ? "" : APPStrings.Lastname.required
        case .email:
            return isValid ? "" : APPStrings.Email.required
        case .streetNumber:
            return isValid ? "" : APPStrings.Street.Number.required
        case .streetName:
            return isValid ? "" : APPStrings.Street.Name.required
        case .locality:
            return isValid ? "" : APPStrings.Street.Locality.required
        case .postalCode:
            return isValid ? "" : APPStrings.Street.Postalcode.required
        case .cardOwner:
            return isValid ? "" : APPStrings.NewCard.cardOwnerError
        case .cardNumber:
            return isValid ? "" : APPStrings.NewCard.cardNumberError
        case .expireDate:
            return isValid ? "" : APPStrings.NewCard.expireDateError
        case .cvvCode:
            return isValid ? "" : APPStrings.NewCard.cvvCodeError

        }
        

    }
    
    func updateEditValue(v: String)  {
        editedValue = v
        validate()
    }
    
    func validate()  {
        switch type {
        case .firstName, .lastName, .streetName, .locality:
            isValid = !editedValue.isEmpty
        case .email:
            isValid = editedValue.isAValidEmailAddress()
        case .streetNumber, .postalCode:
            if let number = Int(editedValue), number > 0 {
                isValid = true
            } else {
                isValid = false
            }
        case .cardOwner:
            isValid = !editedValue.isEmpty
        case .cardNumber:
            isValid =  STPCardValidator.validationState(forNumber: editedValue, validatingCardBrand: true) == .valid
        case .expireDate:
            let splittedDate = editedValue.components(separatedBy: AppStyle.expirationDateSeparator)
            guard let expMonth = splittedDate.first, let expYear = splittedDate.last else {
                isValid = false
                return
            }
            isValid =  STPCardValidator.validationState(forExpirationMonth: expMonth) == .valid &&  STPCardValidator.validationState(forExpirationYear: expYear, inMonth: expMonth) == .valid
        case .cvvCode:
            isValid =  STPCardValidator.validationState(forCVC: editedValue, cardBrand: .unknown) == .valid
        }
        
    }
    
    
    init(type: FormItemType, value: String) {
        self.type = type
        self.value = value
        self.editedValue = value
        self.isValid = true
    }
    
}
