//
//  UserProfile.swift
//  oribiky
//
//  Created by Aymen Harrath on 26/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct UserProfile {
    
    // MARK/ - Variables
    let phoneNumber: String
    let email: String
    let newsletter: Bool
    let firstname: String
    let lastname: String
    let lastLogin: String
    let fcmRegistrationId: String
    let device: String
    let packages: [Package]
    let coupons: [Coupon]
    let subscription: Subscription
    let address:Address
    let card: Card

    // MARK: - Types
    struct Card {
        let cardBrand: String
        let lastDigits: String
    }
    
    struct Address {
        let streetNumber: Int?
        let streetName: String?
        let locality: String?
        let postalCode: Int?
                
        func literal() -> String {
            guard let streetName = streetName, let streetNumber = streetNumber, let postalCode = postalCode, let locality = locality
            else { return "" }
            
            return  "\(streetNumber) "  + streetName + " \(postalCode), " + locality
        }
    }
    
    struct Package {
        let credit: Double
        let expireAt: String
    }
    struct Coupon {
        let name: String
    }

    struct Subscription {
        let status: Bool
        let details: SubscriptionDetails
        let endsAt: String?
        let billingPeriodEndsAt: String?

    }
    
    
    struct SubscriptionDetails {
        let type: String
        let freeMinutes: Double
        let freeMinutesWe: Double
        let extraMinutePrice: Double
        let validated: Int?
    }

    // MARK: - Init
    init() {
        
        phoneNumber = ""
        email = ""
        firstname = ""
        lastname = ""
        lastLogin = ""
        fcmRegistrationId = ""
        device = ""
        newsletter = false
        let subscriptionDetails = SubscriptionDetails(type: "", freeMinutes: 0, freeMinutesWe: 0, extraMinutePrice: 0, validated: nil )
        packages = []
        coupons = []
        subscription = Subscription(status: false, details: subscriptionDetails, endsAt: "", billingPeriodEndsAt: "")
        address = Address(streetNumber: nil, streetName: nil, locality: nil, postalCode: nil)
        card = Card(cardBrand: "", lastDigits: "")

    }
    
    
    init(profileResponse: ProfileResponse) {
        
        phoneNumber = profileResponse.phoneNumber
        email = profileResponse.email ?? ""
        firstname = profileResponse.firstname ?? ""
        lastname = profileResponse.lastname ?? ""
        newsletter = profileResponse.newsletter ?? false
        lastLogin = profileResponse.lastLogin ?? ""
        fcmRegistrationId = profileResponse.fcmRegistrationId ?? ""
        device = profileResponse.device ?? ""
        packages = profileResponse.packages?.compactMap({ Package(credit: $0.credit, expireAt: $0.expireAt)}) ?? []
        coupons = profileResponse.coupons?.compactMap({Coupon(name: $0.name)}) ?? []
        let subscriptionDetails = SubscriptionDetails(type: profileResponse.subscription?.details?.type ?? "",
                                                      freeMinutes: profileResponse.subscription?.details?.freeMinutes ?? 0,
                                                      freeMinutesWe: profileResponse.subscription?.details?.freeMinutesWe ?? 0,
                                                      extraMinutePrice: profileResponse.subscription?.details?.extraMinutePrice ?? 0, validated: profileResponse.subscription?.details?.validated)
        subscription = Subscription(status: profileResponse.subscription?.status ?? false,
                                    details: subscriptionDetails,
                                    endsAt: profileResponse.subscription?.endsAt?.components(separatedBy: "T").first,
                                    billingPeriodEndsAt:  profileResponse.subscription?.billingPeriodEndsAt?.components(separatedBy: "T").first)
        
        address = Address(streetNumber: Int(profileResponse.address?.streetNumber ?? ""),
                          streetName: profileResponse.address?.streetName,
                          locality: profileResponse.address?.locality,
                          postalCode: profileResponse.address?.postalCode)
        
        card = Card(cardBrand: profileResponse.card?.cardBrand ?? "", lastDigits: profileResponse.card?.lastDigits ?? "")

    }
    
    // MARK: - Methods
    
    func haveCard() -> Bool {
        return !card.lastDigits.isEmpty
    }
    
    func isSubscribed() -> Bool {
        return subscription.status
    }
    //        return subscription.endsAt != nil && subscription.billingPeriodEndsAt == nil
    func isSubscriptionActive() -> Bool {
        return subscription.status  && subscription.billingPeriodEndsAt != nil
    }
}
