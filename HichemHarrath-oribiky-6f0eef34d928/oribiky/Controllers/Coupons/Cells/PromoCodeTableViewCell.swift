//
//  PromoCodeTableViewCell.swift
//  oribiky
//
//  Created by Aymen Harrath on 01/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class PromoCodeTableViewCell: UITableViewCell, ViewModelConfigurable {
    
    var viewModel: PromoCodeCellViewModel!
    
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var codeLabelBgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView () {
        selectionStyle = .none
        
        codeLabelBgView.backgroundColor = AppStyle.Colors.mainGray.withAlphaComponent(0.3)
        codeLabel.font = AppStyle.Text.medium(size: 14).font
        
    }

    func configureWithViewModel() {
        codeLabel.text = viewModel.name
    }

}
