//
//  CouponsViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 30/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class CouponsViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {
    
    // MARK: - Variable
    var viewModel: CouponsViewModel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var promoCodeMessageLabel: UILabel!
    @IBOutlet weak var promoCodeTextField: UITextField!
    @IBOutlet weak var promoCodeLineView: UIView!
    @IBOutlet weak var promoCodeIndicationLabel: UILabel!
    @IBOutlet weak var addCodeBtn: RoundedButton!
    
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var initialContentSizeHeight: CGFloat = 0.0
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func configureWithViewModel() {
        
        let defaultCenter = NotificationCenter.default
        defaultCenter.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        defaultCenter.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if viewModel.items.count > 0 {
            tableViewHeightConstraint.constant = viewModel.items[0].height * CGFloat(viewModel.items.count)
        }

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if scrollView.contentSize.height == 0 {
            return
        }
        initialContentSizeHeight = scrollView.contentSize.height
    }

    // MARK: - Private
    private func setupView() {
        
        //add menu && notification
        addRighNavigationItems()

        addBackNavigationItem()
        
        title = APPStrings.Coupons.title
        
        titleLabel.text = APPStrings.Coupons.Header.title.uppercased()
        titleLabel.textColor = AppStyle.Colors.mainBlue
        titleLabel.font = AppStyle.Text.medium(size: 16).font
        
        messageLabel.text = APPStrings.Coupons.Message.One.title
        messageLabel.textColor = AppStyle.Colors.black
        messageLabel.font = AppStyle.Text.regular(size: 14).font

        promoCodeMessageLabel.text = APPStrings.Coupons.PromoCode.title
        promoCodeMessageLabel.textColor = AppStyle.Colors.black
        promoCodeMessageLabel.font = AppStyle.Text.regular(size: 14).font

        promoCodeIndicationLabel.text = nil
        promoCodeIndicationLabel.font = AppStyle.Text.regular(size: 11).font
        
        addCodeBtn.backgroundColor = AppStyle.Colors.mainBlue
        addCodeBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        addCodeBtn.titleLabel?.font = AppStyle.Buttons.medium16.font
        addCodeBtn.setTitle(APPStrings.Coupons.PromoCode.AddBtn.title, for: .normal)
        
        promoCodeLineView.backgroundColor = AppStyle.Colors.mainBlue
        
        promoCodeTextField.textAlignment = .center
        
        promoCodeTextField.delegate = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyoard(_:)))
        scrollView.addGestureRecognizer(tapGestureRecognizer)

        let nib = UINib.init(nibName: String(describing: PromoCodeTableViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: PromoCodeTableViewCell.self))
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
    }
    
    
    // MARK: Keyboard Events
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
            let end = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue else {
                return
        }
        let keyboardHeight = end.cgRectValue.size.height
        var contentSize = scrollView.contentSize
        let calculatedHeight =  view.frame.height - addCodeBtn.frame.origin.y - addCodeBtn.frame.height - 10 //pading
        contentSize.height = initialContentSizeHeight + keyboardHeight - calculatedHeight
        scrollView.contentSize = contentSize
        scrollView.setContentOffset(CGPoint(x: 0, y: max(0,keyboardHeight - calculatedHeight)), animated: true)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        var contentSize = scrollView.contentSize
        contentSize.height = initialContentSizeHeight
        scrollView.contentSize = contentSize
        scrollView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    @objc func hideKeyoard (_ recognize: UITapGestureRecognizer) {
        if promoCodeTextField.isFirstResponder {
            promoCodeTextField.resignFirstResponder()
        }
    }
    

    @IBAction func addBtnPressed(_ sender: RoundedButton) {
        guard let code = promoCodeTextField.text,  code.count != 0 else {
            promoCodeIndicationLabel.textColor = AppStyle.Colors.red
            promoCodeIndicationLabel.text = APPStrings.Coupons.PromoCode.Empty.title
            return
        }
        self.startLoading()
        viewModel.callWs(code: code, success: {[weak self] (user) in
            self?.stopLoading()
            
            BikeRentManager.shared.updateUser(user: user)
            
            self?.promoCodeIndicationLabel.textColor = AppStyle.Colors.mainGreen
            self?.promoCodeIndicationLabel.text = APPStrings.Coupons.PromoCode.Success.title
            
            self?.tableView.reloadData()

        }) {[weak self]  (errorStr, errors) in
            self?.stopLoading()
            self?.promoCodeIndicationLabel.textColor = AppStyle.Colors.red
            self?.promoCodeIndicationLabel.text = errors?.values.first?.first?.localized()

        }
        
    }
    
}

extension CouponsViewController: UITextFieldDelegate {
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        promoCodeTextField.resignFirstResponder()
        return true
    }
}

//MARK: - UITableViewDataSource
extension CouponsViewController:  UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PromoCodeTableViewCell.self), for: indexPath) as? PromoCodeTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(to: viewModel.items[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.items[indexPath.row].height
    }
}


