//
//  AuthenticationViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 27/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class AuthenticationViewController: UIViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    
    // MARK: - Variables
    var viewModel: AuthenticateViewModel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var phonePrefixLabel: UILabel!
    @IBOutlet weak var countryFlagImageView: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var askForCodeBtn: RoundedButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
        phonePrefixLabel.text = viewModel.selectedCountry.phoneCode
        countryFlagImageView.image = viewModel.selectedCountry.flag
        
        var mapTabBarItem = UITabBarItem()
        switch viewModel.tabbarItemIndex {
        case 1:
            mapTabBarItem = UITabBarItem(title: APPStrings.TabBar.subscriptionTitle, image: Asset.TabBar.subscription.image, selectedImage: Asset.TabBar.subscription.image)
        case 2:
            mapTabBarItem = UITabBarItem(title: APPStrings.TabBar.reportTitle, image: Asset.TabBar.report.image, selectedImage: Asset.TabBar.report.image)
        case 3:
            mapTabBarItem = UITabBarItem(title: APPStrings.TabBar.profileTitle, image: Asset.TabBar.profile.image, selectedImage: Asset.TabBar.profile.image)
        default:
            break
        }
        tabBarItem = mapTabBarItem
        
        if viewModel.isPushed {
            addBackNavigationItem()
        }
    }
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Private
    private func setupView() {
        title = APPStrings.Authenticate.title
        countryFlagImageView.round(value: 4)
        
        phonePrefixLabel.textColor = AppStyle.Colors.mainGray
        phonePrefixLabel.font = AppStyle.Text.medium(size: 14.0).font
        
        phoneNumberTextField.font = AppStyle.Text.medium(size: 14.0).font
        phoneNumberTextField.textColor = AppStyle.Colors.mainGray
        
        titleLabel.font = AppStyle.Text.bold(size: 16).font
        titleLabel.textColor = AppStyle.Colors.black
        titleLabel.text = APPStrings.Authenticate.titleLabelText
        
        errorLabel.font = AppStyle.Text.regular(size: 10).font
        errorLabel.textColor = AppStyle.Colors.red
        errorLabel.text = ""
        
        separatorView.backgroundColor = AppStyle.Colors.black
        
        askForCodeBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        askForCodeBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        askForCodeBtn.backgroundColor = AppStyle.Colors.mainBlue
        askForCodeBtn.setTitle(APPStrings.Authenticate.askForCodeBtnTitle.uppercased(), for: .normal)
        
        
        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(hideKeyboardGesture)
    }
    
    // MARK: - Actions
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    @IBAction func selectCountryBtnPressed(_ sender: UIButton) {
        viewModel.showCountryPopup {[weak self] in
            self?.configureWithViewModel()
        }
    }
    
    @IBAction func askForCodeBtnPressed(_ sender: RoundedButton) {
        phoneNumberTextField.resignFirstResponder()
        var enteredPhoneNumber =  phoneNumberTextField.text ?? ""
        // check fields validity
        if !enteredPhoneNumber.isNumbersOnly {
            errorLabel.text = APPStrings.Authenticate.phoneErrorTex
            return
        } else {
            errorLabel.text = ""
        }
        // append prefix
        enteredPhoneNumber = (phonePrefixLabel.text ?? "") + enteredPhoneNumber
        
        // should call WS
        startLoading()
        viewModel.callWS(phoneNumber: enteredPhoneNumber, success: { [weak self] code in
            // hide loader
            self?.stopLoading()
            // we should navigate to next View Controller to enter the co
            self?.viewModel.navigateToEnterCode(code: code, phoneNumber: enteredPhoneNumber)
            }, failure: { [weak self] (errorStr) in
                self?.stopLoading()
                // show error message
                self?.errorLabel.text = errorStr.localized()
                //self?.viewModel.showErrorPopuo(subtitle: errorStr)
        })
    }
}
