//
//  EnterAuthenticationCodeViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 27/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class EnterAuthenticationCodeViewController: UIViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    // MARK: - Variables
    var viewModel: EnterCodeViewModel!
    let launchViewModel = LaunchViewModel()

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var codeView: AuthenticationCodeView!
    @IBOutlet weak var askAgainForCodeBtn: RoundedButton!
    @IBOutlet weak var validateBtn: RoundedButton!
    @IBOutlet weak var errorLabel: ErrorLabel!
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
    }

    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackNavigationItem()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // show keyboard
        codeView.launch()
    }

    // MARK: - Private
    private func setupView() {
        
        title = APPStrings.Authenticate.title
        
        titleLabel.font = AppStyle.Text.bold(size: 16).font
        titleLabel.textColor = AppStyle.Colors.black
        titleLabel.text = APPStrings.Authenticate.EnterCode.titleLabelText

        askAgainForCodeBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        askAgainForCodeBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        askAgainForCodeBtn.backgroundColor = AppStyle.Colors.mainBlue
        askAgainForCodeBtn.alpha = 0.0 // hide askAgainForCodeBtn btn
    askAgainForCodeBtn.setTitle(APPStrings.Authenticate.EnterCode.askAgainForCodeBtnTitle.uppercased(), for: .normal)
        validateBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        validateBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        validateBtn.backgroundColor = AppStyle.Colors.mainBlue
        validateBtn.setTitle(APPStrings.Authenticate.EnterCode.validateBtnTitle.uppercased(), for: .normal)
        // show btn after 60s
        askAgainForACodeTimer()
    }

    // MARK: - Actions
    @IBAction func validateBtnPressed(_ sender: RoundedButton) {
        
        if !codeView.isCodeValid { return }
        // show loader
        startLoading()
        viewModel.callWSVerify(enteredCode: codeView.retreiveCode(), success: { [weak self] in
            // hide loader
            guard let `self` = self else { return }
            self.launchViewModel.updateUserDataAfterLogin(completion: {
                self.stopLoading()
                self.viewModel.handleAuthenticationSuccess()
            })
        }, failure: { [weak self] errorStr in
            self?.stopLoading()
            self?.errorLabel.text = self?.viewModel.errorMessage
            self?.viewModel.showErrorPopuo(subtitle: errorStr)
        })
    }
    
    @IBAction func askAgainForBtnPressed(_ sender: RoundedButton) {
        // show loader
        startLoading()

        viewModel.callWSAuthenticate(success: { [weak self] code in
            // hide loader
            self?.stopLoading()
            self?.viewModel.updateCode(code)
            
            }, failure: { [weak self] errorStr in
                // hide loader
                self?.stopLoading()
                self?.errorLabel.text = errorStr
        })
    }

    private func askAgainForACodeTimer() {
        Timer.scheduledTimer(withTimeInterval: 61, repeats: false) { [weak self](timer) in
            self?.askAgainForCodeBtn.fadeIn()
            timer.invalidate()
        }
    }
    


}
