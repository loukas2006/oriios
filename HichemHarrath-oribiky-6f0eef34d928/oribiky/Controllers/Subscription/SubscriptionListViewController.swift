//
//  SubscriptionListViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 04/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class SubscriptionListViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {
    
    // MARK: - Variables
    var viewModel: SubscriptionListViewModel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var minutesLabel: UILabel!
    @IBOutlet weak var minutesBackView: UIView!
    @IBOutlet weak var minutesFooterView: UIView!
    @IBOutlet weak var minutesImageView: UIImageView!
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        minutesLabel.attributedText = viewModel.calculateMinutesBalance()

    }
    // MARK: - View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // tableView.reloadData()
    }
    // MARK: - Private
    private func setupView() {
        
        //add menu && notification
        addRighNavigationItems()

        title = APPStrings.Subscription.List.title
        
        // configure tabbar item
        let subscriptiontabBarItem = UITabBarItem(title: APPStrings.TabBar.subscriptionTitle, image: Asset.TabBar.subscription.image, selectedImage: Asset.TabBar.subscription.image)
        tabBarItem = subscriptiontabBarItem

        // register Table View
        tableView.dataSource = self
        
        tableView.separatorStyle = .none
        let cellNib = UINib(nibName: String(describing: SubscriptionCategoryTableViewCell.self), bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: String(describing: SubscriptionCategoryTableViewCell.self))
        
        // automatic row height
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50

        descriptionLabel.font = AppStyle.Text.medium(size: 14.0).font
        descriptionLabel.text = APPStrings.Subscription.List.description
        descriptionLabel.textColor = AppStyle.Colors.black
        // add back btn
        if !viewModel.isDisplayedInTabbarItem {
            addBackNavigationItem()
        }
        tableView.reloadData()
        
        minutesBackView.backgroundColor = AppStyle.Colors.lightBlue
        minutesBackView.round(value: 8.0)
        minutesImageView.image = Asset.Subscription.info.image
        minutesLabel.font = AppStyle.Text.regular(size: 13).font
        minutesLabel.textColor = AppStyle.Colors.white
    }
    // MARK: - Actions
    @IBAction func packageMinutesBtnPressed (_ sender : UIButton) {
        viewModel.handlePackageMinutesSelection()
    }

}
extension SubscriptionListViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // return viewModel.dataSource[indexPath.row].items.compactMap({ $0.type.cellHeight}).reduce(0, +) + 80.0
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SubscriptionCategoryTableViewCell.self)) as? SubscriptionCategoryTableViewCell else { return UITableViewCell()}
       
        cell.configure(to: viewModel.dataSource[indexPath.row])
        
        cell.subscriptionSelectionComletionHandler = { [weak self] subscription in
            guard let `self` = self else { return }
            self.viewModel.navigateToDetails(subscription)
        }
        
        return cell
    }
}
