//
//  DocumentCollectionViewCell.swift
//  oribiky
//
//  Created by Hichem Harrath on 04/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class DocumentCollectionViewCell: UICollectionViewCell {

    // MARK: - Variables
    
    @IBOutlet weak var documentImageView: UIImageView!
    @IBOutlet weak var deleteBtn: RoundedButton!
    var deleteBtnAction: ((Int) -> Void)?

    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    // MARK: - Private
    private func setupCell() {
        deleteBtn.backgroundColor = AppStyle.Colors.red
        deleteBtn.setImage(Asset.Profile.iconDelete.image, for: .normal)
        documentImageView.image = Asset.Subscription.documentPlaceholder.image
    }
    
    // MARK: - Public
    static func size() -> CGSize {
        let availableWidth = (UIScreen.main.bounds.width - 50)/2
        let cellWidth = min(157, availableWidth)
        let cellHeight: CGFloat = 175.0
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func configure(image: UIImage?, atIndex index: Int, deleteAction: @escaping (Int) -> Void) {
        deleteBtnAction = deleteAction
        deleteBtn.tag = index
        if let image = image {
            documentImageView.image = image
        } else {
            documentImageView.image = Asset.Subscription.documentPlaceholder.image
        }
    }
    // MARK: - Actions
    @IBAction func deleteBtnPressed(_ sender: RoundedButton) {
        documentImageView.image = Asset.Subscription.documentPlaceholder.image
        deleteBtnAction?(sender.tag)
    }

}
