//
//  NeededDocumentsTableViewCell.swift
//  oribiky
//
//  Created by Hichem Harrath on 04/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class NeededDocumentsTableViewCell: UITableViewCell {

    // MARK: - Variables
    @IBOutlet weak var titleBackView: UIView!

    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    var detailsLabelHeightConstraint : NSLayoutConstraint?
    var expandBtnAction: (() -> Void)?
    
    var isExpanded: Bool = false {
        didSet {
            if isExpanded {
                // true should remove the constraint
                detailsLabelHeightConstraint?.isActive = false
            } else {
                detailsLabelHeightConstraint = detailsLabel.heightAnchor.constraint(equalToConstant: 0)
                detailsLabelHeightConstraint?.isActive = true
            }
            layoutIfNeeded()
        }
    }
    
    // MARK: - Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    // MARK: - Public
    func configure(type: EligibilityType, expandAction: @escaping () -> Void) {
        
        categoryTitleLabel.text = type.title
        detailsLabel.text = type.details
        expandBtnAction = expandAction
    }

    // MARK: - Private
    private func setupCell() {
        selectionStyle = .none
        
        detailsLabelHeightConstraint = detailsLabel.heightAnchor.constraint(equalToConstant: 0)
        detailsLabelHeightConstraint?.isActive = true
        layoutIfNeeded()
        
        categoryTitleLabel.textColor = AppStyle.Colors.white
        categoryTitleLabel.font = AppStyle.Text.medium(size: 14).font
        
        detailsLabel.textColor = AppStyle.Colors.mainGray
        detailsLabel.font = AppStyle.Text.regular(size: 12).font

        titleBackView.backgroundColor = AppStyle.Colors.lightBlue
        titleBackView.round(value: 4)
    }
    
    // MARK: - Actions
    @IBAction func expandBtnPressed(_ sender: UIButton) {
        isExpanded = !isExpanded
        expandBtnAction?()
    }
}
