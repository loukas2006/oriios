//
//  MySubscriptionDetailsTableViewCell.swift
//  oribiky
//
//  Created by Hichem Harrath on 18/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit



class MySubscriptionDetailsTableViewCell: UITableViewCell, ViewModelConfigurable {
    
    
    // MARK: - Variables
    var viewModel: MySubscriptionDetailsViewModel!
    @IBOutlet weak var detailTitleLabel: UILabel!
    @IBOutlet weak var detailValueLabel: UILabel!
    @IBOutlet weak var detailValueBackView: UIView!
    
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        detailTitleLabel.text = viewModel.title
        detailValueLabel.text = viewModel.value
        if let state = viewModel.state {
            detailValueBackView.backgroundColor = state ? AppStyle.Colors.mainGreen : AppStyle.Colors.red
        } else {
            detailValueBackView.backgroundColor = AppStyle.Colors.lightBlue
        }
        
    }

    // MARK: - View Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCell()
    }

    // MARK: - Private
    private func setupCell() {
        selectionStyle = .none
        
        detailTitleLabel.font = AppStyle.Text.medium(size: 13.0).font
        detailTitleLabel.textColor = AppStyle.Colors.black
        
        detailValueLabel.font = AppStyle.Text.regular(size: 13.0).font
        detailValueLabel.textColor = AppStyle.Colors.white
        
        detailValueBackView.round(value: 2)
        
    }

}
