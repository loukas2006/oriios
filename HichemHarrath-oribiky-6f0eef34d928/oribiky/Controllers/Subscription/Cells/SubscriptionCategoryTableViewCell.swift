//
//  SubscriptionCategoryTableViewCell.swift
//  oribiky
//
//  Created by Hichem Harrath on 17/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class SubscriptionCategoryTableViewCell: UITableViewCell, ViewModelConfigurable {

    // MARK: - Variables
    
    var viewModel: SubscriptionCategoryViewModel!

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewTitleLabel: UILabel!
    @IBOutlet weak var headerViewSubtitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableviewHeightConstraint: NSLayoutConstraint!
    
    var subscriptionSelectionComletionHandler: ((SubscriptionViewModel) -> ())?


    // MARK: - Init
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCell()
    }
    
    // MARK: - SubscriptionCategoryViewModel
    func configureWithViewModel() {
        headerViewTitleLabel.text = viewModel.category.title
        headerViewSubtitleLabel.text = "" //viewModel.category.subtitle
        tableviewHeightConstraint.constant = viewModel.items.compactMap({ $0.type.cellHeight}).reduce(0, +)
        
//        let maskLayer = CAShapeLayer()
//        let corners = UIRectCorner.bottomRight.union(UIRectCorner.bottomLeft)
//        let path = UIBezierPath(roundedRect: tableView.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: 8.0, height: 8.0))
//        maskLayer.path = path.cgPath
//        tableView.layer.mask = maskLayer
//        tableView.clipsToBounds = true
//
//        layoutSubviews()
    }
    

    // MARK: - Private
    private func setupCell() {
        backView.applyShadow(color: AppStyle.Colors.mainBlue, radius: 4.0, cornerRadius: 8.0)
        backView.backgroundColor = AppStyle.Colors.mainBlue
        //backView.clipsToBounds = true
        selectionStyle = .none
        
        headerView.backgroundColor = AppStyle.Colors.clear
        headerViewTitleLabel.textColor = AppStyle.Colors.white
        headerViewTitleLabel.font = AppStyle.Text.bold(size: 14.0).font
        
        headerViewSubtitleLabel.textColor = AppStyle.Colors.verLightBlue
        headerViewSubtitleLabel.font = AppStyle.Text.bold(size: 14.0).font
        
        // register table view cell
        tableView.dataSource = self
        tableView.delegate = self
        tableView.round(value: 8.0)
        
        let cellNib = UINib(nibName: String(describing: SubscriptionTableViewCell.self), bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: String(describing: SubscriptionTableViewCell.self))
        
        
        // automatic row height
//        tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.estimatedRowHeight = 50

    }
}

extension SubscriptionCategoryTableViewCell: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.items[indexPath.row].type.cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SubscriptionTableViewCell.self)) as? SubscriptionTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(to: viewModel.items[indexPath.row])
        return cell
    }
    
}


extension SubscriptionCategoryTableViewCell: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        subscriptionSelectionComletionHandler?(viewModel.items[indexPath.row])
    }
}
