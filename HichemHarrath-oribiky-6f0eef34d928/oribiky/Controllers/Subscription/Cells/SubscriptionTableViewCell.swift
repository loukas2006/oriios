//
//  SubscriptionTableViewCell.swift
//  oribiky
//
//  Created by Hichem Harrath on 04/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class SubscriptionTableViewCell: UITableViewCell, ViewModelConfigurable {
    
    // MARK: - Variables
    private let arrowLeadingConstraintDefault: CGFloat = 8.0
    var viewModel: SubscriptionViewModel!
    
    
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var subscriptionTitleLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var arrowLeadingConstraint: NSLayoutConstraint!
    
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        // configure the cell with its view model
        subscriptionTitleLabel.text = viewModel.type.title
        // set title pading
        if viewModel.type.hasPadding {
            arrowLeadingConstraint.constant = 3 * arrowLeadingConstraintDefault
        } else {
            arrowLeadingConstraint.constant = arrowLeadingConstraintDefault
        }
        if viewModel.type.priceHighlighted {
            highlightedPriceView()
        } else {
            normalPriceView()
        }
        
        priceLabel.text = viewModel.type.price
    }
    
    // MARK: - Private
    private func setupCell() {
        selectionStyle = .none

        subscriptionTitleLabel.font = AppStyle.Text.bold(size: 13).font
        subscriptionTitleLabel.textColor = AppStyle.Colors.black
        
        priceLabel.font = AppStyle.Text.boldItalic(size: 11).font
        priceView.round(value: 2)

        arrowImageView.image = Asset.Subscription.redArrow.image
    }

    private func highlightedPriceView() {
        priceView.backgroundColor = AppStyle.Colors.red
        priceLabel.textColor = AppStyle.Colors.white
    }
    private func normalPriceView() {
        priceView.backgroundColor = AppStyle.Colors.white
        priceLabel.textColor = AppStyle.Colors.red
    }
    
}
