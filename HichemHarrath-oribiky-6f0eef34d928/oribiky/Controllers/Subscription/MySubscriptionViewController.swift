//
//  MySubscriptionViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 17/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class MySubscriptionViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {

    // MARK: - Variables
    var viewModel: SubscriptionViewModel!

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var subscriptionTitleLabel: UILabel!
    @IBOutlet weak var cancelSubscriptionImageView: UIImageView!
    @IBOutlet weak var cancelSubscriptionLabel: UILabel!
    @IBOutlet weak var cancelSubscriptionBackView: UIView!
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var descriptionContenTextView: UITextView!
    @IBOutlet weak var descriptionContenTextViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var detailsBackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var uploadView: UIView!
    @IBOutlet weak var uploadViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var uploadFilesBtn: RoundedButton!
    @IBOutlet weak var headerButtonBackView: UIView!
    
    var datasource = [MySubscriptionDetailsViewModel]() {
        didSet {
            tableView.reloadData()
            updateHeaderBtn()
            detailsBackViewHeightConstraint.constant = CGFloat(datasource.count) * 90.0
        }
    }
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        subscriptionTitleLabel.text = viewModel.type.title
        descriptionContenTextView.attributedText = viewModel.type.fullDetails
        /*[self.textView sizeThatFits:CGSizeMake(self.textView.frame.size.width, CGFLOAT_MAX)].height*/
        descriptionContenTextViewHeightConstraint.constant = descriptionContenTextView.sizeThatFits(CGSize(width: descriptionContenTextView.frame.width, height: .greatestFiniteMagnitude)).height
        
        datasource = BikeRentManager.shared.getUserSubscriptionDetails()
        view.layoutIfNeeded()
        
        headerButtonBackView.isHidden = viewModel.type == .delivery

    }
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        updateUploadFileView()
    }
    
    // MARK: - Private
    private func setupView() {
        
        addRighNavigationItems()
        
        title = APPStrings.MySubscription.title

        // configure tabbar item
        let subscriptiontabBarItem = UITabBarItem(title: APPStrings.TabBar.subscriptionTitle, image: Asset.TabBar.subscription.image, selectedImage: Asset.TabBar.subscription.image)
        tabBarItem = subscriptiontabBarItem

        
        headerView.backgroundColor = AppStyle.Colors.lightBlue
        // subscription Title
        subscriptionTitleLabel.textColor = AppStyle.Colors.mainBlue
        // subscription description
        descriptionTitleLabel.text = APPStrings.MySubscription.Description.title
        descriptionTitleLabel.textColor = AppStyle.Colors.black
        descriptionTitleLabel.font = AppStyle.Text.medium(size: 14).font
        
        descriptionContenTextView.textColor = AppStyle.Colors.black
        descriptionContenTextView.font = AppStyle.Text.regular(size: 14).font
        descriptionContenTextView.tintColor = AppStyle.Colors.lightBlue

        // configure table view$
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.dataSource = self
        let cellNib = UINib(nibName: String(describing: MySubscriptionDetailsTableViewCell.self), bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: String(describing: MySubscriptionDetailsTableViewCell.self))
        
    
    }
    private func updateUploadFileView() {
        if viewModel.type == .students {
            // check for elligibility
            if let validated = BikeRentManager.shared.getUser().subscription.details.validated {
                switch validated {
                case 0:
                    uploadViewHeightConstraint.constant = 60.0
                    // wait to upload
                    uploadFilesBtn.isEnabled = true
                    uploadFilesBtn.backgroundColor = AppStyle.Colors.mainBlue
                    uploadFilesBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
                    uploadFilesBtn.titleLabel?.numberOfLines = 0
                    uploadFilesBtn.setTitle(APPStrings.MySubscription.UploadFilesBtn.title.uppercased(), for: .normal)
                    uploadFilesBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
                case 1:
                    uploadViewHeightConstraint.constant = 0.0
                    
                case 2:
                    uploadViewHeightConstraint.constant = 60.0
                    // wait for admin validation
                    uploadFilesBtn.isEnabled = false
                    uploadFilesBtn.backgroundColor = AppStyle.Colors.clear
                    uploadFilesBtn.titleLabel?.font = AppStyle.Text.bold(size: 13.0).font
                    uploadFilesBtn.titleLabel?.numberOfLines = 0
                    uploadFilesBtn.setTitleColor(AppStyle.Colors.mainBlue, for: .normal)
                    uploadFilesBtn.setTitle(APPStrings.MySubscription.UploadFilesState.inProgress.uppercased(), for: .normal)
                default:
                    break
                }
            } else {
                uploadViewHeightConstraint.constant = 0.0
            }
        } else {
            uploadViewHeightConstraint.constant = 0.0
        }
        
        view.layoutIfNeeded()


    }
    
    private func updateHeaderBtn() {
        // cancel / reactivate subscription btn
        if BikeRentManager.shared.getUser().isSubscriptionActive() {
            
            cancelSubscriptionImageView.image = Asset.MySubscription.cancel.image
            cancelSubscriptionBackView.backgroundColor = AppStyle.Colors.mainBlue
            cancelSubscriptionBackView.round()
            cancelSubscriptionLabel.text = APPStrings.MySubscription.CancelBtn.title
            cancelSubscriptionLabel.font = AppStyle.Text.bold(size: 14.0).font
            cancelSubscriptionLabel.textColor = AppStyle.Colors.white
            
        } else {
            
            cancelSubscriptionImageView.image = Asset.MySubscription.reactivate.image
            cancelSubscriptionBackView.backgroundColor = AppStyle.Colors.mainBlue
            cancelSubscriptionBackView.round()
            cancelSubscriptionLabel.text = APPStrings.MySubscription.ReactivateBtn.title
            cancelSubscriptionLabel.font = AppStyle.Text.bold(size: 14.0).font
            cancelSubscriptionLabel.textColor = AppStyle.Colors.white
        }
    }

    // MARK: - Actions
    @IBAction func cancelSubscriptionBtnPressed(_ sender: UIButton) {
        startLoading()
        viewModel.handleCancelSubscriptionAction(success: { [weak self] in
            self?.stopLoading()
            self?.datasource = BikeRentManager.shared.getUserSubscriptionDetails()
        }, failure: { [weak self] (errorStr) in
            self?.stopLoading()
            self?.viewModel.showErrorPopuo(subtitle: errorStr)
        })
    }
    @IBAction func uploadFilesBtnPressed(_ sender: RoundedButton) {
        viewModel.navigateToEligibility()
    }
    
}
extension MySubscriptionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MySubscriptionDetailsTableViewCell.self)) as?  MySubscriptionDetailsTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(to: datasource[indexPath.row])
        
        return cell
    }
}
