//
//  SubscriptionEligibilityViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 04/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class SubscriptionEligibilityViewController: UIViewController, StoryboardIdentifiable, ViewModelConfigurable {

    // MARK: - Navigation
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var takeAPictureImageView: UIImageView!
    @IBOutlet weak var takeAPictureLabel: UILabel!
    @IBOutlet weak var takeAPictureBackView: UIView!
    
    @IBOutlet weak var myPicturesImageView: UIImageView!
    @IBOutlet weak var myPicturesLabel: UILabel!
    @IBOutlet weak var myPicturesBackView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var neededDocumentsTitleLabel: UILabel!
    @IBOutlet weak var validateButton: RoundedButton!
    @IBOutlet weak var separatorView: UIView!
    

    var viewModel: SubscriptionEligibilityViewModel!

    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
    }
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Private
    private func setupView() {
        
        title = APPStrings.Subscription.Eligibility.title
        
        // register collection view cell
        let cellNib = UINib(nibName: String(describing: DocumentCollectionViewCell.self), bundle: Bundle.main)
        collectionView.register(cellNib, forCellWithReuseIdentifier: String(describing: DocumentCollectionViewCell.self))
        collectionView.dataSource = self
        collectionView.delegate = self

        // register tableView
        let tableViewCellNib = UINib(nibName: String(describing: NeededDocumentsTableViewCell.self), bundle: Bundle.main)
        tableView.register(tableViewCellNib, forCellReuseIdentifier: String(describing: NeededDocumentsTableViewCell.self))
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.01))
        // back button
        addBackNavigationItem()
        
        takeAPictureImageView.image = Asset.Subscription.takePicture.image
        takeAPictureLabel.text = APPStrings.Subscription.Eligibility.takeAPictureBtnTitle
        takeAPictureLabel.font = AppStyle.Text.bold(size: 14.0).font
        takeAPictureLabel.textColor = AppStyle.Colors.white
        takeAPictureBackView.backgroundColor = AppStyle.Colors.mainBlue
        takeAPictureBackView.round()
        
        myPicturesImageView.image = Asset.Subscription.myPictures.image
        myPicturesLabel.text = APPStrings.Subscription.Eligibility.myPicturesBtnTitle
        myPicturesLabel.font = AppStyle.Text.bold(size: 14.0).font
        myPicturesLabel.textColor = AppStyle.Colors.white
        myPicturesBackView.backgroundColor = AppStyle.Colors.mainBlue
        myPicturesBackView.round()
        
        headerView.backgroundColor = AppStyle.Colors.lightBlue
        
        neededDocumentsTitleLabel.textColor = AppStyle.Colors.mainBlue
        neededDocumentsTitleLabel.font = AppStyle.Text.bold(size: 16.0).font
        neededDocumentsTitleLabel.text = APPStrings.Subscription.Eligibility.neededDocuments.uppercased()
        
        validateButton.setTitleColor(AppStyle.Colors.white, for: .normal)
        validateButton.titleLabel?.font = AppStyle.Buttons.medium16.font
        validateButton.setTitle(APPStrings.Common.validate, for: .normal)
        validateButton.backgroundColor = AppStyle.Colors.mainBlue
        
        separatorView.backgroundColor = AppStyle.Colors.mainBlue

    }
    
    // MARK: - Actions
    @IBAction func takeAPictureBtnAction(_ sender: UIButton){
        viewModel.takeAPicture { [weak self] in
            self?.collectionView.reloadData()
        }
    }
    @IBAction func chooseFromMyPicturesBtnAction(_ sender: UIButton){
        viewModel.chooseFromMyPictures { [weak self] in
            self?.collectionView.reloadData()
        }
    }
    
    @IBAction func validateBtnAction(_ sender: UIButton) {
        if viewModel.documentsDatasource.count < 2 { return }
        startLoading()
        viewModel.callSubscribeWS(success: { [weak self] in
            // subcription succeeded
            self?.stopLoading()
            self?.viewModel.handleSubscriptionSuccess()
        }, failure:  { [weak self] errorStr in
            // show error
            self?.stopLoading()
            print(errorStr)
        })
    }
}

// MARK: - UICollectionViewDataSource
extension SubscriptionEligibilityViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2// we have only tow documents
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DocumentCollectionViewCell.self), for: indexPath) as? DocumentCollectionViewCell else {
            return  UICollectionViewCell()
        }
        var image: UIImage?
        if indexPath.row < viewModel.documentsDatasource.count {
            image = viewModel.documentsDatasource[indexPath.row]
        }
        cell.configure(image: image, atIndex: indexPath.row) { [weak self] (index) in
            self?.viewModel.deleteImageAtIndex(index)
            self?.collectionView.reloadData()
        }
        return cell
    }
}
// MARK: - UICollectionViewDelegateFlowLayout
extension SubscriptionEligibilityViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return DocumentCollectionViewCell.size()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = DocumentCollectionViewCell.size().width * 2
        let totalSpacingWidth: CGFloat = 10.0
        
        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
   
}

extension SubscriptionEligibilityViewController: UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.neededDocumentsCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NeededDocumentsTableViewCell.self)) as? NeededDocumentsTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(type: viewModel.neededDocumentsCategories[indexPath.row]) {
            tableView.reloadData()
        }
        return cell
     }
}
