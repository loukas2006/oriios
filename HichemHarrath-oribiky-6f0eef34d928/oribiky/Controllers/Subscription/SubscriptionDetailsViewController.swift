//
//  SubscriptionDetailsViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 04/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class SubscriptionDetailsViewController: UIViewController, ViewModelConfigurable, StoryboardIdentifiable {
    
  
    // MARK: - Variables
    var viewModel: SubscriptionViewModel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var neededDocumentsTitleLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var subscriptionTitleLabel: UILabel!
    @IBOutlet weak var subscriptionDetailsLabel: UILabel!
    @IBOutlet weak var subscribeBtn: RoundedButton!
    @IBOutlet weak var headerView: UIView!
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        subscriptionTitleLabel.text = viewModel.type.title
        subscriptionDetailsLabel.attributedText = viewModel.type.fullDetails
        tableViewBackViewHeightConstraint.constant = (viewModel.type  == .students) ? (self.tableView.contentSize.height + 80) : 0

    }
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    // MARK: - Private
    private func setupView() {
        
        title = APPStrings.Subscription.List.title
        headerView.backgroundColor = AppStyle.Colors.lightBlue
        subscriptionTitleLabel.font = AppStyle.Text.bold(size: 18).font
        subscriptionTitleLabel.textColor = AppStyle.Colors.white
        
        subscriptionDetailsLabel.font = AppStyle.Text.regular(size: 15).font
        subscriptionDetailsLabel.textColor = AppStyle.Colors.black
        
        subscribeBtn.setTitle(APPStrings.Subscription.Details.subscribeBtnTitle.uppercased(), for: .normal)
        subscribeBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        subscribeBtn.backgroundColor = AppStyle.Colors.mainBlue
        subscribeBtn.setTitleColor(AppStyle.Colors.white, for: .normal)

        // add back btn
        addBackNavigationItem()
        
        separatorView.backgroundColor = AppStyle.Colors.mainBlue
        neededDocumentsTitleLabel.textColor = AppStyle.Colors.mainBlue
        neededDocumentsTitleLabel.font = AppStyle.Text.bold(size: 16.0).font
        neededDocumentsTitleLabel.text = APPStrings.Subscription.Eligibility.neededDocuments.uppercased()
        
        // register tableView
        let tableViewCellNib = UINib(nibName: String(describing: NeededDocumentsTableViewCell.self), bundle: Bundle.main)
        tableView.register(tableViewCellNib, forCellReuseIdentifier: String(describing: NeededDocumentsTableViewCell.self))
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.01))
        tableView.isScrollEnabled = false


    }
    
    // MARK: - Actions
    @IBAction func subscribBtnPressed(_ sender: RoundedButton) {
        
        if BikeRentManager.shared.getUser().haveCard() {
            
            viewModel.showValidationPopup(subscription: viewModel) { [weak self ] in
                guard let `self` = self else { return }
                    self.startLoading()
                    self.viewModel.callWS(success: {
                        self.stopLoading()
                        self.viewModel.handleSubscriptionSuccess()
                        print("subcribed with success")
                    }, failure: { (errorStr) in
                        self.stopLoading()
                        self.viewModel.showErrorPopuo(subtitle: errorStr)
                    })
            }
        } else {
            //add credit card
            viewModel.showScanCardPopup(user: BikeRentManager.shared.getUser())
        }
    }

}
extension SubscriptionDetailsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.neededDocumentsCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NeededDocumentsTableViewCell.self)) as? NeededDocumentsTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(type: viewModel.neededDocumentsCategories[indexPath.row]) {[weak self] in
            guard let `self` = self else {return}
            tableView.reloadData()
            self.contentView.layoutIfNeeded()
            self.tableViewBackViewHeightConstraint.constant = self.tableView.contentSize.height 
        }
        return cell
    }
}
