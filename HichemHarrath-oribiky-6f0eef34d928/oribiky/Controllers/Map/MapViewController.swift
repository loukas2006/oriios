//
//  MapViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit
import MapKit


class MapViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {
    
    private let defaultRegionRadius: CLLocationDistance = 100
    //MARK: - Variables
    var viewModel: MapViewModel!
    private var bikeArray = [Bike]() {
        didSet {
            addBikesAnnotations()
        }
    }
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var centerOnUserLocationBtn: UIButton!
    @IBOutlet weak var noLocationView: UIView!
    @IBOutlet weak var noLocationBgImageVIew: UIImageView!
    @IBOutlet weak var noLocationImageView: UIImageView!
    @IBOutlet weak var noLocationHeaderView: UIView!
    @IBOutlet weak var noLocationHeaderTitleLabel: UILabel!
    @IBOutlet weak var noLocationHeaderSubtitleLabel: UILabel!
    @IBOutlet weak var noLocationBtn: RoundedButton!
    @IBOutlet weak var searchView: SearchBikeView!

    private var userLocation: CLLocation? {
        didSet {
            guard let location = userLocation else { return }
            //Zoom to user location
            centerMapOnLocation(location: location)
            centerOnUserLocationBtn.isHidden = false

        }
    }
    private var centreLocation: CLLocation? {
        willSet (newValue) {
        guard let centreLocation = centreLocation else {
            return
        }
            if newValue?.distance(from: centreLocation) ?? 0 > CLLocationDistance(100)  {
                getNearbyBikes(newValue)
            }
        }
    }
    
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkLocationAuthorization), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        // search view call
        //
        searchView.searchTextValidated = { [weak self] searchText in
            guard let `self` = self else { return }
            
            self.viewModel.callWS(code: searchText, success: { [weak self] (bikes) in
                guard let `self` = self else { return }

                self.searchView.activityIndicator.stopAnimating()
                if let bike = bikes.first, bikes.count == 1 {
                    self.viewModel.rentNowAction(bike)
                } else {
                    self.searchView.bikeIdTextField.text = ""
                    self.searchView.validateBtn.isEnabled = false
                    self.viewModel.handleBikeNotFound(text: searchText)
                }
                
            }, failure: { [weak self]  (errorStr) in
                self?.searchView.activityIndicator.stopAnimating()
                print(errorStr)
            })

        }
        searchView.searchTextChanged = { searchText in
            print(searchText)
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorization()
    }

    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // to prevent calling bike ws from other views
        viewModel.stopUpdatingUserLocation()
    }
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {

//        // check if we have the user location
      //  guard let location = userLocation else { return }
        //getNearbyBikes(location)
    }
    
    // MARK: - Private
    
    @objc private func checkLocationAuthorization () {
        
        viewModel.askEnablingLocationIfNeeded {[weak self] (location: CLLocation?) in
            guard let `self` = self else {return}
            
            if let userLoc = location {
                self.userLocation = userLoc
                self.getNearbyBikes(self.userLocation)
                self.noLocationView.isHidden = true
                self.navigationItem.title = APPStrings.Map.title
            } else {
                self.noLocationView.isHidden = false
                self.navigationItem.title = nil
            }
        }

    }
    private func setupView() {
        
        //add menu && notification
        addRighNavigationItems()

        
        // set title
        navigationItem.title = APPStrings.Map.title
        // configure tabbar item
        tabBarItem = UITabBarItem(title: APPStrings.TabBar.mapTitle,
                                  image: Asset.TabBar.map.image,
                                  selectedImage: Asset.TabBar.map.image)
        // map
        
        noLocationBgImageVIew.image = Asset.Demo.backgroundStep1.image
        noLocationImageView.image = Asset.Demo.step1.image
        noLocationView.isHidden = true
        
        centerOnUserLocationBtn.setImage(Asset.Map.centerLocation.image, for: .normal)
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        
        noLocationHeaderView.backgroundColor = AppStyle.Colors.mainBlue
        
        noLocationBtn.setTitle(APPStrings.ActivateBluetooth.autoriseBtnTitle.uppercased(), for: .normal)
        noLocationBtn.backgroundColor = AppStyle.Colors.mainBlue
        noLocationBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        noLocationBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        
        noLocationHeaderTitleLabel.font = AppStyle.Text.bold(size: 22).font
        noLocationHeaderTitleLabel.textColor = AppStyle.Colors.white
        noLocationHeaderTitleLabel.text  = APPStrings.Map.NoLocationView.title.uppercased()
        
        noLocationHeaderSubtitleLabel.font = AppStyle.Text.medium(size: 18).font
        noLocationHeaderSubtitleLabel.textColor = AppStyle.Colors.white
        noLocationHeaderSubtitleLabel.text  = APPStrings.Map.NoLocationView.subtitle


    }
    
    
    
    private func addBikesAnnotations() {
        // remove all annotations
        let newBikesCoordinates = bikeArray.compactMap { CLLocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude)}
        
        guard let mapView = mapView else {return}
        // newBikesCoordinates remove all pins thar are already in map
        for pin in mapView.annotations {
            if pin is MKUserLocation {
                continue
            }
            let pinLocation = CLLocation(latitude: pin.coordinate.latitude, longitude: pin.coordinate.longitude)
            var doesPinExists = false
            
            for bikeCoordinate in newBikesCoordinates {
                let bikeLocation = CLLocation(latitude: bikeCoordinate.latitude,
                                              longitude: bikeCoordinate.longitude)

                if bikeLocation.distance(from: pinLocation) == 0 {
                    doesPinExists = true
                    break
                }
            }
            
            if !doesPinExists {
                mapView.removeAnnotation(pin)
            }
            
        }

        // find Only new bikes to be added to map
        var annotationsToAdd = [Bike]()
        
        bikeArray.forEach { (bike) in
            
            let bikeLocation = CLLocation(latitude: bike.latitude, longitude: bike.longitude)
            var doesBikeExistsOnMap = false
            for pin in mapView.annotations {
                if pin is MKUserLocation {
                    continue
                }
                if bikeLocation.distance(from: CLLocation(latitude: pin.coordinate.latitude, longitude: pin.coordinate.longitude)) == 0 {
                    doesBikeExistsOnMap = true
                }
            }
            if !doesBikeExistsOnMap {
                annotationsToAdd.append(bike)
            }
        }
        

        // add only the new annotaions
        annotationsToAdd.forEach { bike in
            let annotation = BikeAnnotation(bike)
            mapView.addAnnotation(annotation)
        }
    }
    
    private func getNearbyBikes(_ location: CLLocation?) {
        
        guard let location = location else { return }
        // 1 - show loader
        startLoading()
        viewModel.callws(userLocation: "\(location.coordinate.latitude),\(location.coordinate.longitude) ", success: { [weak self] (bikes) in
            // 2 - hide loader
            self?.stopLoading()
            guard let strongSelf = self else { return }
            strongSelf.bikeArray = bikes
            //  print(strongSelf.bikeArray)
            }, failure:{ [weak self] errorStr in
                self?.stopLoading()
                // 2 - hide loader
                self?.viewModel.showErrorPopuo(subtitle: errorStr)
        })
    }
    
    func centerMapOnLocation(location: CLLocation) {
        
        let regionRadius: CLLocationDistance = defaultRegionRadius
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView?.setRegion(coordinateRegion, animated: true)
    }

    
    //MARK: - Actions
    
    @IBAction func centerOnUserLocationBtnPressed(_ sender: UIButton) {
        guard let location = userLocation else {
            return
        }
        centerMapOnLocation(location: location)
    }
    @IBAction func autoriseLocationBtnPressed(_ sender: RoundedButton) {
        viewModel.askEnablingLocationIfNeeded()
    }
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if let annotation = view.annotation as? BikeAnnotation{
            viewModel.handleBikeSelection(annotation.bike)
        } else if let clusterAnnotation = view.annotation as? MKClusterAnnotation {
            viewModel.handleBikeClusterSelection(bikes: clusterAnnotation.memberAnnotations.compactMap({($0 as? BikeAnnotation)?.bike}))
        }
        mapView.deselectAnnotation(view.annotation, animated: true)

    }
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        centreLocation = CLLocation(latitude: mapView.region.center.latitude,
                                    longitude:mapView.region.center.longitude)
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        switch annotation {
        case let bikeAnnotation as BikeAnnotation:
            
            var annotationView: BikeAnnotationView
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: String(describing: BikeAnnotationView.self)) as? BikeAnnotationView {
                dequeuedView.annotation = bikeAnnotation
                annotationView = dequeuedView
            } else {
                annotationView = BikeAnnotationView(annotation: annotation, reuseIdentifier: String(describing: BikeAnnotationView.self))
            }
            
            annotationView.clusteringIdentifier = bikeAnnotation.type.rawValue
            
            return annotationView

        case let clusterAnnotation as MKClusterAnnotation:
            
            var clusterAnnotationView: MKMarkerAnnotationView

            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: String(describing:MKMarkerAnnotationView.self)) as? MKMarkerAnnotationView {
                dequeuedView.annotation = clusterAnnotation
                clusterAnnotationView = dequeuedView
            } else {
                clusterAnnotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: String(describing: MKMarkerAnnotationView.self))
            }

            clusterAnnotationView.displayPriority = .required
            clusterAnnotationView.markerTintColor = AppStyle.Colors.mainBlue
            clusterAnnotationView.annotation = clusterAnnotation
            
            return clusterAnnotationView
            
        default:
            return nil
        }
        
    }
    
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        if let userAnnotation = mapView.view(for: mapView.userLocation) {
            userAnnotation.isEnabled = false
        }
    }
    
    
    func mapView(_ mapView: MKMapView, clusterAnnotationForMemberAnnotations memberAnnotations: [MKAnnotation]) -> MKClusterAnnotation {
        let cluster = MKClusterAnnotation(memberAnnotations: memberAnnotations)
        cluster.title = nil
        cluster.subtitle = nil
        
        return cluster
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

}

extension MapViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
}

