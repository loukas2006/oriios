//
//  ActivateBluetoothViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 09/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class ActivateBluetoothViewController: UIViewController, ViewModelConfigurable, StoryboardIdentifiable {
    
    // MARK: - Variables
    var viewModel: ActivateBluetoothViewModel!

    @IBOutlet weak var bluetoothImageView: UIImageView!
    @IBOutlet weak var validateBtn: RoundedButton!
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
    }
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
   
    
    
    // MARK: - Private
    private func setupView() {
        title = APPStrings.ActivateBluetooth.title
        
        validateBtn.setTitle(APPStrings.ActivateBluetooth.autoriseBtnTitle.uppercased(), for: .normal)
        validateBtn.backgroundColor = AppStyle.Colors.mainBlue
        validateBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        validateBtn.titleLabel?.font = AppStyle.Buttons.medium12.font

        addBackNavigationItem()
    }
    
    // MARK: - Actions
    @IBAction func validateBtnPressed(_ sender: RoundedButton) {
        viewModel.showActivationPopup()
    }
}
