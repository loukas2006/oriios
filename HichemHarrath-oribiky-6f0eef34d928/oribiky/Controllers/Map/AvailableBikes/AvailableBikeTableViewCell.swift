//
//  AvailableBikeTableViewCell.swift
//  oribiky
//
//  Created by Hichem Harrath on 12/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class AvailableBikeTableViewCell: UITableViewCell, ViewModelConfigurable {
    
    

    // MARK: - Variables
    var viewModel: AvailableBikeViewModel!

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bikeImageView: UIImageView!
    @IBOutlet weak var bikeIdentifierLabel: UILabel!
    
    func configureWithViewModel() {
        
        let title = APPStrings.Map.RentBikePopup.title(viewModel.bike.code, "\(viewModel.bike.distance)")
        let attributedTitle = title.highlightColor(highlightedTexts: [viewModel.bike.code, "\(viewModel.bike.distance)"], font: AppStyle.Text.bold(size: 12.0).font, color: AppStyle.Colors.red, lineSpace: 2, alignment: .left)
        
        bikeIdentifierLabel.attributedText = attributedTitle
    }

  
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    // MARK: - Private
    private func setupCell() {
        
        selectionStyle = .none
        
        bgView.backgroundColor = AppStyle.Colors.verLightBlue
        bikeImageView.image = Asset.Map.availableBike.image
        bikeIdentifierLabel.font = AppStyle.Text.medium(size: 12.0).font
    }
    
}
