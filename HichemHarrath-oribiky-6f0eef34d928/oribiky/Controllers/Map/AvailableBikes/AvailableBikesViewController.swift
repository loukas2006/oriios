//
//  AvailableBikesViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 13/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class AvailableBikesViewController: UIViewController, StoryboardIdentifiable {

    // MARK: - Variables

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - ViewModelConfigurable

    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Private
    private func setupView() {
        title = APPStrings.AvailableBikes.title
        // add back btn
        addBackNavigationItem()

        // register tableview cell
        let cellNib = UINib(nibName: String(describing: AvailableBikeTableViewCell.self), bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: String(describing: AvailableBikeTableViewCell.self))
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        tableView.separatorStyle = .none
        // description label
        descriptionLabel.textColor = AppStyle.Colors.mainBlue
        descriptionLabel.text = APPStrings.AvailableBikes.description
        descriptionLabel.font = AppStyle.Text.medium(size: 16.0).font
    }
    
}
// MARK: - UITableViewDataSource
extension AvailableBikesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AvailableBikeTableViewCell.self)) else { return UITableViewCell()}
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension AvailableBikesViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
