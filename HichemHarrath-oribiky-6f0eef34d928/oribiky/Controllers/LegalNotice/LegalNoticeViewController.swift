//
//  LegalNoticeViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 08/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved
//

import UIKit

class LegalNoticeViewController: BaseViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    // MARK: - Variables

    var viewModel: LegalNoticeViewModel!
    
    @IBOutlet weak var termsBtn: UIButton!
    @IBOutlet weak var privacyPolicy: UIButton!
    @IBOutlet weak var termsTapIndicatorImageView: UIImageView!
    @IBOutlet weak var privacyPolicyTapIndicatorImageView: UIImageView!
    @IBOutlet weak var termsView: UIView!
    @IBOutlet weak var privacyPolicyView: UIView!

    
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView ()
    }
    
    private func setupView () {
        
        //add menu && notification
        addRighNavigationItems()

        addBackNavigationItem()
        
        title = APPStrings.HamburgerMenu.legalNotice
        
        termsBtn.titleLabel?.font = AppStyle.Buttons.medium16.font
        termsBtn.setTitle(APPStrings.LegalNotice.termsBtnTitle, for: .normal)
        termsBtn.setTitleColor(AppStyle.Colors.mainBlue, for: .normal)
        termsBtn.setTitleColor(AppStyle.Colors.lightBlue, for: .highlighted)
        termsBtn.titleLabel?.textAlignment = .left
        termsBtn.titleLabel?.numberOfLines = 0
        
        privacyPolicy.titleLabel?.font = AppStyle.Buttons.medium16.font
        privacyPolicy.setTitle(APPStrings.LegalNotice.privacyPolicyBtnTitle, for: .normal)
        privacyPolicy.setTitleColor(AppStyle.Colors.mainBlue, for: .normal)
        privacyPolicy.setTitleColor(AppStyle.Colors.lightBlue, for: .highlighted)
        privacyPolicy.titleLabel?.textAlignment = .left
        privacyPolicy.titleLabel?.numberOfLines = 0

        termsTapIndicatorImageView.image = Asset.Subscription.redArrow.image
        privacyPolicyTapIndicatorImageView.image = Asset.Subscription.redArrow.image
        
        
        termsView.applyShadow(cornerRadius: 5)
        privacyPolicyView.applyShadow(cornerRadius: 5)

    }

    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
    }
    @IBAction func sectionBtnPressed(_ sender: UIButton) {
        if sender == termsBtn {
            viewModel.navigateToTermsOfUse(pdf: PDF.termsOfUseAndSale)
            
        }else {
            viewModel.navigateToTermsOfUse(pdf: PDF.privacyPolicy)

        }
    }
    
}
