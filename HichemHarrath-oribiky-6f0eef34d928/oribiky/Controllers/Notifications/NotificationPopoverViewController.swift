//
//  PopoverViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 27/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class NotificationPopoverViewController: UIViewController, StoryboardIdentifiable, ViewModelConfigurable {

    // MARK: - Variables
    var viewModel: NotificationsPopoverViewModel!
    var items = [NotificationViewModel]() {
        didSet {
            tableView.reloadData()
        }
    }

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var popoverTitleLabel: UILabel!
    @IBOutlet weak var seeAllBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!

    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let ids = items.filter {  $0.seen == false }.compactMap { String($0.identifier) }
        if ids.count == 0 { return }
        viewModel.callSetReadNotificationsWS(newIds:ids) {
            BikeRentManager.shared.updateNewNotifications(notifications: [])
        }

    }
    // MARK: ViewModelConfigurable
    func configureWithViewModel() {
        items = BikeRentManager.shared.getNewNotificationsList()
    }

    // MARK: - Private
    private func setupView() {
        navigationController?.popoverPresentationController?.backgroundColor = AppStyle.Colors.red
        headerView.backgroundColor = AppStyle.Colors.red
        
        popoverTitleLabel.textColor = AppStyle.Colors.white
        popoverTitleLabel.font = AppStyle.Text.medium(size: 12.0).font
        popoverTitleLabel.text = "Notifications".uppercased()
        
        seeAllBtn.setTitleColor(AppStyle.Colors.black, for: .normal)
        seeAllBtn.titleLabel?.font = AppStyle.Text.medium(size: 12.0).font
        
        // register table view
        let cellNib = UINib(nibName: String(describing: NotificationPopoverTableViewCell.self), bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: String(describing: NotificationPopoverTableViewCell.self))
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorInset  = .zero
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        
        seeAllBtn.setAttributedTitle("Notifications anterieures".uppercased().makeUnderlined(fontSize: 10.0), for: .normal)
        
    }
    // MARK: - Private
    @IBAction func seeAllBtnPressed(_ sender: UIButton) {
        viewModel.navigateToNotificationList(controller: self)

    }
    
}
extension NotificationPopoverViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NotificationPopoverTableViewCell.self)) as? NotificationPopoverTableViewCell else {
            return UITableViewCell()
        }
       cell.configure(to:items[indexPath.row])
//        cell.notificationTitleLabel.text = "Réservation expiré"
//        cell.notificationDescriptionLabel.text = "Votre réservation a expirée sans être annuler.\nDix euros de frais ont été prélevé sur votre compte. Pensez à annuler votre reservation si vous ne souhaitez plus ..."
        return cell
    }
}

extension NotificationPopoverViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.navigateToNotificationList(controller: self)
    }
}
