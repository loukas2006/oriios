//
//  NotificationPopoverTableViewCell.swift
//  oribiky
//
//  Created by Hichem Harrath on 01/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class NotificationPopoverTableViewCell: UITableViewCell, ViewModelConfigurable {
    
    
    // MARK: - Variables
    var viewModel: NotificationViewModel!
    @IBOutlet weak var notificationTitleLabel: UILabel!
    @IBOutlet weak var notificationDescriptionLabel: UILabel!

    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCell()
    }
    
    // MARK: - Private
    func configureWithViewModel() {
        notificationTitleLabel.attributedText = viewModel.title
        let updatedBody = NSMutableAttributedString(attributedString: viewModel.body)
        updatedBody.addAttributes([NSAttributedString.Key.font :AppStyle.Text.regular(size: 11.0).font], range: NSRange(location: 0, length: viewModel.body.length))
        notificationDescriptionLabel.attributedText = updatedBody
    }
    
    // MARK: - Private
    private func setupCell() {
        selectionStyle = .none
        
        notificationTitleLabel.font = AppStyle.Text.bold(size: 12.0).font
        notificationTitleLabel.textColor = AppStyle.Colors.mainBlue
        
        notificationDescriptionLabel.font = AppStyle.Text.regular(size: 11.0).font
        notificationDescriptionLabel.textColor = AppStyle.Colors.black
    }
}
