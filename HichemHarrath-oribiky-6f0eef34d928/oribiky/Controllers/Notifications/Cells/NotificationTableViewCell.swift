//
//  NotificationTableViewCell.swift
//  oribiky
//
//  Created by Hichem Harrath on 01/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell, ViewModelConfigurable {
    
    
    // MARK: - Variables
    var viewModel: NotificationViewModel!
    
    @IBOutlet weak var notificationTitleLabel: UILabel!
    @IBOutlet weak var notificationDateLabel: UILabel!
    @IBOutlet weak var notificationDescriptionLabel: UILabel!
    @IBOutlet weak var contentBackView: UIView!

    private var iSeen: Bool = false {
        didSet {
            if iSeen {
                UIView.animate(withDuration: 0.1) {
                    self.contentBackView.backgroundColor = AppStyle.Colors.lightGray
                    self.notificationDateLabel.textColor = AppStyle.Colors.mainBlue

                }
            } else {
                UIView.animate(withDuration: 0.1) {
                    self.contentBackView.backgroundColor = AppStyle.Colors.white
                    self.notificationDateLabel.textColor = AppStyle.Colors.red

                }
            }
        }
    }
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCell()
    }

    // MARK: - Private
    func configureWithViewModel() {
        notificationTitleLabel.attributedText = viewModel.title
        notificationDescriptionLabel.attributedText = viewModel.body
        iSeen = viewModel.seen
        notificationDateLabel.text = viewModel.createAtString
    }
    
    // MARK: - Private
    private func setupCell() {
        selectionStyle = .none
        
        notificationTitleLabel.font = AppStyle.Text.bold(size: 14.0).font
        notificationTitleLabel.textColor = AppStyle.Colors.mainBlue
        
        notificationDateLabel.font = AppStyle.Text.medium(size: 12.0).font
        
        notificationDescriptionLabel.font = AppStyle.Text.regular(size: 12.0).font
        notificationDescriptionLabel.textColor = AppStyle.Colors.black
        
        contentBackView.round(value: 8)
    }


}
