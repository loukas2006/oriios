//
//  NotificationListViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 01/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class NotificationListViewController: UIViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    // MARK: - Variables
    private var items = [NotificationViewModel]() {
        didSet {
            emptyView.isHidden = items.count != 0
            tableView.reloadData()
            updateReadNotifications()
            
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    var viewModel: NotificationListViewModel!
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
    }
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startLoading()
        viewModel.callGetNotificationsWS(success: { [weak self] (notifications) in
             self?.stopLoading()
             self?.items = notifications
        }, failure: { [weak self] (errorStr) in
            self?.stopLoading()
            self?.viewModel.showErrorPopuo(subtitle: errorStr)
        })
    }
    // MARK: - Private
    
    
    private func setupView () {
        
        title = APPStrings.Notifications.title
        addBackNavigationItem()
        // register table view cell
        let cellNib = UINib(nibName: String(describing: NotificationTableViewCell.self), bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: String(describing: NotificationTableViewCell.self))
        
        
        emptyLabel.textColor = AppStyle.Colors.mainGray
        emptyLabel.font = AppStyle.Text.regular(size: 15).font
        emptyLabel.text = APPStrings.Notifications.emptyDescription
        emptyView.isHidden = true
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func updateReadNotifications() {
        let ids = items.filter {  $0.seen == false }.compactMap { String($0.identifier) }
        if ids.count == 0 { return }
        viewModel.callSetReadNotificationsWS(newIds: ids) {[weak self] in
            guard let `self` = self else { return }
            
            self.viewModel.callGetNotificationsWS(success: {  (notifications) in
                self.items = notifications
                BikeRentManager.shared.updateNewNotifications(notifications: [NotificationViewModel]())
            }, failure: {(errorStr) in
               // self.viewModel.showErrorPopuo(subtitle: errorStr)
            })
        }
    }
}

extension NotificationListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NotificationTableViewCell.self)) as? NotificationTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(to: items[indexPath.row])

        return cell
    }
}

extension NotificationListViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.handleNotificationSelectionAction(viewModel: items[indexPath.row])
    }
}
