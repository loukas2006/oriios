//
//  UnLockSmartLockViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 14/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit
import LinkaAPIKit

class UnLockSmartLockViewController: UIViewController, ViewModelConfigurable, StoryboardIdentifiable {
    
    @IBOutlet weak var doneBtn: RoundedButton!
    // MARK: - Variables
    var viewModel: UnlockSmartLockViewModel!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var noticeLabel: UILabel!
    
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        super.viewWillAppear(animated)
        doneBtn.backgroundColor = AppStyle.Colors.mainBlue
        doneBtn.setTitle(APPStrings.UnlockSmartLock
            .DoneBtn.title.uppercased(), for: .normal)
        doneBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        doneBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        
        noticeLabel.text = "Assurez-vous que la barre de fermeture du cadenas ne touche pas les rayons de la roue arrière"
    }
    
    @IBAction func btnUnlockPressed(_ sender: Any) {
        
        startLoading()
        
        viewModel.unlockdelegate = self
        viewModel.callCreateRideWs(success: {[weak self] (ride) in
            
            guard let `self` = self else { return }
            self.stopLoading()
            
            self.unlockHelper()
            
            }, failure: {[weak self] (errorStr) in
                self?.stopLoading()
                self?.viewModel.showErrorPopuo(subtitle: errorStr)
        })
    }
    
    private func setupView()  {
        
        // back button
        addBackNavigationItem()
        //tile
        title = APPStrings.UnlockSmartlock.UnlockInProgress.title
        //bike image
        imageView.image = Asset.Map.startDiagnoticBike.image
        
    }

    
    
    
    
    private func unlockHelper () {
        
        self.startLoading(message: APPStrings.LinkaStatus.Unlock.inProgress.uppercased())

        self.viewModel.unlockSmartLock(completion: { (message, state) in
            self.stopLoading()
            //timeout error
            if state == .unknown {
   
                return
            } else if state == .lostConnection {
                // added to handle lost connection while unlocking, go to current ride, ride is created
                self.viewModel.navigateToCurrentRideViewController()
                return
            }

            self.viewModel.navigateToCurrentRideViewController()
        })
        
    }
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
    }
    
    
    func finishRide (address: String) {
        
        self.viewModel.callFinishRideWs(address: address, success: { (rideResponse) in
            
            self.stopLoading()
            self.viewModel.navigateToMapViewController()
            
        }, failure: { (errorStr) in
            self.stopLoading()
            self.viewModel.showErrorPopuo(subtitle: errorStr)
        })
    }
}


extension UnLockSmartLockViewController:LockConnectionServiceDelegate{
    func onPairingUp() {
        
    }
    
    func onPairingSuccess() {
        
    }
    
    func onScanFound() {
        
    }
    
    func onConnected() {
        
    }
    
    func onBatteryPercent(batteryPercent: Int) {
        
    }
    
    func onUnlockStarted() {
        
    }
    
    func onLockStarted() {
        
    }
    
    func onLock() {
                
        self.viewModel.getEndAddress(completion: { [weak self] (address) in
            
            self?.finishRide(address: address)
            
        })
        
        
        
    }
    
    
    
    
    func onUnlock() {
        self.stopLoading()
        self.viewModel.navigateToCurrentRideViewController()
    }
    
    func errorInternetOff() {
        
    }
    
    func errorBluetoothOff() {
        
    }
    
    func errorAppNotInForeground() {
        
    }
    
    func errorInvalidAccessToken() {
        
    }
    
    func errorMacAddress(errorMsg: String) {
        
    }
    
    func errorConnectionTimeout() {
        
    }
    
    func errorScanningTimeout() {
        
    }
    
    func errorLockMoving(tryAgainCompletion: @escaping (Bool) -> Void) {
        
    }
    
    func errorLockStall(tryAgainCompletion: @escaping (Bool) -> Void) {
        
    }
    
    func errorLockJam() {
        
    }
    
    func errorUnexpectedDisconnect() {
        
    }
    
    func errorLockingTimeout(tryAgainCompletion: @escaping (Bool) -> Void) {
        
    }
    
    func errorUnlockingTimeout(tryAgainCompletion: @escaping (Bool) -> Void) {
        self.stopLoading()

        viewModel.showUnlockingTimeOutPopup(okHandler: {
            tryAgainCompletion(true)
            self.startLoading()
        },noHandler:{
            tryAgainCompletion(false)
            let linkaLockService:LockConnectionService =   LockConnectionService.sharedInstance
            linkaLockService.doLock(macAddress: LinkaManager.shared.getMacAdress(), delegate: self)
            self.startLoading()
        })

    }
}
