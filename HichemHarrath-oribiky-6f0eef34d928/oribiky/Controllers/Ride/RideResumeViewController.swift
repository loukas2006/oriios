//
//  RideResumeViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 20/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class RideResumeViewController: BaseViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    //MARK: - Variables
    var viewModel: RideResumeViewModel!
    
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var distanceImageView: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!

    @IBOutlet weak var co2View: UIView!
    @IBOutlet weak var co2ImageView: UIImageView!
    @IBOutlet weak var co2Label: UILabel!

    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!

    @IBOutlet weak var startAddressImageView: UIImageView!
    @IBOutlet weak var startAddressLabel: UILabel!

    @IBOutlet weak var endAddressImageView: UIImageView!
    @IBOutlet weak var endAddressLabel: UILabel!

    @IBOutlet weak var rideRouteImageView: UIImageView!
    
    //MARK: - View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        viewModel.createSnapshot(rect: rideRouteImageView.bounds) {[weak self](image) in
            if let image = image {
                self?.rideRouteImageView.image = image
            }
        }

    }
    //MARK: - Private
    private func setupView () {
        
        //add menu && notification
        addRighNavigationItems()

        title = APPStrings.RideResume.title
        
        addBackNavigationItem()
        
        
        startAddressLabel.font = AppStyle.Text.regular(size: 13).font
        endAddressLabel.font = AppStyle.Text.regular(size: 13).font
        
        startAddressLabel.textColor = AppStyle.Colors.mainGray
        endAddressLabel.textColor = AppStyle.Colors.mainGray

        distanceView.backgroundColor = AppStyle.Colors.mainBlue
        timeView.backgroundColor = AppStyle.Colors.mainBlue
        co2View.backgroundColor = AppStyle.Colors.mainBlue
        priceView.backgroundColor = AppStyle.Colors.red
        
        distanceLabel.textColor = AppStyle.Colors.white
        timeLabel.textColor = AppStyle.Colors.white
        co2Label.textColor = AppStyle.Colors.white
        priceLabel.textColor = AppStyle.Colors.white

        distanceLabel.font = AppStyle.Text.bold(size: 12).font
        timeLabel.font = AppStyle.Text.bold(size: 12).font
        co2Label.font = AppStyle.Text.bold(size: 12).font
        priceLabel.font = AppStyle.Text.bold(size: 12).font

        distanceImageView.image = Asset.Ride.distance.image
        timeImageView.image = Asset.Ride.time.image
        co2ImageView.image = Asset.Ride.co2.image
        priceImageView.image = Asset.Ride.price.image
        
//        distanceLabel.text = "\(viewModel.ride.distance ?? 0)km"
//        timeLabel.text = "\(viewModel.ride.minutes ?? 0)min"
//        co2Label.text = "\(round(viewModel.ride.distance ?? 0) * 249 )g"
//        priceLabel.text = "\(viewModel.ride.price ?? 0)€"
//
//        startAddressLabel.text = viewModel.ride.startAddress ?? ""
//        endAddressLabel.text = viewModel.ride.endAddress ?? ""
        
        startAddressImageView.image = Asset.Ride.flagStart.image
        endAddressImageView.image = Asset.Ride.flagEnd.image

    }
    
    func configureWithViewModel() {
        
        timeLabel.text = "\(Int(viewModel.ride.minutes ?? 0)) min"
        distanceLabel.text = "\(viewModel.ride.distance ?? 0) km"
        co2Label.text = "\(Int(viewModel.ride.distance ?? 0 ) * 249 ) g"
        priceLabel.text = "\((viewModel.ride.price ?? 0 ) ) €"
        startAddressLabel.text = viewModel.ride.startAddress
        endAddressLabel.text = viewModel.ride.endAddress

    }

    


}
