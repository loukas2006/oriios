//
//  RideConfirmationViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 31/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class RideConfirmationViewController: UIViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    

    //MARK: - Variables
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var yesBtn: RoundedButton!
    @IBOutlet weak var noBtn: RoundedButton!
    
    var viewModel: RideConfirmationViewModel!

    //MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView ()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func configureWithViewModel() {
        
    }

    //MARK: - Private

    private func setupView()  {
        //back btn
        addBackNavigationItem()
        // yes btn
        yesBtn.backgroundColor = AppStyle.Colors.mainBlue
        yesBtn.setTitle(APPStrings.Common.yes, for: .normal)
        yesBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        yesBtn.hasShadow = true
        // no btn
        noBtn.backgroundColor = AppStyle.Colors.red
        noBtn.setTitle(APPStrings.Common.no, for: .normal)
        noBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        noBtn.hasShadow = true
        
        messageLabel.font = AppStyle.Text.book(size: 16.0).font
        messageLabel.textColor = AppStyle.Colors.mainBlue
        messageLabel.text = APPStrings.FinishDiagnistic.message
        
        headerView.backgroundColor = AppStyle.Colors.mainBlue

        titleLabel.font = AppStyle.Text.bold(size: 24).font
        titleLabel.textColor = AppStyle.Colors.white
        titleLabel.text  = APPStrings.FinishDiagnistic.title.uppercased()

        subTitleLabel.font = AppStyle.Text.bold(size: 24).font
        subTitleLabel.textColor = AppStyle.Colors.mainGray
        subTitleLabel.text  = APPStrings.FinishDiagnistic.subTitle.uppercased()

    }

    //MARK: - Actions

    @IBAction func yesBtnPressed(_ sender: RoundedButton) {

        viewModel.navigateToUnLockSmartLockViewController()
        
    }
    @IBAction func noBtnPressed(_ sender: RoundedButton) {
        viewModel.navigateToMapViewController()
    }

}
