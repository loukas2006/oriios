//
//  LockSmartLockViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 17/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class LockSmartLockViewController: UIViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    //MARK: - Variables
    var viewModel: LockSmartLockViewModel!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var startBtn: RoundedButton!
    
    //MARK: - View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func configureWithViewModel() {
        
    }
    //MARK: - Private
    private func setupView()  {
        
        // back button
        addBackNavigationItem()
        //tile
        title = APPStrings.LockSmartlockk.title
        //bike image
        imageView.image = Asset.Map.lockSmartLock.image
        
        startBtn.setTitle(APPStrings.Common.start.uppercased(), for: .normal)
        startBtn.backgroundColor = AppStyle.Colors.mainBlue
        startBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        startBtn.titleLabel?.font = AppStyle.Buttons.medium16.font
    }
    
    private func turnOffSmartLock () {
        
        self.startLoading()
        
        self.viewModel.getEndAddress(completion: { [weak self] (address) in
            guard let `self` = self else {return}
            if !address.isEmpty {
                
                self.viewModel.callSearchParkingWs(success: { (parkingResponse) in
                    self.stopLoading()
                    //check distance between location and parking
                    if let parkingDistance = Double(parkingResponse.distance), parkingDistance  * 1000 <= 10 {
                        
                        self.finishRide(address: address)
                        
                    } else {
                        //location is not a parking, should show go to parking popup
                        self.stopLoading()
                        self.viewModel.showParkingNotAvailablePopup(completion: {(localiseParking) in
                            if localiseParking {
                                // show maps action sheet
                                print(parkingResponse)
                                self.viewModel.navigateToParkingActionSheet(parking: parkingResponse.parking)
                            }else {
                                // show declare parking popup
                                self.viewModel.showDeclareBikeParkingPopup(address: address, completion: {
                                    self.startLoading()
                                    self.viewModel.callAddNewParkingWs(address: address, success: { (parking) in
                                        self.stopLoading()
                                        self.finishRide(address: parking.address)
                                    }, failure: { (errorStr) in
                                        self.stopLoading()
                                        self.viewModel.showErrorPopuo(subtitle: errorStr)
                                    })
                                })
                            }
                        })
                        print("LinkaManager this is not a parking...")
                    }
                }, failure: { (errorStr) in
                    self.stopLoading()
                    self.viewModel.showErrorPopuo(subtitle: errorStr)
                })
            } else {
                self.stopLoading()
                self.viewModel.showErrorPopuo(subtitle: "Unable to get address")
            }
        })
    }
    
    func finishRide (address: String) {
        
        self.startLoading(message: APPStrings.LinkaStatus.Lock.inProgress.uppercased())
        
        self.viewModel.lockSmartLock(completion: {[weak self] (message, state) in
            guard let `self` = self else {return}
            // stalled pop up
            if state == .stalled {
                self.stopLoading()
                self.viewModel.showLinkaStalledPopup {
                    self.startBtn.setTitle(APPStrings.Common.reStart.uppercased(), for: .normal)
                }
                return
            }
            //inmoove popup
            if state == .inMove {
                self.stopLoading()
                self.viewModel.showLinkaInMovePopup {
                    self.startBtn.setTitle(APPStrings.Common.reStart.uppercased(), for: .normal)
                }
                return
            }
    
            //Time out popup
            if state == .unknown {
                self.stopLoading()
                self.viewModel.showErrorPopuo(subtitle: message)
                return
            }
            //smartlock locked so we can finish the ride
            self.viewModel.callFinishRideWs(address: address, success: { (rideResponse) in
                
                self.stopLoading()
                self.viewModel.navigateToRideResume(ride: rideResponse)
                
            }, failure: { (errorStr) in
                self.stopLoading()
                self.viewModel.showErrorPopuo(subtitle: errorStr)
            })
        })
        
    }
    //MARK: - Actions
    
    @IBAction func startBtnPressed(_ sender: Any) {
        turnOffSmartLock()
    }
}
