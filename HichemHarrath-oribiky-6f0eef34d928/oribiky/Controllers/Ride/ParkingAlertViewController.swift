//
//  ParkingAlertViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 25/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class ParkingAlertViewController: UIAlertController, ViewModelConfigurable {
    
    //MARK: - Variables
    var viewModel: ParkingAlertViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    override var preferredStyle: UIAlertControllerStyle {
        return .actionSheet
    }
    
    private func setupView() {
        title = APPStrings.DeclareParking.UnavailablePopup.localiseActionTitle
    }
    
    func configureWithViewModel() {
        
        if viewModel.canOpenGoogleMaps {
            addAction(viewModel.openGoogleMapsAction())
        }
        addAction(viewModel.openApplePlans())
        addAction(viewModel.cancelAction)

    }

}
