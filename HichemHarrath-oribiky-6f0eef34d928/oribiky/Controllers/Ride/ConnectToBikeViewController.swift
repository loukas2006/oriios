//
//  ConnectToBikeViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 11/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class ConnectToBikeViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {
    
    // MARK: - Variables
    var viewModel: ConnectToBikeViewModel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var unlockImageView: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var doneBtn: RoundedButton!
    @IBOutlet weak var bikeNumberLabel: UILabel!
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        if let code = BikeRentManager.shared.getBike()?.code {
            bikeNumberLabel.attributedText = APPStrings.UnlockSmartlock.bikeNumberTitle(code).uppercased().highlightColor(highlightedTexts: [code], font: AppStyle.Text.medium(size: 14.0).font, color: AppStyle.Colors.red)
        }
    }
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    // MARK: - Actions
    @IBAction func doneBtnPressed(_ sender: RoundedButton) {
        startLoading()
        
        viewModel.handleLinkaScan(success: {[weak self] in
            self?.stopLoading()
            self?.viewModel.navigateToBikeDiagnostic()
        }, failure: {[weak self] (errorStr) in
            self?.stopLoading()
            self?.viewModel.showErrorPopuo(title: "", subtitle: errorStr)
        })
        /*
        viewModel.handleLinkaScan(success: {[weak self] in
            self?.stopLoading()
            self?.viewModel.navigateToBikeDiagnostic()
        }, failure: {[weak self] (errorStr) in
            self?.stopLoading()
            self?.viewModel.showErrorPopuo(title: "", subtitle: errorStr)
        })
 */
    }

    // MARK: - Private
    private func setupView() {

        //add menu && notification
        addRighNavigationItems()

        // set title
        title = APPStrings.UnlockSmartlock.title
        // back button
        addBackNavigationItem()
        // description
        headerView.backgroundColor = AppStyle.Colors.mainBlue
        descriptionLabel.textColor = AppStyle.Colors.white
        descriptionLabel.font  = AppStyle.Text.medium(size: 16.0).font
        descriptionLabel.text = APPStrings.UnlockSmartlock.description
        
        
        bikeNumberLabel.textColor = AppStyle.Colors.mainBlue
        bikeNumberLabel.font  = AppStyle.Text.medium(size: 14.0).font

        
        unlockImageView.image = Asset.Map.unlockSmartLock.image
        
        doneBtn.backgroundColor = AppStyle.Colors.mainBlue
        doneBtn.setTitle(APPStrings.UnlockSmartlock.doneBtnTitle.uppercased(), for: .normal)
        doneBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        doneBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
    }
}
