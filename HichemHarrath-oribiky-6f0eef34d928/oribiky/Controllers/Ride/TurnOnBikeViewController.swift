//
//  TurnOnBikeViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 13/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class TurnOnBikeViewController: BaseViewController, StoryboardIdentifiable, ViewModelConfigurable {
    

    // MARK: - Variables
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var bikeImageView: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var doneBtn: RoundedButton!

    var viewModel: TurnOnBikeViewModel!


    // MARK: - View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    
    func configureWithViewModel() {
        
    }

    // MARK: - Private
    private func setupView() {
        //add menu && notification
        addRighNavigationItems()

        // set title
        title = APPStrings.TurnOnBike.title
        // back button
        addBackNavigationItem()
        // description
        headerView.backgroundColor = AppStyle.Colors.mainBlue
        descriptionLabel.textColor = AppStyle.Colors.white
        descriptionLabel.font  = AppStyle.Text.medium(size: 16.0).font
        descriptionLabel.text = APPStrings.TurnOnBike.description
        
        bikeImageView.image = Asset.Map.turnOnBike.image
        
        doneBtn.backgroundColor = AppStyle.Colors.mainBlue
        doneBtn.setTitle(APPStrings.TurnOnBike.doneBtnTitle.uppercased(), for: .normal)
        doneBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        doneBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
    }

    // MARK: - Actions
    @IBAction func doneBtnPressed(_ sender: RoundedButton) {
        viewModel.navigateToBikeDiagnostic()
    }
}
