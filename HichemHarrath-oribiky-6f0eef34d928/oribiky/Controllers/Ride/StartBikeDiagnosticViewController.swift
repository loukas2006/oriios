//
//  StartBikeDiagnosticViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 09/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class StartBikeDiagnosticViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {
    
    
    //MARK: - Variables
    @IBOutlet weak var bikeImageView: UIImageView!
    
    @IBOutlet weak var doneBtn: RoundedButton!
    @IBOutlet weak var messageLabel: UILabel!

    var viewModel: StartBikeDiagnosticViewModel!

    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView ()
    }
    
    //MARK: - Private
    func setupView () {
        //add menu && notification
        addRighNavigationItems()

        addBackNavigationItem()
        
        title = APPStrings.StartDiagnistic.title
        
        doneBtn.backgroundColor = AppStyle.Colors.mainBlue
        doneBtn.setTitle(APPStrings.StartDiagnistic.DoneBtn.title.uppercased(), for: .normal)
        doneBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        doneBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        
        messageLabel.text = APPStrings.UnlockSmartlock.message
        messageLabel.font = AppStyle.Text.medium(size: 13).font
        messageLabel.textColor = AppStyle.Colors.black
        
        bikeImageView.image = Asset.Map.startDiagnoticBike.image
    }

    //MARK: - ViewModelConfigurable

    func configureWithViewModel() {
        
    }

    @IBAction func doneBtnPressed(_ sender: RoundedButton) {
        viewModel.navigateToBikeDiagnostic()
    }
    

}
