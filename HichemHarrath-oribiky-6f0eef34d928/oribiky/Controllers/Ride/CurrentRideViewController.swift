//
//  CurrentRideViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 07/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class CurrentRideViewController: BaseViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    //MARK: - Variables
    @IBOutlet weak var finishRideBtn: RoundedButton!
    @IBOutlet weak var distanceIconImageView: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceBgView: UIView!
    
    @IBOutlet weak var pauseRideBtn: RoundedButton!
    
    @IBOutlet weak var timeIconImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeBgView: UIView!
    
    @IBOutlet weak var co2IconImageView: UIImageView!
    @IBOutlet weak var co2Label: UILabel!
    @IBOutlet weak var co2BgView: UIView!
    
    var viewModel: CurrentRideViewModel!
    var rideIsPaused = false
    
    static func time(_ seconds: Int) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        return formatter.string(from: TimeInterval(seconds))!
    }
    
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
            guard let `self` = self else { return }
            let rideTime = self.viewModel.calculateTime(for: BikeRentManager.shared.getCurrentRide())
            self.timeLabel.text = "\(CurrentRideViewController.time(rideTime)) min"
            self.distanceLabel.text = "\(round(1000 * (LocationManager.shared.rideDistance.value / 1000)) / 1000 ) km"
            self.co2Label.text = "\(Int((round(1000 * (LocationManager.shared.rideDistance.value / 1000)) / 1000) * 249) ) g"
            
        }
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navigationController?.navigationBar.isHidden = false
        
        
        
    }
    func configureWithViewModel() {
        tabBarItem = UITabBarItem(title: APPStrings.TabBar.mapTitle, image: Asset.TabBar.map.image, selectedImage: Asset.TabBar.map.image)
    }
    
    //MARK: - Private
    private func setupView () {
        
        //add menu && notification
        addRighNavigationItems()

        
        title = APPStrings.CurrentRide.title
        
        finishRideBtn.setTitle(APPStrings.CurrentRide.finishBtnTitle.uppercased(), for: .normal)
        finishRideBtn.backgroundColor = AppStyle.Colors.mainBlue
        finishRideBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        finishRideBtn.titleLabel?.font = AppStyle.Buttons.medium16.font

        pauseRideBtn.setTitle("  " + APPStrings.CurrentRide.pauseBtnTitle.uppercased(), for: .normal)
        pauseRideBtn.setImage(Asset.Ride.pauseRide.image, for: .normal)
        pauseRideBtn.backgroundColor = AppStyle.Colors.mainBlue
        pauseRideBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        pauseRideBtn.titleLabel?.font = AppStyle.Buttons.medium16.font

        
        
        distanceBgView.backgroundColor = AppStyle.Colors.mainBlue
        timeBgView.backgroundColor = AppStyle.Colors.mainBlue
        co2BgView.backgroundColor = AppStyle.Colors.mainBlue
        
        distanceLabel.textColor = AppStyle.Colors.white
        timeLabel.textColor = AppStyle.Colors.white
        co2Label.textColor = AppStyle.Colors.white
        
        distanceLabel.font = AppStyle.Text.bold(size: 12).font
        timeLabel.font = AppStyle.Text.bold(size: 12).font
        co2Label.font = AppStyle.Text.bold(size: 12).font
        
        distanceLabel.text = ""
        timeLabel.text = ""
        co2Label.text = ""
        
        
        distanceIconImageView.image = Asset.Ride.distance.image
        timeIconImageView.image = Asset.Ride.time.image
        co2IconImageView.image = Asset.Ride.co2.image
        
        
    }
    
    //MARK: - Actions
    @IBAction func finshRideBtnPressed(_ sender: RoundedButton) {
        
        
        //show confirmation pop up
        // if no nothing
        // if yes
        //show loader
        // 1 get ride location and finish adress
        //search parking
        // if parking ok =>  lock smartlock + finish ride + show ride resume
        // if not parking => hide loader + show parking not available popup
        // go to google maps or show declare parking popup
        //if cancel do nothing
        //if yes call declare parking ws
        //if success =>  lock smartlock + finish ride + show ride resume
        // if failure show error
        
        viewModel.showFinishRideConfirmationPopUp { [weak self] in
            guard let `self` = self else {return}
            self.viewModel.navigateToLockSmartLock()
        }
    }
    @IBAction func pauseBtnPressed(_ sender: RoundedButton) {
        
        if rideIsPaused {
            
            self.startLoading(message: APPStrings.LinkaStatus.Unlock.inProgress.uppercased())
            let unlockViewModel = UnlockSmartLockViewModel(status: [])
            unlockViewModel.unlockSmartLock { [weak self] (error, state) in
                guard let `self` = self else {return}
                self.stopLoading()

                //timeout error
                if state == .unknown || state == .lostConnection {
                 
                    return
                }

                //update pause btn
                self.pauseRideBtn.setTitle("  " + APPStrings.CurrentRide.pauseBtnTitle.uppercased(), for: .normal)
                self.pauseRideBtn.setImage(Asset.Ride.pauseRide.image, for: .normal)
                self.rideIsPaused = false


            }

            
        } else {
            

            
            viewModel.showPauseRideConfirmationPopUp {
                
                self.startLoading(message: APPStrings.LinkaStatus.Lock.inProgress.uppercased())

                let lockViewModel = LockSmartLockViewModel()
                lockViewModel.lockSmartLock(completion: { [weak self] (message, state) in
                    guard let `self` = self else {return}
                    self.stopLoading()

                    // stalled pop up
                    if state == .stalled {
                        lockViewModel.showLinkaStalledPopup {}
                        return
                    }
                    //inmoove popup
                    if state == .inMove {
                        lockViewModel.showLinkaInMovePopup {}
                        return
                    }
                    
                    //Time out popup
                    if state == .unknown {
                        lockViewModel.showErrorPopuo(subtitle: message)
                        return
                    }

                    //update pause btn
                    self.pauseRideBtn.setTitle("  " + APPStrings.CurrentRide.continueBtnTitle.uppercased(), for: .normal)
                    self.pauseRideBtn.setImage(Asset.Ride.continueRide.image, for: .normal)

                    self.rideIsPaused = true
                })
            }

        }
    }
    
}
