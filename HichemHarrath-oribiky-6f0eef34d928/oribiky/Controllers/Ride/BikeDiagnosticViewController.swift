//
//  BikeDiagnosticViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 14/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class BikeDiagnosticViewController: UIViewController, StoryboardIdentifiable, ViewModelConfigurable {

    // MARK: - Variables
    var viewModel: BikeDiagnosticViewModel!
    private var pages = [DemoPageView]()

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var yesBtn: RoundedButton!
    @IBOutlet weak var noBtn: RoundedButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var stepTitleLabel: UILabel!
    @IBOutlet weak var stepSubTitleLabel: UILabel!
    @IBOutlet weak var stepDescriptionLabel: UILabel!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var topHeaderViewConstraint: NSLayoutConstraint!
    
    private var currentPageIndex: Int = 0 {
        didSet { updateView() }
    }

    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        pages = createPages()
        setupPagesInScrollView()
        // to fill initial values
        currentPageIndex = 0
//        skipBtn.isEnabled = false
    }

    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }

    // MARK: - Private
    private func setupView() {
        scrollView.isPagingEnabled = true
        scrollView.isScrollEnabled = false

        headerView.backgroundColor = AppStyle.Colors.mainBlue
        // yes btn
        yesBtn.setImage(Asset.Diagnostic.diagnoticOK.image, for: .normal)
        yesBtn.backgroundColor = AppStyle.Colors.mainBlue
        yesBtn.setTitle("", for: .normal)
        yesBtn.hasShadow = true
        // no btn
        noBtn.setImage(Asset.Diagnostic.diagnoticKO.image, for: .normal)
        noBtn.backgroundColor = AppStyle.Colors.red
        noBtn.setTitle("", for: .normal)
        noBtn.hasShadow = true
        // pageControll
        pageControl.pageIndicatorTintColor = AppStyle.Colors.white
        pageControl.currentPageIndicatorTintColor = AppStyle.Colors.red
        pageControl.numberOfPages = viewModel.pagesModels.count
        pageControl.currentPage = 0
        // header view
        headerView.backgroundColor = AppStyle.Colors.mainBlue
        stepTitleLabel.font = AppStyle.Text.bold(size: 24).font
        stepTitleLabel.textColor = AppStyle.Colors.white
        
        stepSubTitleLabel.font = AppStyle.Text.bold(size: 24).font
        stepSubTitleLabel.textColor = AppStyle.Colors.red
        stepDescriptionLabel.font = AppStyle.Text.book(size: 16.0).font
        stepDescriptionLabel.textColor = AppStyle.Colors.white
        // skip Btn
        skipBtn.setTitle(APPStrings.BikeDiagnostic.SkipBtn.title.uppercased(), for: .normal)
        skipBtn.setTitleColor(AppStyle.Colors.red, for: .normal)
        skipBtn.titleLabel?.font = AppStyle.Buttons.medium10.font
        
        // update top margins if there is a safe area
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.delegate?.window
            let topPadding = window??.safeAreaInsets.top
            topHeaderViewConstraint.constant = topPadding ?? 0.0
        }
    }

    private func createPages() -> [DemoPageView] {
        view.layoutIfNeeded()
        var pages = [DemoPageView]()
        viewModel.pagesModels.forEach { (diagnosticStep) in
            let page: DemoPageView = DemoPageView(frame: scrollView.frame)
            page.setImage(diagnosticStep.stepImage)
            pages.append(page)
        }
        return pages
    }
    
    private func updateView() {
        // update data according to page
        pageControl.currentPage = currentPageIndex
        stepTitleLabel.text = viewModel.pagesModels[currentPageIndex].title?.uppercased() ?? ""
        stepSubTitleLabel.text = viewModel.pagesModels[currentPageIndex].subTitle ?? ""
        stepDescriptionLabel.text = viewModel.pagesModels[currentPageIndex].stepDescription ?? ""
        pages[currentPageIndex].updateImagePosition(constant: headerView.frame.height + (viewModel.pagesModels[currentPageIndex].stepImageTopMargin ?? 0.0) )
        view.layoutIfNeeded()

    }
    
    private func setupPagesInScrollView() {
        var topPadding: CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.delegate?.window
            topPadding = window??.safeAreaInsets.top ?? 0
        }
        scrollView.contentSize = CGSize(width: scrollView.frame.width * CGFloat(pages.count), height: scrollView.frame.height - (topPadding + 63.0))
        for i in 0 ..< pages.count {
            pages[i].frame = CGRect(x: scrollView.frame.width * CGFloat(i),
                                    y: 0,
                                    width: scrollView.frame.width,
                                    height: scrollView.frame.height - (topPadding + 63.0))
            scrollView.addSubview(pages[i])
        }
       // scrollView.contentOffset = CGPoint(x: 0, y: 0)

    }
    private func nextStep() {
        if currentPageIndex + 1 < pages.count {
            let nextFrame = pages[currentPageIndex + 1].frame
            scrollView.scrollRectToVisible(nextFrame, animated: true)
            currentPageIndex += 1
//            skipBtn.isEnabled = true
            return
        }
        if viewModel.validateDiagnostic() {
            viewModel.navigateToUnLockSmartLock()
        } else {
            viewModel.navigateToRideConfirmation()
        }
        
    }
    // MARK: - Actions
    @IBAction func yesBtnPressed(_ sender: RoundedButton) {
        viewModel.pagesModels[currentPageIndex].stateOK()
        nextStep()
    }
    @IBAction func noBtnPressed(_ sender: RoundedButton) {
        viewModel.pagesModels[currentPageIndex].stateKO()
        nextStep()
    }
    @IBAction func skipBtnPressed(_ sender: UIButton) {
        viewModel.navigateToUnLockSmartLock()
    }
    
}
