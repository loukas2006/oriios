//
//  SplashViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 13/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController, ViewModelConfigurable, StoryboardIdentifiable{
    
    var viewModel: SplashViewModel!

    //MARK: - Variables
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var howItWorksBtn: RoundedButton!
    @IBOutlet weak var startNowBtn: RoundedButton!
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        titleLabel.text = viewModel.titleLabelText
        subTitleLabel.text = viewModel.subTitleLabelText
        howItWorksBtn.setTitle(viewModel.howItWorksBtnTitle, for: .normal)
        startNowBtn.setTitle(viewModel.startNowBtnTitle, for: .normal)
    }

    // MARK: - Private
    private func setupView() {
        
        view.backgroundColor = AppStyle.Colors.mainBlue
        
        titleLabel.font = AppStyle.Text.medium(size: 30).font
        titleLabel.textColor = AppStyle.Colors.lightBlue
        
        subTitleLabel.font = AppStyle.Text.medium(size: 26).font
        subTitleLabel.textColor = AppStyle.Colors.white

        howItWorksBtn.titleLabel?.font = AppStyle.Text.medium(size: 18).font
        howItWorksBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        howItWorksBtn.backgroundColor = AppStyle.Colors.lightBlue

        startNowBtn.titleLabel?.font = AppStyle.Text.medium(size: 18).font
        startNowBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        startNowBtn.backgroundColor = AppStyle.Colors.red
    }

    //MARK: - Actions
    @IBAction func howItWorksBtnPressed(_ sender: RoundedButton) {
       /* let newCardViewController: NewCardViewController = UIStoryboard.init(storyboard: .card).instanciate()
        present(newCardViewController.embededInMainNavigationController(), animated: true, completion: nil)*/
        
        viewModel.navigateToDemo()
    }
    
    
    @IBAction func startNowBtnPressed(_ sender: RoundedButton) {
        viewModel.navigateToTabbar()
    }
}
