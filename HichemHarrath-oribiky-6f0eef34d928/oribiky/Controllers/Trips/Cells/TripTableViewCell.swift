//
//  TripTableViewCell.swift
//  oribiky
//
//  Created by Hichem Harrath on 24/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class TripTableViewCell: UITableViewCell, ViewModelConfigurable {

    // MARK: - Variables
    var viewModel: TripViewModel!

    @IBOutlet weak var dateBackView: UIView!
    @IBOutlet weak var adressesBackView: UIView!
    @IBOutlet weak var startAddressImageView: UIImageView!
    @IBOutlet weak var endAddressImageView: UIImageView!
    @IBOutlet weak var dateImageView: UIImageView!
    @IBOutlet weak var startAddressLabel: UILabel!
    @IBOutlet weak var endAddressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var deleteViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var tripViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var deleteBtn: UIButton!
    var tripSelectionDidChange: (()->())?
    
    var isEditMode: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.1) {
                if self.isEditMode {
                    self.deleteViewWidthConstraint.constant = 40.0
                    self.tripViewTrailingConstraint.constant = -40.0
                } else {
                    self.deleteViewWidthConstraint.constant = 0.0
                    self.tripViewTrailingConstraint.constant = 0.0
                }
                self.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
        startAddressLabel.text = viewModel.trip.startAddress
        endAddressLabel.text = viewModel.trip.endAddress
        dateLabel.text = viewModel.startDateStr.uppercased()
        isEditMode = viewModel.isEditMode
        deleteBtn.isSelected = viewModel.isSelectedToDelete
    }

    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    // MARK: - Private
    private func setupCell() {
        
        selectionStyle = .none
        startAddressImageView.image = Asset.Ride.flagStart.image
        endAddressImageView.image = Asset.Ride.flagEnd.image
        
        startAddressLabel.font = AppStyle.Text.medium(size: 12.0).font
        startAddressLabel.textColor = AppStyle.Colors.black
        
        endAddressLabel.font = AppStyle.Text.medium(size: 12.0).font
        endAddressLabel.textColor = AppStyle.Colors.black
        
        adressesBackView.backgroundColor = AppStyle.Colors.verLightBlue
        
        dateBackView.backgroundColor = AppStyle.Colors.mainBlue
        dateImageView.image = Asset.Ride.tripCalendar.image
        dateLabel.textColor = AppStyle.Colors.white
        dateLabel.font = AppStyle.Text.medium(size: 7.0).font
        
        deleteBtn.setImage(Asset.Card.checkboxOff.image, for: .normal)
        deleteBtn.setImage(Asset.Card.checkboxOn.image, for: .selected)
    }

    @IBAction func deleteBtnPressed(_ sender: UIButton) {
        deleteBtn.isSelected = !deleteBtn.isSelected
        viewModel.isSelectedToDelete = deleteBtn.isSelected
        tripSelectionDidChange?()
    }
    
}
