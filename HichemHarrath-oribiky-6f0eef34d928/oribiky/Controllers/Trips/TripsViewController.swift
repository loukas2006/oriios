//
//  TripsViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class TripsViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {
   
    // MARK: - Variables

    var trips = [[TripViewModel]]() {
        didSet {
            headerView.isUserInteractionEnabled = !(trips.count == 0)
            tableView.reloadData()
        }
    }
    var currentPage: Int = 0 {
        didSet {
            previousBtn.isEnabled = currentPage != 0
            nextBtn.isEnabled = (trips.count - 1) != currentPage
        }
    }
    
    var viewModel: TripsListViewModel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var deleteOneImageView: UIImageView!
    @IBOutlet weak var deleteAllImageView: UIImageView!
    @IBOutlet weak var deleteOneLabel: UILabel!
    @IBOutlet weak var deleteAllLabel: UILabel!
    @IBOutlet weak var deleteOneBackView: UIView!
    @IBOutlet weak var deleteAllBackView: UIView!
    @IBOutlet weak var nextPreviousBackView: UIView!
    @IBOutlet weak var nextBtn: RoundedButton!
    @IBOutlet weak var previousBtn: RoundedButton!
    @IBOutlet weak var validateBackView: UIView!
    @IBOutlet weak var cancelBtn: RoundedButton!
    @IBOutlet weak var validateBtn: RoundedButton!
    
    @IBOutlet weak var tableView: UITableView!

    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
        
        startLoading()
        viewModel.callws(success: { [weak self] (allRides) in
            guard let `self` = self else { return }
            self.stopLoading()
            self.trips = allRides.compactMap({ TripViewModel(trip: $0)}).chunks(size: 5)
        }) { [weak self] (errorStr) in
            guard let `self` = self else { return }
            self.stopLoading()
            self.trips = []
            self.viewModel.showErrorPopuo(subtitle: errorStr)
        }
    }

    // MARK: - View Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }


    // MARK: - Private
    private func setupView() {
        //add menu && notification
        addRighNavigationItems()

        // set title
        title = APPStrings.TabBar.tripsTitle
        
        //add back button
        addBackNavigationItem()
        
        headerView.backgroundColor = AppStyle.Colors.lightBlue

        // delete one
        deleteOneImageView.image = Asset.Ride.deleteOne.image
        deleteOneBackView.backgroundColor = AppStyle.Colors.mainBlue
        deleteOneBackView.round()
        deleteOneLabel.text = APPStrings.Trips.DeleteOneBtn.title
        deleteOneLabel.font = AppStyle.Text.bold(size: 14.0).font
        deleteOneLabel.textColor = AppStyle.Colors.white

        // delete all
        deleteAllImageView.image = Asset.Ride.deleteAll.image
        deleteAllBackView.backgroundColor = AppStyle.Colors.mainBlue
        deleteAllBackView.round()
        deleteAllLabel.text = APPStrings.Trips.DeleteAllBtn.title
        deleteAllLabel.font = AppStyle.Text.bold(size: 14.0).font
        deleteAllLabel.textColor = AppStyle.Colors.white
        
        // next Btn
        nextBtn.setImage(Asset.Demo.next.image, for: .normal)
        nextBtn.backgroundColor = AppStyle.Colors.mainBlue
        nextBtn.setTitle("", for: .normal)
        
        // previous btn
        previousBtn.setImage(Asset.Demo.previous.image, for: .normal)
        previousBtn.backgroundColor = AppStyle.Colors.mainBlue
        previousBtn.setTitle("", for: .normal)
        previousBtn.isEnabled = false
        
        cancelBtn.backgroundColor = AppStyle.Colors.red
        cancelBtn.setTitle(APPStrings.Common.cancel.uppercased(), for: .normal)
        cancelBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        cancelBtn.titleLabel?.font = AppStyle.Text.medium(size: 12.0).font
        cancelBtn.hasShadow = true
        
        validateBtn.backgroundColor = AppStyle.Colors.mainBlue
        validateBtn.setTitle(APPStrings.Trips.DeleteBtn.title.uppercased(), for: .normal)
        validateBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        validateBtn.titleLabel?.font = AppStyle.Text.medium(size: 12.0).font
        validateBtn.hasShadow = true

        validateBackView.isHidden = true
        nextPreviousBackView.isHidden = false
        validateBtn.isEnabled = false 
        // register tableview cell
        let cellNib = UINib(nibName: String(describing: TripTableViewCell.self), bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: String(describing: TripTableViewCell.self))
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
    }
    // MARK: - Actions
    @IBAction func deleteOneBtnPressed(_ sender: UIButton) {
        validateBackView.isHidden = false
        nextPreviousBackView.isHidden = true

        trips[currentPage].forEach { $0.isEditMode = true }
        tableView.reloadData()
    }
    
    @IBAction func deleteAllBtnPressed(_ sender: UIButton) {
        validateBackView.isHidden = false
        nextPreviousBackView.isHidden = true
        
        trips[currentPage].forEach { (tripViewModel) in
            tripViewModel.isEditMode = true
            tripViewModel.isSelectedToDelete = true
        }
        validateBtn.isEnabled = true
        tableView.reloadData()
    }

    @IBAction func nextBtnPressed(_ sender: UIButton) {
        if currentPage < trips.count - 1 {
            currentPage += 1
        }
        tableView.reloadSections(IndexSet(integer: 0), with: .left)
    }
    
    @IBAction func previousBtnPressed(_ sender: UIButton) {
        if currentPage > 0 {
            currentPage -= 1
        }
        tableView.reloadSections(IndexSet(integer: 0), with: .right)
    }
    
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        validateBackView.isHidden = true
        nextPreviousBackView.isHidden = false
        
        trips[currentPage].forEach { (tripViewModel) in
            tripViewModel.isEditMode = false
            tripViewModel.isSelectedToDelete = false
        }
        tableView.reloadData()
    }
    
    @IBAction func validateBtnPressed(_ sender: UIButton) {
        
        let tripsToDelete = self.trips[self.currentPage].filter({$0.isSelectedToDelete == true})

        viewModel.showDeleteTripsConfirmationPopup(tripsCount: tripsToDelete.count) { [weak self] in
            guard let `self` = self else {return}
            
            self.startLoading()
            
            self.validateBackView.isHidden = true
            self.nextPreviousBackView.isHidden = false
            
            
            self.viewModel.callDeleteWs(ids: tripsToDelete.compactMap({ $0.trip.rideId }), success: { [weak self](allRides) in
                guard let `self` = self else { return }
                self.stopLoading()
                self.currentPage = 0
                self.trips = allRides.compactMap({ TripViewModel(trip: $0)}).chunks(size: 5)
                }, failure: {  [weak self] (errorStr) in
                    guard let `self` = self else { return }
                    self.stopLoading()
                    self.viewModel.showErrorPopuo(subtitle: errorStr)
                    
            })

        }
    }
}

extension  TripsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if trips.count > 0 {
            return trips[currentPage].count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TripTableViewCell.self)) as? TripTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(to: trips[currentPage][indexPath.row])
        
        cell.tripSelectionDidChange = {
            self.validateBtn.isEnabled = self.trips[self.currentPage].filter({$0.isSelectedToDelete == true}).count > 0
        }
        return cell
    }
}

extension TripsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.navigateToRideDetails(ride: trips[currentPage][indexPath.row].trip)
    }
}
