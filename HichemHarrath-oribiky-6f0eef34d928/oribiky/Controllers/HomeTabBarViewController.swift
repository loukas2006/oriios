//
//  HomeTabBarViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit


class HomeTabBarViewController: UITabBarController, ViewModelConfigurable, StoryboardIdentifiable {
  
    // MARK: - Variables
    var viewModel: TabbarViewModel!
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(subscriptionNotificationHandler(notif:)), name: Notification.Name(rawValue: Constant.reservationRequirementsNotificationName), object: nil)

    }
    // MARK: - View Cycle

    @objc func subscriptionNotificationHandler(notif : Notification) {
        
        
        if let stepType = notif.object as? StepType {
            switch stepType {
            case .subscription(let type):
                viewModel.handleSubscriptionNotification(index: selectedIndex, subscription: type)
                default:
                break
            }
        }
        
    }
    func configureWithViewModel() {
        //tabBar
        delegate = self
    }

    
    
}
extension HomeTabBarViewController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        guard let index = viewControllers?.index(of: viewController) else {
            return false
            
        }
      
        //••••••••••••
        // check if we have an authentication controller in the tabbar
        // should found an other soltion for this problem.

//        let isAuthenticationController = (viewController as? UINavigationController)?.viewControllers.first is AuthenticationViewController
        
//        viewModel.selectetTabbarItem(index: index, isAuthenticationController)
        if selectedIndex == index {
            return true
        }
        viewModel.selectetTabbarItem(index: index, (viewController as? UINavigationController)?.viewControllers.first)

        return false
    }
}
