//
//  DemoViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class DemoViewController: UIViewController, StoryboardIdentifiable, ViewModelConfigurable {
    

    // MARK: - Variables
    var viewModel: DemoViewModel!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nextBtn: RoundedButton!
    @IBOutlet weak var previousBtn: RoundedButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var backgroundImageView: UIImageView!
    // header
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var stepTitleLabel: UILabel!
    @IBOutlet weak var stepSubTitleLabel: UILabel!
    @IBOutlet weak var skipDemoBtn: UIButton!
    @IBOutlet weak var topHeaderViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextBtnWidthConstraint: NSLayoutConstraint!

    private var pages = [DemoPageView]()

    private var currentPageIndex: Int = 0 {
        didSet { updateView() }
    }
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
    }
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    // MARK: - Private
    private func setupView() {

        backgroundImageView.image = Asset.Demo.backgroundStep1.image
        // create pages
        pages = createPages()
        setupPagesInScrollView()
        
        // to fill initial values
        currentPageIndex = 0
        // scrollView
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
        // next Btn
        nextBtn.setImage(Asset.Demo.next.image, for: .normal)
        nextBtn.backgroundColor = AppStyle.Colors.mainBlue
        nextBtn.setTitle("", for: .normal)
        nextBtn.hasShadow = true
        // previous btn
        previousBtn.setImage(Asset.Demo.previous.image, for: .normal)
        previousBtn.backgroundColor = AppStyle.Colors.mainBlue
        previousBtn.setTitle("", for: .normal)
        previousBtn.hasShadow = true
        // pageControll
        pageControl.pageIndicatorTintColor = AppStyle.Colors.white
        pageControl.currentPageIndicatorTintColor = AppStyle.Colors.red
        // header view
        headerView.backgroundColor = AppStyle.Colors.mainBlue
        stepTitleLabel.font = AppStyle.Text.bold(size: 24).font
        stepSubTitleLabel.font = AppStyle.Text.book(size: 16).font
        
        headerView.layer.shadowColor = AppStyle.Colors.shadow.cgColor
        headerView.layer.shadowOffset = CGSize(width: 0, height: 5)
        headerView.layer.masksToBounds = false
        headerView.layer.shadowRadius = 5
        headerView.layer.shadowOpacity = 1
        // skip btn
        skipDemoBtn.setTitle(APPStrings.Demo.skipDemoBtnTitle.uppercased(), for: .normal)
        skipDemoBtn.setTitleColor(AppStyle.Colors.red, for: .normal)
        skipDemoBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        
        // update top margins if there is a safe area
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.delegate?.window
            let topPadding = window??.safeAreaInsets.top
            topHeaderViewConstraint.constant = topPadding ?? 0.0
        }
    }
    private func updateView() {
        // update data according to page
        pageControl.currentPage = currentPageIndex
        stepTitleLabel.text = viewModel.pagesModels[currentPageIndex].title?.uppercased() ?? ""
        stepSubTitleLabel.text = viewModel.pagesModels[currentPageIndex].subTitle ?? ""
        view.layoutIfNeeded()
        pages[currentPageIndex].updateImagePosition(constant: headerView.frame.height + (viewModel.pagesModels[currentPageIndex].stepImageTopMargin ?? 0.0) )
        backgroundImageView.isHidden = currentPageIndex != 0
        previousBtn.isEnabled = currentPageIndex != 0

        updateNextBtn()
    }
    
    private func updateNextBtn () {
        // last page
        if currentPageIndex == pages.count - 1 {
            UIView.animate(withDuration: 0.15) {
                self.nextBtnWidthConstraint.constant = 0.7 * UIScreen.main.bounds.width
                self.view.layoutIfNeeded()
            }
            nextBtn.setImage(nil, for: .normal)
            nextBtn.setTitle(APPStrings.Demo.letsStartBtnTitle.uppercased(), for: .normal)
            nextBtn.titleLabel?.font = AppStyle.Buttons.medium12.font

        } else {
            UIView.animate(withDuration: 0.15) {
                self.nextBtnWidthConstraint.constant = 41.0
                self.view.layoutIfNeeded()
            }
            nextBtn.setImage(Asset.Demo.next.image, for: .normal)
            nextBtn.setTitle("", for: .normal)
        }
    }
    private func createPages() -> [DemoPageView] {
      
        view.layoutIfNeeded()
        
        var pages = [DemoPageView]()
        viewModel.pagesModels.forEach { (demoPageViewModel) in
            let page: DemoPageView = DemoPageView(frame: scrollView.frame)
            page.setImage(demoPageViewModel.stepImage)
            pages.append(page)
        }
        return pages
    }
    
    private func setupPagesInScrollView() {
        scrollView.contentSize = CGSize(width: scrollView.frame.width * CGFloat(pages.count), height: scrollView.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< pages.count {
            pages[i].frame = CGRect(x: scrollView.frame.width * CGFloat(i),
                                    y: 0,
                                    width: scrollView.frame.width,
                                    height: scrollView.frame.height)
            scrollView.addSubview(pages[i])
        }
    }
    
    // MARK: - Actions
    @IBAction func nextBtnPressed(_ sender: RoundedButton) {
        if currentPageIndex == pages.count - 1 {
            // last page, start app
            viewModel.navigateToTabbar()
        } else {
            if currentPageIndex + 1 < pages.count {
                let nextFrame = pages[currentPageIndex + 1].frame
                scrollView.scrollRectToVisible(nextFrame, animated: true)
                currentPageIndex += 1
            }
        }
    }
    
    @IBAction func previousBtnPressed(_ sender: RoundedButton) {
        if currentPageIndex - 1 >= 0 {
            let previousFrame = pages[currentPageIndex - 1].frame
            scrollView.scrollRectToVisible(previousFrame, animated: true)
            currentPageIndex -= 1
        }
    }
    
    @IBAction func skipBtnPressed(_ sender: UIButton) {
        viewModel.navigateToTabbar()
    }
}
extension DemoViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        currentPageIndex = Int(round(scrollView.contentOffset.x/scrollView.frame.width))
    }
}
