//
//  EditProfileViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 19/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    
    //MARK: - Variables
    var viewModel: EditProfileViewModel!

    var   cellsDataSource = [EditableFormItemViewModel]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelBtn: RoundedButton!
    @IBOutlet weak var validateBtn: RoundedButton!
    @IBOutlet weak var newsletterView: UIView!
    @IBOutlet weak var newsletterLabel: UILabel!
    @IBOutlet weak var newsLetterSwitch: UISwitch!

    // MARK: - View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        cellsDataSource = viewModel.prepareDataSource()
    }
    
    func configureWithViewModel() {
        
    }
    
    
    //MARK: - Private
    private func setupView() {
        //add menu && notification
        addRighNavigationItems()

        //navigation
        addBackNavigationItem()
        
        //view
        title = APPStrings.EditProfile.title
        
        //tableview
        let nib = UINib.init(nibName: String(describing: EditFormItemTableViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: EditFormItemTableViewCell.self))
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        //validate Button
        validateBtn.setTitle(APPStrings.EditProfile.validateBtnTitle.uppercased(), for: .normal)
        validateBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        validateBtn.backgroundColor = AppStyle.Colors.mainBlue
        //Cancel Button
        cancelBtn.setTitle(APPStrings.EditProfile.cancelBtnTitle.uppercased(), for: .normal)
        cancelBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        cancelBtn.backgroundColor = AppStyle.Colors.red
        
        //Newsletter Label
        newsletterLabel.text = APPStrings.EditProfile.newsLetterLabelText
        newsletterLabel.textColor = AppStyle.Colors.black
        newsletterLabel.font = AppStyle.Text.regular(size: 12).font
        //termsOfUseAndSaleSwitch
        newsLetterSwitch.onTintColor = AppStyle.Colors.mainBlue
        newsLetterSwitch.transform = CGAffineTransform(scaleX: 0.8, y: 0.75)
        newsLetterSwitch.isOn = viewModel.isNewsLetterAccespted()
        //Hide Newsletter if editing address
        if viewModel.editType == .address {
            newsletterView.clipsToBounds = true
            newsletterView.addHeightConstraint(constant: 0)
        }
        
    }
    
    // MARK: - Actions
    
    @IBAction func cancelBtnPressed(_ sender: RoundedButton) {
        viewModel.navigateToProfileViewController()
    }
    @IBAction func validateBtnPressed(_ sender: RoundedButton) {
        //1 validate fields
        //2 call ws

        
        if viewModel.isDataSourceValid(dataSource: cellsDataSource) {
            startLoading()
            viewModel.callWs(dataSource: cellsDataSource, success: {[weak self] (userProfile) in
                self?.stopLoading()
                self?.viewModel.dismiss()
            }, failure: {[weak self] (errorStr, errors) in
                guard let strongSelf = self else { return }
                    for item in strongSelf.cellsDataSource {
                        item.wsErrorStr = errors?[item.type.rawValue]?.first?.localized()
                    }
//                strongSelf.viewModel.showErrorPopuo(subtitle: errorStr)
                strongSelf.stopLoading()
                strongSelf.tableView.reloadData()
            })
        }
        tableView.reloadData()
    }

    @IBAction func newsLetterSwitchPressed(_ sender: UISwitch) {
        viewModel.togglePromotionalOffersOption(value: sender.isOn)
    }
}

extension EditProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: EditFormItemTableViewCell.self), for: indexPath) as? EditFormItemTableViewCell else {
            return UITableViewCell()
        }
        let item = cellsDataSource[indexPath.row]
        
        cell.configure(to: item)
        //handle completion handler
        cell.configureCompletionHandler = { editedValue in
            item.updateEditValue(v: editedValue)
        }

        
        return cell
    }
    
}

