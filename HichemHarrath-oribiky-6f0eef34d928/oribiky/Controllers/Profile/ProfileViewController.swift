//
//  ProfileViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {

    //MARK: - Variables
    var viewModel: ProfileViewModel!

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var editProfileIconImageView: UIImageView!
    @IBOutlet weak var editProfileIconBgView: UIView!

    @IBOutlet weak var myRideIconImageView: UIImageView!
    @IBOutlet weak var myTripsBgView: UIView!

    @IBOutlet weak var editProfileTitleLabel: UILabel!
    @IBOutlet weak var myRidesLabel: UILabel!

    var cellsDataSources = [[ProfileItemViewModel]]() {
        didSet{
            tableView.reloadData()
        }
    }
    
    
    //MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        cellsDataSources = viewModel.preparedataSource()
    }
    
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // 1 - show loader
        startLoading()
        
        viewModel.callGetProfileWs(success: { (userProfile) in
            self.stopLoading()
            print(userProfile)
            self.cellsDataSources = self.viewModel.preparedataSource()

        }, failure: { (errorStr) in
            self.stopLoading()
            print(errorStr)

        })
    }
    
    //MARK: - Private
    private func setupView() {
        
        //add menu && notification
        addRighNavigationItems()
        //view controller title
        title = APPStrings.Profile.title
        // configure tabbar item
        let profileTabBarItem = UITabBarItem(title: APPStrings.TabBar.profileTitle,
                                             image: Asset.TabBar.profile.image,
                                             selectedImage: Asset.TabBar.profile.image)
        tabBarItem = profileTabBarItem
        
        // table View
        tableView.delegate = self
        tableView.dataSource = self
        
        let nib = UINib.init(nibName: String(describing: FormItemTableViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: FormItemTableViewCell.self))

        headerView.backgroundColor = AppStyle.Colors.lightBlue
        
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.backgroundColor = AppStyle.Colors.lightGray

        tableView.alwaysBounceVertical = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        
        // edit profile btn
        editProfileIconImageView.image = Asset.Profile.iconEdit.image
        editProfileIconBgView.backgroundColor = AppStyle.Colors.mainBlue
        editProfileIconBgView.round()
        
        // manage my trips btn
        myRideIconImageView.image = Asset.TabBar.trips.image
        myTripsBgView.backgroundColor = AppStyle.Colors.mainBlue
        myTripsBgView.round()
        
        editProfileTitleLabel.font = AppStyle.Text.bold(size: 14.0).font
        editProfileTitleLabel.textColor = AppStyle.Colors.white
        editProfileTitleLabel.text = APPStrings.EditProfile.title
        
        myRidesLabel.font = AppStyle.Text.bold(size: 14.0).font
        myRidesLabel.textColor = AppStyle.Colors.white
        myRidesLabel.text = APPStrings.TabBar.tripsTitle
        
        
        
    }
    
    //MARK: - Actions
    @IBAction func editProfileBtnPressed(_ sender: UIButton) {
        viewModel.navigateToEditProfile()
    }
    
    @IBAction func manageSubscriptionBtnPressed(_ sender: UIButton) {
        AppCoordinator.shared.transition(to: .trips(TripsListViewModel()), type: .push, animated: true)
    }
}

//MARK: - UITableViewDataSource
extension ProfileViewController:  UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cellsDataSources.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsDataSources[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FormItemTableViewCell.self), for: indexPath) as? FormItemTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(to: cellsDataSources[indexPath.section][indexPath.row])
        cell.delegate = self
        return cell
    }
}

//MARK: - UITableViewDelegate
extension ProfileViewController: UITableViewDelegate {
    
}

//MARK: - FormItemDelegate
extension ProfileViewController: FormItemDelegate {
    
    func didPressFirstAccessoryBtn(cellConfiguration: CellConfiguration) {
        switch cellConfiguration {
        case .address:
            viewModel.navigateToEditAdress()
        case .cardAdd where BikeRentManager.shared.getCurrentRide() == nil:
            viewModel.navigateToAddCard()
        case .cardEdit where BikeRentManager.shared.getCurrentRide() == nil:
            viewModel.navigateToAddCard()
        default:
            break
        }
    }
    
    func didPressSecondAccessoryBtn(cellConfiguration: CellConfiguration) {
        
    }
}
