//
//  ProfileTableViewCell.swift
//  oribiky
//
//  Created by Aymen Harrath on 16/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit


protocol FormItemDelegate {
    func didPressFirstAccessoryBtn (cellConfiguration: CellConfiguration)
    func didPressSecondAccessoryBtn (cellConfiguration: CellConfiguration)
}
private let accessoryBtnDefaultHeight: CGFloat = 30

enum CellConfiguration {
    case cardAdd
    case cardEdit
    case address
    case other
    
    var firstBtnImage: UIImage? {
        switch self {
        case .address: return Asset.Profile.iconEdit.image
        case .cardAdd: return Asset.Profile.iconAdd.image
        case .cardEdit: return Asset.Profile.iconEdit.image
        case .other: return nil
        }
    }
}


class FormItemTableViewCell: UITableViewCell, ViewModelConfigurable {
 
    
    

    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var firstAccessoryBtn: RoundedButton!
    @IBOutlet weak var secondAccessoryBtn: RoundedButton!

    @IBOutlet weak var firstAccessoryBtnHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var secondAccessoryBtnHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconImageViewWidthConstraint: NSLayoutConstraint!
//    @IBOutlet weak var iconImageViewHeightConstraint: NSLayoutConstraint!

    var delegate: FormItemDelegate?
    var viewModel: ProfileItemViewModel!
    


    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    

    private func setupCell () {
        
        itemTitleLabel.font = AppStyle.Text.bold(size: 14).font
        itemTitleLabel.textColor = AppStyle.Colors.mainGray

        valueLabel.font = AppStyle.Text.medium(size: 14).font
        valueLabel.textColor = AppStyle.Colors.black

        firstAccessoryBtn.backgroundColor = AppStyle.Colors.mainBlue
        
//        secondAccessoryBtn.backgroundColor = AppStyle.Colors.red
//        secondAccessoryBtn.setImage(Asset.Profile.iconDelete.image, for: .normal)

        valueLabel.numberOfLines = 0
    }
    
    func configureWithViewModel() {
        
        fillCell()
    }

    
    private func fillCell()  {
        
        iconImageViewWidthConstraint.constant = 0
//        iconImageViewHeightConstraint.constant = 0

        switch viewModel.cellConfig {
        case .other:
            firstAccessoryBtnHeightConstraint.constant = 0
            secondAccessoryBtnHeightConstraint.constant = 0
            firstAccessoryBtn.isHidden = true
            secondAccessoryBtn.isHidden = true
            backgroundColor = AppStyle.Colors.white

        case .address:
            firstAccessoryBtnHeightConstraint.constant = accessoryBtnDefaultHeight
            secondAccessoryBtnHeightConstraint.constant = 0
            firstAccessoryBtn.isHidden = false
            secondAccessoryBtn.isHidden = true
            backgroundColor = AppStyle.Colors.clear
            
        case .cardAdd, .cardEdit:
            firstAccessoryBtnHeightConstraint.constant = accessoryBtnDefaultHeight
            secondAccessoryBtnHeightConstraint.constant = 0
            firstAccessoryBtn.isHidden = false
            secondAccessoryBtn.isHidden = true
            backgroundColor = AppStyle.Colors.clear
            if let image =  StripePaymentManager.shared.getCardBrandImage(brand: BikeRentManager.shared.getUser().card.cardBrand) {
                iconImageView.image = image
                iconImageViewWidthConstraint.constant = image.size.width 
//                iconImageViewHeightConstraint.constant = image.size.height / 2
            }
        }

        itemTitleLabel.text = viewModel.title.uppercased()
        valueLabel.text = viewModel.value
        firstAccessoryBtn.setImage(viewModel.cellConfig.firstBtnImage, for: .normal)

    }
    
    // MARK: - Actions
    @IBAction func firstAccessoryBtnPressed(_ sender: RoundedButton) {
        self.delegate?.didPressFirstAccessoryBtn(cellConfiguration: viewModel.cellConfig)
    }
    
    @IBAction func secondAccessoryBtnPressed(_ sender: RoundedButton) {
        self.delegate?.didPressSecondAccessoryBtn(cellConfiguration: viewModel.cellConfig)

    }
    
    
}
