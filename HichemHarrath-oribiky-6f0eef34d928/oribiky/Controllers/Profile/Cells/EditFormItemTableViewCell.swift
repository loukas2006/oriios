//
//  EditFormItemTableViewCell.swift
//  oribiky
//
//  Created by Aymen Harrath on 19/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class EditFormItemTableViewCell: UITableViewCell, ViewModelConfigurable {
    
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var separatorViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    
    var configureCompletionHandler : ((String) -> Void)?
    
    var viewModel: EditableFormItemViewModel!
    
    
    
    var previousTextFieldContent: String?
    var previousSelection: UITextRange?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    //MARK: - Private
    private func setupCell () {
        
        itemTitleLabel.font = AppStyle.Text.bold(size: 14).font
        itemTitleLabel.textColor = AppStyle.Colors.mainGray
        
        valueTextField.font = AppStyle.Text.medium(size: 14).font
        valueTextField.textColor = AppStyle.Colors.black

        separatorView.backgroundColor = AppStyle.Colors.mainBlue
        separatorViewHeightConstraint.constant = 0.5
        

    }
    
    func configureWithViewModel() {


        fill()
        switch viewModel.type {
        case .cardNumber, .expireDate, .cvvCode:
            valueTextField.delegate = self
        default:
            break
        }
        
        
    }
    
    func fill()  {
        
        itemTitleLabel.text = viewModel.type.title.uppercased()
        valueTextField.text = viewModel.editedValue
        errorDescriptionLabel.text = (viewModel.wsErrorStr ?? "").count > 0 ? viewModel.wsErrorStr : viewModel.isValidStr
        valueTextField.keyboardType = viewModel.keyboardType
        valueTextField.autocapitalizationType = viewModel.capitalizationType

    }
    
    
    @IBAction func TextFieldTextChanged(_ sender: UITextField) {
        
        
        switch viewModel.type {
        case .cardOwner:
            sender.text = sender.text?.uppercased()
        case .cardNumber:
            reformatAsCardNumber(textField: sender)
        case .expireDate:
            reformatAsExpiration(textField: sender)
        case .cvvCode:
            reformatAsCVV(textField: sender)
        default:
            break
        }
        
        configureCompletionHandler?(sender.text ?? "")

    }
    
    
}

//MARK: - UITextFieldDelegate
extension EditFormItemTableViewCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if viewModel.type == .cardNumber  {
            previousTextFieldContent = textField.text
            previousSelection = textField.selectedTextRange
        }
        return true
        
    }
}

//MARK: - Formatting card number
extension EditFormItemTableViewCell {
    
    func reformatAsCardNumber(textField: UITextField) {
        var targetCursorPosition = 0
        if let startPosition = textField.selectedTextRange?.start {
            targetCursorPosition = textField.offset(from: textField.beginningOfDocument, to: startPosition)
        }
        
        var cardNumberWithoutSpaces = ""
        if let text = textField.text {
            cardNumberWithoutSpaces = self.removeNonDigits(string: text, andPreserveCursorPosition: &targetCursorPosition)
        }
        
        if cardNumberWithoutSpaces.count > 19 {
            textField.text = previousTextFieldContent
            textField.selectedTextRange = previousSelection
            return
        }
        
        let cardNumberWithSpaces = self.insertCreditCardSpaces(cardNumberWithoutSpaces, preserveCursorPosition: &targetCursorPosition)
        textField.text = cardNumberWithSpaces
        
        if let targetPosition = textField.position(from: textField.beginningOfDocument, offset: targetCursorPosition) {
            textField.selectedTextRange = textField.textRange(from: targetPosition, to: targetPosition)
        }
    }
    
    func removeNonDigits(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
        var digitsOnlyString = ""
        let originalCursorPosition = cursorPosition
        
        for i in Swift.stride(from: 0, to: string.count, by: 1) {
            let characterToAdd = string[string.index(string.startIndex, offsetBy: i)]
            if characterToAdd >= "0" && characterToAdd <= "9" {
                digitsOnlyString.append(characterToAdd)
            }
            else if i < originalCursorPosition {
                cursorPosition -= 1
            }
        }
        
        return digitsOnlyString
    }
    
    func insertCreditCardSpaces(_ string: String, preserveCursorPosition cursorPosition: inout Int) -> String {
        // Mapping of card prefix to pattern is taken from
        // https://baymard.com/checkout-usability/credit-card-patterns
        
        // UATP cards have 4-5-6 (XXXX-XXXXX-XXXXXX) format
        let is456 = string.hasPrefix("1")
        
        // These prefixes reliably indicate either a 4-6-5 or 4-6-4 card. We treat all these
        // as 4-6-5-4 to err on the side of always letting the user type more digits.
        let is465 = [
            // Amex
            "34", "37",
            
            // Diners Club
            "300", "301", "302", "303", "304", "305", "309", "36", "38", "39"
            ].contains { string.hasPrefix($0) }
        
        // In all other cases, assume 4-4-4-4-3.
        // This won't always be correct; for instance, Maestro has 4-4-5 cards according
        // to https://baymard.com/checkout-usability/credit-card-patterns, but I don't
        // know what prefixes identify particular formats.
        let is4444 = !(is456 || is465)
        
        var stringWithAddedSpaces = ""
        let cursorPositionInSpacelessString = cursorPosition
        
        for i in 0..<string.count {
            let needs465Spacing = (is465 && (i == 4 || i == 10 || i == 15))
            let needs456Spacing = (is456 && (i == 4 || i == 9 || i == 15))
            let needs4444Spacing = (is4444 && i > 0 && (i % 4) == 0)
            
            if needs465Spacing || needs456Spacing || needs4444Spacing {
                stringWithAddedSpaces.append(" ")
                
                if i < cursorPositionInSpacelessString {
                    cursorPosition += 1
                }
            }
            
            let characterToAdd = string[string.index(string.startIndex, offsetBy:i)]
            stringWithAddedSpaces.append(characterToAdd)
        }
        
        return stringWithAddedSpaces
    }
    
    
    func reformatAsExpiration(textField: UITextField) {
        guard let string = textField.text else { return }
        let expirationString = AppStyle.expirationDateSeparator
        let cleanString = string.replacingOccurrences(of: expirationString, with: "", options: .literal, range: nil)
        
        if cleanString.count >= 3 {
            
            let monthString = cleanString[Range(0...1)]
            if let month = Int(monthString) {
                if month < 1 || month > 12 {
                    textField.text = ""
                    return
                }
            }
            var yearString: String
            if cleanString.count == 3 {
                yearString = cleanString[2]
            } else {
                yearString = cleanString[Range(2...3)]
            }
            textField.text = monthString + expirationString + yearString
        } else {
            textField.text = cleanString
        }
        print(textField.text)

    }
    
    
    func reformatAsCVV(textField: UITextField) {
        guard let string = textField.text else { return }
        if string.count > 4 {
            textField.text = string[0..<4]
            print(textField.text)
        }
    }
    
}


