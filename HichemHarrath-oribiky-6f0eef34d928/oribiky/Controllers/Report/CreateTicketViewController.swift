//
//  CreateTicketViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 28/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class CreateTicketViewController: BaseViewController, StoryboardIdentifiable, ViewModelConfigurable {

    //MARK:- Variables
    var viewModel: CreateTicketViewModel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var subjectLable: UILabel!
    @IBOutlet weak var subjectTextField: UITextField!
    @IBOutlet weak var subjectLineView: UIView!
    
    @IBOutlet weak var messageLable: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var messageLineView: UIView!
    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var subjectErrorLabel: UILabel!
    @IBOutlet weak var messageErrorLabel: UILabel!
    
    @IBOutlet weak var sendBtn: RoundedButton!
    
    
    var initialContentSizeHeight: CGFloat = 0.0
    var isKeyboardVisible = false
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
    }
    
    private func setupView () {
        //add menu && notification
        addRighNavigationItems()

        addBackNavigationItem()
        title = APPStrings.Assistance.CreateTicket.title
        
        sendBtn.backgroundColor = AppStyle.Colors.mainBlue
        sendBtn.titleLabel?.font = AppStyle.Buttons.medium16.font
        sendBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        sendBtn.setTitle(APPStrings.Common.send, for: .normal)

        subjectLineView.backgroundColor = AppStyle.Colors.mainBlue
        messageLineView.backgroundColor = AppStyle.Colors.mainBlue
        
        subjectLable.textColor = AppStyle.Colors.black
        subjectLable.font = AppStyle.Text.medium(size: 14).font
        subjectLable.text = APPStrings.Assistance.Subject.title
        subjectTextField.font = AppStyle.Text.regular(size: 14).font
        subjectTextField.delegate = self
        
        messageLable.textColor = AppStyle.Colors.black
        messageLable.font = AppStyle.Text.medium(size: 14).font
        messageLable.text = APPStrings.Assistance.Message.title
        messageTextView.delegate = self
        messageTextView.font = AppStyle.Text.regular(size: 14).font

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyoard(_:)))
        scrollView.addGestureRecognizer(tapGestureRecognizer)

        subjectErrorLabel.text = nil
        subjectErrorLabel.font = AppStyle.Text.regular(size: 11).font
        subjectErrorLabel.textColor = AppStyle.Colors.red

        messageErrorLabel.text = nil
        messageErrorLabel.font = AppStyle.Text.regular(size: 11).font
        messageErrorLabel.textColor = AppStyle.Colors.red


    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if scrollView.contentSize.height == 0 {
            return
        }
        initialContentSizeHeight = scrollView.contentSize.height
    }


    //MARK:- ViewModelConfigurable

    func configureWithViewModel() {
        
        let defaultCenter = NotificationCenter.default
        defaultCenter.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        defaultCenter.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        defaultCenter.addObserver(self, selector: #selector(keyboardDidHide(_:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)

    }

    
    // MARK: - Keyboard Events handlers
    @objc func keyboardDidShow(_ notification: Notification) {
        isKeyboardVisible = true
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        
        var contentSize = scrollView.contentSize
        contentSize.height = initialContentSizeHeight
        scrollView.contentSize = contentSize
        scrollView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    @objc func keyboardDidHide(_ notification: Notification) {
        isKeyboardVisible = false
    }

    @objc func hideKeyoard (_ recognize: UITapGestureRecognizer) {
        if subjectTextField.isFirstResponder {
            subjectTextField.resignFirstResponder()
        }else if messageTextView.isFirstResponder {
            messageTextView.resignFirstResponder()
            
        }
    }
    // MARK: - Actions
    
    @IBAction func sendBtnPressed(_ sender: RoundedButton) {
        
        guard let subject = subjectTextField.text, subject.count > 0 else {
            subjectErrorLabel.text = APPStrings.Assistance.CreateTicket.subjectError
            return
        }
        
        guard let message = messageTextView.text, message.count > 0 else {
            messageErrorLabel.text = APPStrings.Assistance.CreateTicket.messageError
            return
        }
        
        startLoading()
        viewModel.callWS(subject: subject, message: message, success: { [weak self] in
            self?.stopLoading()
            self?.viewModel.dismiss()
        }) {[weak self] (errorStr) in
            self?.stopLoading()
            self?.viewModel.showErrorPopuo(subtitle: errorStr)
        }
        
    }
    
}

extension CreateTicketViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        messageErrorLabel.text = nil

        if isKeyboardVisible && scrollView.contentOffset != .zero {
            return
        }

        let calculatedHeight =  view.frame.height - sendBtn.frame.origin.y - messageContainerView.frame.height - sendBtn.frame.height
        var contentSize = scrollView.contentSize
        contentSize.height = initialContentSizeHeight  - calculatedHeight
        scrollView.contentSize = contentSize
        scrollView.setContentOffset(CGPoint(x: 0, y: calculatedHeight), animated: true)

    }
}

extension CreateTicketViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        subjectErrorLabel.text = nil
    }

}
