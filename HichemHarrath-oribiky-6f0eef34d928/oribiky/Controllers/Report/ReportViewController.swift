//
//  ReportViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class ReportViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {

    // MARK: - Variables
    var viewModel: ReportViewModel!
    @IBOutlet weak var complaintsView: UIView!
    @IBOutlet weak var faqView: UIView!
    @IBOutlet weak var ticketsButton: UIButton!
    @IBOutlet weak var faqButton: UIButton!
    @IBOutlet weak var ticketsTapIndicatorImageView: UIImageView!
    @IBOutlet weak var faqTapIndicatorImageView: UIImageView!

    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var phoneIconImageView: UIImageView!
    @IBOutlet weak var timeIconImageView: UIImageView!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var verticalSeparatorView: UIView!
    @IBOutlet weak var chatBtn: RoundedButton!
    

    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
    }
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

  
    // MARK: - Private
    private func setupView() {
        //add menu && notification
        addRighNavigationItems()

        // set title
        title = APPStrings.TabBar.reportTitle
        
        //header view
        headerView.backgroundColor = AppStyle.Colors.lightBlue
        
        ticketsButton.titleLabel?.font = AppStyle.Buttons.medium16.font
        ticketsButton.setTitle(APPStrings.Assistance.MyTicketsBtn.title, for: .normal)
        ticketsButton.setTitleColor(AppStyle.Colors.mainBlue, for: .normal)
        ticketsButton.setTitleColor(AppStyle.Colors.lightBlue, for: .highlighted)
        ticketsButton.contentHorizontalAlignment = .left
        faqButton.titleLabel?.font = AppStyle.Buttons.medium16.font
        faqButton.setTitle(APPStrings.Assistance.FaqBtn.title, for: .normal)
        faqButton.setTitleColor(AppStyle.Colors.mainBlue, for: .normal)
        faqButton.setTitleColor(AppStyle.Colors.lightBlue, for: .highlighted)
        faqButton.titleLabel?.textAlignment = .left
        faqButton.contentHorizontalAlignment = .left

        ticketsTapIndicatorImageView.image = Asset.Subscription.redArrow.image
        faqTapIndicatorImageView.image = Asset.Subscription.redArrow.image
        
        
        complaintsView.applyShadow(cornerRadius: 5)
        faqView.applyShadow(cornerRadius: 5)
        
        // configure tabbar item
        let mapTabBarItem = UITabBarItem(title: APPStrings.TabBar.reportTitle, image: Asset.TabBar.report.image, selectedImage: Asset.TabBar.report.image)
        tabBarItem = mapTabBarItem
        
        chatBtn.backgroundColor = AppStyle.Colors.red
        chatBtn.applyShadow(color: AppStyle.Colors.black.withAlphaComponent(0.5), radius: 16, cornerRadius: chatBtn.frame.width/2)
        
        timeLabel.text = APPStrings.Assistance.ChatTime.title
        timeLabel.textColor = AppStyle.Colors.white
        timeLabel.font = AppStyle.Text.bold(size: 14).font
        
        phoneLabel.text = "0" + Constant.ClientSupport.phoneNumber.replacingOccurrences(of: " ", with: ".")
        phoneLabel.textColor = AppStyle.Colors.white
        phoneLabel.font = AppStyle.Text.bold(size: 14).font

    }

    // MARK: - Actions
    @IBAction func navigateBtnPressed(_ sender: UIButton) {
        
        switch sender {
        case faqButton:
            viewModel.navigateToPdfViewer(withPdf: .faq)
        case ticketsButton:
            
            viewModel.navigateToMyTicketsList()
        default:
            break
        }
    }
    @IBAction func chatBtnPressed(_ sender: RoundedButton) {
        viewModel.navigateToChat()
    }
    @IBAction func phoneBtnPressed(_ sender: Any) {
        viewModel.callSupport()
    }
    
}
