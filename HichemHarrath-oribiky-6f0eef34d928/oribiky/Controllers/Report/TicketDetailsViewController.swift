//
//  TicketDetailsViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 03/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class TicketDetailsViewController: BaseViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconContainerView: UIView!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    @IBOutlet weak var tableView: UITableView!
    //MARK: - Variables
    var viewModel: TicketDetailsViewModel!
    
    
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    func configureWithViewModel() {
        
    }
    
    // MARK: - Private
    private func setupView() {
        //add menu && notification
        addRighNavigationItems()

        addBackNavigationItem()
        
        title = APPStrings.Assistance.MyTicketsList.title
        
        
        headerView.backgroundColor = AppStyle.Colors.lightBlue
        
        iconContainerView.backgroundColor = AppStyle.Colors.mainBlue
        iconContainerView.round()
        
        idLabel.font = AppStyle.Text.medium(size: 14).font
        idLabel.textColor = AppStyle.Colors.mainBlue
        descriptionLabel.font = AppStyle.Text.regular(size: 14).font
        descriptionLabel.textColor = AppStyle.Colors.white
        //Fill in fields
        iconImageView.image = viewModel.ticket.statusImage
        idLabel.text = "#" + viewModel.ticket.id
        descriptionLabel.text = viewModel.ticket.subject
        
        let nib = UINib.init(nibName: String(describing: TicketEventTableViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: TicketEventTableViewCell.self))
        tableView.dataSource = self

        tableView.separatorStyle = .none
    }

}

//MARK: - UITableViewDataSource
extension TicketDetailsViewController:  UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TicketEventTableViewCell.self), for: indexPath) as? TicketEventTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(to: TicketEventViewModel(event: viewModel.events[indexPath.row]))
        return cell
    }
}
