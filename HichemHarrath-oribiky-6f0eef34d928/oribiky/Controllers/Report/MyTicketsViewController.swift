//
//  MyTicketsViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 28/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class MyTicketsViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {
    
    // MARK: - Variables
    var viewModel: MyTicketsViewModel!
    var tickets = [TicketViewModel]() {
        didSet {
            if tickets.count == 0 {
                noTicketsLabel.text = APPStrings.Assistance.MyTicketsList.noTicketsTitle
            } else {
                noTicketsLabel.text = nil
            }

            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var createTicketButton: UIButton!
    @IBOutlet weak var headerVIew: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var noTicketsLabel = UILabel()
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        startLoading()
        viewModel.callWs(success: { [weak self] (tickets) in
            self?.stopLoading()
            self?.tickets = tickets.compactMap({TicketViewModel(ticket: $0)})
        }) { [weak self] (errorStr) in
            self?.stopLoading()
            self?.viewModel.showErrorPopuo(subtitle: errorStr)
        }

    }
    // MARK: - Private
    private func setupView() {
        
        //add menu && notification
        addRighNavigationItems()

        addBackNavigationItem()
        
        title = APPStrings.Assistance.MyTicketsList.title
        
        headerVIew.backgroundColor = AppStyle.Colors.white
        createTicketButton.backgroundColor = AppStyle.Colors.mainBlue
        createTicketButton.setTitleColor(AppStyle.Colors.white, for: .normal)
        createTicketButton.titleLabel?.font = AppStyle.Buttons.medium16.font
        createTicketButton.setTitle(APPStrings.Assistance.MyTicketsList.CreateTicketBtn.title, for: .normal)
        
        let nib = UINib.init(nibName: String(describing: TicketTableViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: TicketTableViewCell.self))
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorColor = AppStyle.Colors.mainBlue
        tableView.separatorInset = .zero
        tableView.backgroundColor = AppStyle.Colors.lightBlue
        
        noTicketsLabel.frame = tableView.bounds
        noTicketsLabel.font = AppStyle.Text.medium(size: 16).font
        noTicketsLabel.textColor = AppStyle.Colors.white
        noTicketsLabel.text = nil
        noTicketsLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        noTicketsLabel.textAlignment = .center
        tableView.addSubview(noTicketsLabel)

    }
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
    }
    //MARK:- Actions
    @IBAction func createBtnPressed(_ sender: RoundedButton) {
        viewModel.navigateToCreateTicket()
    }
    

}

//MARK: - UITableViewDataSource
extension MyTicketsViewController:  UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TicketTableViewCell.self), for: indexPath) as? TicketTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(to: tickets[indexPath.row])
        return cell
    }
}


//MARK: - UITableViewDataSource
extension MyTicketsViewController:  UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.navigateToTicketDetails(ticketViewModel: tickets[indexPath.row])
    }
    
}
