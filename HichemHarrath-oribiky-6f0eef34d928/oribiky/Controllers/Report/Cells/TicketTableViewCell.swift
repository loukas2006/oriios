//
//  TicketTableViewCell.swift
//  oribiky
//
//  Created by Aymen Harrath on 28/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class TicketTableViewCell: UITableViewCell, ViewModelConfigurable {
    
    //MARK: - Variables
    var viewModel: TicketViewModel!
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconContainerView: UIView!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    //MARK: - View Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }
    
    
    func configureWithViewModel() {
        
        iconImageView.image = viewModel.ticket.statusImage
        idLabel.text = "#" + viewModel.ticket.id
        descriptionLabel.text = viewModel.ticket.subject
        dateLabel.text = viewModel.ticket.date?.getTicketDateStr()
        
    }

    
    //MARK: - Private
    private func setupView () {
        selectionStyle = .none
        
        backgroundColor = AppStyle.Colors.lightBlue
        
        iconContainerView.backgroundColor = AppStyle.Colors.mainBlue
        iconContainerView.round()
        
        idLabel.font = AppStyle.Text.medium(size: 13).font
        idLabel.textColor = AppStyle.Colors.mainBlue
        
        descriptionLabel.font = AppStyle.Text.regular(size: 13).font
        descriptionLabel.textColor = AppStyle.Colors.white
        
        dateLabel.font = AppStyle.Text.regular(size: 11).font
        dateLabel.textColor = AppStyle.Colors.white.withAlphaComponent(0.6)

        dateImageView.image = Asset.Assistance.dateReclamation.image

        separatorView.backgroundColor = AppStyle.Colors.clear
        
    }

}
