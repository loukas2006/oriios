//
//  TicketEventTableViewCell.swift
//  oribiky
//
//  Created by Aymen Harrath on 03/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class TicketEventTableViewCell: UITableViewCell, ViewModelConfigurable {
    

    //MARK: - Variables
    
    var viewModel: TicketEventViewModel!

    @IBOutlet weak var conversationTitleLabel: UILabel!
    @IBOutlet weak var conversationMessageLabel: UILabel!
    @IBOutlet weak var horizentalPaddingView: UIView!
    @IBOutlet weak var horizentalPaddingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var verticalPaddingView: UIView!
    @IBOutlet weak var verticalPaddingWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var verticalLineView: UIView!
    @IBOutlet weak var ticketEditedView: UIView!
    @IBOutlet weak var ticketEditedLabelBgView: UIView!
    @IBOutlet weak var ticketEditedLabel: UILabel!

    //MARK: - View Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }
    
    func configureWithViewModel() {
        guard let eventType = viewModel.event.type else {return}
        print("eventType == \(eventType)")
        switch eventType {
        case EventType.message.rawValue:
            ticketEditedView.isHidden = true
            
            if let type = viewModel.event.author?.type,  type == .client {
                conversationTitleLabel.text = viewModel.event.title.uppercased()
                conversationMessageLabel.text = viewModel.event.message

                verticalPaddingWidthConstraint.constant = 0
                horizentalPaddingHeightConstraint.constant = 0
            } else {
                conversationTitleLabel.text = viewModel.event.title.uppercased()
                conversationMessageLabel.text = viewModel.event.message
                verticalPaddingWidthConstraint.constant = 20
                horizentalPaddingHeightConstraint.constant = 10
            }
        case EventType.assigneeChanged.rawValue:
            ticketEditedView.isHidden = false
            ticketEditedLabel.text = APPStrings.Assistance.TicketDetails.assigneeChanged(viewModel.event.assignee?.name ?? "")
        case EventType.statusChanged.rawValue:
            ticketEditedView.isHidden = false
            
            ticketEditedLabel.text = APPStrings.Assistance.TicketDetails.statusChanged(TicketStatus(rawValue: viewModel.event.previous ?? "")?.title ?? "", TicketStatus(rawValue: viewModel.event.current ?? "")?.title ?? "")
        default:
            break
            
        }
    }

    //MARK: - Private
    private func setupView () {
        
        selectionStyle = .none
        
        horizentalPaddingView.backgroundColor = AppStyle.Colors.clear
        verticalPaddingView.backgroundColor = AppStyle.Colors.clear
        verticalPaddingView.clipsToBounds = true
        horizentalPaddingView.clipsToBounds = true
        verticalLineView.backgroundColor = AppStyle.Colors.mainBlue
        
        conversationTitleLabel.font = AppStyle.Text.bold(size: 12).font
        conversationMessageLabel.font  = AppStyle.Text.regular(size: 12).font
        
        conversationTitleLabel.textColor = AppStyle.Colors.mainBlue
        conversationMessageLabel.textColor = AppStyle.Colors.mainBlue

        ticketEditedLabel.font  = AppStyle.Text.medium(size: 12).font
        ticketEditedLabel.textColor = AppStyle.Colors.mainGray
        ticketEditedLabel.textAlignment = .center
        
        ticketEditedLabelBgView.backgroundColor = AppStyle.Colors.lightGray

        
        //initial values
        conversationTitleLabel.text = nil
        conversationMessageLabel.text = nil
        verticalPaddingWidthConstraint.constant = 0
        horizentalPaddingHeightConstraint.constant = 0


    }
}
