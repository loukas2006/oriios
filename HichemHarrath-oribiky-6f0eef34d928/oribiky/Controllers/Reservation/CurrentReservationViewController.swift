//
//  CurrentReservationViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 05/01/2019.
//  Copyright © 2019 Oribiky. All rights reserved.
//

import UIKit
import MapKit

class CurrentReservationViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {
    
    //MARK: - Variables
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var distanceImageView: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var cancelBtn: RoundedButton!
    @IBOutlet weak var validateBtn: RoundedButton!
    
    @IBOutlet weak var mapView: MKMapView!
    
    var viewModel: CurrentReservationViewModel!
    var timer: Timer?
    
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
            
            guard let `self` = self else {return}
            
            let remainigSeconds = self.viewModel.remainingReservationTime()
            self.timeLabel.text = CurrentReservationViewController.time(remainigSeconds)
            if remainigSeconds <= 0 {
                timer.invalidate()
                //update current reservation
                BikeRentManager.shared.updateCurrentReservation(reservation: nil)

                self.viewModel.goBackToTabBar()
            }
            let remainingDistance = self.viewModel.remainingReservationDistance(userLocation: self.mapView.userLocation.location)
            self.distanceLabel.text = "\(round(1000 * (remainingDistance / 1000)) / 1000 ) km"
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    static func time(_ seconds: Int) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        return formatter.string(from: TimeInterval(seconds))!
    }

    
    //MARK: - ViewModelConfigurable
    
    func configureWithViewModel() {
        tabBarItem = UITabBarItem(title: APPStrings.TabBar.mapTitle, image: Asset.TabBar.map.image, selectedImage: Asset.TabBar.map.image)

    }
    
    //MARK: - Private
    
    private func setupView () {
        
        //add menu && notification
        addRighNavigationItems()

        title = APPStrings.Reservation.title
        
        distanceView.backgroundColor = AppStyle.Colors.mainBlue
        timeView.backgroundColor = AppStyle.Colors.mainBlue
        
        distanceLabel.font = AppStyle.Text.bold(size: 12).font
        timeLabel.font = AppStyle.Text.bold(size: 12).font
        
        distanceLabel.textColor = AppStyle.Colors.white
        timeLabel.textColor = AppStyle.Colors.white

        distanceImageView.image = Asset.Ride.distance.image
        timeImageView.image = Asset.Ride.time.image
        
        validateBtn.setTitle(APPStrings.Common.rent.uppercased(), for: .normal)
        validateBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        validateBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        validateBtn.backgroundColor = AppStyle.Colors.mainBlue
        
        cancelBtn.setTitle(APPStrings.Common.cancel.uppercased(), for: .normal)
        cancelBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        cancelBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        cancelBtn.backgroundColor = AppStyle.Colors.red
        
        timeLabel.text = nil
        distanceLabel.text = nil

        mapView.showsUserLocation = true
        mapView.delegate = self

        if let bike = viewModel.reservation?.bike {
            let annotation = BikeAnnotation(bike)
            mapView.addAnnotation(annotation)
            centerMapOnLocation()
        }

    }
    
    func centerMapOnLocation() {
        
        var zoomRect = MKMapRectNull
        for annotation in mapView.annotations {
            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1)
            zoomRect = MKMapRectUnion(zoomRect, pointRect)
        }
        mapView.setVisibleMapRect(MKMapRectInset(zoomRect, 0.1, 0.1), animated: true)
    }
    
    @IBAction func cancelBtnPressed(_ sender: RoundedButton) {
        self.startLoading()
        viewModel.callCancelReservationWS(success: {
           print("didCancel")
            self.stopLoading()
            self.viewModel.goBackToTabBar()
        },failure: { (error) in
            self.stopLoading()
            self.viewModel.showErrorPopuo(subtitle: error)
        })
    }
    
    @IBAction func validateBtnPressed(_ sender: RoundedButton) {
        viewModel.goToConnectBike()
    }
}

extension CurrentReservationViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let bikeAnnotation = annotation as? BikeAnnotation {
            
            var annotationView: BikeAnnotationView
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: String(describing: BikeAnnotationView.self)) as? BikeAnnotationView {
                dequeuedView.annotation = bikeAnnotation
                annotationView = dequeuedView
            } else {
                annotationView = BikeAnnotationView(annotation: annotation, reuseIdentifier: String(describing: BikeAnnotationView.self))
            }
            
            annotationView.clusteringIdentifier = bikeAnnotation.type.rawValue
            
            return annotationView

        }
        
        return nil
        
    }

    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        centerMapOnLocation()
    }
}
