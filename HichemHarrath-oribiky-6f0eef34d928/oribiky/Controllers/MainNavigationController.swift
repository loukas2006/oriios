//
//  MainNavigationController.swift
//  oribiky
//
//  Created by Aymen Harrath on 16/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNavigationBar()
        
        view.backgroundColor = AppStyle.Colors.mainBlue
    }
    
    //MARK: - Private
    private func setupNavigationBar () {
        
//        //nav bar
//        navigationBar.barTintColor = AppStyle.Colors.mainBlue
//        navigationBar.isOpaque = true


        //title
        let titleAttributesDict = [NSAttributedStringKey.font : AppStyle.Text.medium(size: 17).font,
                                   NSAttributedStringKey.foregroundColor : AppStyle.Colors.white]
        navigationBar.titleTextAttributes = titleAttributesDict
        
    }
}

// MARK: - Private

private extension MainNavigationController {
    func setupUI() {
        

        interactivePopGestureRecognizer?.delegate = self
        delegate = self
    }
}

// MARK: - UINavigationControllerDelegate

extension MainNavigationController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if navigationController.viewControllers.count > 1 {
            self.interactivePopGestureRecognizer?.isEnabled = true
        } else {
            self.interactivePopGestureRecognizer?.isEnabled = false
        }
        
        //check only for pop
        
        if let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from),
            let toViewController = navigationController.transitionCoordinator?.viewController(forKey: .to),
            navigationController.viewControllers.contains(toViewController),
            !navigationController.viewControllers.contains(fromViewController) {
            //it's pop
           // AppCoordinator.shared.pop(animated: true)
            AppCoordinator.shared.updateCurrent(controller: toViewController)
        }
    }
}

// MARK: - UIGestureRecognizerDelegate
extension MainNavigationController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
