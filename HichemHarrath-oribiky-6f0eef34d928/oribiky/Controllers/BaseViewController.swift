//
//  BaseViewController.swift
//  oribiky
//
//  Created by Hichem Harrath on 17/01/2019.
//  Copyright © 2019 Oribiky. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(handleNewNotificationsNumberNotification), name: Notification.Name(rawValue: Constant.newNotificationsNumberNotificationName), object: nil)
    }
    
    @objc func handleNewNotificationsNumberNotification () {
        updateNotificationBadgeValue(value: BikeRentManager.shared.getNewNotificationsCount() == 0 ? "" : "\(BikeRentManager.shared.getNewNotificationsCount())")
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name:Notification.Name(Constant.newNotificationsNumberNotificationName) , object: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
