//
//  PDFViewerViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 13/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class PDFViewerViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {
    //MARK: - Variables
    var viewModel: PdfViewerViewModel!
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
        
        loadPDF()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func configureWithViewModel() {
        
    }

    //MARK: - Private

    private func loadPDF () {
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = AppStyle.Colors.mainBlue
        
        if let request = viewModel.pdf.request {
            self.activityIndicator.startAnimating()
            webView.loadRequest(request)
        } else {
            viewModel.showErrorPopuo(subtitle: "Impossible de télécharger le fichier")
        }
    }
    
    private func setupView () {
        //add menu && notification
        addRighNavigationItems()

        //navigation
        addBackNavigationItem()
        //view
        title = viewModel.pdf.title
        //webview
        webView.delegate = self
        
        webView.backgroundColor = AppStyle.Colors.white
    }
    

}

extension PDFViewerViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()
    }
}
