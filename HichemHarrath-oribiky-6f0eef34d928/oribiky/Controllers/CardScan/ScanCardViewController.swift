//
//  ScanCardViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 29/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class ScanCardViewController: BaseViewController, ViewModelConfigurable, StoryboardIdentifiable {
    var viewModel: ScanCardViewModel!

    // MARK: - Variables
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var scanView: CardIOView!
    
    
    // MARK: - View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        scanView.hideCardIOLogo = true

        //user's device is capable of scanning cards
        if CardIOUtilities.canReadCardWithCamera() {
            
            scanView.delegate = self
            //Call to speed up the subsequent launch of card.io scanning
            CardIOUtilities.preloadCardIO()
            
        }else {
            //present alert : unable to scan card due to device problem
        }
        
    }

     // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
    }

    // MARK: - private
    private func setupView() {
        
        //add menu && notification
        addRighNavigationItems()

        addBackNavigationItem()
        
        title = APPStrings.ScanCard.title
        
        messageLabel.font = AppStyle.Text.regular(size: 14).font
        messageLabel.textColor = AppStyle.Colors.black
        
        scanView.guideColor = AppStyle.Colors.mainBlue
        scanView.scanInstructions = APPStrings.ScanCard.userGuideMessage

    }
}

extension ScanCardViewController: CardIOViewDelegate {

    func cardIOView(_ cardIOView: CardIOView!, didScanCard cardInfo: CardIOCreditCardInfo!) {
        viewModel.scanCompletionHandler(cardInfo)
    }
    
    
}
