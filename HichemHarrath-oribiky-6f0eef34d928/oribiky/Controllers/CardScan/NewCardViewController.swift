//
//  NewCardViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 24/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class NewCardViewController: BaseViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    
    // MARK: - Variables
    var viewModel: EditProfileViewModel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var termsOfUseAndSaleLabel: UILabel!
    @IBOutlet weak var termsOfUseAndSaleSwitch: UISwitch!

    @IBOutlet weak var confirmBtn: RoundedButton!
    @IBOutlet weak var cardScanBtn: RoundedButton!
    

    var newCardViewModel: ScanCardViewModel!
    
    
    var cellsDataSource = [EditableFormItemViewModel]()
    
    // MARK: - View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //datasource
        cellsDataSource = viewModel.prepareDataSource()

        //did scan card
        newCardViewModel =  ScanCardViewModel(completion: { [weak self] (cardInfo) in

            AppCoordinator.shared.pop(animated: true)
            guard let strongSelf = self else { return }
            strongSelf.fillInScannedCardInfo(cardInfo: cardInfo)
        })

        setupView ()
        
    }
    
    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
    }
    
    
    //MARK: - Private
    private func setupView () {
        //add menu && notification
        addRighNavigationItems()

        //title
        title = APPStrings.NewCard.title
        
        //tableView
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib.init(nibName: String(describing: EditFormItemTableViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: EditFormItemTableViewCell.self))
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        
        
        
        //Terms of sale elements
        let rangeStartLocation = APPStrings.NewCard.checkBoxActionTitle.count
        var attributedString = NSMutableAttributedString(string: APPStrings.NewCard.acceptTermOfUseAndSaleBtnTitle)
        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location:rangeStartLocation , length: attributedString.length - rangeStartLocation))
        termsOfUseAndSaleLabel.attributedText = attributedString
        termsOfUseAndSaleLabel.font = AppStyle.Text.regular(size: 12).font
        
        //Confirm btn
        confirmBtn.backgroundColor = AppStyle.Colors.mainBlue
        confirmBtn.setTitle(APPStrings.NewCard.confirmBtnTitle.uppercased(), for: .normal)
        confirmBtn.setTitleColor(AppStyle.Colors.white, for: .normal)
        confirmBtn.titleLabel?.font = AppStyle.Buttons.medium12.font
        confirmBtn.hasShadow = true
        
        //termsOfUseAndSaleSwitch
        termsOfUseAndSaleSwitch.onTintColor = AppStyle.Colors.mainBlue
        termsOfUseAndSaleSwitch.transform = CGAffineTransform(scaleX: 0.8, y: 0.75)
        
        //cardScan btn
        attributedString = NSMutableAttributedString(string: APPStrings.NewCard.scanBtnTitle.uppercased())
        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: attributedString.length))
        cardScanBtn.backgroundColor = AppStyle.Colors.clear
        cardScanBtn.setAttributedTitle(attributedString, for: .normal)
        cardScanBtn.setTitleColor(AppStyle.Colors.mainBlue, for: .normal)
        cardScanBtn.titleLabel?.font = AppStyle.Text.regular(size: 12).font
        
        //navigation
        addBackNavigationItem()
        
    }
    
    private func fillInScannedCardInfo (cardInfo: CardIOCreditCardInfo) {
        
        viewModel.editableItem(forType: .cardNumber, in: cellsDataSource)?.editedValue = cardInfo.cardNumber
        viewModel.editableItem(forType: .expireDate, in: cellsDataSource)?.editedValue = "\(cardInfo.expiryMonth)" + AppStyle.expirationDateSeparator + "\(cardInfo.expiryYear)"
        tableView.reloadData()

    }
    
    //MARK: - Actions
    
    func addCard() {

        if viewModel.isDataSourceValid(dataSource: cellsDataSource) {
            startLoading()
            
            viewModel.callAddCardWs(dataSource: cellsDataSource, redirection: { [weak self] in
                guard let `self` = self else {return}
                self.stopLoading()
                
                StripePaymentManager.shared.threeDSRedirectionPopup(redirectionViewController: self, completion: { (succeeded) in
                    if succeeded {
                        
                        self.startLoading()
                        self.viewModel.submitCardWs(dataSource: self.cellsDataSource, success: {[weak self] (userProfile) in
                            
                            self?.stopLoading()
                            self?.viewModel.handleAddCardSuccess()
                            
                            }, failure: { [weak self] (errorStr, errors) in
                                guard let strongSelf = self else {return}
                                if let errorsArray = errors {
                                    for item in strongSelf.cellsDataSource {
                                        item.wsErrorStr = errorsArray[item.type.rawValue]?.first
                                    }
                                } else {
                                    strongSelf.viewModel.showErrorPopuo(subtitle: APPStrings.NewCard.CardValidateError.subtitle)
                                }
                                strongSelf.stopLoading()
                                strongSelf.tableView.reloadData()
                        })
                    } else {
                        
                    }
                })
                }, success: {[weak self] (userProfile) in
                    self?.stopLoading()
                    self?.viewModel.handleAddCardSuccess()
                    
                }, failure: { [weak self] (errorStr, errors) in
                    guard let strongSelf = self else {return}
                    if let errorsArray = errors {
                        for item in strongSelf.cellsDataSource {
                            item.wsErrorStr = errorsArray[item.type.rawValue]?.first
                        }
                    } else {
                        strongSelf.viewModel.showErrorPopuo(subtitle: APPStrings.NewCard.CardValidateError.subtitle)
                    }
                    strongSelf.stopLoading()
                    strongSelf.tableView.reloadData()
            })
            
        }
        tableView.reloadData()
    }
    
    @IBAction func confirmBtnPressed(_ sender: RoundedButton) {
        
        if !newCardViewModel.isTermsAccepted() {
            newCardViewModel.showTermsErrorPopUp()
        } else if !BikeRentManager.shared.getUser().haveCard() {
            newCardViewModel.showCautionRequiredPopup { [weak self] in
                self?.addCard()
            }
        } else {
            self.addCard()
        }
        
    }
    
    @IBAction func cardScanBtnPressed(_ sender: RoundedButton) {
        viewModel.navigateToScanCard(newCardViewModel: newCardViewModel)
    }
    
    @IBAction func termsOfUseAndSaleSwitchPressed(_ sender: UISwitch) {
        newCardViewModel.toggleOption(option: .termsOfUseAndSale, value: sender.isOn)
    }
    

    @IBAction func showTermsOfSaleBtnPressed(_ sender: UIButton) {
        newCardViewModel.navigateToPdfViewer(withPdf: .termsOfUseAndSale)
    }
    
}


extension NewCardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: EditFormItemTableViewCell.self), for: indexPath) as? EditFormItemTableViewCell else {
            return UITableViewCell()
        }
        let item = cellsDataSource[indexPath.row]
        cell.configure(to: item)
        cell.configureCompletionHandler = { editedValue in
            item.updateEditValue(v: editedValue)
            
        }

        return cell
    }
    
}

