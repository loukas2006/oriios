//
//  LaunchViewController.swift
//  oribiky
//
//  Created by Aymen Harrath on 27/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController, StoryboardIdentifiable, ViewModelConfigurable {
    
    // MARK: - Varibales
    var viewModel: LaunchViewModel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        activityIndicator.startAnimating()
        viewModel.updateUserLaunchData(completion:{ [weak self] in
            guard let `self` = self else { return }
            self.activityIndicator.stopAnimating()
            self.viewModel.showApplicationHome()
        })

    }

    // MARK: - ViewModelConfigurable
    func configureWithViewModel() {
        
    }
    
    // MARK: - Private
    private func setupView () {
        
        view.backgroundColor = AppStyle.Colors.black
    }
}
