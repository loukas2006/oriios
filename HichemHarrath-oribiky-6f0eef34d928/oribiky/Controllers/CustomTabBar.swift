//
//  CustomTabBar.swift
//  oribiky
//
//  Created by Aymen Harrath on 30/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class CustomTabBar: UITabBar {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        items?.forEach({ (item) in
            item.titlePositionAdjustment = UIOffsetMake(0, -5.0)
        })
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        
        super.sizeThatFits(size)
        guard let window = UIApplication.shared.keyWindow else {
            return super.sizeThatFits(size)
        }
        var sizeThatFits = super.sizeThatFits(size)
        if #available(iOS 11.0, *) {
            sizeThatFits.height = window.safeAreaInsets.bottom + 63
        } else {
             sizeThatFits.height = 63
        }
        return sizeThatFits
    }
    
    
}
