//
//  LocationManager.swift
//  oribiky
//
//  Created by Hichem Harrath on 10/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import CoreLocation

enum LocationManagerAuthorizationStatus {
    case notDetermined
    case notAccorded
    case accorded// if we have the permission we should send user location
}
class LocationManager: NSObject {
    
    // MARK: - Types
    enum LocationMangerMode {
        case unknown
        case gettingUserLocation
        case trakingRide
    }


    // MARK: - Singleton
    static let shared = LocationManager()
    // MARK: - Init
    override init() {
        super.init()
        //set location manager delegate
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
    }
    // MARK: - Variables
    var rideLocations = [CLLocationCoordinate2D]()
    var rideDistance = Measurement(value: 0, unit: UnitLength.meters)
    var userCurrentLocation: CLLocation?
    
    private let locationManager = CLLocationManager()
    private var mode : LocationMangerMode = .unknown
    private var askAuthorizationCompletion: ((CLAuthorizationStatus) -> ())?
    private var userLocationCompletion: ((CLLocation?) -> ())?

    // MARK: - Public
    func verifyAuthorizationStatus(completion: @escaping (LocationManagerAuthorizationStatus) -> ()) {

        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            // user has not yet made a choice
            completion(.notDetermined)
        } else {
            if shouldAskLocationAuthorization() {
                // user denied or refused , we should ask again.
                completion(.notAccorded)
            } else {
                // all is good, we have authorization, we should get user location and call completion
                completion(.accorded)
            }
        }
    }
    
    func askAuthorization(completion: @escaping (CLAuthorizationStatus) -> ()) {
        askAuthorizationCompletion = completion
        locationManager.requestAlwaysAuthorization()
    }
    
    func getUserLocation(completion: @escaping (CLLocation?) -> Void) {
        mode = .gettingUserLocation
        userLocationCompletion = completion
        startLocationService()
    }
    
    func calculateDistance(locations : [BikeLocation]) -> Double {
        if locations.count == 0 {
            return 0
        }
        let coordinates = locations.compactMap({CLLocation(latitude: $0.latitude, longitude: $0.longitude)})
        var distance = 0.0
        for i in 0..<coordinates.count - 1 {
            distance += coordinates[i].distance(from: coordinates[i + 1])
        }
        return distance
    }
    
    func startTrackingRide(currentRideLocations: [BikeLocation] = []) {
        mode = .trakingRide
        rideLocations = currentRideLocations.compactMap({CLLocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude)})
        rideDistance = rideDistance + Measurement(value: calculateDistance(locations: currentRideLocations), unit: UnitLength.meters)
        startLocationService()
        
    }
    
    func endTrackingRide() {
        mode = .unknown
        rideLocations.removeAll()
        rideDistance = Measurement(value: 0, unit: UnitLength.meters)
        stopLocationService()
    }
    
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //saving user location
        userCurrentLocation = locations.last
        // call completion and provide user location
        if (mode == .gettingUserLocation) {
            userLocationCompletion?(locations.last)
            userLocationCompletion = nil
        } else if (mode == .trakingRide) {
            print(" locations count == %d", locations.count)
            handleTrackingLocations(locations)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        userLocationCompletion?(nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        askAuthorizationCompletion?(status)
    }
}
// MARK: - Private Helpers
extension LocationManager {
    
    private func shouldAskLocationAuthorization() -> Bool {
        let authStatus: CLAuthorizationStatus = CLLocationManager.authorizationStatus()
        return authStatus == .denied || authStatus == .restricted
    }
    
    private func startLocationService() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    private func stopLocationService() {
        locationManager.stopUpdatingLocation()
    }
    private func handleTrackingLocations(_ locations: [CLLocation] ) {
        
        for newLocation in locations {

            if let lastCoordinate = rideLocations.last {
                let delta = newLocation.distance(from: CLLocation(latitude: lastCoordinate.latitude, longitude: lastCoordinate.longitude))
                print("delta == \(delta)")
                if Measurement(value: delta, unit: UnitLength.meters) >= Measurement(value: 500, unit: UnitLength.meters) {
                    rideLocations.append(newLocation.coordinate)
                }
                rideDistance = rideDistance + Measurement(value: delta, unit: UnitLength.meters)
            } else {
                rideLocations.append(newLocation.coordinate)
            }
          //  print(" rideLocations count == %d", rideLocations.count)

        }
    }

}

extension LocationManager {
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
//        print("start geocoding....latitude = \(latitude)  longitude= \(longitude)")
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { placemarks, error in
//            print("start did answer....\n placemarks ==\(placemarks) error == \(error)")
            guard let placemark = placemarks?.first, error == nil else {
                completion(nil, error)
                return
            }
            completion(placemark, nil)
        }
    }
    //    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
    //        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1)}
    //    }
    
}
