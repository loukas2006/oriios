//
//  StripePaymentManager.swift
//  oribiky
//
//  Created by Aymen Harrath on 06/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import Stripe

class StripePaymentManager {

    //MARk: - Singleton
    static let shared = StripePaymentManager()
    //MARK: - Variables
    private var redirectionContext: STPRedirectContext?
    
    private var redirectionSource : STPSource?
    private var threeDSecureSourceId: String?
    //MARK: - Initializer
    init() {
        STPPaymentConfiguration.shared().publishableKey = HTTPAPI.Stripe.publishableKey
    }
    
    //MARK: - Public

    func addCardProcess(card: STPCardParams, redirection: @escaping () -> Void, success:@escaping (ProfileResponse) -> Void,
                        failure:@escaping BaseServiceFailure) {

        checkThreeDSecureSupport(card: card) { [weak self] (isRedirectionRequired, error, source) in
            guard let `self` = self else {return}
            if error != nil {
             print("a problem !! card fields are wrong !! ")
                return
            }
            self.redirectionSource = source

            if isRedirectionRequired {
                redirection()
            } else {
                self.threeDSecureSourceId = nil
                self.submit(withCardParams: card, success: success, failure: failure)
            }
        }
    }
    // submit card token to backend via add card WS
    func submit (withCardParams card: STPCardParams, success:@escaping (ProfileResponse) -> Void,
                 failure:@escaping BaseServiceFailure) {
        
        
        guard let source =  redirectionSource else {
            // Present error to user...
            failure("Une erreur est survenue veuillez réessayer",nil)
            return
        }

        let body =  PaymentRequestBody(aTokenId: self.threeDSecureSourceId ?? source.stripeID)
        PaymentService.charge(body: body, success: success, failure: failure)

        
        /*
        STPAPIClient.shared().createToken(withCard: card) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                // Present error to user...
                failure(error?.localizedDescription ?? "", nil)
                return
            }
            
            
            let body =  PaymentRequestBody(aTokenId: source.stripeID)
            PaymentService.charge(body: body, success: success, failure: failure)

            /*
            let body =  PaymentRequestBody(aTokenId: token.tokenId)
            PaymentService.addCard(body: body, success: { (_) in
                
                guard let source = self.redirectionSource else {
                    failure("no source", nil)
                    return
                }
                let body =  PaymentRequestBody(aTokenId: source.stripeID)
                PaymentService.charge(body: body, success: success, failure: failure)

            }, failure: failure)
            */
        }
 */
    }
    
    
    func checkThreeDSecureSupport (card: STPCardParams, completion: @escaping (Bool, Error?, STPSource?) -> Void) {
        /*
        let testCard = STPCardParams()
        testCard.name = "Harrath Aymen"
        testCard.number = "4242424242424242" //4000000000003063
        testCard.expMonth = 06
        testCard.expYear = 21
        testCard.cvc = "123"
        */
        let sourceParams = STPSourceParams.cardParams(withCard: card)
        sourceParams.amount = NSNumber(value: HTTPAPI.Stripe.cautionAmount)
        
        STPAPIClient.shared().createSource(with: sourceParams) {(source, error) in
            guard let source = source else {
                print("unable to get source")
                completion(false, error, nil)
                return
            }
            completion(source.cardDetails?.threeDSecure == .required, nil, source)
        }

    }
    

    func threeDSRedirectionPopup(redirectionViewController: UIViewController, completion: @escaping (Bool) -> Void) {
        
        guard let source = redirectionSource else {
            completion(false)
            return
        }
        let authorizeRedirectionAction = PopupAction(title: APPStrings.Common.ok, style: .validate) {
            // hide popup
            AppCoordinator.shared.pop(animated: true)
            
            let threeDSParams = STPSourceParams.threeDSecureParams(withAmount: HTTPAPI.Stripe.cautionAmount, currency: "eur", returnURL: "oribiky://stripe-redirect", card: source.stripeID)
            
            STPAPIClient.shared().createSource(with: threeDSParams, completion: { [weak self] (source, error) in
                
                
                guard let src = source else {
                    completion(false)
                    return
                }

                if let _ = src.redirect {
                    self?.redirectionContext = STPRedirectContext(source: src, completion: { (sourceId, clientSecret, error) in
                        print("did finish redirection porcess")
                        self?.threeDSecureSourceId = sourceId
                        completion(true)
                    })
                    self?.redirectionContext?.startRedirectFlow(from: redirectionViewController)
                } else {
                    print("redirect is nil can't redirect user")
                }
            })
        }
        let viewModel = PopupViewModel(type: .info(title: nil, subTitle: APPStrings.NewCard.ThreeDSecurePopup.subtitle, action: authorizeRedirectionAction))
        
        AppCoordinator.shared.transition(to: .popup(viewModel), type: .popup, animated: true)
    }
    
    
    func getCardBrandImage(brand: String) -> UIImage? {
        let cardBrand = STPCard.brand(from: brand)
        return cardBrand == .unknown ? nil : STPImageLibrary.brandImage(for: cardBrand)
    }

}
