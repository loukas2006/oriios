//
//  BikeRentManager.swift
//  oribiky
//
//  Created by Aymen Harrath on 09/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

class BikeRentManager {
    
    //MARK: - Singleton
    static let shared = BikeRentManager()
    
    //MARK: - Vars
    private var user: UserProfile?
    private var currentBike: Bike?
    private var currentRide: RideResponse?
    private var currentReservation: ReservationResponse?
    private var newNotifications: [NotificationViewModel]? {
        didSet {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.newNotificationsNumberNotificationName), object: nil)
        }
    }
    
    var hasRide : Bool {
        return currentRide != nil
    }
    var hasReservation : Bool {
        return currentReservation != nil
    }
    //MARK: - Public
    func updateUser(user: UserProfile?) {
        guard let user = user else { return }
        self.user = user
    }
    func updateBike(bike: Bike?) {
        currentBike = bike
    }
    func updateCurrentRide(ride: RideResponse?) {
        currentRide = ride
        currentBike = ride?.bike
    }
    func updateCurrentReservation (reservation: ReservationResponse?) {
        currentReservation = reservation
        currentBike = reservation?.bike
    }
    
    func updateNewNotifications(notifications: [NotificationViewModel]?) {
        newNotifications = notifications
    }
    
    func getNewNotificationsCount() -> Int {
        guard let notifications = newNotifications else { return 0 }
        return notifications.count
    }
    
    func getNewNotificationsList() -> [NotificationViewModel] {
        return newNotifications ?? [NotificationViewModel]()
    }
    
    func getUser() -> UserProfile {
        return user ?? UserProfile()
    }
    
    func getBike() -> Bike? {
        return currentBike
    }
    
    func getCurrentRide() -> RideResponse? {
        return currentRide
    }
    func getCurrentReservation() -> ReservationResponse? {
        return currentReservation
    }


    func getUSerSubscriptionType() -> SubscriptionType {
        guard let currentUser = user else {
            return .minutePrice
        }
       return SubscriptionType(rawValue: currentUser.subscription.details.type) ?? .minutePrice

    }
    
    func getUserSubscriptionDetails () -> [MySubscriptionDetailsViewModel] {
        var details = [MySubscriptionDetailsViewModel]()
        guard  let currentUser = user else {
            return details
        }
        
        var subscriptionItemDetailsValue = ""
        var subscriptionItemDetailsState = false
        var dateDetailsTitle = ""
        var dateDetailsValue = ""
        if (currentUser.isSubscriptionActive()) {
            subscriptionItemDetailsValue = "Active"
            subscriptionItemDetailsState = true
            dateDetailsTitle = "Prochaine date de renouvellement"
            dateDetailsValue = currentUser.subscription.billingPeriodEndsAt ?? ""
        } else {
            subscriptionItemDetailsValue = "Annulé"
            subscriptionItemDetailsState = false
            dateDetailsTitle = "Date fin abonnement :"
            dateDetailsValue = currentUser.subscription.endsAt ?? ""
        }
        
        let subscriptionItem = MySubscriptionDetailsViewModel(title: "Abonnement :", value: subscriptionItemDetailsValue, state: subscriptionItemDetailsState)
        let subscriptionDate = MySubscriptionDetailsViewModel(title: dateDetailsTitle, value: dateDetailsValue , state: nil)

        let packagesItem  = MySubscriptionDetailsViewModel(title: "Minutes prépayées dans mon compte :", value: "\(currentUser.packages.compactMap({  Int($0.credit) }).reduce(0, +)) minutes", state: nil)

        details = [subscriptionItem, subscriptionDate, packagesItem ]

        return details
    }


}
