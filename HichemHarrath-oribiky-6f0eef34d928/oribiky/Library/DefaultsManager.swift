//
//  DefaultsManager.swift
//  oribiky
//
//  Created by Hichem Harrath on 29/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct DefaultsManager {
    
    private struct Keys {
        static let token = "Token"
        static let pushDeviceToken = "PushToken"
        static let spalshWasShown = "SpalshWasShown"
    }

    // MARK: Variables
    static let shared = DefaultsManager()

    var isConnected: Bool {
        return token != ""
    }
    
    var shouldShowSplash: Bool {
        return !UserDefaults.standard.bool(forKey: Keys.spalshWasShown)
    }
    
    var token: String {
        return UserDefaults.standard.string(forKey: Keys.token) ?? ""
    }
    
    var deviceToken: String {
        return UserDefaults.standard.string(forKey: Keys.pushDeviceToken) ?? ""
    }
    
    // MARK: Public
    func saveToken(tokerStr: String) {
        let token = String(format: HTTPAPI.TokenFormat, tokerStr)
        UserDefaults.standard.set(token, forKey: Keys.token)
    }
    func removeToken() {
        UserDefaults.standard.removeObject(forKey: Keys.token)
        BikeRentManager.shared.updateUser(user: nil)
        BikeRentManager.shared.updateNewNotifications(notifications: [NotificationViewModel]())
    }
    
    func didShowSplash()  {
        UserDefaults.standard.set(true, forKey: Keys.spalshWasShown)
    }
    
    func saveDeviceToken(_ deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        UserDefaults.standard.set(token, forKey: Keys.pushDeviceToken)
    }
    
    func saveFCMToken(_ deviceToken: String) {
        UserDefaults.standard.set(deviceToken, forKey: Keys.pushDeviceToken)
    }
    
    
}
