//
//  ViewModelConfigurable.swift
//  oribiky
//
//  Created by Hichem Harrath on 22/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import UIKit

protocol ViewModelConfigurable {
    associatedtype ViewModelType
    
    var viewModel: ViewModelType! { get set }
    func configureWithViewModel()
}

extension ViewModelConfigurable where Self: UIViewController {
    
    mutating func configure(with model: ViewModelType) {
        viewModel = model
        loadViewIfNeeded()
        configureWithViewModel()
    }
}
extension ViewModelConfigurable where Self: UITableViewCell {
  
    mutating func configure(to model: ViewModelType) {
        viewModel = model
        configureWithViewModel()
    }
}

extension ViewModelConfigurable where Self: UIView {
    
    mutating func configure(to model: ViewModelType) {
        viewModel = model
        configureWithViewModel()
    }
}
