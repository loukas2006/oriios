//
//  StoryboardInitializable.swift
//  oribiky
//
//  Created by Aymen Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit


protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

extension StoryboardIdentifiable where Self: UIViewController {

    static var storyboardIdentifier: String {
        return String(describing: Self.self)
    }

}
