//
//  ReservationRequirementsProcessManager.swift
//  oribiky
//
//  Created by Hichem Harrath on 28/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

enum StepStatus {
    case created
    case started
    case ended
}

enum StepType {
    case authentication
    case payment
    case subscription(SubscriptionType)
}

struct Step {
    let type: StepType
    var status: StepStatus
    
    init(type: StepType) {
        self.type = type
        self.status = .created
    }
    
    mutating func start () {
        status = .started
    }
    mutating func end () {
        status = .ended
    }
    func isEnded() -> Bool {
        return status == .ended
    }


}
class ReservationRequirementsProcessManager {
   
    // MARK: - Variables
    static let shared = ReservationRequirementsProcessManager()

    private var processCompletionHandler : (()-> Void)?
  
    private var currentStep: Step?
    private var previousStep: Step?
    private var finalStep: Step?
    
    private var userSelectedSubscription: SubscriptionType?

    private var transitionForCurrentStep: SceneTransitionType {
        if let previsous = previousStep, previsous.status == .ended {
            return .push
        }
        return .modal
    }
    
    private var shouldEmbedStepInNavigation:  Bool {
        if let previsous = previousStep, previsous.status == .ended {
            return false
        }
        return true
    }
    
    // MARK: - Public
    func handleSteps ()  {
        NotificationCenter.default.addObserver(ReservationRequirementsProcessManager.shared, selector: #selector(stepsNotificationHandler(notif:)), name: Notification.Name(rawValue: Constant.reservationRequirementsNotificationName), object: nil)
    }
    func ignoreSteps ()  {
        NotificationCenter.default.removeObserver(ReservationRequirementsProcessManager.shared, name: Notification.Name(rawValue: Constant.reservationRequirementsNotificationName), object: nil)

    }
    @objc func stepsNotificationHandler(notif : Notification) {
        
        currentStep?.end()
        
        if let stepType = notif.object as? StepType {
            switch stepType {
            case .subscription(let type):
                userSelectedSubscription = type
            default:
                break
            }
        }
        
        start(finalStep: finalStep, completion: processCompletionHandler)
        
        if let stepType = notif.object as? StepType {
            switch stepType {
            case .subscription(_):
                currentStep = nil
            default:
                break
            }
        }

    }
    func start(finalStep: Step? = Step(type: .subscription(.none)), completion: (()-> Void)? ) {
        // save completion
        processCompletionHandler = completion
        self.finalStep = finalStep
        let user = BikeRentManager.shared.getUser()
        
        if !DefaultsManager.shared.isConnected {
            // user NOT connected
            // present Authentication
            previousStep = nil
            currentStep = Step(type: .authentication)
            currentStep?.start()
            showAuthentication()
        } else {
            if let step = finalStep, case StepType.authentication = step.type {
                if let current = currentStep , current.isEnded() {
                    AppCoordinator.shared.pop(animated: true) {
                        completion?()
                    }
                } else {
                    completion?()
                }
                return
            }
            // user is Connected
            // check if user has Card
            if !user.haveCard() {
                // user does NOT have a card
                // show add card
                previousStep = currentStep
                currentStep = Step(type: .payment)
                currentStep?.start()
                showAddCard()
            } else {
                if let step = finalStep, case StepType.payment = step.type {
                    if let current = currentStep , current.isEnded() {
                        AppCoordinator.shared.pop(animated: true) {
                            completion?()
                        }
                    } else {
                        completion?()
                    }
                    return
                }

                // check if user is subscribed
                if !user.isSubscribed() {
                    // user is NOT subscribed
                    // show subscription List
                    previousStep = currentStep
                    currentStep = Step(type: .subscription(SubscriptionType(rawValue: user.subscription.details.type) ?? .minutePrice))
                    currentStep?.start()
                    if let subscriptionType = userSelectedSubscription, subscriptionType == .minutePrice {
                        // dismiss to root
                        AppCoordinator.shared.pop(animated: true) {
                            completion?()
                        }
                        userSelectedSubscription = nil
                    } else {
                        showSubscriptionList()
                    }
                } else {
                    // all process is successfully finished
                    // user ready to continue bike rent
                    // TO DO: Check condiwtion
                    if let current = currentStep , current.isEnded() {
                        AppCoordinator.shared.pop(animated: true) {
                            completion?()
                        }
                    } else {
                        completion?()
                    }
                    
                }
            }
        }
    }
    // MARK: - Private
    private func showAuthentication() {
        AppCoordinator.shared.transition(to: .authenticate(AuthenticateViewModel(index: 0, pushed: true), shouldEmbedStepInNavigation),
                                         type: transitionForCurrentStep,
                                         animated: true)
    }
    
    private func showAddCard()  {
        
        let popupBtnAction = PopupAction(title: APPStrings.ScanCard.Popup.scanBtnTitle, style: .validate, action: { [weak self] in
            // cardViewModel
            guard let `self` = self else { return }
            let editProfileViewModel = EditProfileViewModel(aUserProfile:BikeRentManager.shared.getUser(), anEditType: .card, isFromAuthenticationProcess: true)
            
            AppCoordinator.shared.pop(animated: true, completion: {
                AppCoordinator.shared.transition(to: .newCard(editProfileViewModel, self.shouldEmbedStepInNavigation), type: self.transitionForCurrentStep, animated: true)
            })
        })
        
        let popupViewModel = PopupViewModel(type: .info(title: nil, subTitle: APPStrings.ScanCard.Popup.subtitle, action: popupBtnAction))
        
        AppCoordinator.shared.transition(to: .popup(popupViewModel), type: .popup, animated: true)
        
    }
    
    private func showSubscriptionList() {
        AppCoordinator.shared.transition(to: .subscriptionList(SubscriptionListViewModel(), shouldEmbedStepInNavigation), type: transitionForCurrentStep, animated: true)
    }

}
//// MARK: - BaseViewModelDelegate
//extension ReservationRequirementsProcessManager: BaseViewModelDelegate {
//
//    func userDidAuthenticate() {
//        // authentication succeeded
//        currentStep?.end()
//        start(completion: processCompletionHandler)
//
//    }
//
//    func userDidAddACard()  {
//        // add card succeeded
//        currentStep?.end()
//        start(completion: processCompletionHandler)
//    }
//
//    func userDidSubscribe()  {
//        // subscription succeeded
//        currentStep?.end()
//        start(completion: processCompletionHandler)
//        currentStep = nil
//    }
//}
