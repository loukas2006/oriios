//
//  ImagePickerManager.swift
//  oribiky
//
//  Created by Hichem Harrath on 06/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class ImagePickerManager: NSObject {
    
    // MARK: - Variables
    static let shared = ImagePickerManager()
    var imagePickedCompletion: ((UIImage?) -> Void)?

    // MARK: - Private
  
    func openCamera(completion: @escaping (UIImage?) -> Void){
        if  UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickedCompletion = completion
            AppCoordinator.shared.transition(to: .imagePicker(.camera, self), type: .modal, animated: true)
        }
    }
    
    func photoLibrary(completion: @escaping (UIImage?) -> Void){
       
        if  UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePickedCompletion = completion
            AppCoordinator.shared.transition(to: .imagePicker(.photoLibrary, self), type: .modal, animated: true)
        }
    }
}

extension ImagePickerManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePickedCompletion?(nil) // no image selected
        AppCoordinator.shared.pop(animated: true)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imagePickedCompletion?(image)
        } else {
            imagePickedCompletion?(nil)
        }
        // dismiss the uiimagepicker controller
        AppCoordinator.shared.pop(animated: true)
    }
}
