//
//  LiveChatManager.swift
//  oribiky
//
//  Created by Aymen Harrath on 05/12/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import LiveChat

class LiveChatManager: NSObject {
    //MARK: - Singleton
    static let shared = LiveChatManager()
    
    //MARK: - Init
    override init() {
        super.init()
        self.setup()
    }
    
    //MARK: - Private
    private func setup () {
        
        LiveChat.licenseId =  HTTPAPI.chatLicenseId // Set your licence number here
//        LiveChat.groupId = "0" // Optionally, you can set specific group
        
        // User name and email can be provided if known
        LiveChat.name = BikeRentManager.shared.getUser().firstname
        LiveChat.email = BikeRentManager.shared.getUser().email
        
        LiveChat.delegate = self

    }
    
    //MARK: - Actions
    
    func present () {
        //Presenting chat
        LiveChat.presentChat()
    }
    func clearSession () {
        //Clearing session
        LiveChat.clearSession()
    }

}

//MARK: - LiveChatDelegate
extension LiveChatManager: LiveChatDelegate {
    
    func received(message: LiveChatMessage) {
        if !LiveChat.isChatPresented {
            // Notifying user
        }
    }
    
    func handle(URL: URL) {
    }
    func chatPresented() {
    }
    func chatDismissed() {
//        self.clearSession()
    }

}
