//
//  LinkaManager.swift
//  SmartLock
//
//  Created by Aymen Harrath on 10/10/2018.
//  Copyright © 2018 Aymen Harrath. All rights reserved.
//

import Foundation
import LinkaAPIKit
import CoreLocation
import PromiseKit


enum LinkaSetupError: Error {
    case setupFetchToken
    case setupEmptyResponse
    case scanTimeOut
    case pairNotFound
    case pairNetwork
    case find
    
    var message: String {
        
        switch self {
        case .setupFetchToken:
            print("setupFetchToken error !")
        case .setupEmptyResponse:
            print("setupEmptyResponse error !")
        case .scanTimeOut:
            print("scanTimeOut error !")
        case .pairNotFound:
            print("pairNotFound error !")
        case .pairNetwork:
            print("pairNetwork error !")
        case .find:
            print("find error !")
        }
        return APPStrings.SmartLock.ConnectionError.message
    }
    
}

typealias LinkaInteraction = (communication: LinkaBLECommunicationManager, connection: LinkaBLEConnectManager)

class LinkaManager  {
    
    //MARK: - Variables
    private static var LinkaMerchantAPIKey = "3611db42-ec55-41eb-81fc-9c3a1dc8688b"
    private static var LinkaMerchantSecretKey = "753407e4-9402-40ae-9139-7b894f081103"
    
    static let shared = LinkaManager()
    
    private var scanCompletionHandler : ((Bool) -> Void)?
    private var connectedLinkas = [Linka]()

    private var linkaList  = [Linka]()
    private var peripheralList  = [DiscoveredPeripherals]()
    
    private var scanTimeOutTimer: Timer?

    private var setupFulfil: ((Bool) -> Void)?
    private var setupReject: ((LinkaSetupError) -> Void)?
    private var scanFulfil: (([DiscoveredPeripherals]) -> Void)?
    private var scanReject: ((LinkaSetupError) -> Void)?



    //MARK: - Init
    init() {
        LinkaLocationManager.shared.initializeLocationManager()
        LinkaAPIService.setAPIProtocol(_protocol: self)
        LocksController.initialize()
        _ = LinkaBLECentralManager.sharedInstance()
        /* Log Level
         * Set the Log Level of the LINKA API Kit
         *
         * You can set the Log to be saved as a String
         * You can also call Helpers.getDebugString() to get the debug log
         * Or Helpers.clearDebugString() to clear the string
         */
        Helpers.setLogLevel(.DEBUG_AND_SAVE_TO_STRING)

    }
    
    //MARK: - Public
    func setupAndScan(imei: String?) -> Promise<DiscoveredPeripherals> {
        return firstly {
            LinkaManager.shared.setup()
            }.then { _ in
                LinkaManager.shared.startScan()
            }.then { peripherals in
                LinkaManager.shared.find(imei: imei)
        }
    }
    
    //MARK: - Private
    // fetch access token to be able to scan peripherals
    private func setup() -> Promise<Bool> {
        return Promise { seal  in
            self.setupFulfil = seal.fulfill
            self.setupReject = seal.reject
            fetchAccessToken()
        }
    }

    // start discovering available peripherals
    private func startScan() -> Promise<[DiscoveredPeripherals]> {
        return Promise { seal  in
            self.scanFulfil = seal.fulfill
            self.scanReject = seal.reject
            
            LinkaBLECentralManager.sharedInstance().migrateDatabase() // TODO Refactor location
            LinkaBLECentralManager.sharedInstance().addDiscoverDelegate(delegate: self)
            LinkaBLECentralManager.sharedInstance().startScan()
            
            scanTimeOutTimer = Timer.scheduledTimer(withTimeInterval: Constant.Linka.scanTimeOut, repeats: false, block: { [weak self] (timer) in
                guard let `self` = self else {return}
                self.scanReject?(LinkaSetupError.scanTimeOut)
                self.stopScan()
                self.scanTimeOutTimer?.invalidate()
                self.scanTimeOutTimer = nil
            })

        }
        
    }

    // stop discovering peripherals
    private func stopScan() {
        LinkaBLECentralManager.sharedInstance().removeDiscoverDelegate(delegate: self)
        LinkaBLECentralManager.sharedInstance().stopScan()
        LinkaBLECentralManager.sharedInstance().scannedPeripherals.removeAll()
        peripheralList.removeAll()
        scanCompletionHandler = nil
    }
    //get perioheral for bike imei
    private func find (imei: String?) -> Promise<DiscoveredPeripherals> {
        return Promise{ seal in
            if let p = peripheralList.first(where: {Linka.makeLinka($0).getMACAddress() == imei}) {
                seal.fulfill(p)
            } else {
                seal.reject(LinkaSetupError.find)
            }
        }
    }
    
    func getMacAdress()->String{
    
        return BikeRentManager.shared.getBike()?.imei ?? ""
    }


    //paire specific peripheral to execute user actions
    func paire(peripheral: DiscoveredPeripherals) -> Promise<LinkaInteraction>{
        
        return Promise { seal in
            
            
            LinkaMerchantAPIService.tryPreparePairingUp( peripheral, progressCallback: { (isLoading, state) in
                if state == 0 {
                    // registering access keys
                    print("Registering access keys")
                } else if state == 1 {
                    // pairing up lock
                    print("Pairing up lock")
                }
            }, callback: { (linka, isValid, showError, errorCode) in
                
                if !isValid && showError {
                    // it's registration fail / network error
                    print("Error: \(errorCode)")
                    
                    seal.reject(LinkaSetupError.pairNetwork)
                    
                } else {
                    print("Successfully paired up the lock")
                }
            }, successCallback: {(connectManager, communicationManager) in
                if let comManager = communicationManager,let conManager = connectManager {
                    seal.fulfill((comManager,conManager))
                } else {
                    seal.reject(LinkaSetupError.pairNotFound)
                }
            })
            
        }
    }

}

//MARK: - LinkaAPIProtocol
extension LinkaManager: LinkaAPIProtocol {
    
    func Linka_locationManager() -> CLLocationManager! {
        return LinkaLocationManager.shared.locationManager
    }
    
    func LinkaMerchantAPI_getAPIKey() -> String! {
        return LinkaManager.LinkaMerchantAPIKey
    }
    
    func LinkaMerchantAPI_getSecretKey() -> String! {
        return LinkaManager.LinkaMerchantSecretKey
    }
    
    func LinkaMerchantAPI_getIsButtonUsed() -> Bool {
        // IMPORTANT
        // Set this value to TRUE if the user must double press the power button to lock the device. If this value is TRUE, locking through app is disabled.
        // Set this value to FALSE if the locking command is sent through the app. If this value is FALSE, the device will not lock if any motion is detected.
        return false
    }
    
}

//MARK: - LinkaBLECentralManagerDiscoverDelegate
extension LinkaManager: LinkaBLECentralManagerDiscoverDelegate {

    func linka_discover_delegate_instance() -> AnyObject {
        return self
    }
    
    func onDiscoverNewPeripheral(discoveredPeripheral: DiscoveredPeripherals) {
        var connected : Bool = false
        for linka in connectedLinkas {
            if linka.getPeripheral().identifier.uuidString == discoveredPeripheral.peripheral.identifier.uuidString {
                // omit connected devices
                connected = true
            }
        }
        if !connected {
            var isUnique = true
            for p in peripheralList {
                if p === discoveredPeripheral {
                    isUnique = false
                }
            }
            if isUnique {
                peripheralList.append(discoveredPeripheral)
                linkaList.append(Linka.makeLinka(discoveredPeripheral))
                self.scanFulfil?(peripheralList)
                //Scan timeOut invalidate, we did find linka
                scanTimeOutTimer?.invalidate()

            }
        }
    }

}


//MARK: - Private
extension LinkaManager {
    
     func fetchAccessToken () {
        
        _ = LinkaMerchantAPIService.fetch_access_token { (responseObject, errorObject) in
            
            if let _ = errorObject {
                //failure
                self.setupReject?(LinkaSetupError.setupFetchToken)
            } else {
                if let _ = responseObject {
                    //success
                    self.setupFulfil?(true)
                    return
                }
                //failure
                self.setupReject?(LinkaSetupError.setupEmptyResponse)
            }
        }
    }

}
