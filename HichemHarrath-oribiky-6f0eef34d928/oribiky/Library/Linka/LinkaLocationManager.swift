//
//  LinkaLocationManager.swift
//  SmartLock
//
//  Created by Aymen Harrath on 10/10/2018.
//  Copyright © 2018 Aymen Harrath. All rights reserved.
//

import Foundation
import CoreLocation
import LinkaAPIKit

class LinkaLocationManager: NSObject {
    
    //MARK: - Variables
    var locationManager : CLLocationManager!
    var curLocation : CLLocation!
    
    //MARK: - Singleton
    static let shared = LinkaLocationManager()
    
    
    //MARK: - Methods
    func initializeLocationManager() {
        locationManager = LinkaMerchantAPIService.makeLocationManager()
        locationManager.delegate = self
        locationManager.startMonitoringSignificantLocationChanges()
    }

    
}

extension LinkaLocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        curLocation = locations.first
    }
}


