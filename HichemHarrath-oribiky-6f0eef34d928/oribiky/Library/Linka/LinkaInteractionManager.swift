//
//  LinkaInteractionManager.swift
//  oribiky
//
//  Created by Aymen Harrath on 12/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import LinkaAPIKit
import PromiseKit

enum LinkaOperationType {
    case lock
    case unlock
    case none
}

enum LinkaOperationState: Int {
    case started
    case stalled
    case inMove
    case inProgress
    case finished
    case unknown
    case lostConnection
    
}

enum LinkaInteractionError: Error {
    case problem
}


class LinkaInteractionManager: NSObject {
    
    //MARK: - Singleton
    static let shared = LinkaInteractionManager()
    
    //MARK: - Variables
    private var managers: LinkaInteraction!
    
    private var isConnected : Bool = false
    private var currentOperation: LinkaOperationType = .none

    private var lockFulfill: ((LinkaOperationState) -> Void)?
    private var lockReject: ((Error) -> Void)?
    
    private var unlockFulfill: ((LinkaOperationState) -> Void)?
    private var unlockReject: ((Error) -> Void)?

    //MARK: - Private
    private func lock (managers: LinkaInteraction) -> Promise<LinkaOperationState> {
        return Promise { [weak self] seal in
            guard let `self` = self else {return}
            self.lockFulfill = seal.fulfill
            self.lockReject = seal.reject
            if !managers.communication.doAction_Lock() {
                self.lockReject?(LinkaInteractionError.problem)
            }
        }
    }
    
    private func unlock (managers: LinkaInteraction) -> Promise<LinkaOperationState> {
        return Promise { [weak self] seal in
            guard let `self` = self else {return}
            self.unlockFulfill = seal.fulfill
            self.unlockReject = seal.reject
            if !managers.communication.doAction_Unlock() {
                self.unlockReject?(LinkaInteractionError.problem)
            }
        }
    }
    
    private func connect (managers: LinkaInteraction) -> Promise<Bool> {
        //save communication and connection managers for further use and listen for callbacks
        self.managers = managers
        handleConnectionStateCallback()
        handleLockStatusCallback()

        return Promise { seal in
            let connectionState = managers.connection.connect()
            managers.communication.onLockSettled = { [weak self] in
                guard let `self` = self else {return}
                _ = self.managers.communication.doAction_WriteAudioSetting(setting: 3)
                _ = self.managers.communication.doAction_SetUnlockedDisconnectedSleep(sleepSeconds: "86400")
                _ = self.managers.communication.doAction_SetLockSleep(sleepSeconds: "300")
                
                if connectionState {
                    seal.fulfill(true)
                } else {
                    seal.reject(LinkaInteractionError.problem)
                }
            }
            managers.communication.onLockBlocked_API = { [weak self] p in
                self?.lockFulfill?(.inMove)
            }
        }
    }
    
    func reset()  {
        //print("Linkamanager reset interaction")
        managers.communication.stopCommunication()
        _ = managers.connection.disconnect()
        managers.connection.stopCommunication()
        managers = nil
    }

    
    private func handleConnectionStateCallback () {
        managers.connection.callback = { [weak self] (peripheral, error) -> Void in
            guard let `self` = self else { return }
            if (error == nil) {
                print("LinkaManager isConnected = true")
                self.isConnected = true
            } else {
                print("LinkaManager isConnected = false")
                self.isConnected = false
            }
        }
        
        managers.connection.callbackOnLostConnection = { [weak self] (peripheral, error) -> Void in
            print("LinkaManager callbackOnLostConnection !!!!! ")
            guard let `self` = self else { return }
            self.isConnected = false
            self.reset()
            self.unlockFulfill?(.lostConnection)
        }
        
    }
    
    
    private func handleLockStatusCallback () {
        
        
        
        managers.communication.onFetchLockStatusPacket = { [weak self] (peripheral, lockStatusPacket, error) -> (Void) in
            
            if let statusPacket = lockStatusPacket {
                print(statusPacket.GetTransitionReason())

                if statusPacket.GetTransitionReason() == StateTransitionReason.REASON_STALL {

                    if statusPacket.GetTransitionReasonPrev() != statusPacket.GetTransitionReason() {
                        // We stalled... don't let rental end yet
                        self?.lockFulfill?(.stalled)
//                        self?.currentOperation = .unlock
                    } else {
                        print("ELSE GetTransitionReasonPrev")
                    }
                    
                }else {
                    
                    print("ELSE StateTransitionReason.REASON_STALL")
                }
                print("statusPacket.GetLockState() == \(statusPacket.GetLockState()) currentOperation = \(String(describing: self?.currentOperation))")
                
                if self?.currentOperation == .lock && statusPacket.GetLockState() == .lock_LOCKED {
                    self?.lockFulfill?(.finished)
                } else {
                    
                }
                if self?.currentOperation == .unlock && statusPacket.GetLockState() == .lock_UNLOCKED {
                    self?.unlockFulfill?(.finished)
                } else {
                    
                }
            }
            
        }
        
    }

    
    //MARK: - Public Actions
    func connectAndUnlock(managers: LinkaInteraction) -> Promise<LinkaOperationState> {
        //set current operation
        currentOperation = .unlock
        
        return firstly {
            self.connect(managers: managers)
            }.then { _ in
                self.unlock(managers: managers)
            }
    }
    
    func connectAndLock(managers: LinkaInteraction) -> Promise<LinkaOperationState> {
        //set current operation
        currentOperation = .lock
        
        return firstly {
            self.connect(managers: managers)
            }.then { _ in
                self.lock(managers: managers)
        }
    }

    

    
}

