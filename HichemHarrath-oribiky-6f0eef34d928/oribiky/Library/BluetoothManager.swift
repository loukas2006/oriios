//
//  BluetoothManager.swift
//  oribiky
//
//  Created by Hichem Harrath on 28/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import CoreBluetooth

class BluetoothManager: NSObject {
    
    // MARK: - Variables
    static let shared = BluetoothManager()
    private let bluetoothManager = CBCentralManager()
    
    var isPoweredOn: Bool = false
    
//    print("•••• \(bluetoothManager.state.rawValue)")
//    return bluetoothManager.state == .poweredOn || /*FOR TESTING*/bluetoothManager.state == .unsupported

    var stateChangeHandler: (()->())?
    
    // MARK: - Init
    override init() {
        super.init()
        bluetoothManager.delegate = self
    }

}
extension BluetoothManager: CBCentralManagerDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        // notify for handler change
        isPoweredOn = false
        
        switch central.state {
        case .poweredOn:
            print("Bluetooth is poweredOn.")
            isPoweredOn = true
            break
        case .poweredOff:
            print("Bluetooth is poweredOff.")
            break
        case .resetting:
            print("Bluetooth is resetting.")
            break
        case .unauthorized:
            print("Bluetooth is unauthorized.")
            break
        case .unsupported:
            print("Bluetooth is unsupported.")
            break
        case .unknown:
            break
        default:
            break
        }
        
        stateChangeHandler?()
        
    }
}
