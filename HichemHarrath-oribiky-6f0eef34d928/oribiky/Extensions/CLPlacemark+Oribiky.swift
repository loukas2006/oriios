//
//  CLPlacemark+Oribiky.swift
//  oribiky
//
//  Created by Aymen Harrath on 10/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import CoreLocation

extension CLPlacemark {
    func fullAddress () -> String {
        var addressString : String = ""
        
        if subThoroughfare != nil {
            addressString = addressString + subThoroughfare! + " "
        }
        if subLocality != nil {
            addressString = addressString + subLocality! + ", "
        }
        if thoroughfare != nil {
            addressString = addressString + thoroughfare! + ", "
        }
        if postalCode != nil {
            addressString = addressString + postalCode! + " "
        }
        if locality != nil {
            addressString = addressString + locality! + ", "
        }
        
        if country != nil {
            addressString = addressString + country!
        }
        return addressString
    }
}
