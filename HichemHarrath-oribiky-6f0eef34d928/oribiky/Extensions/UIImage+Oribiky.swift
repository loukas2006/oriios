//
//  UIImage+Oribiky.swift
//  oribiky
//
//  Created by Hichem Harrath on 17/10/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation


public enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UIImage {
    
    public func base64(format: ImageFormat) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = UIImagePNGRepresentation(self)
        case .jpeg(let compression): imageData = UIImageJPEGRepresentation(self, compression)
        }
        return imageData?.base64EncodedString()
    }
}
