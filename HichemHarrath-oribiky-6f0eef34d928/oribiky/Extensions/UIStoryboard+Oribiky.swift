//
//  UIStoryboard+Oribiky.swift
//  oribiky
//
//  Created by Aymen Harrath on 15/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

extension UIStoryboard {
  
    //MARK: - enum All storyboards
    enum Storyboard: String {
        case main
        case splash
        case map
        case trips
        case report
        case profile
        case demo
        case card
        case authenticate
        case subscription
        case ride

        var fileName: String {
            return rawValue.capitalized
        }
    }
    
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.fileName, bundle: bundle)
    }
    
    func instanciate<T: UIViewController>() -> T where T: StoryboardIdentifiable{
        guard let vc = self.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Could not instanciate viewControllerWithIdentifier \(T.storyboardIdentifier)")
        }
        return vc
    }
}
