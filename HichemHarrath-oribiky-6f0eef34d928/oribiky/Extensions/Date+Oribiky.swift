//
//  Date+Oribiky.swift
//  oribiky
//
//  Created by Hichem Harrath on 12/01/2019.
//  Copyright © 2019 Oribiky. All rights reserved.
//

import Foundation

extension Date {
    
    var timeAgoString: String {
        
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < self ? now : self
        let latest = (earliest == now) ? self : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if let years = components.year , years > 0 {
            return years > 1 ? APPStrings.Notification.TimeSinceNow.years(years) : APPStrings.Notification.TimeSinceNow.year
        }
        if let months = components.month, months > 0 {
            return months > 1 ? APPStrings.Notification.TimeSinceNow.months(months) : APPStrings.Notification.TimeSinceNow.month
        }
        if let weekOfYear = components.weekOfYear , weekOfYear > 0{
            return weekOfYear > 1 ? APPStrings.Notification.TimeSinceNow.weeks(weekOfYear) : APPStrings.Notification.TimeSinceNow.week
        }
        if let days = components.day , days > 0{
            return days > 1 ? APPStrings.Notification.TimeSinceNow.days(days) : APPStrings.Notification.TimeSinceNow.day
        }
        if let hours = components.hour , hours > 0{
            return hours > 1 ? APPStrings.Notification.TimeSinceNow.hours(hours) : APPStrings.Notification.TimeSinceNow.hour
        }
        if let minutes = components.minute , minutes > 0{
            return minutes > 1 ? APPStrings.Notification.TimeSinceNow.minutes(minutes) : APPStrings.Notification.TimeSinceNow.minute
        }
        if let seconds = components.second , seconds > 0{
            return seconds > 1 ? APPStrings.Notification.TimeSinceNow.seconds(seconds) : APPStrings.Notification.TimeSinceNow.second
        }
        return ""

    }
}
