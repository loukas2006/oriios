//
//  UIViewController+Oribiky.swift
//  oribiky
//
//  Created by Aymen Harrath on 16/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

extension UIViewController{
    
    
    func embededInMainNavigationController() -> MainNavigationController {
        return MainNavigationController(rootViewController: self)
    }
    // MARK: - clear nav bar
    private func clearNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColor.clear
        navigationController?.navigationBar.backgroundColor = UIColor.clear
        navigationController?.view.backgroundColor = AppStyle.Colors.mainBlue
    }
    
    private func mainNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = nil
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = AppStyle.Colors.mainBlue
        navigationController?.navigationBar.backgroundColor = AppStyle.Colors.mainBlue
        navigationController?.view.backgroundColor = AppStyle.Colors.mainBlue
    }
    
    // MARK: - Public
    
    func addBackNavigationItem() {
        addNavigationItem(withAction: #selector(pop(animated:)), navigationItem: .back, position: .left)
    }
    
    func addDismissNavigationItem(position: Position) {
        addNavigationItem(withAction:  #selector(pop(animated:)), navigationItem: .dismiss, position: position)
    }
    func addCustomNavigationItem(withAction action: Selector, position: Position, text: String) {
        addNavigationItem(withAction: action, text: text, position: position)
    }
    
    func addRighNavigationItems()  {
        addHamburgerMenuNavigationItem()
        addNotificationNavigationItem()
    }
    
    // MARK: - Private
    private func addNavigationItem(withAction action: Selector, navigationItem: NavigationItem, position: Position) {
        let barButtonItem = UIBarButtonItem(
            image: navigationItem.image,
            style: .plain,
            target: self,
            action: nil)
        
        switch position {
        case .left:
            self.navigationItem.leftBarButtonItem = barButtonItem
            self.navigationItem.leftBarButtonItem?.action = action
        case .right:
            self.navigationItem.rightBarButtonItem = barButtonItem
            self.navigationItem.rightBarButtonItem?.action = action
        }
        
    }
    
    private func addHamburgerMenuNavigationItem() {
        addNavigationItem(withAction: #selector(menu(animated:)), navigationItem: .menu, position: .right)
    }
    
    private func addNotificationNavigationItem() {
        let badgeBtn = BadgeButton(type: .custom)
        badgeBtn.setImage(NavigationItem.notification.image, for: .normal)
        badgeBtn.setImage(NavigationItem.notificationOpened.image, for: .selected)
        badgeBtn.imageView?.contentMode = .scaleAspectFit
        badgeBtn.addTarget(self, action: #selector(popover(animated:)), for: .touchUpInside)
        badgeBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 30)
        self.navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: badgeBtn))
        updateNotificationBadgeValue(value: BikeRentManager.shared.getNewNotificationsCount() == 0 ? "" : "\(BikeRentManager.shared.getNewNotificationsCount())")
        
    }
    
    func updateNotificationBadgeValue(value: String) {
        
        if let norificationBarButtonItem = navigationItem.rightBarButtonItems?.last, let badgeBtn = norificationBarButtonItem.customView as? BadgeButton {
            badgeBtn.updateBadgeValue(value)
        }
    }

    
//    private func appendNavigationItem(withAction action: Selector, navigationItem: NavigationItem, position: Position) {
//        let barButtonItem = UIBarButtonItem(
//            image: navigationItem.image,
//            style: .plain,
//            target: self,
//            action: nil)
//        switch position {
//        case .left:
//            barButtonItem.action = action
//            self.navigationItem.leftBarButtonItems?.append(barButtonItem)
//        case .right:
//            barButtonItem.action = action
//            self.navigationItem.rightBarButtonItems?.append(barButtonItem)
//        }
//    }

    
    private func addNavigationItem(withAction action: Selector, text: String, position: Position) {
        let barButtonItem = UIBarButtonItem(
            title: text,
            style: .plain,
            target: self,
            action: nil)
        
        switch position {
        case .left:
            self.navigationItem.leftBarButtonItem = barButtonItem
            self.navigationItem.leftBarButtonItem?.action = action
        case .right:
            self.navigationItem.rightBarButtonItem = barButtonItem
            self.navigationItem.rightBarButtonItem?.action = action
        }
    }
    
    
    
    @objc private func pop(animated: Bool) {
        
        if let burgerMenu = view.subviews.first(where: { (subview) -> Bool in subview is HamburgerMenuView }) as? HamburgerMenuView {
            burgerMenu.toggle(sender: nil) {
                AppCoordinator.shared.pop(animated: true)
            }
        }else {
            AppCoordinator.shared.pop(animated: true)
        }

    }
    
    @objc private func menu(animated: Bool) {
        if let burgerMenu = view.subviews.first(where: { (subview) -> Bool in subview is HamburgerMenuView }) as? HamburgerMenuView {
            burgerMenu.toggle(sender: self.navigationItem.rightBarButtonItem)
        } else {
            let menu = HamburgerMenuView(from: self)
            view.addSubview(menu)
            menu.toggle(sender: self.navigationItem.rightBarButtonItem)
        }
    }
    
    @objc private func popover(animated: Bool) {
        
        if DefaultsManager.shared.isConnected {
            
            if let burgerMenu = view.subviews.first(where: { (subview) -> Bool in subview is HamburgerMenuView }) as? HamburgerMenuView {
                burgerMenu.toggle(sender: nil) {[weak self] in
                    self?.popoverHandel()
                }
            }else {
                popoverHandel()
            }

        } else {
            
            ReservationRequirementsProcessManager.shared.handleSteps()
            ReservationRequirementsProcessManager.shared.start(finalStep:  Step(type: .authentication)) { [weak self] in
                ReservationRequirementsProcessManager.shared.ignoreSteps()
                    if let burgerMenu = self?.view.subviews.first(where: { (subview) -> Bool in subview is HamburgerMenuView }) as? HamburgerMenuView {
                        burgerMenu.toggle(sender: nil) {[weak self] in
                            self?.popoverHandel()
                        }
                    }else {
                        self?.popoverHandel()
                    }
                
            }

        }


    }
    private func popoverHandel () {
    
        let viewModel = NotificationsPopoverViewModel()
        if let norificationBarButtonItem = navigationItem.rightBarButtonItems?.last {
            updateNotificationBtnState(selected: true)
            if BikeRentManager.shared.getNewNotificationsCount() > 0 {
                
                AppCoordinator.shared.transition(to: .notificationPopover(viewModel),
                                                 type: .popover(preferredContentSize: viewModel.preferredSize, sender: norificationBarButtonItem),
                                                 animated: true, completion: {[weak self] in
                                                    self?.updateNotificationBtnState(selected: false)
                })
            } else {
                
                AppCoordinator.shared.transition(to: .notificationList(NotificationListViewModel()),
                                                 type: .modal,
                                                 animated: true, completion: {[weak self] in
                                                    self?.updateNotificationBtnState(selected: false)
                })
                
            }
        }

    }
    private func updateNotificationBtnState(selected: Bool) {
        if let norificationBarButtonItem = navigationItem.rightBarButtonItems?.last , let btn = norificationBarButtonItem.customView as? BadgeButton {
            btn.isSelected = selected
        }
    }
    func showReachabilityView () {
        if let navController = navigationController, navController.isNavigationBarHidden == false {
            if let noInternetView = view.subviews.first(where: { (subview) -> Bool in subview is NoInternetConnectionView }) as? NoInternetConnectionView {
                noInternetView.show()
            }else {
                let noInternetView = NoInternetConnectionView()
                view.addSubview(noInternetView)
                noInternetView.show()
                
            }
        }
    }
    func hideReachabilityView (animated: Bool) {
        if let navController = navigationController, navController.isNavigationBarHidden == false {
            if let noInternetView = view.subviews.first(where: { (subview) -> Bool in subview is NoInternetConnectionView }) as? NoInternetConnectionView {
                noInternetView.hide(animated: animated)
            }
        }
    }

    private enum NavigationItem {
        case back
        case dismiss
        case menu
        case notification
        case notificationOpened

        var image: UIImage {
            switch self {
            case .back: return Asset.Navigation.back.image
            case .dismiss: return Asset.Popup.closeCross.image
            case .menu: return Asset.HamburgerMenu.menu.image
            case .notification: return Asset.Notification.notification.image
            case .notificationOpened: return Asset.Notification.notificationOpened.image
            }
        }
    }
    enum Position {
        case left
        case right
    }
}

extension UIViewController: NVActivityIndicatorViewable {
    func startLoading() {
        view.endEditing(true) //dismiss keyboard if needed
        startAnimating(messageFont: AppStyle.Text.regular(size: 12).font, type: .circleStrokeSpin, color: AppStyle.Colors.mainBlue)
    }
    func stopLoading() {
        stopAnimating()
    }
    func startLoading(message: String) {
        startAnimating(message: message, messageFont: AppStyle.Text.bold(size: 12).font, type: .circleStrokeSpin, color: AppStyle.Colors.white, backgroundColor: AppStyle.Colors.black.withAlphaComponent(0.6) , textColor: AppStyle.Colors.white)
        NVActivityIndicatorPresenter.sharedInstance.setMessage(message)
    }
}

extension UIViewController{
    var isModal: Bool {
        
        if let navigationController = self.navigationController{
            if navigationController.viewControllers.first != self{
                return false
            }
        }
        
        if self.presentingViewController != nil {
            return true
        }
        
        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        }
        
        if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }

        return false
    }
}
