//
//  UIView+Oribiky.swift
//  oribiky
//
//  Created by Aymen Harrath on 17/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

extension UIView {
    
    func applyShadow(color : UIColor = AppStyle.Colors.shadow, radius: CGFloat = 10, cornerRadius: CGFloat = 0) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.masksToBounds = false
        layer.shadowRadius = radius
        layer.shadowOpacity = 0.7
        layer.cornerRadius = cornerRadius
    }
    
    func round (value: CGFloat = 0) {
        layer.cornerRadius = value == 0 ? bounds.height / 2 : value
        layer.masksToBounds = true
    }
    
    func round(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }

    
    func border(width: CGFloat = 1, color: UIColor = AppStyle.Colors.mainBlue){
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }
    func addCenterInContainerConstraints()  {
        let centerX = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: superview, attribute: .centerX, multiplier: 1, constant: 0)
        let centerY = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: superview, attribute: .centerY, multiplier: 1, constant: 0)
        
        addConstraints([centerX,centerY])
    }
    
    func constraintViewToBounds(viewToBound: UIView, top: CGFloat = 0 , bottom: CGFloat = 0 , leading: CGFloat = 0, trailing: CGFloat = 0) {
        
        // To allow constraints
        viewToBound.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(item: viewToBound,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self,
                                               attribute: .top,
                                               multiplier: 1,
                                               constant: top)
        let leadingConstraint = NSLayoutConstraint(item: viewToBound,
                                                   attribute: .leading,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .leading,
                                                   multiplier: 1,
                                                   constant: leading)
        let trailingConstraint = NSLayoutConstraint(item: viewToBound,
                                                    attribute: .trailing,
                                                    relatedBy: .equal,
                                                    toItem: self,
                                                    attribute: .trailing,
                                                    multiplier: 1,
                                                    constant: trailing)
        let bottomConstraint = NSLayoutConstraint(item: viewToBound,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: self,
                                                  attribute: .bottom,
                                                  multiplier: 1,
                                                  constant: bottom)
        
        addConstraints([topConstraint, leadingConstraint, trailingConstraint, bottomConstraint])
    }
    
    func addHeightConstraint(constant: CGFloat = 0) {
        let heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: constant)
        addConstraint(heightConstraint)
    }
    
    func fixedWidthAndHeight(width: CGFloat = 0, height: CGFloat = 0) {
        let heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height)
        let widthConstraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width)
        self.addConstraints([heightConstraint, widthConstraint])
        
    }

    /**
     Moves this view into position, as though it were popping out of the screen.
     - Parameters:
     - fromScale: starting scale for the view, should be between 0 and 1
     - duration: duration of the animation, in seconds
     - delay: delay before the animation starts, in seconds
     - completion: block executed when the animation ends
     */
    @discardableResult func popIn(fromScale: CGFloat = 0.5,
                                  duration: TimeInterval = 0.5,
                                  delay: TimeInterval = 0,
                                  completion: ((Bool) -> Void)? = nil) -> UIView {
        isHidden = false
        alpha = 0
        transform = CGAffineTransform(scaleX: fromScale, y: fromScale)
        UIView.animate(
            withDuration: duration, delay: delay, usingSpringWithDamping: 0.55, initialSpringVelocity: 3,
            options: .curveEaseOut, animations: {
                self.transform = .identity
                self.alpha = 1
        }, completion: completion)
        return self
    }
    
    /**
     Moves this view out of position, as though it were withdrawing into the screen.
     - Parameters:
     - toScale: ending scale for the view, should be between 0 and 1
     - duration: duration of the animation, in seconds
     - delay: delay before the animation starts, in seconds
     - completion: block executed when the animation ends
     */
    @discardableResult func popOut(toScale: CGFloat = 0.5,
                                   duration: TimeInterval = 0.3,
                                   delay: TimeInterval = 0,
                                   completion: ((Bool) -> Void)? = nil) -> UIView {
        let endTransform = CGAffineTransform(scaleX: toScale, y: toScale)
        let prepareTransform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        UIView.animateKeyframes(
            withDuration: duration, delay: delay, options: .calculationModeCubic, animations: {
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2) {
                    self.transform = prepareTransform
                }
                UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.3) {
                    self.transform = prepareTransform
                }
                UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
                    self.transform = endTransform
                    self.alpha = 0
                }
        }, completion: completion)
        return self
    }
    
    func fadeIn(duration: TimeInterval = 0.3, completion: ((Bool) -> Void)? = nil) {
        self.alpha = 0.0
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }
    func fadeOut(duration: TimeInterval = 0.3, completion: ((Bool) -> Void)? = nil) {
        self.alpha = 1.0
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    func yTranslation(value: CGFloat, duration: TimeInterval = 0.2, completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: duration, animations: {
            self.frame.origin.y = value
        }, completion: completion)
    }
    
    func yTranslationWithFadeIn(value: CGFloat, duration: TimeInterval = 0.2, completion: ((Bool) -> Void)? = nil) {
        self.alpha = 0.0
        UIView.animate(withDuration: duration, animations: {
            self.frame.origin.y = value
            self.alpha = 1.0
        }, completion: completion)
    }
    func yTranslationWithFadeOut(value: CGFloat, duration: TimeInterval = 0.2, completion: ((Bool) -> Void)? = nil) {
        self.alpha = 1.0
        UIView.animate(withDuration: duration, animations: {
            self.frame.origin.y = value
            self.alpha = 0.0
        }, completion: completion)
    }

    
    func addBlur (effect: UIBlurEffectStyle) {
        //only apply the blur if the user hasn't disabled transparency effects
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            backgroundColor = .clear
            
            let blurEffect = UIBlurEffect(style: effect)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            addSubview(blurEffectView) 
        }
    }



}
