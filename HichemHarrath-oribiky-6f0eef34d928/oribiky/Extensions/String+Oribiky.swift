//
//  String+Oribiky.swift
//  oribiky
//
//  Created by Aymen Harrath on 13/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation


extension String {
    
    
    func isAValidEmailAddress() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with: self)
    }
    
    var isNumbersOnly: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    func asHTMLAttributedString (font: UIFont) -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            let attributedString =  try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            attributedString.addAttributes([NSAttributedString.Key.font :font], range: NSRange(location: 0, length: attributedString.length))
            return attributedString
            
        } catch {
            return NSAttributedString()
        }

    }

    
    
//    subscript(offset: Int) -> Element {
//        return self[index(startIndex, offsetBy: offset)]
//    }

    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(start, offsetBy: r.upperBound - r.lowerBound)
        return String(self[start ..< end])
    }
    
    func makeBold(highlightedTexts: [String], fontSize: CGFloat) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttributes([NSAttributedString.Key.font : AppStyle.Text.regular(size: fontSize).font], range: NSRange(location: 0, length: self.count))

        for text in highlightedTexts {
            let range = self.range(of: text)
            let startLocation = range?.lowerBound.encodedOffset ?? 0
            attributedString.addAttributes([NSAttributedString.Key.font : AppStyle.Text.bold(size: fontSize).font], range: NSRange(location: startLocation, length: text.count))
        }
        return attributedString
    }
    
    func makeUnderlined(fontSize: CGFloat) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttributes([NSAttributedString.Key.font : AppStyle.Text.medium(size: fontSize).font, NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue], range: NSRange(location: 0, length: self.count))
        return attributedString

    }
    
    func highlightColor(highlightedTexts: [String], font: UIFont, color: UIColor, lineSpace: Double? = 0, alignment: NSTextAlignment? = nil ) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        
        attributedString.addAttributes([NSAttributedString.Key.font :font], range: NSRange(location: 0, length: self.count))

        for text in highlightedTexts {
            let range = self.range(of: text)
            let startLocation = range?.lowerBound.encodedOffset ?? 0
            attributedString.addAttributes([NSAttributedString.Key.foregroundColor : color], range: NSRange(location: startLocation, length: text.count))
        }
        if lineSpace != nil {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = CGFloat(lineSpace ?? 0) // Whatever line spacing you want in points
            paragraphStyle.alignment = alignment ?? .center
            attributedString.addAttribute(NSAttributedString.Key.paragraphStyle,
                                          value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        }

        return attributedString
        
    }


}
extension String {
    
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "\(self)", comment: "")
    }
}

extension String {
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }

}

extension String {
    
    func getTicketDateStr () -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constant.Date.Ticketsformat
        guard let date = dateFormatter.date(from: self) else { return nil }
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: date)

    }

}
