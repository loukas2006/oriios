//
//  AppDelegate+Theme.swift
//  oribiky
//
//  Created by Hichem Harrath on 29/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

extension AppDelegate {
    
    static func applyThemes() {
        AppDelegate.configureAppearenceForNavigationBar()
        AppDelegate.configureAppearenceForTabBarItem()
    }
    
    private static func configureAppearenceForNavigationBar() {
        UIApplication.shared.statusBarStyle = .lightContent

        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().barTintColor = AppStyle.Colors.mainBlue
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)

        let titleAttributesDict = [NSAttributedStringKey.font : AppStyle.Text.medium(size: 17).font,
                                   NSAttributedStringKey.foregroundColor : AppStyle.Colors.white]
        UINavigationBar.appearance().titleTextAttributes = titleAttributesDict
       // UIBarButtonItem.appearance().setTitleTextAttributes([.font: UIFont.verdana(withSize: 13), .foregroundColor: UIColor.white], for: .normal)
       // UIBarButtonItem.appearance().setTitleTextAttributes([.font: UIFont.verdana(withSize: 13), .foregroundColor: UIColor.white], for: .highlighted)
     //   UIBarButtonItem.appearance().setTitleTextAttributes([.font: UIFont.verdana(withSize: 13), .foregroundColor: UIColor.white], for: .selected)
        //UINavigationBar.appearance().shadowImage = UIImage()
     //   UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
       
    }
    
    private static func configureAppearenceForTabBarItem() {
        UITabBarItem.appearance().setTitleTextAttributes([.font : AppStyle.Text.medium(size: 10).font], for: .normal)
        UITabBar.appearance().tintColor =  AppStyle.Colors.white
        UITabBar.appearance().unselectedItemTintColor =  AppStyle.Colors.whiteWithAlpha
        UITabBar.appearance().barTintColor = AppStyle.Colors.mainBlue
        UITabBar.appearance().isTranslucent = false
    }
    
}
