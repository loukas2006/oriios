//
//  AppDelegate.swift
//  oribiky
//
//  Created by Hichem Harrath on 11/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit
import UserNotifications
import Fabric
import Crashlytics
import Stripe
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window?.makeKeyAndVisible()
       
        // Appearence theme
        AppDelegate.applyThemes()
        LinkaManager.shared.fetchAccessToken()

        //Root Controller
        AppCoordinator.shared.transition(to: .launch(LaunchViewModel()), type: .root, animated: true)
        
        // register for push notifications
        registerForPushNotifications()
        
        //Enable crashlitycs
        Fabric.sharedSDK().debug = true
        Crashlytics.sharedInstance().debugMode = true
        Fabric.with([Crashlytics.self])

        StripePaymentManager.shared

        ReachabilityManager.shared.startListening()
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }

}
// MARK: - Push notifications
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            // 1. Check if permission granted
            guard granted else { return }
            // 2. Attempt registration for remote notifications on the main thread
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // 1. Convert device token to string
        Messaging.messaging().apnsToken = deviceToken

    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // 1. Print out error if PNs registration not successful
        print("Failed to register for remote notifications with error: \(error)")
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // get new notifications
        NotificationService.getNew(handleSessionExpired: false, success: { (responseList) in
            let items = responseList.compactMap({NotificationViewModel(notification: $0)})
            BikeRentManager.shared.updateNewNotifications(notifications: items)
            // update badge button number after receiving the psuh
            AppCoordinator.shared.currentViewController?.updateNotificationBadgeValue(value: BikeRentManager.shared.getNewNotificationsCount() == 0 ? "" : "\(BikeRentManager.shared.getNewNotificationsCount())")

        }, failure: { (errorStr, errors) in
            print(errorStr)
        })


        
    }
    
    // This method handles opening native URLs (e.g., "yourexampleapp://")
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let stripeHandled = Stripe.handleURLCallback(with: url)
        print(url)
        if (stripeHandled) {
            return true
        } else {
            // This was not a stripe url – do whatever url handling your app
            // normally does, if any.
        }
        return false
    }
        
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        
        DefaultsManager.shared.saveFCMToken(fcmToken)

    }

    
}
