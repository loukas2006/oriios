//
//  AppStyle.swift
//  oribiky
//
//  Created by Aymen Harrath on 13/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
import UIKit

struct AppStyle {
    
    static let expirationDateSeparator = "/"
    
    struct Colors {
        static let mainBlue = UIColor(red: 15/255.0, green: 28/255.0, blue: 65/255.0, alpha: 1)
        static let lightBlue = UIColor(red: 93/255.0, green: 127/255.0, blue: 140/255.0, alpha: 1)
        static let verLightBlue = UIColor(red: 93/255.0, green: 127/255.0, blue: 140/255.0, alpha: 0.3)
        static let red = UIColor(red: 255/255.0, green: 2/255.0, blue: 29/255.0, alpha: 1)
        static let mainGray = UIColor(red: 102/255.0, green: 102/255.0, blue: 102/255.0, alpha: 1)
        static let lightGray = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1)
        static let white = UIColor.white
        static let black = UIColor.black
        static let clear = UIColor.clear
        static let shadow = UIColor(red: 15/255.0, green: 28/255.0, blue: 65/255.0, alpha: 0.3)
        static let whiteWithAlpha = UIColor(white: 1, alpha: 0.5)
        static let mainGreen = UIColor(red: 1/255.0, green: 187/255.0, blue: 121/255.0, alpha: 1)

    }
    
    enum Buttons {
        case medium10
        case medium12
        case medium16
        case medium18
        
        var font: UIFont {
            
            switch self {
            case .medium10:
                return UIFont(name: "GothamRounded-Medium", size: 10) ?? UIFont.systemFont(ofSize: 10)
            case .medium12:
                return UIFont(name: "GothamRounded-Medium", size: 12) ?? UIFont.systemFont(ofSize: 12)
            case .medium16:
                return UIFont(name: "GothamRounded-Medium", size: 16) ?? UIFont.systemFont(ofSize: 18)
            case .medium18:
                return UIFont(name: "GothamRounded-Medium", size: 18) ?? UIFont.systemFont(ofSize: 18)
        }

        }
    }
    
    enum Text {
        case regular(size: CGFloat)
        case medium(size: CGFloat)
        case bold(size: CGFloat)
        case book(size: CGFloat)
        case italic(size: CGFloat)
        case mediumItalic(size: CGFloat)
        case boldItalic(size: CGFloat)

        var font: UIFont {
            switch self {
            case .regular(let size):
                return UIFont(name: "GothamRounded-Book", size: size) ?? UIFont.systemFont(ofSize: size)
            case .medium(let size):
                return UIFont(name: "GothamRounded-Medium", size: size) ?? UIFont.systemFont(ofSize: size)
            case .bold(let size):
                return UIFont(name: "GothamRounded-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
            case .book(let size):
                return UIFont(name: "GothamRounded-Book", size: size) ?? UIFont.systemFont(ofSize: size)
            case .italic(let size):
                return UIFont(name: "GothamRounded-BookItalic", size: size) ?? UIFont.systemFont(ofSize: size)
            case .mediumItalic(let size):
                return UIFont(name: "GothamRounded-MediumItalic", size: size) ?? UIFont.systemFont(ofSize: size)
            case .boldItalic(let size):
                return UIFont(name: "GothamRounded-BoldItalic", size: size) ?? UIFont.systemFont(ofSize: size)
            }
        }

    }
}
