//
//  Networking.swift
//  oribiky
//
//  Created by Hichem Harrath on 11/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation
/*
 HERE WE SHOULD ADD ALL WEB SERVICES CONSTANTS
 */
struct HTTPAPI {
   
    static let baseURL = "https://api-dev.oribiky.com/api/"
    static let TokenFormat = "Bearer %@"
    static let AuthorizationHeaderKey = "Authorization"

    static let deviceOS = "IOS"
    
    static let chatLicenseId =  "10304522"
    
    //Stripe
    struct Stripe {
        static let cautionAmount: UInt = 3000
        static let publishableKey: String = "pk_test_EUodLpEEmIQ3q6b0CgELkakV"
    }

    //Report
    struct LiveChatTicket {
        static let baseURL = "https://api.livechatinc.com/"
        static let getList = "tickets"
        static let create = "tickets"
        
        static let licenceID = "10304522";
        static let agentEmail = "tech@oribiky.com";
        static let agentAPIKey = "1d8a847ae3ab86bdc0dce1449bd9d0d9";
        
        static let TokenFormat = "Basic %@"
        static let APIVersionKey = "x-api-version"
        static let APIVersionValue = "2"

    }
    
    //PDF Files
    struct PDF {
        static let termsOfUseAndSale = "https://www.oribiky.com/conditions-generales/"
        static let faq = "https://www.oribiky.com/faq/"
        static let privacyPolicy = "https://www.oribiky.com/politique-de-confidentialite/" 

    }


    // Authentication methods
    struct Authentication {
        //POST - Demander code de confirmation
        static let authenticate = "authenticate"
        //POST - Validation du code de confirmation"
        static let verify = "authenticate/%@/verify"
        //POST - Rafrechire token d'authentification
        static let refreshToken = "token/refresh"
    }
    
    // Bikes methods
    struct Bike {
        //GET - Récupérer la liste des vélos.
        static let get = "bikes"
    }
    
    //Profile mthods
    struct Profile {
        static let get = "profile"
        static let edit = "profile"
        static let editAddress = "profile/address"
        
    }
    
    struct Payment {
        static let addCard = "payment"
        static let updateCard = "payment"
        static let charge = "stripe"

    }
    //Subscription mthods
    struct Subscribe {
        static let subscribe = "subscriptions"
        static let unsubscribe = "subscriptions"
        static let previewChange = "subscriptions/preview/change"
        static let change = "subscriptions/change"
        static let reactivate = "subscriptions/reactivate"
    }
    struct Package {
        static let packages = "packages"
    }
    struct Coupon {
        static let add = "coupons"
    }

    
    struct Ride {
        static let get = "rides"
        static let create = "rides"
        static let delete = "rides"
        static let deleteAll = "rides/delete/all"
        static let getCurrent = "rides/current"
        static let finish = "rides/%@/finish"
        
    }
    
    struct Reservation {
        static let create = "reservations"
        static let current = "reservations/current"
        static let cancel = "reservations/%@/cancel"
        
    }
    
    struct Parking {
        static let search = "parkings/search"
        static let getAll = "parkings"
        static let addNew = "parkings"

    }

    struct Notification {
        static let getAll = "notifications"
        static let markAsRead = "notifications"
        static let getNew = "notifications/new"
    }


}


