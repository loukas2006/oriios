//
//  Constant.swift
//  oribiky
//
//  Created by Aymen Harrath on 17/11/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation

struct Constant {
    struct Date {
        static let format = "yyyy-MM-dd HH:mm:ss" //2018-11-13 20:28:24
        static let Ticketsformat = "yyyy-MM-dd'T'HH:mm:ssZ"
    }
    struct Linka {
        static let scanTimeOut: TimeInterval = 5
    }
    static let reservationRequirementsNotificationName = "ReservationRequirementsNotification"
    static let newNotificationsNumberNotificationName = "newNotificationsNumberNotificationName"

    struct ClientSupport {
        static let phoneNumber = "9 87 18 20 16"
    }

    static let reserversationDuration = 10 * 60
}
