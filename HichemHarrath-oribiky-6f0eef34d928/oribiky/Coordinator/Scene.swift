//
//  Scene.swift
//  oribiky
//
//  Created by Hichem Harrath on 20/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

enum Scene {
    case splash(SplashViewModel)
    case demo(DemoViewModel)
    case tabbar(TabbarViewModel)
    case tabbarItem
    case map(MapViewModel)
    case popup(PopupViewModel)
    case editProfile(EditProfileViewModel)
    case newCard(EditProfileViewModel, Bool)
    case scanCard(ScanCardViewModel)
    case authenticate(AuthenticateViewModel, Bool)// if true, we should embed our controller in a navigation controller
    case enterCode(EnterCodeViewModel)
    case trips(TripsListViewModel)
    case report(ReportViewModel)
    case profile(ProfileViewModel)
    case subscriptionList(SubscriptionListViewModel, Bool)// if true, we should embed our controller in a navigation controller
    case subscriptionDetails(SubscriptionViewModel)
    case subscriptionEligibility(SubscriptionEligibilityViewModel)
    case imagePicker(UIImagePickerController.SourceType, Any)
    case pdfViewer(PdfViewerViewModel, Bool)
    case activateBluetooth(ActivateBluetoothViewModel)
    case connectToBike(ConnectToBikeViewModel)
    case turnOnBike(TurnOnBikeViewModel)
    case bikeDiagnostic(BikeDiagnosticViewModel)
    case launch(LaunchViewModel)
    case startDiagnostic(StartBikeDiagnosticViewModel)
    case rideConfirmation(RideConfirmationViewModel)
    case currentRide(CurrentRideViewModel)
    case lockSmartLock(LockSmartLockViewModel)
    case unlockSmartLock(UnlockSmartLockViewModel)
    case mySubscription(SubscriptionViewModel)
    case rideResume(RideResumeViewModel, Bool)
    case parkingAlert(ParkingAlertViewModel)
    case myTicketsList(MyTicketsViewModel)
    case createTicket(CreateTicketViewModel)
    case coupons(CouponsViewModel)
    case ticketDetails(TicketDetailsViewModel)
    case notificationPopover(NotificationsPopoverViewModel)
    case notificationList(NotificationListViewModel)
    case legalNotice(LegalNoticeViewModel)
    case currentReservation(CurrentReservationViewModel)


}

extension Scene {
    
    func viewController() -> UIViewController {
        switch self {
        case let .splash(viewModel):
            var vc : SplashViewController = UIStoryboard(storyboard: .splash).instanciate()
            vc.configure(with: viewModel)
            return  vc
        case let .demo(viewModel):
            var vc : DemoViewController = UIStoryboard(storyboard: .demo).instanciate()
            vc.configure(with: viewModel)
            return  vc
        case let .tabbar(viewModel):
            var vc : HomeTabBarViewController = UIStoryboard(storyboard: .main).instanciate()
            vc.configure(with: viewModel)
            // tabbar items
            var currentRideViewController: CurrentRideViewController = UIStoryboard(storyboard: .ride).instanciate()
            currentRideViewController.configure(with: CurrentRideViewModel())

            var currentReservationViewController: CurrentReservationViewController = UIStoryboard(storyboard: .ride).instanciate()
            currentReservationViewController.configure(with: CurrentReservationViewModel())

            var mapViewController: MapViewController = UIStoryboard(storyboard: .map).instanciate()
            mapViewController.configure(with: MapViewModel())

//            var tripsViewController: TripsViewController = UIStoryboard(storyboard: .trips).instanciate()
//            tripsViewController.configure(with: TripsViewModel())
            var subscriptionViewController: SubscriptionListViewController = UIStoryboard(storyboard: .subscription).instanciate()
            subscriptionViewController.configure(with: SubscriptionListViewModel(isDisplayedInTabbarItem: true))
            
            var mySubscriptionViewController: MySubscriptionViewController = UIStoryboard(storyboard: .subscription).instanciate()
            // default is minute price means no subscription YET
            mySubscriptionViewController.configure(with: SubscriptionViewModel(SubscriptionType(rawValue: BikeRentManager.shared.getUser().subscription.details.type) ?? .minutePrice))

             var reportViewController: ReportViewController = UIStoryboard(storyboard: .report).instanciate()
             reportViewController.configure(with: ReportViewModel())

            var profileViewController: ProfileViewController = UIStoryboard(storyboard: .profile).instanciate()
            profileViewController.configure(with: ProfileViewModel())
            //add either current ride or map view controller
            
//            if !DefaultsManager.shared.isConnected {
//                var controllers = [UIViewController]()
//                controllers.append(mapViewController.embededInMainNavigationController())
//                for i in 1..<4 {
//                    var vc : AuthenticationViewController = UIStoryboard(storyboard: .authenticate).instanciate()
//                    vc.configure(with: AuthenticateViewModel(index: i, pushed: false))
//                    controllers.append(vc.embededInMainNavigationController())
//                }
//                vc.viewControllers = controllers
//                return vc
//            }
            
            var firstNavController = mapViewController.embededInMainNavigationController()
            
            if BikeRentManager.shared.hasRide {
                firstNavController = currentRideViewController.embededInMainNavigationController()
            }else if BikeRentManager.shared.hasReservation {
                firstNavController = currentReservationViewController.embededInMainNavigationController()
            }
            
            vc.viewControllers = [firstNavController ,
                                  BikeRentManager.shared.getUser().isSubscribed() ? mySubscriptionViewController.embededInMainNavigationController():
            subscriptionViewController.embededInMainNavigationController(),
            reportViewController.embededInMainNavigationController(),
            profileViewController.embededInMainNavigationController()]
            
            return  vc
        case let .map(viewModel):
            var vc : MapViewController = UIStoryboard(storyboard: .map).instanciate()
            vc.configure(with: viewModel)
            return  vc 
        case let .popup(viewModel):
            var vc = PopupViewController()
            vc.configure(with: viewModel)
            return vc
        case let .editProfile(viewModel):
            var vc : EditProfileViewController = UIStoryboard(storyboard: .profile).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .newCard(viewModel, embedInNavigation):
            var vc : NewCardViewController = UIStoryboard(storyboard: .card).instanciate()
            vc.configure(with: viewModel)
            return embedInNavigation ? vc.embededInMainNavigationController() : vc
        case let .scanCard(viewModel):
            var vc : ScanCardViewController = UIStoryboard(storyboard: .card).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .authenticate(viewModel, embedInNavigation):
            var vc : AuthenticationViewController = UIStoryboard(storyboard: .authenticate).instanciate()
            vc.configure(with: viewModel)
            return embedInNavigation ? vc.embededInMainNavigationController() : vc
        case let .enterCode(viewModel):
            var vc : EnterAuthenticationCodeViewController = UIStoryboard(storyboard: .authenticate).instanciate()
            vc.configure(with: viewModel)
            return vc
        case .tabbarItem:
            return UIViewController() // I dont need The tabbar controller
        case let .trips(viewModel):
            var vc: TripsViewController = UIStoryboard(storyboard: .trips).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .report(viewModel):
            var vc: ReportViewController = UIStoryboard(storyboard: .report).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .profile(viewModel):
            var vc: ProfileViewController = UIStoryboard(storyboard: .profile).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .subscriptionList(viewModel, embedInNavigation):
            var vc: SubscriptionListViewController = UIStoryboard(storyboard: .subscription).instanciate()
            vc.configure(with: viewModel)
            return embedInNavigation ? vc.embededInMainNavigationController() : vc
        case let .subscriptionDetails(viewModel):
            var vc: SubscriptionDetailsViewController = UIStoryboard(storyboard: .subscription).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .subscriptionEligibility(viewModel):
        var vc: SubscriptionEligibilityViewController = UIStoryboard(storyboard: .subscription).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .imagePicker(sourceType, delegate):
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = delegate as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                myPickerController.sourceType = sourceType
                return myPickerController
        case let .pdfViewer(viewModel, embedInNavigation):
                var vc: PDFViewerViewController = UIStoryboard(storyboard: .card).instanciate()
                vc.configure(with: viewModel)
                return embedInNavigation ? vc.embededInMainNavigationController() : vc

        case let .activateBluetooth(viewModel):
            var vc : ActivateBluetoothViewController = UIStoryboard(storyboard: .map).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .connectToBike(viewModel):
            var vc : ConnectToBikeViewController = UIStoryboard(storyboard: .map).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .turnOnBike(viewModel):
            var vc : TurnOnBikeViewController = UIStoryboard(storyboard: .map).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .bikeDiagnostic(viewModel):
            var vc : BikeDiagnosticViewController = UIStoryboard(storyboard: .map).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .launch(viewModel):
            var vc: LaunchViewController = UIStoryboard(storyboard: .main).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .rideConfirmation(viewModel):
            var vc: RideConfirmationViewController = UIStoryboard(storyboard: .map).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .startDiagnostic(viewModel):
            var vc: StartBikeDiagnosticViewController = UIStoryboard(storyboard: .map).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .currentRide(viewModel):
            var vc: CurrentRideViewController = UIStoryboard(storyboard: .ride).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .lockSmartLock(viewModel):
            var vc: LockSmartLockViewController = UIStoryboard(storyboard: .ride).instanciate()
            //viewModel.lockdelegate = vc
            vc.configure(with: viewModel)
            return vc
        case let .unlockSmartLock(viewModel):
            var vc: UnLockSmartLockViewController = UIStoryboard(storyboard: .ride).instanciate()
            viewModel.unlockdelegate = vc
            vc.configure(with: viewModel)
            return vc
        case let .mySubscription(viewModel):
            var vc: MySubscriptionViewController = UIStoryboard(storyboard: .subscription).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .rideResume(viewModel, embedInNavigation):
            var vc: RideResumeViewController = UIStoryboard(storyboard: .ride).instanciate()
            vc.configure(with: viewModel)
            return embedInNavigation ? vc.embededInMainNavigationController() : vc
        case let .parkingAlert(viewModel):
            var vc = ParkingAlertViewController()
            vc.configure(with: viewModel)
            return vc
        case let .myTicketsList(viewModel):
            var vc: MyTicketsViewController = UIStoryboard(storyboard: .report).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .notificationPopover(viewModel):
            var vc: NotificationPopoverViewController = UIStoryboard(storyboard: .main).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .notificationList(viewModel):
            var vc: NotificationListViewController = UIStoryboard(storyboard: .main).instanciate()
            vc.configure(with: viewModel)
            return vc.embededInMainNavigationController()

        case let .createTicket(viewModel):
            var vc: CreateTicketViewController = UIStoryboard(storyboard: .report).instanciate()
            vc.configure(with: viewModel)
            return vc
        case let .coupons(viewModel):
            var vc: CouponsViewController = UIStoryboard(storyboard: .subscription).instanciate()
            vc.configure(with: viewModel)
            return vc.embededInMainNavigationController()
        case let .ticketDetails(viewModel):
            var vc: TicketDetailsViewController = UIStoryboard(storyboard: .report).instanciate()
            vc.configure(with: viewModel)
            return vc

        case let .legalNotice(viewModel):
            var vc: LegalNoticeViewController = UIStoryboard(storyboard: .report).instanciate()
            vc.configure(with: viewModel)
            return vc.embededInMainNavigationController()
            
        case let .currentReservation(viewModel):
            var vc: CurrentReservationViewController = UIStoryboard(storyboard: .ride).instanciate()
            vc.configure(with: viewModel)
            return vc

        }
        
    }
}
