//
//  AppCoordinator.swift
//  oribiky
//
//  Created by Hichem Harrath on 20/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

class AppCoordinator: NSObject {
   
    //MARK: - Singleton
    static let shared = AppCoordinator()
    
    //MARK: - Variables
    private var window: UIWindow
    private(set) var currentViewController: UIViewController? {
        didSet {
            print("AnyAppCoordinator Current == \(String(describing: currentViewController!.self))")
            if ReachabilityManager.shared.isConnectedToInternet {
                currentViewController?.hideReachabilityView(animated: false)
            } else {
                currentViewController?.showReachabilityView()
            }
        }
    }
    //private var tabBarController: UITabBarController?
    
    //MARK: - Init
    override init() {
        guard let window = UIApplication.shared.delegate?.window as? UIWindow else {
            fatalError("NO window found")
        }
        self.window = window
        currentViewController = window.rootViewController
    }
    
    //MARK: - Private
    private func actualViewController(for viewController: UIViewController) -> UIViewController {
        if let navigationController = viewController as? UIImagePickerController {
            return navigationController
        } else if let navigationController = viewController as? UINavigationController,
            let firstController = navigationController.viewControllers.last {
            return firstController
        } else if let tabBarController = viewController as? UITabBarController,
            let selectedViewController = tabBarController.selectedViewController {
            return actualViewController(for: selectedViewController)
        } else {
            return viewController
        }
    }
}

extension AppCoordinator: SceneCoordinatorType {
    
    func updateCurrent(controller: UIViewController) {
        currentViewController = actualViewController(for: controller)
    }
    
    func transition(to scene: Scene, type: SceneTransitionType, animated: Bool, completion:(() -> Void)? = nil) {
        
        let viewController = scene.viewController()
        
        switch type {
        case .root:
            currentViewController = actualViewController(for: viewController)
            if animated {
                window.layer.add(WindowTransition().animation, forKey: kCATransition)
            }
            window.rootViewController = viewController
            window.makeKeyAndVisible()
            completion?()
            
        case .rootController:
            guard let navigationController = currentViewController?.navigationController else {
                fatalError("Can't change root without a navigation controller")
            }
            navigationController.viewControllers = [viewController]
            currentViewController = viewController
            completion?()
        case .push:
            guard let navigationController = currentViewController?.navigationController else {
                fatalError("Can't push a view controller without a current navigation controller")
            }
            navigationController.pushViewController(viewController, animated: animated)
            currentViewController = actualViewController(for: viewController)
        case .modal:
            currentViewController?.present(viewController, animated: animated) {
               completion?()
            }
            currentViewController = actualViewController(for: viewController)
        case .alert:
            currentViewController?.present(viewController, animated: animated) {
                completion?()
            }
        case .popup:
            viewController.modalPresentationStyle = .overFullScreen
            viewController.modalTransitionStyle = .crossDissolve
            currentViewController?.present(viewController, animated: animated, completion: {
                completion?()
            })
            currentViewController = viewController
        case let .popover(preferredContentSize, sender):
            
            let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: viewController)
            viewController.preferredContentSize = preferredContentSize
            presentationController.barButtonItem = sender
            presentationController.permittedArrowDirections = [.up]
            currentViewController?.present(viewController, animated: animated, completion: {
                completion?()
            })
           // currentViewController = viewController

        case let .tabbarItem(selectIndex, index):
            
            guard var tabBarVC = currentViewController?.tabBarController else {
                fatalError("Can't switch a view controller without a current tab bar controller")
            }
            
            while tabBarVC.tabBarController != nil {
                tabBarVC = tabBarVC.tabBarController!
            }
            guard let viewController = tabBarVC.viewControllers?[index] else {
                fatalError("Index not in range of the tab bar controller's view controllers.")
            }
            if selectIndex {
                tabBarVC.selectedIndex = index
            }
            currentViewController = actualViewController(for: viewController)
            completion?()
        }
    }
    
    func pop(animated: Bool, completion:(() -> Void)? = nil) {
        
        
        if let presenter = currentViewController?.presentingViewController {
            currentViewController?.dismiss(animated: animated) {
                self.currentViewController = self.actualViewController(for: presenter)
                completion?()
            }
            
        }
        
            /*    if let current = currentViewController,
             let navigationController = current.navigationController,
             navigationController.viewControllers.firstIndex(of: current) ?? -1 > 0 {
             // POP
             guard navigationController.popViewController(animated: animated) != nil else {
             fatalError("can't navigate back from \(String(describing: currentViewController))")
             }
             if let lastController = navigationController.viewControllers.last {
             currentViewController = actualViewController(for: lastController)
             } else {
             currentViewController = nil
             }
             
             } else { */
//            currentViewController?.dismiss(animated: animated) {
//                self.currentViewController = self.actualViewController(for: presenter)
//                completion?()
//            }
            //}
            
            
         else if let navigationController = currentViewController?.navigationController {
            
            guard navigationController.popViewController(animated: animated) != nil else {
                fatalError("can't navigate back from \(String(describing: currentViewController))")
            }
            if let lastController = navigationController.viewControllers.last {
                currentViewController = actualViewController(for: lastController)
            } else {
                currentViewController = nil
            }
            completion?()
        } else {
            fatalError("Not a modal, no navigation controller: can't navigate back from \(String(describing: currentViewController))")
        }
    }
}

