//
//  SceneTransitionType.swift
//  oribiky
//
//  Created by Hichem Harrath on 20/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import Foundation


enum SceneTransitionType {
    // you can extend this to add animated transition types,
    // interactive transitions and even child view controllers!
    case root       // make view controller the root view controller
    case push       // push view controller to navigation stack
    case modal      // present view controller modally
    case alert      // present alert
    case popup      // presenting PopupView Controler
    case popover(preferredContentSize: CGSize, sender: UIBarButtonItem)      // presenting popover Controler
    case tabbarItem(selectIndex: Bool, index: Int)      // presenting PopupView Controler
    case rootController
}
