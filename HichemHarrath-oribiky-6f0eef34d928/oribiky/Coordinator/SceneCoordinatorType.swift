//
//  SceneCoordinatorType.swift
//  oribiky
//
//  Created by Hichem Harrath on 20/09/2018.
//  Copyright © 2018 Oribiky. All rights reserved.
//

import UIKit

protocol SceneCoordinatorType: class {
    
    func transition(to scene: Scene, type: SceneTransitionType, animated: Bool, completion:(() -> Void)?)
    func pop(animated: Bool, completion:(() -> Void)?)
}
