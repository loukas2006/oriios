// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum APPStrings {

  internal enum Assistance {
    internal enum ChatTime {
      /// De 9H à 18H
      internal static let title = APPStrings.tr("Localizable", "Assistance.chatTime.title")
    }
    internal enum CreateTicket {
      /// Veuillez saisir le message de votre réclamation
      internal static let messageError = APPStrings.tr("Localizable", "Assistance.createTicket.messageError")
      /// Veuillez saisir le sujet de votre réclamation
      internal static let subjectError = APPStrings.tr("Localizable", "Assistance.createTicket.subjectError")
      /// Créer une réclamation
      internal static let title = APPStrings.tr("Localizable", "Assistance.createTicket.title")
    }
    internal enum FaqBtn {
      /// FAQ
      internal static let title = APPStrings.tr("Localizable", "Assistance.faqBtn.title")
    }
    internal enum Message {
      /// Message
      internal static let title = APPStrings.tr("Localizable", "Assistance.message.title")
    }
    internal enum MyTicketsBtn {
      /// Mes réclamations
      internal static let title = APPStrings.tr("Localizable", "Assistance.myTicketsBtn.title")
    }
    internal enum MyTicketsList {
      /// Aucune réclamation trouvée
      internal static let noTicketsTitle = APPStrings.tr("Localizable", "Assistance.myTicketsList.noTicketsTitle")
      /// Mes réclamations
      internal static let title = APPStrings.tr("Localizable", "Assistance.myTicketsList.title")
      internal enum CreateTicketBtn {
        /// Créer une réclamation
        internal static let title = APPStrings.tr("Localizable", "Assistance.myTicketsList.createTicketBtn.title")
      }
    }
    internal enum NoEmailError {
      /// Vous avez besoin d'une adresse email pour créer des réclamations et accéder au tchat.
      internal static let subtitle = APPStrings.tr("Localizable", "Assistance.noEmailError.subtitle")
    }
    internal enum Subject {
      /// Sujet
      internal static let title = APPStrings.tr("Localizable", "Assistance.subject.title")
    }
    internal enum TicketDetails {
      /// Votre demande est assignée au support technique %@
      internal static func assigneeChanged(_ p1: String) -> String {
        return APPStrings.tr("Localizable", "Assistance.ticketDetails.assigneeChanged", p1)
      }
      /// Le status de votre demande est passé de %@ à %@
      internal static func statusChanged(_ p1: String, _ p2: String) -> String {
        return APPStrings.tr("Localizable", "Assistance.ticketDetails.statusChanged", p1, p2)
      }
      /// le %@, %@ écrit:
      internal static func titleFormat(_ p1: String, _ p2: String) -> String {
        return APPStrings.tr("Localizable", "Assistance.ticketDetails.titleFormat", p1, p2)
      }
    }
    internal enum TicketStatus {
      /// Encours d'analyse
      internal static let `open` = APPStrings.tr("Localizable", "Assistance.ticketStatus.open")
      /// En attente
      internal static let pending = APPStrings.tr("Localizable", "Assistance.ticketStatus.pending")
      /// Résolu
      internal static let solved = APPStrings.tr("Localizable", "Assistance.ticketStatus.solved")
      /// Indésirable
      internal static let spam = APPStrings.tr("Localizable", "Assistance.ticketStatus.spam")
    }
  }

  internal enum Coupons {
    /// Coupons
    internal static let title = APPStrings.tr("Localizable", "Coupons.title")
    internal enum Header {
      /// Code Promo
      internal static let title = APPStrings.tr("Localizable", "Coupons.header.title")
    }
    internal enum Message {
      internal enum AucunCode {
        /// Aucun Code Promo
        internal static let title = APPStrings.tr("Localizable", "Coupons.message.aucunCode.title")
      }
      internal enum Mutiple {
        /// Codes actuellement activés sur votre compte
        internal static let title = APPStrings.tr("Localizable", "Coupons.message.mutiple.title")
      }
      internal enum One {
        /// Code actuellement activé sur votre compte
        internal static let title = APPStrings.tr("Localizable", "Coupons.message.one.title")
      }
    }
    internal enum PromoCode {
      /// Utiliser un code promo
      internal static let title = APPStrings.tr("Localizable", "Coupons.promoCode.title")
      internal enum AddBtn {
        /// Ajouter
        internal static let title = APPStrings.tr("Localizable", "Coupons.promoCode.addBtn.title")
      }
      internal enum Empty {
        /// Veuillez saisir le code promo
        internal static let title = APPStrings.tr("Localizable", "Coupons.promoCode.empty.title")
      }
      internal enum Fail {
        /// Code erroné
        internal static let title = APPStrings.tr("Localizable", "Coupons.promoCode.fail.title")
      }
      internal enum Success {
        /// Code ajouté avec succès
        internal static let title = APPStrings.tr("Localizable", "Coupons.promoCode.success.title")
      }
    }
  }

  internal enum HamburgerMenu {
    /// Coupons
    internal static let coupons = APPStrings.tr("Localizable", "HamburgerMenu.coupons")
    /// FAQ
    internal static let faq = APPStrings.tr("Localizable", "HamburgerMenu.faq")
    /// Mentions légales
    internal static let legalNotice = APPStrings.tr("Localizable", "HamburgerMenu.legalNotice")
    /// Connexion
    internal static let login = APPStrings.tr("Localizable", "HamburgerMenu.login")
    /// Déconnexion
    internal static let logout = APPStrings.tr("Localizable", "HamburgerMenu.logout")
    /// Réclamations
    internal static let tickets = APPStrings.tr("Localizable", "HamburgerMenu.tickets")
    internal enum Lougout {
      internal enum ConfimationPopup {
        /// Voulez-vous se déconnecter ?
        internal static let subtitle = APPStrings.tr("Localizable", "HamburgerMenu.lougout.confimationPopup.subtitle")
      }
    }
  }

  internal enum LegalNotice {
    /// Politiques de confidentialités
    internal static let privacyPolicyBtnTitle = APPStrings.tr("Localizable", "LegalNotice.privacyPolicyBtnTitle")
    /// Conditions générales de vente et d'utilisation du service
    internal static let termsBtnTitle = APPStrings.tr("Localizable", "LegalNotice.TermsBtnTitle")
  }

  internal enum Notification {
    internal enum TimeSinceNow {
      /// Il y a 1 jour
      internal static let day = APPStrings.tr("Localizable", "Notification.TimeSinceNow.day")
      /// Il y a %d jours
      internal static func days(_ p1: Int) -> String {
        return APPStrings.tr("Localizable", "Notification.TimeSinceNow.days", p1)
      }
      /// Il y a 1 heure
      internal static let hour = APPStrings.tr("Localizable", "Notification.TimeSinceNow.hour")
      /// Il y a %d heures
      internal static func hours(_ p1: Int) -> String {
        return APPStrings.tr("Localizable", "Notification.TimeSinceNow.hours", p1)
      }
      /// Il y a 1 minute
      internal static let minute = APPStrings.tr("Localizable", "Notification.TimeSinceNow.minute")
      /// Il y a %d minutes
      internal static func minutes(_ p1: Int) -> String {
        return APPStrings.tr("Localizable", "Notification.TimeSinceNow.minutes", p1)
      }
      /// Il y a 1 mois
      internal static let month = APPStrings.tr("Localizable", "Notification.TimeSinceNow.month")
      /// Il y a %d mois
      internal static func months(_ p1: Int) -> String {
        return APPStrings.tr("Localizable", "Notification.TimeSinceNow.months", p1)
      }
      /// Maintenant
      internal static let second = APPStrings.tr("Localizable", "Notification.TimeSinceNow.second")
      /// Il y a %d secondes
      internal static func seconds(_ p1: Int) -> String {
        return APPStrings.tr("Localizable", "Notification.TimeSinceNow.seconds", p1)
      }
      /// Il y a 1 semaine
      internal static let week = APPStrings.tr("Localizable", "Notification.TimeSinceNow.week")
      /// Il y a %d semaines
      internal static func weeks(_ p1: Int) -> String {
        return APPStrings.tr("Localizable", "Notification.TimeSinceNow.weeks", p1)
      }
      /// Il y a 1 an
      internal static let year = APPStrings.tr("Localizable", "Notification.TimeSinceNow.year")
      /// Il y a %d ans
      internal static func years(_ p1: Int) -> String {
        return APPStrings.tr("Localizable", "Notification.TimeSinceNow.years", p1)
      }
    }
  }

  internal enum Notifications {
    /// Aucunes notifications
    internal static let emptyDescription = APPStrings.tr("Localizable", "Notifications.emptyDescription")
    /// Notifications
    internal static let title = APPStrings.tr("Localizable", "Notifications.title")
    internal enum Popover {
      /// Notifications antérieures
      internal static let seeAllBtnTitle = APPStrings.tr("Localizable", "Notifications.popover.seeAllBtnTitle")
    }
  }

  internal enum RideResume {
    /// Mon Trajet
    internal static let title = APPStrings.tr("Localizable", "RideResume.title")
  }

  internal enum ActivateBluetooth {
    /// Autoriser
    internal static let autoriseBtnTitle = APPStrings.tr("Localizable", "activateBluetooth.autoriseBtnTitle")
    /// Activer Bluetooth
    internal static let title = APPStrings.tr("Localizable", "activateBluetooth.title")
    internal enum Popup {
      /// Veuillez activer le Bluetooth depuis les paramètres de votre mobile?
      internal static let subTitle = APPStrings.tr("Localizable", "activateBluetooth.popup.subTitle")
      /// Pour utiliser un vélo vous devez activer votre bluetooth
      internal static let title = APPStrings.tr("Localizable", "activateBluetooth.popup.title")
    }
  }

  internal enum AddressCell {
    /// Ville: 
    internal static let city = APPStrings.tr("Localizable", "addressCell.City")
    /// Numero: 
    internal static let number = APPStrings.tr("Localizable", "addressCell.number")
    /// Libellé: 
    internal static let street = APPStrings.tr("Localizable", "addressCell.street")
    /// code postal: 
    internal static let zipCode = APPStrings.tr("Localizable", "addressCell.zipCode")
  }

  internal enum Authenticate {
    /// recevoir un code
    internal static let askForCodeBtnTitle = APPStrings.tr("Localizable", "authenticate.askForCodeBtnTitle")
    /// Si vous ne le recevez pas dans 1min, vous pouvez refaire une demande en appuyant de nouveau sur le bouton recevoir code
    internal static let askForCodePopupSubtitle = APPStrings.tr("Localizable", "authenticate.askForCodePopupSubtitle")
    /// Vous allez recevoir un code par SMS qu'il faut saisir afin de valider votre authentification
    internal static let askForCodePopupTitle = APPStrings.tr("Localizable", "authenticate.askForCodePopupTitle")
    /// Merci de vérifier votre numéro de téléphone!
    internal static let phoneErrorTex = APPStrings.tr("Localizable", "authenticate.phoneErrorTex")
    /// Authentification
    internal static let title = APPStrings.tr("Localizable", "authenticate.title")
    /// Merci de saisir votre numéro de téléphone
    internal static let titleLabelText = APPStrings.tr("Localizable", "authenticate.titleLabelText")
    internal enum EnterCode {
      /// Demander un nouveau code
      internal static let askAgainForCodeBtnTitle = APPStrings.tr("Localizable", "authenticate.enterCode.askAgainForCodeBtnTitle")
      /// Merci de saisir votre code
      internal static let titleLabelText = APPStrings.tr("Localizable", "authenticate.enterCode.titleLabelText")
      /// S'identifier
      internal static let validateBtnTitle = APPStrings.tr("Localizable", "authenticate.enterCode.validateBtnTitle")
    }
  }

  internal enum AvailableBikes {
    /// Vélo %@
    internal static func bikeTitle(_ p1: String) -> String {
      return APPStrings.tr("Localizable", "availableBikes.bikeTitle", p1)
    }
    /// Merci de sélectionner le vélo souhaité
    internal static let description = APPStrings.tr("Localizable", "availableBikes.description")
    /// Rechercher un vélo
    internal static let title = APPStrings.tr("Localizable", "availableBikes.title")
  }

  internal enum BikeDiagnostic {
    /// Vérification
    internal static let stepTitle = APPStrings.tr("Localizable", "bikeDiagnostic.stepTitle")
    internal enum Brakes {
      /// Des freins
      internal static let subTitle = APPStrings.tr("Localizable", "bikeDiagnostic.brakes.subTitle")
    }
    internal enum Lights {
      /// Lumière
      internal static let subTitle = APPStrings.tr("Localizable", "bikeDiagnostic.lights.subTitle")
    }
    internal enum Loop {
      /// Est-ce que la chaine est bien attachée
      internal static let description = APPStrings.tr("Localizable", "bikeDiagnostic.loop.description")
      /// Chaine de sécurité
      internal static let subTitle = APPStrings.tr("Localizable", "bikeDiagnostic.loop.subTitle")
    }
    internal enum SkipBtn {
      /// Passer la vérification
      internal static let title = APPStrings.tr("Localizable", "bikeDiagnostic.skipBtn.title")
    }
    internal enum Tires {
      /// Pneux
      internal static let subTitle = APPStrings.tr("Localizable", "bikeDiagnostic.tires.subTitle")
    }
  }

  internal enum Common {
    /// Annuler
    internal static let cancel = APPStrings.tr("Localizable", "common.cancel")
    /// Confirmer
    internal static let confirm = APPStrings.tr("Localizable", "common.confirm")
    /// Error
    internal static let errorTitle = APPStrings.tr("Localizable", "common.errorTitle")
    /// Non
    internal static let no = APPStrings.tr("Localizable", "common.no")
    /// Aucune Connexion Internet
    internal static let noInternetConnection = APPStrings.tr("Localizable", "common.noInternetConnection")
    /// Ok
    internal static let ok = APPStrings.tr("Localizable", "common.ok")
    /// Oribiky A
    internal static let oribikyHave = APPStrings.tr("Localizable", "common.oribikyHave")
    /// Louer
    internal static let rent = APPStrings.tr("Localizable", "common.rent")
    /// Recommencer
    internal static let reStart = APPStrings.tr("Localizable", "common.reStart")
    /// Envoyer
    internal static let send = APPStrings.tr("Localizable", "common.send")
    /// votre session a expiré. veuillez vous reconnecter pour continuer.
    internal static let sessionExpired = APPStrings.tr("Localizable", "common.sessionExpired")
    /// Commencer
    internal static let start = APPStrings.tr("Localizable", "common.start")
    /// Valider
    internal static let validate = APPStrings.tr("Localizable", "common.validate")
    /// Oui
    internal static let yes = APPStrings.tr("Localizable", "common.yes")
    /// Vous avez
    internal static let youHave = APPStrings.tr("Localizable", "common.youHave")
  }

  internal enum Country {
    /// Recherche...
    internal static let searchTextFieldTitle = APPStrings.tr("Localizable", "country.searchTextFieldTitle")
  }

  internal enum Coupon {
    /// Code promo incorrect
    internal static let notExist = APPStrings.tr("Localizable", "coupon.not_exist")
  }

  internal enum CurrentRide {
    ///   reprendre ma location
    internal static let continueBtnTitle = APPStrings.tr("Localizable", "currentRide.continueBtnTitle")
    /// Terminer ma location
    internal static let finishBtnTitle = APPStrings.tr("Localizable", "currentRide.finishBtnTitle")
    ///   pause
    internal static let pauseBtnTitle = APPStrings.tr("Localizable", "currentRide.pauseBtnTitle")
    /// Location encours
    internal static let title = APPStrings.tr("Localizable", "currentRide.title")
    internal enum ConfirmationPopup {
      /// Voulez-vous terminer votre location ?
      internal static let subtitle = APPStrings.tr("Localizable", "currentRide.confirmationPopup.subtitle")
    }
    internal enum PauseConfirmationPopup {
      /// Pendant 10 minutes vous pouvez garer votre vélo en dehors d'un emplacement autorisé. \nAttention de respecter l'espace public et les autres usagers
      internal static let subtitle = APPStrings.tr("Localizable", "currentRide.pauseConfirmationPopup.subtitle")
      /// Location en pause
      internal static let title = APPStrings.tr("Localizable", "currentRide.pauseConfirmationPopup.title")
    }
  }

  internal enum DeclareParking {
    /// 20 minutes gratuites
    internal static let freeMinutes = APPStrings.tr("Localizable", "declareParking.freeMinutes")
    /// Vous pouvez déclarer cet emplacement comme un nouveau parking à vélos.\n\n Nous vous offrirons 20 minutes gratuites  d'utilisation pour vous remercier !\n\n En cas de déclaration abusive, une pénalité forfaitaire de 50 euros vous sera appliquée pour dédommager le temps de déplacements de nos équipes.\n\n\n Le crédit de minutes vous sera crédité après vérification par nos équipes.
    internal static let fullDescription = APPStrings.tr("Localizable", "declareParking.fullDescription")
    /// Le crédit de minutes vous sera crédité après vérification par nos équipes.
    internal static let notice = APPStrings.tr("Localizable", "declareParking.notice")
    /// Oui, je veux déclarer ce parking à vélos
    internal static let validateBtnTitle = APPStrings.tr("Localizable", "declareParking.validateBtnTitle")
    internal enum UnavailablePopup {
      /// Déclarer un nouveau parking à vélos
      internal static let declareActionTitle = APPStrings.tr("Localizable", "declareParking.unavailablePopup.declareActionTitle")
      /// Localiser le parking le plus proche
      internal static let localiseActionTitle = APPStrings.tr("Localizable", "declareParking.unavailablePopup.localiseActionTitle")
      /// Ajouter un nouvel emplacement de parking à vélos vous permet de gagner des minutes gratuites !
      internal static let notice = APPStrings.tr("Localizable", "declareParking.unavailablePopup.notice")
      /// Vous ne pouvez pas garer votre vélo à cet emplacement.
      internal static let title = APPStrings.tr("Localizable", "declareParking.unavailablePopup.title")
    }
  }

  internal enum Demo {
    /// Autoriser l'application à me localiser pour activer tout les vélos les plus proches
    internal static let firstStepSubTitle = APPStrings.tr("Localizable", "demo.firstStepSubTitle")
    /// Etape 1/4 :\nPartager ma position
    internal static let firstStepTitle = APPStrings.tr("Localizable", "demo.firstStepTitle")
    /// Choisisser un emplacement recommandé par l'application, attachez le vélo avec la chaine et mettez fin à votre trajet avec votre smartphone
    internal static let fourthStepSubTitle = APPStrings.tr("Localizable", "demo.fourthStepSubTitle")
    /// Etape 4/4 :\nVérrouiller le velo
    internal static let fourthStepTitle = APPStrings.tr("Localizable", "demo.fourthStepTitle")
    /// C'est parti !
    internal static let letsStartBtnTitle = APPStrings.tr("Localizable", "demo.letsStartBtnTitle")
    /// à l'aide de votre smartphone
    internal static let secondStepSubTitle = APPStrings.tr("Localizable", "demo.secondStepSubTitle")
    /// Etape 2/4 :\nDéverrouiller le vélo
    internal static let secondStepTitle = APPStrings.tr("Localizable", "demo.secondStepTitle")
    /// passer la démo
    internal static let skipDemoBtnTitle = APPStrings.tr("Localizable", "demo.skipDemoBtnTitle")
    /// Profiter du confort de l'assistance électrique !
    internal static let thirdStepSubTitle = APPStrings.tr("Localizable", "demo.thirdStepSubTitle")
    /// Etape 3/4 : C'est parti !
    internal static let thirdStepTitle = APPStrings.tr("Localizable", "demo.thirdStepTitle")
  }

  internal enum EditProfile {
    /// Veuillez saisir votre ville
    internal static let addressCityError = APPStrings.tr("Localizable", "editProfile.addressCityError")
    /// Veuillez saisir le nom de la voie
    internal static let addressStreetError = APPStrings.tr("Localizable", "editProfile.addressStreetError")
    /// Veuillez saisir un numéro valide
    internal static let addressStreetNumberError = APPStrings.tr("Localizable", "editProfile.addressStreetNumberError")
    /// Veuillez saisir un code postale valide
    internal static let addressZipCodeError = APPStrings.tr("Localizable", "editProfile.addressZipCodeError")
    /// Annuler
    internal static let cancelBtnTitle = APPStrings.tr("Localizable", "editProfile.cancelBtnTitle")
    /// Veuillez saisir une adresse mail valide
    internal static let emailError = APPStrings.tr("Localizable", "editProfile.emailError")
    /// Veuillez saisir votre nom
    internal static let firstNameError = APPStrings.tr("Localizable", "editProfile.firstNameError")
    /// Veuillez saisir votre prénom
    internal static let lastNameError = APPStrings.tr("Localizable", "editProfile.lastNameError")
    /// J'accepte de recevoir des offres personnalisées par email. Votre email ne sera pas commercialisé
    internal static let newsLetterLabelText = APPStrings.tr("Localizable", "editProfile.newsLetterLabelText")
    /// Editer mon profil
    internal static let title = APPStrings.tr("Localizable", "editProfile.title")
    /// Valider
    internal static let validateBtnTitle = APPStrings.tr("Localizable", "editProfile.validateBtnTitle")
  }

  internal enum Email {
    /// Invalid email address
    internal static let invalid = APPStrings.tr("Localizable", "email.invalid")
    /// E-mail is required
    internal static let `required` = APPStrings.tr("Localizable", "email.required")
  }

  internal enum FinishDiagnistic {
    /// Continuer à louer le vélo ?
    internal static let message = APPStrings.tr("Localizable", "finishDiagnistic.message")
    /// terminée
    internal static let subTitle = APPStrings.tr("Localizable", "finishDiagnistic.subTitle")
    /// Verfication
    internal static let title = APPStrings.tr("Localizable", "finishDiagnistic.title")
  }

  internal enum Firstname {
    /// Lastname is too long
    internal static let long = APPStrings.tr("Localizable", "firstname.long")
    /// Lastname is required
    internal static let `required` = APPStrings.tr("Localizable", "firstname.required")
    /// Lastname is too short
    internal static let short = APPStrings.tr("Localizable", "firstname.short")
  }

  internal enum Lastname {
    /// Firstname is too long
    internal static let long = APPStrings.tr("Localizable", "lastname.long")
    /// Firstname is required
    internal static let `required` = APPStrings.tr("Localizable", "lastname.required")
    /// Firstname is too short
    internal static let short = APPStrings.tr("Localizable", "lastname.short")
  }

  internal enum LinkaStatus {
    /// Connexion encours
    internal static let connection = APPStrings.tr("Localizable", "linkaStatus.connection")
    internal enum Lock {
      /// Verrouillage terminé
      internal static let finished = APPStrings.tr("Localizable", "linkaStatus.lock.finished")
      /// Vérrouillage encours
      internal static let inProgress = APPStrings.tr("Localizable", "linkaStatus.lock.inProgress")
      /// Verrouillage commencé
      internal static let started = APPStrings.tr("Localizable", "linkaStatus.lock.started")
    }
    internal enum Unlock {
      /// Déverrouillage terminé
      internal static let finished = APPStrings.tr("Localizable", "linkaStatus.unlock.finished")
      /// Déverrouillage encours
      internal static let inProgress = APPStrings.tr("Localizable", "linkaStatus.unlock.inProgress")
      /// Déverrouillage commencé
      internal static let started = APPStrings.tr("Localizable", "linkaStatus.unlock.started")
    }
  }

  internal enum LockSmartlockk {
    /// Vérrouillage encours
    internal static let title = APPStrings.tr("Localizable", "lockSmartlockk.title")
    internal enum InMove {
      /// Problème de vérrouillage! Merci de ne pas bouger le vélo.
      internal static let subtitle = APPStrings.tr("Localizable", "lockSmartlockk.inMove.subtitle")
    }
    internal enum StalledPopup {
      /// Problème de vérrouillage! Attention à ne pas gêner le fermeture du cadenas.
      internal static let subtitle = APPStrings.tr("Localizable", "lockSmartlockk.stalledPopup.subtitle")
    }
  }

  internal enum Map {
    /// Carte
    internal static let title = APPStrings.tr("Localizable", "map.title")
    internal enum Cluster {
      internal enum AvailableBikes {
        /// Merci de sélectionner le velo souhaité
        internal static let title = APPStrings.tr("Localizable", "map.cluster.availableBikes.title")
      }
    }
    internal enum Localisation {
      internal enum Popup {
        /// Voulez vous autoriser la localisation
        internal static let subtitle = APPStrings.tr("Localizable", "map.localisation.popup.subtitle")
        /// Localisation
        internal static let title = APPStrings.tr("Localizable", "map.localisation.popup.title")
      }
    }
    internal enum NoLocationView {
      /// Partager ma position
      internal static let subtitle = APPStrings.tr("Localizable", "map.noLocationView.subtitle")
      /// se localiser
      internal static let title = APPStrings.tr("Localizable", "map.noLocationView.title")
    }
    internal enum RentBikePopup {
      /// Plus tard
      internal static let layterBtnTitle = APPStrings.tr("Localizable", "map.rentBikePopup.layterBtnTitle")
      /// Maintenant
      internal static let nowBtnTitle = APPStrings.tr("Localizable", "map.rentBikePopup.nowBtnTitle")
      /// VÉLO %@ \nAUTONOMIE : %@ Km
      internal static func title(_ p1: String, _ p2: String) -> String {
        return APPStrings.tr("Localizable", "map.rentBikePopup.title", p1, p2)
      }
    }
    internal enum SearchView {
      /// Ce vélo est introuvable ou réservé par un autre utilisateur.
      internal static let noBikeFound = APPStrings.tr("Localizable", "map.searchView.noBikeFound")
      /// Entrer le numéro du vélo
      internal static let placeholder = APPStrings.tr("Localizable", "map.searchView.placeholder")
      /// Vélo
      internal static let title = APPStrings.tr("Localizable", "map.searchView.title")
    }
  }

  internal enum MySubscription {
    /// Gestion de mon abonnement
    internal static let title = APPStrings.tr("Localizable", "mySubscription.title")
    internal enum CancelBtn {
      /// Annuler\nmon abonnement
      internal static let title = APPStrings.tr("Localizable", "mySubscription.cancelBtn.title")
    }
    internal enum Date {
      /// Prochaine date\nde renouvellement :
      internal static let title = APPStrings.tr("Localizable", "mySubscription.date.title")
    }
    internal enum Description {
      /// Description:
      internal static let title = APPStrings.tr("Localizable", "mySubscription.description.title")
    }
    internal enum EditBtn {
      /// Modifier\nmon abonnement
      internal static let title = APPStrings.tr("Localizable", "mySubscription.editBtn.title")
    }
    internal enum ReactivateBtn {
      /// Réactiver\nmon abonnement
      internal static let title = APPStrings.tr("Localizable", "mySubscription.reactivateBtn.title")
    }
    internal enum State {
      /// Abonnement : 
      internal static let title = APPStrings.tr("Localizable", "mySubscription.state.title")
    }
    internal enum UploadFilesBtn {
      /// Télécharger les justificatifs
      internal static let title = APPStrings.tr("Localizable", "mySubscription.uploadFilesBtn.title")
    }
    internal enum UploadFilesState {
      /// Justificatifs en cours de validation
      internal static let inProgress = APPStrings.tr("Localizable", "mySubscription.uploadFilesState.inProgress")
    }
  }

  internal enum NewCard {
    /// J'accepte les conditions générales de vente et d'utilisation du service
    internal static let acceptTermOfUseAndSaleBtnTitle = APPStrings.tr("Localizable", "newCard.acceptTermOfUseAndSaleBtnTitle")
    /// Num de carte :
    internal static let cardNumber = APPStrings.tr("Localizable", "newCard.cardNumber")
    /// Veuillez saisir un numéro de carte valide
    internal static let cardNumberError = APPStrings.tr("Localizable", "newCard.cardNumberError")
    /// Titulaire :
    internal static let cardOwner = APPStrings.tr("Localizable", "newCard.cardOwner")
    /// Nom du titulaire obligatoire
    internal static let cardOwnerError = APPStrings.tr("Localizable", "newCard.cardOwnerError")
    /// J'accepte 
    internal static let checkBoxActionTitle = APPStrings.tr("Localizable", "newCard.checkBoxActionTitle")
    /// Confirmer
    internal static let confirmBtnTitle = APPStrings.tr("Localizable", "newCard.confirmBtnTitle")
    /// Code CVV :
    internal static let cvvCode = APPStrings.tr("Localizable", "newCard.cvvCode")
    /// Veuillez saisir le code CVV de votre carte
    internal static let cvvCodeError = APPStrings.tr("Localizable", "newCard.cvvCodeError")
    /// Date de validité :
    internal static let expireDate = APPStrings.tr("Localizable", "newCard.expireDate")
    /// Veuillez saisir la date de validité de votre carte
    internal static let expireDateError = APPStrings.tr("Localizable", "newCard.expireDateError")
    /// Scanner la carte
    internal static let scanBtnTitle = APPStrings.tr("Localizable", "newCard.scanBtnTitle")
    /// Données bancaires
    internal static let title = APPStrings.tr("Localizable", "newCard.title")
    internal enum CardValidateError {
      /// La validation de votre carte avec votre banque a échouée. Veuillez utiliser une autre carte bancaire.
      internal static let subtitle = APPStrings.tr("Localizable", "newCard.cardValidateError.subtitle")
    }
    internal enum CautionRequiredPopup {
      /// Une caution remboursable de 28 € + 2 € de frais de gestion sont demandés pour l’utilisation du service.
      internal static let subtitle = APPStrings.tr("Localizable", "newCard.cautionRequiredPopup.subtitle")
    }
    internal enum TermsAcceptationPopup {
      /// Ok
      internal static let okBtnTitle = APPStrings.tr("Localizable", "newCard.termsAcceptationPopup.okBtnTitle")
      /// Merci d’accepter les conditions générales de vente et d’utilisation de service
      internal static let subtitle = APPStrings.tr("Localizable", "newCard.termsAcceptationPopup.subtitle")
    }
    internal enum ThreeDSecurePopup {
      /// Vous allez être redirigé vers le site de votre banque.
      internal static let subtitle = APPStrings.tr("Localizable", "newCard.threeDSecurePopup.subtitle")
    }
  }

  internal enum PdfViewer {
    internal enum Faq {
      /// FAQ
      internal static let title = APPStrings.tr("Localizable", "pdfViewer.faq.title")
    }
    internal enum TermsOfSale {
      /// Terms Of Sale
      internal static let title = APPStrings.tr("Localizable", "pdfViewer.termsOfSale.title")
    }
    internal enum TermsOfUse {
      /// Terms Of Use
      internal static let title = APPStrings.tr("Localizable", "pdfViewer.termsOfUse.title")
    }
  }

  internal enum Phonenumber {
    /// Invalid phone number
    internal static let invalid = APPStrings.tr("Localizable", "phonenumber.invalid")
  }

  internal enum Profile {
    /// Mon Profil
    internal static let title = APPStrings.tr("Localizable", "profile.title")
  }

  internal enum ProfileCell {
    /// Adresse :
    internal static let address = APPStrings.tr("Localizable", "profileCell.address")
    /// Carte :
    internal static let card = APPStrings.tr("Localizable", "profileCell.Card")
    /// Email :
    internal static let email = APPStrings.tr("Localizable", "profileCell.email")
    /// Nom :
    internal static let firstName = APPStrings.tr("Localizable", "profileCell.firstName")
    /// Prenom :
    internal static let lastName = APPStrings.tr("Localizable", "profileCell.lastName")
    /// Tel :
    internal static let phone = APPStrings.tr("Localizable", "profileCell.phone")
  }

  internal enum Reservation {
    /// Réservation
    internal static let title = APPStrings.tr("Localizable", "reservation.title")
    internal enum ErrorPopup {
      /// Ce vélo ne peut pas être réservé. Veuillez en sélectionner un autre. Merci.
      internal static let subtitle = APPStrings.tr("Localizable", "reservation.errorPopup.subtitle")
    }
  }

  internal enum ScanCard {
    /// Merci de valider votre moyen de paiement en scannant votre carte bleu
    internal static let message = APPStrings.tr("Localizable", "scanCard.message")
    /// Scanner la carte
    internal static let title = APPStrings.tr("Localizable", "scanCard.title")
    /// Maintenez la carte à cet endroit.\nElle va être automatiquement scannée
    internal static let userGuideMessage = APPStrings.tr("Localizable", "scanCard.userGuideMessage")
    internal enum Popup {
      /// scanner
      internal static let scanBtnTitle = APPStrings.tr("Localizable", "scanCard.Popup.scanBtnTitle")
      /// Merci de valider votre moyen de paiement en scannant votre carte bleu
      internal static let subtitle = APPStrings.tr("Localizable", "scanCard.Popup.subtitle")
    }
  }

  internal enum SmartLock {
    internal enum ConnectionError {
      /// Connexion impossible avec le vélo.\nVeuillez appuyer de nouveau sur le bouton d'allumage du cadenas
      internal static let message = APPStrings.tr("Localizable", "smartLock.connectionError.message")
    }
  }

  internal enum Splash {
    /// Comment ça marche ?
    internal static let howItWorksBtnTitle = APPStrings.tr("Localizable", "splash.howItWorksBtnTitle")
    /// Utiliser maintenant
    internal static let startNowBtnTitle = APPStrings.tr("Localizable", "splash.startNowBtnTitle")
    /// Votre nouvelle solution de\n partage de vélos\n à assistance électrique en\n libre service !
    internal static let subtitle = APPStrings.tr("Localizable", "splash.subtitle")
    /// Bienvenue sur\n ORIBIKY
    internal static let title = APPStrings.tr("Localizable", "splash.title")
  }

  internal enum StartDiagnistic {
    /// Vous êtes connecté au vélo!
    internal static let title = APPStrings.tr("Localizable", "startDiagnistic.title")
    internal enum DoneBtn {
      /// commencer la vérification
      internal static let title = APPStrings.tr("Localizable", "startDiagnistic.doneBtn.title")
    }
  }
    
    internal enum UnlockSmartLock {
        /// Vous êtes connecté au vélo!
        
        internal enum DoneBtn {
            /// commencer la vérification
            internal static let title = APPStrings.tr("Localizable", "unlockSmartLockViewController.doneBtn.title")
        }
    }

  internal enum Street {
    internal enum Locality {
      /// Locality name is too long
      internal static let long = APPStrings.tr("Localizable", "street.locality.long")
      /// Locality name is required
      internal static let `required` = APPStrings.tr("Localizable", "street.locality.required")
      /// Locality name is too short
      internal static let short = APPStrings.tr("Localizable", "street.locality.short")
    }
    internal enum Name {
      /// Street name is too long
      internal static let long = APPStrings.tr("Localizable", "street.name.long")
      /// Street name is required
      internal static let `required` = APPStrings.tr("Localizable", "street.name.required")
      /// Street name is too short
      internal static let short = APPStrings.tr("Localizable", "street.name.short")
    }
    internal enum Number {
      /// Street number is required
      internal static let `required` = APPStrings.tr("Localizable", "street.number.required")
    }
    internal enum Postalcode {
      /// Postal code is required
      internal static let `required` = APPStrings.tr("Localizable", "street.postalcode.required")
    }
  }

  internal enum Subscription {
    internal enum Delivery {
      /// Vous bénéficiez de la formule livreur-coursier !\n\nTarif 1,8 € de l'heure.\n\nFacturation minimale de 18 € par mois pour 10h d'utilisation.\n\nFacturation complémentaire en fin de mois pour les heures utilisées.\n\nConditions de rétractation : envoyer un email à support@oribiky.com 10 jours avant le renouvellement du contrat.\n\nSupport téléphonique : 09 87 18 20 16
      internal static let details = APPStrings.tr("Localizable", "subscription.delivery.details")
      /// Formule livreur-coursier
      internal static let title = APPStrings.tr("Localizable", "subscription.delivery.title")
      internal enum Details {
        /// Tarif 1,8 € de l'heure.
        internal static let highlighted = APPStrings.tr("Localizable", "subscription.delivery.details.highlighted")
      }
    }
    internal enum Details {
      /// s'abonner
      internal static let subscribe = APPStrings.tr("Localizable", "subscription.details.subscribe")
      /// m'inscrire
      internal static let subscribeBtnTitle = APPStrings.tr("Localizable", "subscription.details.subscribeBtnTitle")
      /// Gestion des abonnements
      internal static let title = APPStrings.tr("Localizable", "subscription.details.title")
      internal enum ValidationPopup {
        /// Non
        internal static let noAction = APPStrings.tr("Localizable", "subscription.details.validationPopup.noAction")
        /// Vous avez choisi de s’inscrire à notre %@
        internal static func subtitle(_ p1: String) -> String {
          return APPStrings.tr("Localizable", "subscription.details.validationPopup.subtitle", p1)
        }
        /// Ok
        internal static let yesAction = APPStrings.tr("Localizable", "subscription.details.validationPopup.yesAction")
      }
    }
    internal enum Eligibility {
      /// Mes photos
      internal static let myPicturesBtnTitle = APPStrings.tr("Localizable", "subscription.eligibility.myPicturesBtnTitle")
      /// justificatifs nécessaires
      internal static let neededDocuments = APPStrings.tr("Localizable", "subscription.eligibility.neededDocuments")
      /// Merci de vérifier que vous êtes éligible à cet offre, sinon des charges supplémentaires seront facturées
      internal static let popupTitle = APPStrings.tr("Localizable", "subscription.eligibility.popupTitle")
      /// Prendre une photo
      internal static let takeAPictureBtnTitle = APPStrings.tr("Localizable", "subscription.eligibility.takeAPictureBtnTitle")
      /// Documents justificatifs
      internal static let title = APPStrings.tr("Localizable", "subscription.eligibility.title")
      internal enum Ass {
        /// justifiant de leur identité (carte d'identité ou passeport exclusivement, en cours de validité), d'un avis non-imposition en France de moins d'un an et d'une attestation de droit à la ASS de moins de 3 mois.
        internal static let details = APPStrings.tr("Localizable", "subscription.eligibility.ass.details")
        /// Personnes non-imposables et bénéficiares de l'allocation de solidarité spécifique (ASS)
        internal static let title = APPStrings.tr("Localizable", "subscription.eligibility.ass.title")
      }
      internal enum Cmu {
        /// justifiant de leur identité (carte d'identité ou passeport exclusivement, en cours de validité), d'un avis non-imposition en France de moins d'un an et d'une attestation de droit à la CMU-C de moins de 3 mois.
        internal static let details = APPStrings.tr("Localizable", "subscription.eligibility.cmu.details")
        /// Personnes non-imposables et allocataires de la CMU-C
        internal static let title = APPStrings.tr("Localizable", "subscription.eligibility.cmu.title")
      }
      internal enum Retired {
        /// justifiant de leur identité (carte d'identité ou passeport exclusivement, en cours de validité), d'une attestation datant de moins de trois mois de paiement d'une retraite en France.
        internal static let details = APPStrings.tr("Localizable", "subscription.eligibility.retired.details")
        /// Personnes retraitées
        internal static let title = APPStrings.tr("Localizable", "subscription.eligibility.retired.title")
      }
      internal enum Rsa {
        /// justifiant de leur identité (carte d'identité ou passeport exclusivement, en cours de validité), d'un avis non-imposition en France de moins d'un an et d'une attestation de droit au RSA de moins de 3 mois.
        internal static let details = APPStrings.tr("Localizable", "subscription.eligibility.rsa.details")
        /// Personnes non-imposables et allocataires du Revenu de Solidarité Active (RSA)
        internal static let title = APPStrings.tr("Localizable", "subscription.eligibility.rsa.title")
      }
      internal enum Student {
        /// justifiant de leur identité (carte d'identité ou passeport exclusivement, en cours de validité), d'un avis non-imposition en France de moins d'un an et d'une carte d'étudiant en cours de validité.
        internal static let details = APPStrings.tr("Localizable", "subscription.eligibility.student.details")
        /// Personnes non-imposables avec un statut d'étudiant
        internal static let title = APPStrings.tr("Localizable", "subscription.eligibility.student.title")
      }
    }
    internal enum LaunchOffer {
      /// Offre réservée aux 200 premiers inscrits.\n\nTrajets illimités avec 20 minutes gratuites d’utilisation chaque heure et 0,05 € la minute supplémentaire seulement.\nValidité : 1 an à compter de la date d’achat. Délai de rétractation de 14 jours conformément aux articles L 221-18 et suivants du code de la consommation.\nPaiement en un seul prélèvement de 239,88 €.
      internal static let details = APPStrings.tr("Localizable", "subscription.launchOffer.details")
      /// 19,99€ /mois
      internal static let price = APPStrings.tr("Localizable", "subscription.launchOffer.price")
      /// Offre de lancement
      internal static let title = APPStrings.tr("Localizable", "subscription.launchOffer.title")
      internal enum Details {
        /// Offre réservée aux 200 premiers inscrits.
        internal static let highlighted = APPStrings.tr("Localizable", "subscription.launchOffer.details.highlighted")
      }
    }
    internal enum List {
      /// Choisissez la meilleure formule selon votre fréquence d'utilisation :
      internal static let description = APPStrings.tr("Localizable", "subscription.list.description")
      /// Gestion des abonnements
      internal static let title = APPStrings.tr("Localizable", "subscription.list.title")
    }
    internal enum MinuteUse {
      /// 0,12 € la minute d’utilisation.\nL’utilisation immédiate de ce service entraîne la renonciation sans réserve au délai de rétractation de 14 jours conformément aux articles L221-18 et suivants du code de la consommation.
      internal static let details = APPStrings.tr("Localizable", "subscription.minuteUse.details")
      /// 0,12 € / minute
      internal static let price = APPStrings.tr("Localizable", "subscription.minuteUse.price")
      /// Libre-service sans abonnement
      internal static let title = APPStrings.tr("Localizable", "subscription.minuteUse.title")
    }
    internal enum Package {
      /// Vous disposez actuellement de %@ prépayées sur votre compte
      internal static func balance(_ p1: String) -> String {
        return APPStrings.tr("Localizable", "subscription.package.balance", p1)
      }
      /// %@ minutes
      internal static func balanceMinutes(_ p1: String) -> String {
        return APPStrings.tr("Localizable", "subscription.package.balanceMinutes", p1)
      }
      internal enum Details {
        internal enum Highlighted {
          /// L’utilisation immédiate de ce service entraîne la renonciation sans réserve au délai de rétractation de 14 jours conformément aux articles L221-18 et suivants du code de la consommation.
          internal static let l22118 = APPStrings.tr("Localizable", "subscription.package.details.highlighted.L221-18")
        }
      }
      internal enum DiscoveryPrice {
        /// Offre réservée aux  500 premiers packs vendus. 240 minutes prépayées utilisables dans les 3 mois suivant la date d’achat. Délai de rétractation de 14 jours conformément aux articles L 221-18 et suivants du code de la consommation. Offre non cumulable avec toute autre offre promotionnelle. Paiement en un seul prélèvement de 24 €.
        internal static let details = APPStrings.tr("Localizable", "subscription.package.discoveryPrice.details")
        /// 24€ soit 0,10€ la minute
        internal static let price = APPStrings.tr("Localizable", "subscription.package.discoveryPrice.price")
        /// 240 minutes prépayées
        internal static let title = APPStrings.tr("Localizable", "subscription.package.discoveryPrice.title")
        internal enum Details {
          /// Offre réservée aux  500 premiers packs vendus.
          internal static let highlighted = APPStrings.tr("Localizable", "subscription.package.discoveryPrice.details.highlighted")
        }
      }
      internal enum GoodPlan {
        /// Offre valable jusqu’au 31/03/2019. Trajets illimités. Validité immédiate et pour une durée de 24h à partir de la date d’achat. Paiement en un seul prélèvement de 9 €.\nL’utilisation immédiate de ce service entraîne la renonciation sans réserve au délai de rétractation de 14 jours conformément aux articles L221-18 et suivants du code de la consommation.
        internal static let details = APPStrings.tr("Localizable", "subscription.package.goodPlan.details")
        /// 9 € la journée
        internal static let price = APPStrings.tr("Localizable", "subscription.package.goodPlan.price")
        /// Bon plan (-40%)
        internal static let title = APPStrings.tr("Localizable", "subscription.package.goodPlan.title")
        internal enum Details {
          /// Offre valable jusqu’au 31/03/2019
          internal static let highlighted = APPStrings.tr("Localizable", "subscription.package.goodPlan.details.highlighted")
        }
      }
      internal enum UnlimitedOneDay {
        /// Trajets illimités. Validité immédiate et pour une durée de 24h à partir de la date d’achat. Paiement en un seul prélèvement de 15 €.\nL’utilisation immédiate de ce service entraîne la renonciation sans réserve au délai de rétractation de 14 jours conformément aux articles L221-18 et suivants du code de la consommation.
        internal static let details = APPStrings.tr("Localizable", "subscription.package.unlimitedOneDay.details")
        /// 15€ la journée
        internal static let price = APPStrings.tr("Localizable", "subscription.package.unlimitedOneDay.price")
        /// Trajets illimités 24h
        internal static let title = APPStrings.tr("Localizable", "subscription.package.unlimitedOneDay.title")
        internal enum Details {
          /// Trajets illimités.
          internal static let highlighted = APPStrings.tr("Localizable", "subscription.package.unlimitedOneDay.details.highlighted")
        }
      }
    }
    internal enum Students {
      /// Moins de 0,50 € par jour !\n\nTrajets illimités avec 20 minutes gratuites d’utilisation chaque heure et 0,05 € la minute supplémentaire seulement.\n\nValidité : 1 an à compter de la date d’achat.\n\nDélai de rétractation de 14 jours conformément aux articles L 221-18 et suivants du code de la consommation.\n\nOffre non cumulable avec toute autre offre promotionnelle.\n\nPaiement en un seul prélèvement de 179,88 €.
      internal static let details = APPStrings.tr("Localizable", "subscription.students.details")
      /// 14,99€ /mois
      internal static let price = APPStrings.tr("Localizable", "subscription.students.price")
      /// Offre étudiants, bas revenus ou retraités
      internal static let title = APPStrings.tr("Localizable", "subscription.students.title")
      internal enum Details {
        /// Moins de 0,50 € par jour !
        internal static let highlighted = APPStrings.tr("Localizable", "subscription.students.details.highlighted")
      }
    }
    internal enum UnlimitedOneYear {
      /// Trajets illimités avec 20 minutes gratuites d’utilisation chaque heure et 0,05 € la minute supplémentaire seulement.\n\nValidité : 1 an à compter de la date d’achat.\n\nDélai de rétractation de 14 jours conformément aux articles L 221-18 et suivants du code de la consommation.\n\nPaiement en un seul prélèvement de 395,88 €.
      internal static let details = APPStrings.tr("Localizable", "subscription.unlimitedOneYear.details")
      /// 32,99€ /mois
      internal static let price = APPStrings.tr("Localizable", "subscription.unlimitedOneYear.price")
      /// TRAJETS ILLIMITES - 1 AN
      internal static let title = APPStrings.tr("Localizable", "subscription.unlimitedOneYear.title")
      internal enum Details {
        /// Trajets illimités
        internal static let highlighted = APPStrings.tr("Localizable", "subscription.unlimitedOneYear.details.highlighted")
      }
    }
  }

  internal enum TabBar {
    /// Carte
    internal static let mapTitle = APPStrings.tr("Localizable", "tabBar.mapTitle")
    /// Mon Profil
    internal static let profileTitle = APPStrings.tr("Localizable", "tabBar.profileTitle")
    /// Assistance
    internal static let reportTitle = APPStrings.tr("Localizable", "tabBar.reportTitle")
    /// Abonnement
    internal static let subscriptionTitle = APPStrings.tr("Localizable", "tabBar.subscriptionTitle")
    /// Mes trajets
    internal static let tripsTitle = APPStrings.tr("Localizable", "tabBar.tripsTitle")
  }

  internal enum Trips {
    internal enum DeleteAllBtn {
      /// Supprimer tout
      internal static let title = APPStrings.tr("Localizable", "trips.deleteAllBtn.title")
    }
    internal enum DeleteBtn {
      /// Supprimer
      internal static let title = APPStrings.tr("Localizable", "trips.deleteBtn.title")
    }
    internal enum DeleteConfirmationPopup {
      internal enum SubTitle {
        /// Voulez-vous supprimer les trajets ?
        internal static let mutiple = APPStrings.tr("Localizable", "trips.deleteConfirmationPopup.subTitle.mutiple")
        /// Voulez-vous supprimer le trajet ?
        internal static let single = APPStrings.tr("Localizable", "trips.deleteConfirmationPopup.subTitle.single")
      }
    }
    internal enum DeleteOneBtn {
      /// Supprimer un trajet
      internal static let title = APPStrings.tr("Localizable", "trips.deleteOneBtn.title")
    }
  }

  internal enum TurnOnBike {
    /// Appuyer sur le bouton situé\nà l'arrière du vélo
    internal static let description = APPStrings.tr("Localizable", "turnOnBike.description")
    /// c'est fait
    internal static let doneBtnTitle = APPStrings.tr("Localizable", "turnOnBike.doneBtnTitle")
    /// Allumer la batterie
    internal static let title = APPStrings.tr("Localizable", "turnOnBike.title")
  }

  internal enum UnlockSmartlock {
    /// Vélo %@
    internal static func bikeNumberTitle(_ p1: String) -> String {
      return APPStrings.tr("Localizable", "unlockSmartlock.BikeNumberTitle", p1)
    }
    /// Appuyer sur le bouton ON\ndu cadenas situé sur la roue arrière
    internal static let description = APPStrings.tr("Localizable", "unlockSmartlock.description")
    /// c'est fait
    internal static let doneBtnTitle = APPStrings.tr("Localizable", "unlockSmartlock.doneBtnTitle")
    /// Merci de Procéder à quelques vérifications
    internal static let message = APPStrings.tr("Localizable", "unlockSmartlock.message")
    /// Allumer le vélo
    internal static let title = APPStrings.tr("Localizable", "unlockSmartlock.title")
    internal enum ErrorPopup {
      /// Ce vélo ne peut pas être loué. Veuillez en sélectionner un autre. Merci.
      internal static let subtitle = APPStrings.tr("Localizable", "unlockSmartlock.errorPopup.subtitle")
    }
    internal enum ScanTimeoutPopup {
      /// Le vélo n'est pas détectable, merci d'allumer le cadenas et réessayer de nouveau
      internal static let subtitle = APPStrings.tr("Localizable", "unlockSmartlock.scanTimeoutPopup.subtitle")
    }
    internal enum UnlockInProgress {
      /// Déverrouillage encours
      internal static let title = APPStrings.tr("Localizable", "unlockSmartlock.unlockInProgress.title")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension APPStrings {
  private static func tr(_ table: String, _ key: String) -> String {
    return NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
  }

  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
