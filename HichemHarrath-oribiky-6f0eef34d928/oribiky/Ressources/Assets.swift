// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  internal typealias AssetColorTypeAlias = NSColor
  internal typealias AssetImageTypeAlias = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  internal typealias AssetColorTypeAlias = UIColor
  internal typealias AssetImageTypeAlias = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal enum Assistance {

    internal static let chatIcon = ImageAsset(name: "Assistance/chatIcon")
    internal static let dateReclamation = ImageAsset(name: "Assistance/dateReclamation")
    internal static let mobileIcon = ImageAsset(name: "Assistance/mobileIcon")
    internal static let reclamationOpened = ImageAsset(name: "Assistance/reclamationOpened")
    internal static let reclamationPending = ImageAsset(name: "Assistance/reclamationPending")
    internal static let reclamationSolved = ImageAsset(name: "Assistance/reclamationSolved")
    internal static let reclamationSpam = ImageAsset(name: "Assistance/reclamationSpam")
    internal static let timingIcon = ImageAsset(name: "Assistance/timingIcon")
  }
  internal enum Card {

    internal static let checkboxOff = ImageAsset(name: "Card/CheckboxOff")
    internal static let checkboxOn = ImageAsset(name: "Card/CheckboxOn")
  }
  internal enum Countries {

    internal static let ad = ImageAsset(name: "Countries/AD")
    internal static let ae = ImageAsset(name: "Countries/AE")
    internal static let af = ImageAsset(name: "Countries/AF")
    internal static let ag = ImageAsset(name: "Countries/AG")
    internal static let ai = ImageAsset(name: "Countries/AI")
    internal static let al = ImageAsset(name: "Countries/AL")
    internal static let am = ImageAsset(name: "Countries/AM")
    internal static let ao = ImageAsset(name: "Countries/AO")
    internal static let aq = ImageAsset(name: "Countries/AQ")
    internal static let ar = ImageAsset(name: "Countries/AR")
    internal static let `as` = ImageAsset(name: "Countries/AS")
    internal static let at = ImageAsset(name: "Countries/AT")
    internal static let au = ImageAsset(name: "Countries/AU")
    internal static let aw = ImageAsset(name: "Countries/AW")
    internal static let ax = ImageAsset(name: "Countries/AX")
    internal static let az = ImageAsset(name: "Countries/AZ")
    internal static let ba = ImageAsset(name: "Countries/BA")
    internal static let bb = ImageAsset(name: "Countries/BB")
    internal static let bd = ImageAsset(name: "Countries/BD")
    internal static let be = ImageAsset(name: "Countries/BE")
    internal static let bf = ImageAsset(name: "Countries/BF")
    internal static let bg = ImageAsset(name: "Countries/BG")
    internal static let bh = ImageAsset(name: "Countries/BH")
    internal static let bi = ImageAsset(name: "Countries/BI")
    internal static let bj = ImageAsset(name: "Countries/BJ")
    internal static let bl = ImageAsset(name: "Countries/BL")
    internal static let bm = ImageAsset(name: "Countries/BM")
    internal static let bn = ImageAsset(name: "Countries/BN")
    internal static let bo = ImageAsset(name: "Countries/BO")
    internal static let br = ImageAsset(name: "Countries/BR")
    internal static let bs = ImageAsset(name: "Countries/BS")
    internal static let bt = ImageAsset(name: "Countries/BT")
    internal static let bv = ImageAsset(name: "Countries/BV")
    internal static let bw = ImageAsset(name: "Countries/BW")
    internal static let by = ImageAsset(name: "Countries/BY")
    internal static let bz = ImageAsset(name: "Countries/BZ")
    internal static let ca = ImageAsset(name: "Countries/CA")
    internal static let cc = ImageAsset(name: "Countries/CC")
    internal static let cd = ImageAsset(name: "Countries/CD")
    internal static let cf = ImageAsset(name: "Countries/CF")
    internal static let cg = ImageAsset(name: "Countries/CG")
    internal static let ch = ImageAsset(name: "Countries/CH")
    internal static let ci = ImageAsset(name: "Countries/CI")
    internal static let ck = ImageAsset(name: "Countries/CK")
    internal static let cl = ImageAsset(name: "Countries/CL")
    internal static let cm = ImageAsset(name: "Countries/CM")
    internal static let cn = ImageAsset(name: "Countries/CN")
    internal static let co = ImageAsset(name: "Countries/CO")
    internal static let cr = ImageAsset(name: "Countries/CR")
    internal static let cu = ImageAsset(name: "Countries/CU")
    internal static let cv = ImageAsset(name: "Countries/CV")
    internal static let cw = ImageAsset(name: "Countries/CW")
    internal static let cx = ImageAsset(name: "Countries/CX")
    internal static let cy = ImageAsset(name: "Countries/CY")
    internal static let cz = ImageAsset(name: "Countries/CZ")
    internal static let de = ImageAsset(name: "Countries/DE")
    internal static let dj = ImageAsset(name: "Countries/DJ")
    internal static let dk = ImageAsset(name: "Countries/DK")
    internal static let dm = ImageAsset(name: "Countries/DM")
    internal static let `do` = ImageAsset(name: "Countries/DO")
    internal static let dz = ImageAsset(name: "Countries/DZ")
    internal static let ec = ImageAsset(name: "Countries/EC")
    internal static let ee = ImageAsset(name: "Countries/EE")
    internal static let eg = ImageAsset(name: "Countries/EG")
    internal static let eng = ImageAsset(name: "Countries/ENG")
    internal static let er = ImageAsset(name: "Countries/ER")
    internal static let es = ImageAsset(name: "Countries/ES")
    internal static let et = ImageAsset(name: "Countries/ET")
    internal static let eu = ImageAsset(name: "Countries/EU")
    internal static let fi = ImageAsset(name: "Countries/FI")
    internal static let fj = ImageAsset(name: "Countries/FJ")
    internal static let fk = ImageAsset(name: "Countries/FK")
    internal static let fm = ImageAsset(name: "Countries/FM")
    internal static let fo = ImageAsset(name: "Countries/FO")
    internal static let fr = ImageAsset(name: "Countries/FR")
    internal static let ga = ImageAsset(name: "Countries/GA")
    internal static let gb = ImageAsset(name: "Countries/GB")
    internal static let gd = ImageAsset(name: "Countries/GD")
    internal static let ge = ImageAsset(name: "Countries/GE")
    internal static let gf = ImageAsset(name: "Countries/GF")
    internal static let gg = ImageAsset(name: "Countries/GG")
    internal static let gh = ImageAsset(name: "Countries/GH")
    internal static let gi = ImageAsset(name: "Countries/GI")
    internal static let gl = ImageAsset(name: "Countries/GL")
    internal static let gm = ImageAsset(name: "Countries/GM")
    internal static let gn = ImageAsset(name: "Countries/GN")
    internal static let gp = ImageAsset(name: "Countries/GP")
    internal static let gq = ImageAsset(name: "Countries/GQ")
    internal static let gr = ImageAsset(name: "Countries/GR")
    internal static let gs = ImageAsset(name: "Countries/GS")
    internal static let gt = ImageAsset(name: "Countries/GT")
    internal static let gu = ImageAsset(name: "Countries/GU")
    internal static let gw = ImageAsset(name: "Countries/GW")
    internal static let gy = ImageAsset(name: "Countries/GY")
    internal static let hk = ImageAsset(name: "Countries/HK")
    internal static let hm = ImageAsset(name: "Countries/HM")
    internal static let hn = ImageAsset(name: "Countries/HN")
    internal static let hr = ImageAsset(name: "Countries/HR")
    internal static let ht = ImageAsset(name: "Countries/HT")
    internal static let hu = ImageAsset(name: "Countries/HU")
    internal static let id = ImageAsset(name: "Countries/ID")
    internal static let ie = ImageAsset(name: "Countries/IE")
    internal static let il = ImageAsset(name: "Countries/IL")
    internal static let im = ImageAsset(name: "Countries/IM")
    internal static let `in` = ImageAsset(name: "Countries/IN")
    internal static let io = ImageAsset(name: "Countries/IO")
    internal static let iq = ImageAsset(name: "Countries/IQ")
    internal static let ir = ImageAsset(name: "Countries/IR")
    internal static let `is` = ImageAsset(name: "Countries/IS")
    internal static let it = ImageAsset(name: "Countries/IT")
    internal static let je = ImageAsset(name: "Countries/JE")
    internal static let jm = ImageAsset(name: "Countries/JM")
    internal static let jo = ImageAsset(name: "Countries/JO")
    internal static let jp = ImageAsset(name: "Countries/JP")
    internal static let ke = ImageAsset(name: "Countries/KE")
    internal static let kg = ImageAsset(name: "Countries/KG")
    internal static let kh = ImageAsset(name: "Countries/KH")
    internal static let ki = ImageAsset(name: "Countries/KI")
    internal static let km = ImageAsset(name: "Countries/KM")
    internal static let kn = ImageAsset(name: "Countries/KN")
    internal static let kp = ImageAsset(name: "Countries/KP")
    internal static let kr = ImageAsset(name: "Countries/KR")
    internal static let kw = ImageAsset(name: "Countries/KW")
    internal static let ky = ImageAsset(name: "Countries/KY")
    internal static let kz = ImageAsset(name: "Countries/KZ")
    internal static let la = ImageAsset(name: "Countries/LA")
    internal static let lb = ImageAsset(name: "Countries/LB")
    internal static let lc = ImageAsset(name: "Countries/LC")
    internal static let lgbt = ImageAsset(name: "Countries/LGBT")
    internal static let li = ImageAsset(name: "Countries/LI")
    internal static let lk = ImageAsset(name: "Countries/LK")
    internal static let lr = ImageAsset(name: "Countries/LR")
    internal static let ls = ImageAsset(name: "Countries/LS")
    internal static let lt = ImageAsset(name: "Countries/LT")
    internal static let lu = ImageAsset(name: "Countries/LU")
    internal static let lv = ImageAsset(name: "Countries/LV")
    internal static let ly = ImageAsset(name: "Countries/LY")
    internal static let ma = ImageAsset(name: "Countries/MA")
    internal static let mc = ImageAsset(name: "Countries/MC")
    internal static let md = ImageAsset(name: "Countries/MD")
    internal static let me = ImageAsset(name: "Countries/ME")
    internal static let mf = ImageAsset(name: "Countries/MF")
    internal static let mg = ImageAsset(name: "Countries/MG")
    internal static let mh = ImageAsset(name: "Countries/MH")
    internal static let mk = ImageAsset(name: "Countries/MK")
    internal static let ml = ImageAsset(name: "Countries/ML")
    internal static let mm = ImageAsset(name: "Countries/MM")
    internal static let mn = ImageAsset(name: "Countries/MN")
    internal static let mo = ImageAsset(name: "Countries/MO")
    internal static let mp = ImageAsset(name: "Countries/MP")
    internal static let mq = ImageAsset(name: "Countries/MQ")
    internal static let mr = ImageAsset(name: "Countries/MR")
    internal static let ms = ImageAsset(name: "Countries/MS")
    internal static let mt = ImageAsset(name: "Countries/MT")
    internal static let mu = ImageAsset(name: "Countries/MU")
    internal static let mv = ImageAsset(name: "Countries/MV")
    internal static let mw = ImageAsset(name: "Countries/MW")
    internal static let mx = ImageAsset(name: "Countries/MX")
    internal static let my = ImageAsset(name: "Countries/MY")
    internal static let mz = ImageAsset(name: "Countries/MZ")
    internal static let na = ImageAsset(name: "Countries/NA")
    internal static let nc = ImageAsset(name: "Countries/NC")
    internal static let ne = ImageAsset(name: "Countries/NE")
    internal static let nf = ImageAsset(name: "Countries/NF")
    internal static let ng = ImageAsset(name: "Countries/NG")
    internal static let ni = ImageAsset(name: "Countries/NI")
    internal static let nir = ImageAsset(name: "Countries/NIR")
    internal static let nl = ImageAsset(name: "Countries/NL")
    internal static let no = ImageAsset(name: "Countries/NO")
    internal static let np = ImageAsset(name: "Countries/NP")
    internal static let nr = ImageAsset(name: "Countries/NR")
    internal static let nu = ImageAsset(name: "Countries/NU")
    internal static let nz = ImageAsset(name: "Countries/NZ")
    internal static let om = ImageAsset(name: "Countries/OM")
    internal static let pa = ImageAsset(name: "Countries/PA")
    internal static let pe = ImageAsset(name: "Countries/PE")
    internal static let pf = ImageAsset(name: "Countries/PF")
    internal static let pg = ImageAsset(name: "Countries/PG")
    internal static let ph = ImageAsset(name: "Countries/PH")
    internal static let pk = ImageAsset(name: "Countries/PK")
    internal static let pl = ImageAsset(name: "Countries/PL")
    internal static let pm = ImageAsset(name: "Countries/PM")
    internal static let pn = ImageAsset(name: "Countries/PN")
    internal static let pr = ImageAsset(name: "Countries/PR")
    internal static let ps = ImageAsset(name: "Countries/PS")
    internal static let pt = ImageAsset(name: "Countries/PT")
    internal static let pw = ImageAsset(name: "Countries/PW")
    internal static let py = ImageAsset(name: "Countries/PY")
    internal static let qa = ImageAsset(name: "Countries/QA")
    internal static let re = ImageAsset(name: "Countries/RE")
    internal static let ro = ImageAsset(name: "Countries/RO")
    internal static let rs = ImageAsset(name: "Countries/RS")
    internal static let ru = ImageAsset(name: "Countries/RU")
    internal static let rw = ImageAsset(name: "Countries/RW")
    internal static let sa = ImageAsset(name: "Countries/SA")
    internal static let sb = ImageAsset(name: "Countries/SB")
    internal static let sc = ImageAsset(name: "Countries/SC")
    internal static let sct = ImageAsset(name: "Countries/SCT")
    internal static let sd = ImageAsset(name: "Countries/SD")
    internal static let se = ImageAsset(name: "Countries/SE")
    internal static let sg = ImageAsset(name: "Countries/SG")
    internal static let sh = ImageAsset(name: "Countries/SH")
    internal static let si = ImageAsset(name: "Countries/SI")
    internal static let sj = ImageAsset(name: "Countries/SJ")
    internal static let sk = ImageAsset(name: "Countries/SK")
    internal static let sl = ImageAsset(name: "Countries/SL")
    internal static let sm = ImageAsset(name: "Countries/SM")
    internal static let sn = ImageAsset(name: "Countries/SN")
    internal static let so = ImageAsset(name: "Countries/SO")
    internal static let sr = ImageAsset(name: "Countries/SR")
    internal static let ss = ImageAsset(name: "Countries/SS")
    internal static let st = ImageAsset(name: "Countries/ST")
    internal static let sv = ImageAsset(name: "Countries/SV")
    internal static let sx = ImageAsset(name: "Countries/SX")
    internal static let sy = ImageAsset(name: "Countries/SY")
    internal static let sz = ImageAsset(name: "Countries/SZ")
    internal static let tc = ImageAsset(name: "Countries/TC")
    internal static let td = ImageAsset(name: "Countries/TD")
    internal static let tf = ImageAsset(name: "Countries/TF")
    internal static let tg = ImageAsset(name: "Countries/TG")
    internal static let th = ImageAsset(name: "Countries/TH")
    internal static let tj = ImageAsset(name: "Countries/TJ")
    internal static let tk = ImageAsset(name: "Countries/TK")
    internal static let tl = ImageAsset(name: "Countries/TL")
    internal static let tm = ImageAsset(name: "Countries/TM")
    internal static let tn = ImageAsset(name: "Countries/TN")
    internal static let to = ImageAsset(name: "Countries/TO")
    internal static let tr = ImageAsset(name: "Countries/TR")
    internal static let tt = ImageAsset(name: "Countries/TT")
    internal static let tv = ImageAsset(name: "Countries/TV")
    internal static let tw = ImageAsset(name: "Countries/TW")
    internal static let tz = ImageAsset(name: "Countries/TZ")
    internal static let ua = ImageAsset(name: "Countries/UA")
    internal static let ug = ImageAsset(name: "Countries/UG")
    internal static let um = ImageAsset(name: "Countries/UM")
    internal static let us = ImageAsset(name: "Countries/US")
    internal static let uy = ImageAsset(name: "Countries/UY")
    internal static let uz = ImageAsset(name: "Countries/UZ")
    internal static let va = ImageAsset(name: "Countries/VA")
    internal static let vc = ImageAsset(name: "Countries/VC")
    internal static let ve = ImageAsset(name: "Countries/VE")
    internal static let vg = ImageAsset(name: "Countries/VG")
    internal static let vi = ImageAsset(name: "Countries/VI")
    internal static let vn = ImageAsset(name: "Countries/VN")
    internal static let vu = ImageAsset(name: "Countries/VU")
    internal static let wf = ImageAsset(name: "Countries/WF")
    internal static let wls = ImageAsset(name: "Countries/WLS")
    internal static let ws = ImageAsset(name: "Countries/WS")
    internal static let xk = ImageAsset(name: "Countries/XK")
    internal static let ye = ImageAsset(name: "Countries/YE")
    internal static let yt = ImageAsset(name: "Countries/YT")
    internal static let za = ImageAsset(name: "Countries/ZA")
    internal static let zet = ImageAsset(name: "Countries/ZET")
    internal static let zm = ImageAsset(name: "Countries/ZM")
    internal static let zw = ImageAsset(name: "Countries/ZW")
  }
  internal enum Demo {

    internal static let backgroundStep1 = ImageAsset(name: "Demo/backgroundStep1")
    internal static let next = ImageAsset(name: "Demo/next")
    internal static let previous = ImageAsset(name: "Demo/previous")
    internal static let step1 = ImageAsset(name: "Demo/step1")
    internal static let step2 = ImageAsset(name: "Demo/step2")
    internal static let step3 = ImageAsset(name: "Demo/step3")
    internal static let step4 = ImageAsset(name: "Demo/step4")
  }
  internal enum HamburgerMenu {

    internal static let login = ImageAsset(name: "HamburgerMenu/login")
    internal static let logout = ImageAsset(name: "HamburgerMenu/logout")
    internal static let menu = ImageAsset(name: "HamburgerMenu/menu")
  }
  internal enum Map {

    internal static let centerLocation = ImageAsset(name: "Map/CenterLocation")
    internal static let lockSmartLock = ImageAsset(name: "Map/LockSmartLock")
    internal static let pinUser = ImageAsset(name: "Map/PinUser")
    internal static let startDiagnoticBike = ImageAsset(name: "Map/StartDiagnoticBike")
    internal static let turnOnBike = ImageAsset(name: "Map/TurnOnBike")
    internal static let activateBluetooth = ImageAsset(name: "Map/activateBluetooth")
    internal static let availableBike = ImageAsset(name: "Map/availableBike")
    internal static let bikeDownPin = ImageAsset(name: "Map/bikeDownPin")
    internal static let bikeLater = ImageAsset(name: "Map/bikeLater")
    internal static let bikeNow = ImageAsset(name: "Map/bikeNow")
    internal static let bikePin = ImageAsset(name: "Map/bikePin")
    internal static let bikeReserved = ImageAsset(name: "Map/bikeReserved")
    internal static let unlockSmartLock = ImageAsset(name: "Map/unlockSmartLock")
  }
  internal enum MySubscription {

    internal static let cancel = ImageAsset(name: "MySubscription/cancel")
    internal static let edit = ImageAsset(name: "MySubscription/edit")
    internal static let reactivate = ImageAsset(name: "MySubscription/reactivate")
  }
  internal enum Navigation {

    internal static let back = ImageAsset(name: "Navigation/back")
    internal static let rightArrow = ImageAsset(name: "Navigation/rightArrow")
  }
  internal enum Notification {

    internal static let notification = ImageAsset(name: "Notification/Notification")
    internal static let notificationOpened = ImageAsset(name: "Notification/NotificationOpened")
  }
  internal enum Popup {

    internal static let closeCross = ImageAsset(name: "Popup/closeCross")
  }
  internal enum Profile {

    internal static let iconAdd = ImageAsset(name: "Profile/IconAdd")
    internal static let iconDelete = ImageAsset(name: "Profile/IconDelete")
    internal static let iconEdit = ImageAsset(name: "Profile/IconEdit")
    internal static let iconSettings = ImageAsset(name: "Profile/IconSettings")
  }
  internal enum Ride {

    internal static let co2 = ImageAsset(name: "Ride/co2")
    internal static let continueRide = ImageAsset(name: "Ride/continueRide")
    internal static let deleteAll = ImageAsset(name: "Ride/deleteAll")
    internal static let deleteOne = ImageAsset(name: "Ride/deleteOne")
    internal static let distance = ImageAsset(name: "Ride/distance")
    internal static let flagEnd = ImageAsset(name: "Ride/flagEnd")
    internal static let flagStart = ImageAsset(name: "Ride/flagStart")
    internal static let pauseRide = ImageAsset(name: "Ride/pauseRide")
    internal static let point = ImageAsset(name: "Ride/point")
    internal static let price = ImageAsset(name: "Ride/price")
    internal static let time = ImageAsset(name: "Ride/time")
    internal static let tripCalendar = ImageAsset(name: "Ride/tripCalendar")
  }
  internal enum Splash {

    internal static let logoORIBIKY = ImageAsset(name: "Splash/LogoORIBIKY")
    internal static let splashBg = ImageAsset(name: "Splash/SplashBg")
    internal static let splashBike = ImageAsset(name: "Splash/SplashBike")
    internal static let logo = ImageAsset(name: "Splash/logo")
  }
  internal enum Subscription {

    internal static let myPictures = ImageAsset(name: "Subscription/MyPictures")
    internal static let takePicture = ImageAsset(name: "Subscription/TakePicture")
    internal static let documentPlaceholder = ImageAsset(name: "Subscription/documentPlaceholder")
    internal static let info = ImageAsset(name: "Subscription/info")
    internal static let redArrow = ImageAsset(name: "Subscription/redArrow")
  }
  internal enum TabBar {

    internal static let map = ImageAsset(name: "TabBar/Map")
    internal static let profile = ImageAsset(name: "TabBar/Profile")
    internal static let report = ImageAsset(name: "TabBar/Report")
    internal static let trips = ImageAsset(name: "TabBar/Trips")
    internal static let subscription = ImageAsset(name: "TabBar/subscription")
  }
  internal enum Diagnostic {

    internal static let brakes = ImageAsset(name: "diagnostic/brakes")
    internal static let diagnoticKO = ImageAsset(name: "diagnostic/diagnoticKO")
    internal static let diagnoticOK = ImageAsset(name: "diagnostic/diagnoticOK")
    internal static let isLoopThere = ImageAsset(name: "diagnostic/isLoopThere")
    internal static let lights = ImageAsset(name: "diagnostic/lights")
    internal static let tires = ImageAsset(name: "diagnostic/tires")
  }

  // swiftlint:disable trailing_comma
  internal static let allColors: [ColorAsset] = [
  ]
  internal static let allDataAssets: [DataAsset] = [
  ]
  internal static let allImages: [ImageAsset] = [
    Assistance.chatIcon,
    Assistance.dateReclamation,
    Assistance.mobileIcon,
    Assistance.reclamationOpened,
    Assistance.reclamationPending,
    Assistance.reclamationSolved,
    Assistance.reclamationSpam,
    Assistance.timingIcon,
    Card.checkboxOff,
    Card.checkboxOn,
    Countries.ad,
    Countries.ae,
    Countries.af,
    Countries.ag,
    Countries.ai,
    Countries.al,
    Countries.am,
    Countries.ao,
    Countries.aq,
    Countries.ar,
    Countries.`as`,
    Countries.at,
    Countries.au,
    Countries.aw,
    Countries.ax,
    Countries.az,
    Countries.ba,
    Countries.bb,
    Countries.bd,
    Countries.be,
    Countries.bf,
    Countries.bg,
    Countries.bh,
    Countries.bi,
    Countries.bj,
    Countries.bl,
    Countries.bm,
    Countries.bn,
    Countries.bo,
    Countries.br,
    Countries.bs,
    Countries.bt,
    Countries.bv,
    Countries.bw,
    Countries.by,
    Countries.bz,
    Countries.ca,
    Countries.cc,
    Countries.cd,
    Countries.cf,
    Countries.cg,
    Countries.ch,
    Countries.ci,
    Countries.ck,
    Countries.cl,
    Countries.cm,
    Countries.cn,
    Countries.co,
    Countries.cr,
    Countries.cu,
    Countries.cv,
    Countries.cw,
    Countries.cx,
    Countries.cy,
    Countries.cz,
    Countries.de,
    Countries.dj,
    Countries.dk,
    Countries.dm,
    Countries.`do`,
    Countries.dz,
    Countries.ec,
    Countries.ee,
    Countries.eg,
    Countries.eng,
    Countries.er,
    Countries.es,
    Countries.et,
    Countries.eu,
    Countries.fi,
    Countries.fj,
    Countries.fk,
    Countries.fm,
    Countries.fo,
    Countries.fr,
    Countries.ga,
    Countries.gb,
    Countries.gd,
    Countries.ge,
    Countries.gf,
    Countries.gg,
    Countries.gh,
    Countries.gi,
    Countries.gl,
    Countries.gm,
    Countries.gn,
    Countries.gp,
    Countries.gq,
    Countries.gr,
    Countries.gs,
    Countries.gt,
    Countries.gu,
    Countries.gw,
    Countries.gy,
    Countries.hk,
    Countries.hm,
    Countries.hn,
    Countries.hr,
    Countries.ht,
    Countries.hu,
    Countries.id,
    Countries.ie,
    Countries.il,
    Countries.im,
    Countries.`in`,
    Countries.io,
    Countries.iq,
    Countries.ir,
    Countries.`is`,
    Countries.it,
    Countries.je,
    Countries.jm,
    Countries.jo,
    Countries.jp,
    Countries.ke,
    Countries.kg,
    Countries.kh,
    Countries.ki,
    Countries.km,
    Countries.kn,
    Countries.kp,
    Countries.kr,
    Countries.kw,
    Countries.ky,
    Countries.kz,
    Countries.la,
    Countries.lb,
    Countries.lc,
    Countries.lgbt,
    Countries.li,
    Countries.lk,
    Countries.lr,
    Countries.ls,
    Countries.lt,
    Countries.lu,
    Countries.lv,
    Countries.ly,
    Countries.ma,
    Countries.mc,
    Countries.md,
    Countries.me,
    Countries.mf,
    Countries.mg,
    Countries.mh,
    Countries.mk,
    Countries.ml,
    Countries.mm,
    Countries.mn,
    Countries.mo,
    Countries.mp,
    Countries.mq,
    Countries.mr,
    Countries.ms,
    Countries.mt,
    Countries.mu,
    Countries.mv,
    Countries.mw,
    Countries.mx,
    Countries.my,
    Countries.mz,
    Countries.na,
    Countries.nc,
    Countries.ne,
    Countries.nf,
    Countries.ng,
    Countries.ni,
    Countries.nir,
    Countries.nl,
    Countries.no,
    Countries.np,
    Countries.nr,
    Countries.nu,
    Countries.nz,
    Countries.om,
    Countries.pa,
    Countries.pe,
    Countries.pf,
    Countries.pg,
    Countries.ph,
    Countries.pk,
    Countries.pl,
    Countries.pm,
    Countries.pn,
    Countries.pr,
    Countries.ps,
    Countries.pt,
    Countries.pw,
    Countries.py,
    Countries.qa,
    Countries.re,
    Countries.ro,
    Countries.rs,
    Countries.ru,
    Countries.rw,
    Countries.sa,
    Countries.sb,
    Countries.sc,
    Countries.sct,
    Countries.sd,
    Countries.se,
    Countries.sg,
    Countries.sh,
    Countries.si,
    Countries.sj,
    Countries.sk,
    Countries.sl,
    Countries.sm,
    Countries.sn,
    Countries.so,
    Countries.sr,
    Countries.ss,
    Countries.st,
    Countries.sv,
    Countries.sx,
    Countries.sy,
    Countries.sz,
    Countries.tc,
    Countries.td,
    Countries.tf,
    Countries.tg,
    Countries.th,
    Countries.tj,
    Countries.tk,
    Countries.tl,
    Countries.tm,
    Countries.tn,
    Countries.to,
    Countries.tr,
    Countries.tt,
    Countries.tv,
    Countries.tw,
    Countries.tz,
    Countries.ua,
    Countries.ug,
    Countries.um,
    Countries.us,
    Countries.uy,
    Countries.uz,
    Countries.va,
    Countries.vc,
    Countries.ve,
    Countries.vg,
    Countries.vi,
    Countries.vn,
    Countries.vu,
    Countries.wf,
    Countries.wls,
    Countries.ws,
    Countries.xk,
    Countries.ye,
    Countries.yt,
    Countries.za,
    Countries.zet,
    Countries.zm,
    Countries.zw,
    Demo.backgroundStep1,
    Demo.next,
    Demo.previous,
    Demo.step1,
    Demo.step2,
    Demo.step3,
    Demo.step4,
    HamburgerMenu.login,
    HamburgerMenu.logout,
    HamburgerMenu.menu,
    Map.centerLocation,
    Map.lockSmartLock,
    Map.pinUser,
    Map.startDiagnoticBike,
    Map.turnOnBike,
    Map.activateBluetooth,
    Map.availableBike,
    Map.bikeDownPin,
    Map.bikeLater,
    Map.bikeNow,
    Map.bikePin,
    Map.bikeReserved,
    Map.unlockSmartLock,
    MySubscription.cancel,
    MySubscription.edit,
    MySubscription.reactivate,
    Navigation.back,
    Navigation.rightArrow,
    Notification.notification,
    Notification.notificationOpened,
    Popup.closeCross,
    Profile.iconAdd,
    Profile.iconDelete,
    Profile.iconEdit,
    Profile.iconSettings,
    Ride.co2,
    Ride.continueRide,
    Ride.deleteAll,
    Ride.deleteOne,
    Ride.distance,
    Ride.flagEnd,
    Ride.flagStart,
    Ride.pauseRide,
    Ride.point,
    Ride.price,
    Ride.time,
    Ride.tripCalendar,
    Splash.logoORIBIKY,
    Splash.splashBg,
    Splash.splashBike,
    Splash.logo,
    Subscription.myPictures,
    Subscription.takePicture,
    Subscription.documentPlaceholder,
    Subscription.info,
    Subscription.redArrow,
    TabBar.map,
    TabBar.profile,
    TabBar.report,
    TabBar.trips,
    TabBar.subscription,
    Diagnostic.brakes,
    Diagnostic.diagnoticKO,
    Diagnostic.diagnoticOK,
    Diagnostic.isLoopThere,
    Diagnostic.lights,
    Diagnostic.tires,
  ]
  // swiftlint:enable trailing_comma
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal struct ColorAsset {
  internal fileprivate(set) var name: String

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  internal var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
}

internal extension AssetColorTypeAlias {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct DataAsset {
  internal fileprivate(set) var name: String

  #if os(iOS) || os(tvOS) || os(OSX)
  @available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
  internal var data: NSDataAsset {
    return NSDataAsset(asset: self)
  }
  #endif
}

#if os(iOS) || os(tvOS) || os(OSX)
@available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
internal extension NSDataAsset {
  convenience init!(asset: DataAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(name: asset.name, bundle: bundle)
    #elseif os(OSX)
    self.init(name: NSDataAsset.Name(asset.name), bundle: bundle)
    #endif
  }
}
#endif

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  internal var image: AssetImageTypeAlias {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = AssetImageTypeAlias(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: NSImage.Name(name))
    #elseif os(watchOS)
    let image = AssetImageTypeAlias(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

internal extension AssetImageTypeAlias {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

private final class BundleToken {}
